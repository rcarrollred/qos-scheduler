# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# See here for image contents: https://github.com/microsoft/vscode-dev-containers/tree/v0.245.0/containers/go/.devcontainer/base.Dockerfile

# [Choice] Go version (use -bullseye variants on local arm64/Apple Silicon): 1, 1.19, 1.18, 1-bullseye, 1.19-bullseye, 1.18-bullseye, 1-buster, 1.19-buster, 1.18-buster
ARG VARIANT="1.19-bullseye"
ARG GOOS="linux"
ARG GOARCH="amd64"
FROM mcr.microsoft.com/vscode/devcontainers/go:0-${VARIANT}

ENV GOOS=$GOOS
ENV GOARCH=$GOARCH

# [Choice] Node.js version: none, lts/*, 18, 16, 14
ARG NODE_VERSION="none"
RUN if [ "${NODE_VERSION}" != "none" ]; then su vscode -c "umask 0002 && . /usr/local/share/nvm/nvm.sh && nvm install ${NODE_VERSION} 2>&1"; fi

# [Optional] Uncomment this section to install additional OS packages.
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends cmake

# Install kubebuilder
RUN curl -sL -o kubebuilder https://go.kubebuilder.io/dl/latest/$(go env GOOS)/$(go env GOARCH) \
    && chmod +x kubebuilder \
    && mv kubebuilder /usr/local/bin/

# install protoc
RUN <<EOF
#!/bin/bash
set -e
case $(go env GOARCH) in
    amd64) arch=x86_64 ;;
    arm64) arch=aarch_64 ;;
esac
curl -sL -o protoc.zip https://github.com/protocolbuffers/protobuf/releases/download/v3.15.8/protoc-3.15.8-$(go env GOOS)-${arch}.zip
unzip protoc.zip -d /usr/local/
chmod +x /usr/local/bin/protoc
chmod 755 -R /usr/local/include
rm -f protoc.zip
EOF

# [Optional] Uncomment the next lines to use go get to install anything else you need
USER vscode
# RUN go get -x <your-dependency-or-tool>
RUN go install honnef.co/go/tools/cmd/staticcheck@latest
RUN go install github.com/onsi/ginkgo/v2/ginkgo@v2.9.2
RUN go install github.com/onsi/gomega/...
RUN go install github.com/golang/mock/mockgen@v1.6.0
RUN go install sigs.k8s.io/kustomize/kustomize/v4@latest

# [Optional] Uncomment this line to install global node packages.
# RUN su vscode -c "source /usr/local/share/nvm/nvm.sh && npm install -g <your-package-here>" 2>&1

