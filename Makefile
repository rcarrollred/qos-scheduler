# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

.DEFAULT_GOAL:=help
.RECIPEPREFIX=>
ifeq ($(filter undefine,$(value .FEATURES)),)
$(error Make v$(MAKE_VERSION) is not supported. $\
        Use at least v3.82)
endif

define mkdir # paths
$(foreach p,$(1),$(eval $(p): ; mkdir -p $$@))
endef
define cleaner # name,paths,dep
.PHONY: clean-$(1)
.IGNORE: clean-$(1)
clean: clean-$(1)
clean-$(1): $(3)
>rm -rf $$(to-remove) $(2)
endef
clean-up=$(eval $(call cleaner,$(1),$(2)))

ifndef SCHEDULER_IGNORE_SETTINGS
ifneq "$(USER)" ""
-include $(USER).mk
endif
endif

curr-branch:=$(or $(CI_COMMIT_REF_NAME),$(shell git symbolic-ref --short HEAD))
main-branch:=$(if $(strip $(subst main,,$(curr-branch))),,yes)

#
#   #    # ###### #      #####
#   #    # #      #      #    #
#   ###### #####  #      #    #
#   #    # #      #      #####
#   #    # #      #      #
#   #    # ###### ###### #
#

define awk-gen-help:=
function line(t,d) {printf "%3s%-10s %s\n"," ",t,d}
/^## [a-zA-Z-]+ ::/ {
    n=split(substr($$0,4),a," :: ");
    line(a[1],a[2]);
    next
}
/^##\+ / { line(" ",substr($$0,4)); next }
/^##/ { printf "%s\n",substr($$0,4) }
END { if (n>0) { printf "\n" } }
endef
userFiles:=$(wordlist 2,$(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))

.PHONY: help
##
## Use one of the following goals:
##
## help :: Show this help text
help:
>@awk '$(strip $(awk-gen-help))' $(firstword $(MAKEFILE_LIST))
>$(if $(userFiles),@awk '$(strip $(awk-gen-help))' $(userFiles))

#
#    ####   ####  #    # ###### #####  #    # #      ###### #####
#   #      #    # #    # #      #    # #    # #      #      #    #
#    ####  #      ###### #####  #    # #    # #      #####  #    #
#        # #      #    # #      #    # #    # #      #      #####
#   #    # #    # #    # #      #    # #    # #      #      #   #
#    ####   ####  #    # ###### #####   ####  ###### ###### #    #
#

.PHONY: scheduler all
scheduler all: .gitlab-ci.yml chart

#
#   #      # #    # #####
#   #      # ##   #   #
#   #      # # #  #   #
#   #      # #  # #   #
#   #      # #   ##   #
#   ###### # #    #   #
#

REUSE=reuse
reuse-report:=deliv/lint/reuse-report.txt

.PHONY: reuse
## reuse :: Check REUSE compliance
reuse: | deliv/lint/
>LANGUAGE=C $(REUSE) lint >$(reuse-report) ; status=$$? ;\
 [ "$${status}" == 0 ] && echo "REUSE compliant" || cat $(reuse-report) ;\
 exit $${status}
$(call mkdir,deliv/lint/)
$(call clean-up,reuse,$(reuse-report))

#
#   #    # ###### #      #    #
#   #    # #      #      ##  ##
#   ###### #####  #      # ## #
#   #    # #      #      #    #
#   #    # #      #      #    #
#   #    # ###### ###### #    #
#

version:="$(strip $(shell cat VERSION))"
helmDir=helm/qos-scheduler
tmpHelmDir:=tmp/helm
charts:=cni common controller network nodedaemon nsconfig optimizer\
	prometheusOperator qos-crds scheduler solver
helm-chart:=deliv/helm/qos-scheduler-$(version).tgz
helm-index:=deliv/helm/index.yaml
helm-repo:=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api
helm-repo-path?=$(if $(main-branch),stable/charts,devel/charts)
helm-policy?=Always
# helm-image-tag?=$(if $(main-branch),$(version),$(curr-branch))
# always use image tag from VERSION file (Docker hub)
helm-image-tag:=$(version)

.PHONY: chart
## chart :: Create archive with charts
chart: $(helm-chart) $(helm-index)

chartFiles:=$(wildcard $(helmDir)/*) \
	$(foreach c,$(charts),\
		$(wildcard $(helmDir)/charts/$(c)/*) \
		$(wildcard $(helmDir)/charts/$(c)/templates/*))

$(call mkdir,$(dir $(helm-chart)) $(tmpHelmDir))
$(helm-chart): $(chartFiles) | $(dir $(helm-chart)) $(tmpHelmDir)
>rsync -qru --delete $(helmDir)/ $(tmpHelmDir)
>for src in $(helmDir)/values.yaml $(helmDir)/charts/*/values.yaml ; do\
    sed -Ee "/^ +pullPolicy:/s/:.+/: $(helm-policy)/" \
        -e "1,/^# External/{/^ +tag:/s/:.+/: '$(helm-image-tag)'/;}" \
    $${src} >$(tmpHelmDir)$${src#"$(helmDir)"} ; \
done

>helm dependency update $(tmpHelmDir)
>helm package --app-version=$(version) --version=$(version) \
     -d $(dir $(helm-chart)) $(tmpHelmDir)

$(helm-index): $(chartFiles) | $(dir $(helm-chart)) $(tmpHelmDir)
>helm repo index $(dir $(helm-index)) --url $(helm-repo)/stable/charts

$(call clean-up,chart,$(dir $(helm-chart)) $(tmpHelmDir))

.PHONY: helm
## helm :: Publish Helm charts
helm: $(helm-chart) $(helm-index)
>curl --request POST --user gitlab-ci-token:$${CI_JOB_TOKEN} \
     --form "chart=@$<" $(helm-repo)/$(helm-repo-path)

#
#   #    # ###### #####   ####  #  ####  #    #
#   #    # #      #    # #      # #    # ##   #
#   #    # #####  #    #  ####  # #    # # #  #
#   #    # #      #####       # # #    # #  # #
#    #  #  #      #   #  #    # # #    # #   ##
#     ##   ###### #    #  ####  #  ####  #    #
#

kind?=revision

define awk-inc-version:=
/^[0-9.]+/{
	if (kind=="mayor") {
		$$1++;$$2=0;$$3=0
	} else if (kind=="minor") {
		$$2++;$$3=0
	} else {
		$$3++
	};
	printf("%d.%d.%d",$$1,$$2,$$3)
}
endef

.PHONY: version
## version :: Increment version number (default: revision)
##+   kind=(mayor|minor|revision)
version:
>awk -v FS=. -v kind=$(kind) '$(strip $(awk-inc-version))' $@ >$@.tmp
>mv $@.tmp $@

sedCmdVersionH=-e "/^ *HELM_(APP_)?VERSION:/s/[0-9.]+/$(strip $(shell cat VERSION))/"

.gitlab-ci.yml: VERSION
>sed -i "" -E $(sedCmdVersionH) $@

#
#    ####  #      ######   ##   #    #
#   #    # #      #       #  #  ##   #
#   #      #      #####  #    # # #  #
#   #      #      #      ###### #  # #
#   #    # #      #      #    # #   ##
#    ####  ###### ###### #    # #    #
#

.PHONY: clean
.IGNORE: clean
## clean :: Remove images and generated files
clean:
>rm -rf deliv/ tmp/
