// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/kubernetes/pkg/scheduler/framework"
	st "k8s.io/kubernetes/pkg/scheduler/testing"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/schedulerplugins"
)
const (
	timeout  = time.Second * 20
	interval = time.Millisecond * 250
)

type planData struct {
	plan       *crd.AssignmentPlan
	planStatus *crd.AssignmentPlanStatus
}

type testData struct {
	name     string
	pod      *v1.Pod
	planData *planData
	nodes    []*v1.Node
	nodeName string
	want     framework.Code
	score    int64
}

func makePod(podName string, agName string) *v1.Pod {
	return st.MakePod().Name(podName).Namespace("default").Label("application-group", agName).Obj()
}

func prepareAssignmentPlan(agName string, assignments map[string]string, channels map[string]([]string)) *planData {
	namespace := "default"
	ps := map[string]crd.AssignmentPlanStatusEntry{}
	for pod, node := range assignments {
      podKey := fmt.Sprintf("%s/%s", namespace, pod)
      channelsNeeded, ok := channels[pod]
      if ok {
		   ps[podKey] = crd.AssignmentPlanStatusEntry{
		   	PreferredNode: node, Channels: channelsNeeded,
         }
      } else {
		   ps[podKey] = crd.AssignmentPlanStatusEntry{
		   	PreferredNode: node, Channels: []string{},
		   }
      }
	}
	ap := &crd.AssignmentPlan{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-assignment-plan", agName),
			Namespace: namespace,
			Labels:    map[string]string{"application-group": agName},
		},
		TypeMeta: metav1.TypeMeta{Kind: "AssignmentPlan", APIVersion: "v1alpha1"},
	}
	status := &crd.AssignmentPlanStatus{
		PodStatus: ps,
	}
	return &planData{plan: ap, planStatus: status}
}

func makeAssignmentPlan(ctx context.Context, planData planData) {
	Expect(planData.plan).NotTo(BeNil())
	var existingPlan crd.AssignmentPlan
   var err error
   // After starting the test suite, the manager has to start the caches. Until it has done that,
   // Get() can error ("the cache is not started, can not read objects"). You cannot really tell when
   // the manager is ready other than by looking for that message, afaict. So this code waits until
   // we either find the plan or get a NotFound error, because once either of those happens, the
   // manager is done setting up the cache.
   Eventually(func() bool {
	   err = k8sClient.Get(ctx, types.NamespacedName{Namespace: planData.plan.Namespace, Name: planData.plan.Name}, &existingPlan)
      return err == nil || errors.IsNotFound(err)
   }, timeout, interval).Should(Equal(true))
	if err == nil {
		existingPlan.Spec = *((planData.plan.Spec).DeepCopy())
		Expect(k8sClient.Update(ctx, &existingPlan)).To(Succeed())
	} else {
		Expect(k8sClient.Create(ctx, planData.plan)).To(Succeed())
		Eventually(func() bool {
			err = k8sClient.Get(ctx, types.NamespacedName{Namespace: planData.plan.Namespace, Name: planData.plan.Name}, &existingPlan)
			return err == nil
		}, timeout, interval).Should(Equal(true))
	}
	if len(planData.planStatus.PodStatus) != 0 {
		existingPlan.Status = *planData.planStatus
		Expect(k8sClient.Status().Update(ctx, &existingPlan)).To(Succeed())
	}
}

var _ = Describe("QoS Scheduler plugin", func() {

	var (
		qp    *schedulerplugins.QosPlugin
		state *framework.CycleState
		node1 *v1.Node
	)

	BeforeEach(func() {
		By("setting up scheduler plugin")
		qp, state = setup()
		Expect(qp).NotTo(BeNil())
		Expect(state).NotTo(BeNil())
		node1 = st.MakeNode().Name("node1").Obj()
		Expect(node1).NotTo(BeNil())
	})

	Context("Pre Filter plugin", func() {
		ctx := context.Background()
		tests := []testData{
			{
				name: "pod with no application-group label",
				pod:  st.MakePod().Obj(),
				want: framework.Success,
			},
			{
				name: "pod with application-group label but no plan entry",
				pod:  makePod("pod-prefilter-without-plan", "ag-prefilter-without-plan"),
				want: framework.Unschedulable,
			},
			{
				name:     "pod with application-group label and plan entry",
				pod:      makePod("pod-prefilter-with-plan", "ag-prefilter-with-plan"),
				planData: prepareAssignmentPlan("ag-prefilter-with-plan", map[string]string{"pod-prefilter-with-plan": "node1"},
                map[string]([]string){}),
				want:     framework.Success,
			},
		}
		for _, ts := range tests {
			It(fmt.Sprintf("schedules %s", ts.name), func() {
				if ts.planData != nil {
					makeAssignmentPlan(ctx, *ts.planData)
				}
				Eventually(func() framework.Code {
					return qp.PreFilter(ctx, state, ts.pod).Code()
				}, timeout, interval).Should(Equal(ts.want))
			})
		}
	})

	Context("Pre Score plugin", func() {
		ctx := context.Background()
		tests := []testData{
			{
				name:  "pod with no application-group label",
				pod:   st.MakePod().Name("no-ag-label").Obj(),
				nodes: make([]*v1.Node, 0),
				want:  framework.Success,
			},
			{
				name:  "pod with application-group label but no plan entry",
				pod:   makePod("pod-presocre-without-plan", "ag-presocre-without-plan"),
				nodes: make([]*v1.Node, 0),
				want:  framework.Unschedulable,
			},
			{
				name:     "pod with application-group label and plan entry but preferred node isn't available",
				pod:      makePod("pod-presocre-without-node", "ag-presocre-without-node"),
				planData: prepareAssignmentPlan("ag-presocre-without-node", map[string]string{"pod-presocre-without-node": "node1"},
               map[string]([]string){}),
				nodes:    make([]*v1.Node, 0),
				want:     framework.Unschedulable,
			},
			{
				name:     "pod with application-group label and plan entry and preferred node is available",
				pod:      makePod("pod-presocre-with-node", "ag-presocre-with-node"),
				planData: prepareAssignmentPlan("ag-presocre-with-node", map[string]string{"pod-presocre-with-node": "node2"},
               map[string]([]string){}),
				nodes: []*v1.Node{ st.MakeNode().Name("node2").Obj() },
				want: framework.Success,
			},
		}
		for _, ts := range tests {
			It(fmt.Sprintf("schedules %s", ts.name), func() {
				if ts.planData != nil {
					makeAssignmentPlan(ctx, *ts.planData)
				}
				Eventually(func() framework.Code {
					return qp.PreScore(ctx, state, ts.pod, ts.nodes).Code()
				}, timeout, interval).Should(Equal(ts.want))
			})
		}
	})

	Context("Score plugin", func() {
		ctx := context.Background()
		tests := []testData{
			{
				name:     "pod with no application-group label",
				pod:      st.MakePod().Obj(),
				nodeName: "dontcare",
				want:     framework.Success,
				score:    int64(0),
			},
			{
				name:     "pod with application-group label but no plan entry",
				pod:      makePod("pod-score-without-plan", "ag-score-without-plan"),
				nodeName: "dontcare",
				want:     framework.Unschedulable,
				score:    int64(0),
			},
			{
				name:     "pod with application-group label and plan entry but preferred node isn't available",
				pod:      makePod("pod-score-without-node", "ag-score-without-node"),
				planData: prepareAssignmentPlan("ag-score-without-node", map[string]string{"pod-score-without-node": "node1"},
               map[string]([]string){}),
				want:     framework.Success,
				nodeName: "wrongnode",
				score:    int64(0),
			},
			{
				name:     "pod with application-group label and plan entry and preferred node is available",
				pod:      makePod("pod-score-with-node", "ag-score-with-node"),
				planData: prepareAssignmentPlan("ag-score-with-node", map[string]string{"pod-score-with-node": "node1"},
               map[string]([]string){}),
				nodeName: "node1",
				want:     framework.Success,
				score:    int64(100),
			},
		}
		for _, ts := range tests {
			It(fmt.Sprintf("scores %s", ts.name), func() {
				if ts.planData != nil {
					makeAssignmentPlan(ctx, *ts.planData)
				}
				var existingPlan crd.AssignmentPlan
				Eventually(func() bool {
					err := k8sClient.Get(ctx,
				 		types.NamespacedName{Namespace: ts.planData.plan.Namespace, Name: ts.planData.plan.Name}, &existingPlan)
					if err != nil { return false }
					_, exists := existingPlan.Status.PodStatus[fmt.Sprintf("%s/%s", ts.planData.plan.Namespace, ts.pod.Name)]
					return exists
				}, timeout, interval).Should(Equal(true))
				Eventually(func() framework.Code {
					score, status := qp.Score(ctx, state, ts.pod, ts.nodeName)
					Expect(score).Should(Equal(ts.score), fmt.Sprintf("error message was: %v with podstatus map %+v", status.Reasons(), existingPlan.Status.PodStatus))
					return status.Code()
				}, timeout, interval).Should(Equal(ts.want))
			})
		}
	})

	Context("Permit plugin", func() {
		ctx := context.Background()
		tests := []testData{
			{
				name:     "pod with no application-group label",
				pod:      st.MakePod().Obj(),
				nodeName: "dontcare",
				want:     framework.Success,
				score:    int64(0),
			},
			{
				name:     "pod with application-group label but no plan entry",
				pod:      makePod("pod-permit-without-plan", "ag-permit-without-plan"),
				nodeName: "dontcare",
				want:     framework.Unschedulable,
				score:    int64(0),
			},
			{
				name:     "pod with application-group label and plan entry but wrong node",
				pod:      makePod("pod-permit-wrong-node", "ag-permit-wrong-node"),
				planData: prepareAssignmentPlan("ag-permit-wrong-node", map[string]string{"pod-permit-wrong-node": "node1"},
               map[string]([]string){}),
				want:     framework.Unschedulable,
				nodeName: "wrongnode",
				score:    int64(0),
			},
			{
				name:     "pod waiting for channels",
				pod:      makePod("pod-permit-wait", "ag-permit-wait"),
				planData: prepareAssignmentPlan("ag-permit-wait", map[string]string{"pod-permit-wait": "node1"},
               map[string]([]string){"pod-permit-wait": []string{"channel1"}}),
				nodeName: "node1",
				want:     framework.Wait,
				score:    int64(2),
			},
		}
		for _, ts := range tests {
			It(fmt.Sprintf("permits %s", ts.name), func() {
				if ts.planData != nil {
					makeAssignmentPlan(ctx, *ts.planData)
				}
				var existingPlan crd.AssignmentPlan
				Eventually(func() bool {
					err := k8sClient.Get(ctx,
				 		types.NamespacedName{Namespace: ts.planData.plan.Namespace, Name: ts.planData.plan.Name}, &existingPlan)
					if err != nil { return false }
					_, exists := existingPlan.Status.PodStatus[fmt.Sprintf("%s/%s", ts.planData.plan.Namespace, ts.pod.Name)]
					return exists
				}, timeout, interval).Should(Equal(true))
				Eventually(func() framework.Code {
					status, _ := qp.Permit(ctx, state, ts.pod, ts.nodeName)
					return status.Code()
				}, timeout, interval).Should(Equal(ts.want))
			})
		}
	})

	Context("Pre Bind plugin", func() {
		ctx := context.Background()
		tests := []testData{
			{
				name:     "pod with no application-group label",
				pod:      st.MakePod().Obj(),
				nodeName: "dontcare",
				want:     framework.Success,
			},
			{
				name:     "pod with application-group label but no plan entry",
				pod:      makePod("pod-prebind-without-plan", "ag-prebind-without-plan"),
				nodeName: "dontcare",
				want:     framework.Unschedulable,
			},
			{
				name:     "pod with application-group label and plan entry but preferred node isn't available",
				pod:      makePod("pod-prebind-without-node", "ag-prebind-without-node"),
				planData: prepareAssignmentPlan("ag-prebind-without-node", map[string]string{},
               map[string]([]string){}),
				want:     framework.Unschedulable,
				nodeName: "wrongnode",
			},
			{
				name:     "pod with application-group label and plan entry and preferred node is available",
				pod:      makePod("pod-prebind-with-plan", "ag-prebind-with-plan"),
				planData: prepareAssignmentPlan("ag-prebind-with-plan", map[string]string{"pod-prebind-with-plan": "node1"},
               map[string]([]string){}),
				nodeName: "node1",
				want:     framework.Success,
			},
		}
		for _, ts := range tests {
			It(fmt.Sprintf("does pre-bind tests for %s", ts.name), func() {
				if ts.planData != nil {
					makeAssignmentPlan(ctx, *ts.planData)
				}
				Eventually(func() framework.Code {
					return qp.PreBind(ctx, state, ts.pod, ts.nodeName).Code()
				}, timeout, interval).Should(Equal(ts.want))
			})
		}
	})
})
