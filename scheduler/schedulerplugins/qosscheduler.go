// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package schedulerplugins contains the custom Kubernetes
// scheduler plugins for the qos system.
package schedulerplugins

import (
	"context"
	"fmt"
	"time"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/klog/v2"
	"k8s.io/kubernetes/pkg/scheduler/framework"
	fwruntime "k8s.io/kubernetes/pkg/scheduler/framework/runtime"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

// Args are how you configure a qos scheduler
type Args struct {
	KubeConfig          string `json:"kubeconfig,omitempty"`
	Master              string `json:"master,omitempty"`
	ScheduleWaitSeconds int    `json:"scheduleWaitSeconds,omitempty"`
}

// QosPlugin implements qos (network-aware) co-scheduling.
type QosPlugin struct {
	Args                *Args
	FrameworkHandle     framework.Handle
	K8sClient           client.Client
	ScheduleWaitSeconds int
	PodChecker          *podChannelStatusChecker
}

// PreFilter is for getting rid of unschedulable Pods early on.
var _ framework.PreFilterPlugin = &QosPlugin{}

// Filter removes from consideration nodes that cannot run a given Pod.
var _ framework.FilterPlugin = &QosPlugin{}

// PostFilter is where you handle pods that couldn't find a feasible node.
var _ framework.PostFilterPlugin = &QosPlugin{}

// PreScore creates a sharable state for Score plugins to use.
var _ framework.PreScorePlugin = &QosPlugin{}

// This gives a score to nodes, but it only sees one node at a time.
// But you can implement NormalizeScore (a ScoreExtension) to see
// the whole NodeScoreList.
var _ framework.ScorePlugin = &QosPlugin{}

// This is for Reserve() and Unreserve().
var _ framework.ReservePlugin = &QosPlugin{}

// This is a chance to reject a pod just before it gets bound.
var _ framework.PreBindPlugin = &QosPlugin{}

// Permit lets you delay binding a pod quite late in the process.
var _ framework.PermitPlugin = &QosPlugin{}

// This is where the binding happens.
var _ framework.BindPlugin = &QosPlugin{}

// This gets called after successful binding. It is just informational.
var _ framework.PostBindPlugin = &QosPlugin{}

const (
	// Name is how the qos scheduler is known.
	Name = "QosScheduler"
)

var (
	scheme = runtime.NewScheme()
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(crd.AddToScheme(scheme))
}

// New creates a QosScheduler instance.
func New(rto runtime.Object, handle framework.Handle) (framework.Plugin, error) {
	args := &Args{}
	if err := fwruntime.DecodeInto(rto, args); err != nil {
		return nil, err
	}
	if args.ScheduleWaitSeconds == 0 {
		args.ScheduleWaitSeconds = 70
	}

	clientConfig, err := config.GetConfig()
	if err != nil {
		klog.Errorf("Cannot load cluster config for API client")
		return nil, err
	}

	// Create a new client which includes our CRD schema
	k8sClient, err := client.New(clientConfig, client.Options{Scheme: scheme})
	if err != nil {
		klog.Errorf("Cannot create clientset")
		return nil, err
	}

	plugin := &QosPlugin{
		Args:                args,
		FrameworkHandle:     handle,
		K8sClient:           k8sClient,
		ScheduleWaitSeconds: args.ScheduleWaitSeconds,
		PodChecker:          NewPodChannelStatusChecker(handle, k8sClient, Name),
	}
	return plugin, nil
}

// Name returns the scheduler's name.
func (qos *QosPlugin) Name() string {
	return Name
}

func (qos *QosPlugin) getPreferredNode(ctx context.Context, pod *v1.Pod) (string, *framework.Status) {
	apList, err := qos.PodChecker.getAssignmentPlanList(pod)
	if err != nil {
		return "", framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Error getting assignment plan list: %v\n", err))
	}
	for idx := range apList.Items {
		if apStatus, ok := apList.Items[idx].Status.PodStatus[podKey(pod.Name, pod.Namespace)]; ok {
			if ok {
				return apStatus.PreferredNode, framework.NewStatus(framework.Success, "")
			}
		}
	}
	return "", framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Missing schedule plan entry for pod %s/%s", pod.Name, pod.Namespace))
}

// Method signatures based on:
// https://github.com/kubernetes/kubernetes/blob/303810c7a22d6dc4f940d7e183f7b0518c00569a/pkg/scheduler/framework/v1alpha1/interface.go#L308

// PreFilter validates whether a pod can be scheduled. It returns framework.Unschedulable
// if the pod cannot be scheduled.
func (qos *QosPlugin) PreFilter(ctx context.Context, state *framework.CycleState, pod *v1.Pod) *framework.Status {
	klog.V(3).Infof("PreFilter called with pod %s", pod.Name)
	if pod.Labels["application-group"] == "" {
		return framework.NewStatus(framework.Success, "")
	}
	_, status := qos.getPreferredNode(ctx, pod)
	return status
}

// PreFilterExtensions is just part of the interface, not using it at the moment.
func (qos *QosPlugin) PreFilterExtensions() framework.PreFilterExtensions {
	return nil
}

// Filter is where you decide whether a pod matches a node.
func (qos *QosPlugin) Filter(ctx context.Context, state *framework.CycleState, pod *v1.Pod, nodeInfo *framework.NodeInfo) *framework.Status {
	klog.V(3).Infof("Filter called with pod %s and node info %v", pod.Name, nodeInfo.Node)
	return framework.NewStatus(framework.Success, "")
}

// PostFilter is called if Filter has rejected a pod/node.
func (qos *QosPlugin) PostFilter(ctx context.Context, state *framework.CycleState,
	pod *v1.Pod, filteredNodeStatusMap framework.NodeToStatusMap) (
	*framework.PostFilterResult, *framework.Status) {
	klog.V(3).Infof("PostFilter called with pod %s", pod.Name)
	return nil, framework.NewStatus(framework.Success, "")
}

// PreScore is called before scoring.
func (qos *QosPlugin) PreScore(ctx context.Context, state *framework.CycleState,
	pod *v1.Pod, nodes []*v1.Node) *framework.Status {
	klog.V(3).Infof("PreScore called with pod %s and %d nodes", pod.Name,
		len(nodes))
	if pod.Labels["application-group"] == "" {
		return framework.NewStatus(framework.Success, "")
	}
	preferredNode, status := qos.getPreferredNode(ctx, pod)
	if !status.IsSuccess() {
		return status
	}
	for _, node := range nodes {
		if node.Name == preferredNode {
			klog.V(3).Infof("Found the preferred node %s for pod %s",
				node.Name, pod.Name)
			return framework.NewStatus(framework.Success, "")
		}
	}
	klog.Errorf("The preferred node %s for pod %s is not in the scheduler's list of nodes for prescoring",
		preferredNode, pod.Name)
	return framework.NewStatus(framework.Unschedulable, "Optimizer plan not feasible")
}

// Score is where you give a score to one node for a given pod.
func (qos *QosPlugin) Score(ctx context.Context, state *framework.CycleState,
	pod *v1.Pod, nodeName string) (int64, *framework.Status) {
	klog.V(3).Infof("Score called with pod %s and node %s", pod.Name, nodeName)
	if pod.Labels["application-group"] == "" {
		return int64(0), framework.NewStatus(framework.Success, "")
	}
	preferredNode, status := qos.getPreferredNode(ctx, pod)
	if !status.IsSuccess() {
		return int64(0), status
	}
	// It's ok for a non-preferred node to be scored. If the preferred node
	// wasn't also being scored, we'd have returned an error in PreScore.
	if nodeName != preferredNode {
		klog.V(3).Infof("node %s is not preferred for pod %s", nodeName, pod.Name)
		return int64(0), framework.NewStatus(framework.Success, "")
	}
	klog.V(3).Infof("node %s is preferred for pod %s", nodeName, pod.Name)
	return int64(100), framework.NewStatus(framework.Success, "")
}

// ScoreExtensions is where you get NormalizeScore.
func (qos *QosPlugin) ScoreExtensions() framework.ScoreExtensions { return qos }

// NormalizeScore makes sure the scores for the various nodes are in a common range.
func (qos *QosPlugin) NormalizeScore(ctx context.Context, state *framework.CycleState,
	pod *v1.Pod, scores framework.NodeScoreList) *framework.Status {
	klog.V(3).Infof("NormalizeScore called with pod %s and %d scored nodes: %+v", pod.Name,
		len(scores), scores)
	return framework.NewStatus(framework.Success, "")
}

// Reserve and Unreserve are informational scheduling phases. They exist to work around
// race conditions when resources on a node are being reserved for a particular pod.
func (qos *QosPlugin) Reserve(ctx context.Context, state *framework.CycleState, pod *v1.Pod, nodeName string) *framework.Status {
	klog.V(3).Infof("Reserve called for pod %+v and node %s\n", *pod, nodeName)
	return nil
}

// Unreserve must be idempotent and must not fail.
func (qos *QosPlugin) Unreserve(ctx context.Context, state *framework.CycleState, pod *v1.Pod,
	nodeName string) {
	klog.V(3).Infof("Unreserve called for pod %+v and node %s\n", *pod, nodeName)
}

// Permit plugins can prevent or delay the binding of a pod.
// Only a permit plugin is supposed to 'approve' a waiting pod (which causes it to be sent to binding).
func (qos *QosPlugin) Permit(ctx context.Context, _ *framework.CycleState, pod *v1.Pod, nodeName string) (
	*framework.Status, time.Duration) {
	klog.V(3).Infof("Permit called for pod %s and node %s\n", pod.Name, nodeName)
	if pod.Labels["application-group"] == "" {
		return framework.NewStatus(framework.Success, ""), 0
	}
	preferredNode, s := qos.getPreferredNode(ctx, pod)
	if !s.IsSuccess() {
		return s, 0
	}
	if nodeName != preferredNode {
		klog.Errorf("pod %s wants to be bound to node %s, not %s", pod.Name, preferredNode, nodeName)
		return framework.NewStatus(framework.Unschedulable, "Pod does not want to be bound to this node"), 0
	}

	var duration time.Duration
	status := qos.PodChecker.getChannelStatus(pod)
	if status.Code() == framework.Wait {
		duration = time.Second * time.Duration(qos.ScheduleWaitSeconds)
	} else {
		duration = 0
	}
	klog.V(3).Infof("Permit returning status %v and timeout %v for pod %s\n", *status, duration, pod.Name)
	return status, duration
}

// PreBind is called right before binding happens.
func (qos *QosPlugin) PreBind(ctx context.Context, _ *framework.CycleState,
	pod *v1.Pod, nodeName string) *framework.Status {
	klog.V(3).Infof("PreBind called for pod %s and node %s\n", pod.Name, nodeName)
	if pod.Labels["application-group"] == "" {
		return framework.NewStatus(framework.Success, "")
	}
	preferredNode, status := qos.getPreferredNode(ctx, pod)
	if !status.IsSuccess() {
		return status
	}
	if nodeName != preferredNode {
		klog.Errorf("pod %s wants to be bound to node %s, not %s",
			pod.Name, preferredNode, nodeName)
		return framework.NewStatus(framework.Unschedulable, "Pod does not want to be bound to this node")
	}
	return framework.NewStatus(framework.Success, "")
}

// Bind takes care of putting a pod onto a node.
// In principle, you can update the pod's target node here by setting
// pod.Spec.NodeName. The scheduler will complain but the pod will end up
// on that node if the Bind() call succeeds.
func (qos *QosPlugin) Bind(ctx context.Context, _ *framework.CycleState, pod *v1.Pod, nodeName string) *framework.Status {
	// This makes the next bind plugin pick up.
	return framework.NewStatus(framework.Skip)
}

// PostBind is an informational extension point. Use this to update the application etc.
func (qos *QosPlugin) PostBind(ctx context.Context, _ *framework.CycleState, pod *v1.Pod, nodeName string) {
	klog.V(3).Infof("PostBind called for pod %s and node %s\n", pod.Name, nodeName)
}
