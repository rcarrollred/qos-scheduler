// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package schedulerplugins

import (
	"context"
	"fmt"
	"time"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog/v2"
	"k8s.io/kubernetes/pkg/scheduler/framework"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	channelStatusCheckIntervalSeconds = 5
)

type podChannelStatusChecker struct {
	frameworkHandle framework.Handle
	k8sClient       client.Client
	pluginName      string
	ctx             context.Context
}

// podKey returns the internal name of the pod
func podKey(podname string, namespace string) string {
	return fmt.Sprintf("%s/%s", namespace, podname)
}

func NewPodChannelStatusChecker(handle framework.Handle, k8sClient client.Client, pluginName string) *podChannelStatusChecker {
	p := podChannelStatusChecker{
		frameworkHandle: handle,
		k8sClient:       k8sClient,
		pluginName:      pluginName,
		ctx:             context.Background(),
	}
	go func() {
		podChannelStatusTicker := time.NewTicker(time.Second * channelStatusCheckIntervalSeconds)
		for range podChannelStatusTicker.C {
			p.frameworkHandle.IterateOverWaitingPods(func(waitingPod framework.WaitingPod) {
				klog.V(3).Infof("is pod %s ready now?", waitingPod.GetPod().Name)
				p.checkPodStatus(waitingPod)
			})
		}
	}()
	return &p
}

// See if the pod can be removed from the waiting list.
func (p *podChannelStatusChecker) checkPodStatus(waitingPod framework.WaitingPod) {
	status := p.getChannelStatus(waitingPod.GetPod())
	klog.V(3).Infof("pod %s now has channel status %v", waitingPod.GetPod().Name, *status)
	if status.Code() == framework.Success {
		waitingPod.Allow(p.pluginName)
	} else if status.Code() == framework.UnschedulableAndUnresolvable {
		waitingPod.Reject(p.pluginName, status.Message())
	}
}

func (p *podChannelStatusChecker) getAssignmentPlanList(pod *v1.Pod) (*crd.AssignmentPlanList, error) {
	apList := &crd.AssignmentPlanList{}
	listOptions := []client.ListOption{
		client.InNamespace(pod.Namespace),
		client.MatchingLabels{"application-group": pod.Labels["application-group"]},
	}
	err := p.k8sClient.List(p.ctx, apList, listOptions...)
	return apList, err
}

func (p *podChannelStatusChecker) getChannels(pod *v1.Pod) ([]string, *framework.Status) {
	apList, err := p.getAssignmentPlanList(pod)
	if err != nil {
		return []string{}, framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Error getting assignment plan list: %v\n", err))
	}
	for idx := range apList.Items {
		if apStatus, ok := apList.Items[idx].Status.PodStatus[podKey(pod.Name, pod.Namespace)]; ok {
			if ok {
				return apStatus.Channels, framework.NewStatus(framework.Success, "")
			}
		}
	}
	return []string{}, framework.NewStatus(framework.Unschedulable, fmt.Sprintf("Missing schedule plan entry for pod %s/%s", pod.Name, pod.Namespace))
}

func (p *podChannelStatusChecker) getChannelStatus(pod *v1.Pod) *framework.Status {
	channelIDs, status := p.getChannels(pod)
	if len(channelIDs) == 0 {
		return status
	}
	ooqos := false
	rejected := false
	allAck := true
	var channel crd.Channel
	for _, channelID := range channelIDs {
		err := p.k8sClient.Get(p.ctx, types.NamespacedName{Namespace: pod.Namespace, Name: channelID}, &channel)
		if err == nil {
			channelStatus := infrastructure.EvaluateChannel(&channel)
			if channelStatus.ChannelStatus != crd.ChannelRunning {
				allAck = false
			}
			if channelStatus.ChannelStatus == crd.ChannelRejected {
				rejected = true
				break
			}
			if channelStatus.ChannelStatus == crd.ChannelOOQOS {
				ooqos = true
				break
			}
		} else {
			allAck = false
		}
	}
	if ooqos {
		// This is theoretically fixable if the channel recovers, but probably not.
		return framework.NewStatus(framework.Unschedulable, "Channel out of qos")
	}
	if rejected {
		return framework.NewStatus(framework.Unschedulable, "Channel rejected")
	}
	if allAck {
		// All channels are good, let the Pod schedule.
		return framework.NewStatus(framework.Success, "")
	}
	// Not all channels are ACK yet, but none have definitely failed. Wait.
	return framework.NewStatus(framework.Wait, "Pod waiting for channels to ACK")
}
