// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"fmt"
	"os"

	"k8s.io/kubernetes/cmd/kube-scheduler/app"

	"siemens.com/qos-scheduler/scheduler/schedulerplugins"
)

func main() {
	command := app.NewSchedulerCommand(
		app.WithPlugin(schedulerplugins.Name, schedulerplugins.New),
	)
	if err := command.Execute(); err != nil {
		fmt.Printf("problem running scheduler plugin: %s", err)
		os.Exit(1)
	}
}
