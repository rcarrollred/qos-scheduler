#!/bin/sh
# SPDX-FileCopyrightText: A kind stranger on the internet
# SPDX-License-Identifier: CC-BY-SA-4.0


# This automates the workaround for k8s.io/kubernetes version pinning.
# I ran this with version set to 1.21.1 and then modified the resulting
# go.mod file to ensure k8s.io.kubernetes was pinned to v1.21.1
# I got this out of the discussion of https://github.com/kubernetes/kubernetes/issues/79384
set -euo pipefail

VERSION=${1#"v"}
if [ -z "$VERSION" ]; then
    echo "Must specify version!"
    exit 1
fi
MODS=($(
    curl -sS https://raw.githubusercontent.com/kubernetes/kubernetes/v${VERSION}/go.mod |
    sed -n 's|.*k8s.io/\(.*\) => ./staging/src/k8s.io/.*|k8s.io/\1|p'
))
for MOD in "${MODS[@]}"; do
    echo "downloading ${MOD}@kubernetes-${VERSION}"
    V=$(
        go mod download -json "${MOD}@kubernetes-${VERSION}" |
        sed -n 's|.*"Version": "\(.*\)".*|\1|p'
    )
    go mod edit "-replace=${MOD}=${MOD}@${V}"
done
go get "k8s.io/kubernetes@v${VERSION}"
