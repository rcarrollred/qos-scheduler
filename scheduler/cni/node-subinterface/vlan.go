// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// This works a lot like the vlan plugin from the cni plugins collection
// but it creates the vlan-tagged subinterface on the node rather than in
// the pod.

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"syscall"

	"github.com/vishvananda/netlink"

	"github.com/containernetworking/cni/pkg/skel"
	"github.com/containernetworking/cni/pkg/types"
	current "github.com/containernetworking/cni/pkg/types/100"
	"github.com/containernetworking/cni/pkg/version"
	"github.com/containernetworking/plugins/pkg/ip"
	"github.com/containernetworking/plugins/pkg/ipam"
	"github.com/containernetworking/plugins/pkg/ns"
	bv "github.com/containernetworking/plugins/pkg/utils/buildversion"
	"github.com/containernetworking/plugins/pkg/utils/sysctl"
)

const defaultParent = "eth0"

type NetConf struct {
	types.NetConf
	Vlan        int    `json:"vlan,omitempty"`
	Parent      string `json:"parent,omitempty"`
	Gateway     string `json:"gateway,omitempty"`
	ThisNodeIP  string `json:"thisNode,omitempty"`
	OtherNodeIP string `json:"otherNode,omitempty"`
	QosPriority int    `json:"qosPriority,omitempty"`
	MaskOnes    int    `json:"maskOnes,omitempty"`

	// These are used by the veth pair creation.
	MTU int `json:"mtu"`
	mac string

	Args struct {
		Cni VlanArgs `json:"cni,omitempty"`
	} `json:"args,omitempty"`
}

type VlanArgs struct {
	Vlan int `json:"vlan,omitempty"`

	// The network interface to create a subinterface of on the node.
	Parent string `json:"parent,omitempty"`

	// The first address from the network range passed in IPs for this
	// pod. By convention, the first address goes to the gateway (aka the
	// node end of the veth pair created for the pod), and the second one
	// goes to the pod end of that veth pair. You would normally do this via
	// IPAM but I have to fit this configuration into the cni args.
	Gateway string `json:"gateway,omitempty"`

	// The IP address to give to the subinterface on the node.
	// You can leave this empty if you want to use the IP address of the `Parent`
	// interface.
	ThisNodeIP string `json:"thisNode,omitempty"`

	// The IP address of the node where the other end of the stream is.
	// This is going to be the next hop in the route for the vlan pod subnet.
	// If you're using a node overlay (i.e. if 'ThisNodeIP' is not empty),
	// then the OtherNodeIP address needs to be from the node overlay.
	// Otherwise it's just the IP address of the tsn-connected interface on the
	// other node.
	OtherNodeIP string `json:"otherNode,omitempty"`

	// The qos egress priority to configure on the network sub-interface associated
	// with this stream.
	QosPriority int `json:"qosPriority,omitempty"`

	// Number of bits to set to 1 in the mask for pod and node.
	MaskOnes int `json:"maskOnes,omitempty"`
}

func init() {
	// this ensures that main runs only on main thread (thread group leader).
	// since namespace ops (unshare, setns) are done for a single thread, we
	// must ensure that the goroutine does not jump from OS thread to thread
	runtime.LockOSThread()
}

func loadNetConf(bytes []byte, envArgs string) (*NetConf, string, error) {
	n := &NetConf{}
	if err := json.Unmarshal(bytes, n); err != nil {
		return nil, "", fmt.Errorf("failed to load netconf: %v", err)
	}
	if n.Vlan < 0 || n.Vlan > 4094 {
		return nil, "", fmt.Errorf("invalid VLAN ID %d (must be between 0 and 4094)", n.Vlan)
	}

	if vlan := n.Args.Cni.Vlan; vlan != 1 {
		n.Vlan = vlan
	}

	if parent := n.Args.Cni.Parent; parent != "" {
		n.Parent = parent
	} else {
		n.Parent = defaultParent
	}

	gateway := n.Args.Cni.Gateway
	if gateway == "" {
		return nil, "", fmt.Errorf("must have a gateway specified in the cni args")
	}
	n.Gateway = gateway

	thisNode := n.Args.Cni.ThisNodeIP
	n.ThisNodeIP = thisNode

	otherNode := n.Args.Cni.OtherNodeIP
	if otherNode == "" {
		return nil, "", fmt.Errorf("must have address of node at the other end of the stream")
	}
	n.OtherNodeIP = otherNode

	n.QosPriority = n.Args.Cni.QosPriority

	n.MaskOnes = n.Args.Cni.MaskOnes
	if n.MaskOnes < 1 || n.MaskOnes > 32 {
		return nil, "", fmt.Errorf("illegal value %d for cidr mask", n.MaskOnes)
	}
	return n, n.CNIVersion, nil
}

// Create a veth pair linking the container network namespace to the
// node network namespace.
func setupVeth(netns ns.NetNS, ifName string, mtu int, mac string) (
	*current.Interface, *current.Interface, error) {
	contIface := &current.Interface{}
	hostIface := &current.Interface{}

	err := netns.Do(func(hostNS ns.NetNS) error {
		// create the veth pair in the container and move host end into host netns
		hostVeth, containerVeth, err := ip.SetupVeth(ifName, mtu, mac, hostNS)
		if err != nil {
			return err
		}
		contIface.Name = containerVeth.Name
		contIface.Mac = containerVeth.HardwareAddr.String()
		contIface.Sandbox = netns.Path()
		hostIface.Name = hostVeth.Name
		return nil
	})
	if err != nil {
		return nil, nil, err
	}

	// need to lookup hostVeth again as its index has changed during ns move
	hostVeth, err := netlink.LinkByName(hostIface.Name)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to lookup %q: %v", hostIface.Name, err)
	}
	hostIface.Mac = hostVeth.Attrs().HardwareAddr.String()

	return hostIface, contIface, nil
}

// This takes a network address range and the address of the gateway as a string.
// It returns the address for the pod and for the gateway as net.IPNet objects.
func calculateAddresses(networkAddress net.IPNet, gatewayIP string) (net.IPNet, net.IPNet, error) {
	if networkAddress.IP.To4() == nil {
		return networkAddress, networkAddress, fmt.Errorf("IPv6 is not supported")
	}
	gatewayAddress := net.ParseIP(gatewayIP)
	if !networkAddress.Contains(gatewayAddress) {
		return networkAddress, networkAddress, fmt.Errorf("the gateway address %q is not part of the network address range %+v", gatewayAddress, networkAddress)
	}
	podAddress := net.IPNet{
		IP:   ip.NextIP(gatewayAddress),
		Mask: networkAddress.Mask,
	}
	return podAddress, net.IPNet{IP: gatewayAddress, Mask: net.CIDRMask(32, 32)}, nil
}

// Modify result so the first IP entry can be used to configure the pod's network interface.
func setupResultForIfaceConfiguration(result *current.Result, podAddress net.IPNet, gatewayAddress net.IPNet, interfaceIndex int) {
	podIpc := result.IPs[0]
	podIpc.Address.IP = podAddress.IP
	podIpc.Address.Mask = podAddress.Mask
	podIpc.Interface = current.Int(interfaceIndex)
	podIpc.Gateway = gatewayAddress.IP
}

// This creates routes equivalent to
// ip route add podAddress.IP dev vethnode proto kernel scope host
// ip route add networkAddress via n.OtherNodeIP dev subinterface
func calculateNodeRoutes(networkAddress *net.IPNet, podAddress net.IPNet,
	otherNodeIP string, vethIndex int, nodeIfIndex int) []netlink.Route {

	routesToAdd := make([]netlink.Route, 2)

	// Route to the pod via the veth end
	routesToAdd[0] = netlink.Route{
		LinkIndex: vethIndex,
		Dst: &net.IPNet{
			IP:   podAddress.IP,
			Mask: net.CIDRMask(32, 32),
		},
		Scope: netlink.SCOPE_HOST,
	}

	routesToAdd[1] = netlink.Route{
		LinkIndex: nodeIfIndex,
		Dst:       networkAddress,
		Gw:        net.ParseIP(otherNodeIP),
	}

	return routesToAdd
}

func calculatePodRoutes(networkAddress *net.IPNet, podAddress net.IPNet,
	gateway net.IPNet, vethIndex int) ([]netlink.Route, []netlink.Route) {

	routesToDelete := make([]netlink.Route, 1)

	// Remove the route that was created automatically.
	routesToDelete[0] = netlink.Route{
		LinkIndex: vethIndex,
		Dst:       networkAddress,
		Scope:     netlink.SCOPE_LINK,
	}

	routesToAdd := make([]netlink.Route, 2)

	// Add link to gateway
	routesToAdd[0] = netlink.Route{
		LinkIndex: vethIndex,
		Dst: &net.IPNet{
			IP:   gateway.IP,
			Mask: net.CIDRMask(32, 32),
		},
		Src:   podAddress.IP,
		Scope: netlink.SCOPE_LINK,
	}

	// Add route to networkaddress via gateway
	routesToAdd[1] = netlink.Route{
		LinkIndex: vethIndex,
		Dst:       networkAddress,
		Gw:        gateway.IP,
	}

	return routesToDelete, routesToAdd
}

func cmdAdd(args *skel.CmdArgs) error {
	var success bool = false

	log.Printf("starting cmdAdd of vlan plugin with args %s\n", args.Args)

	n, cniVersion, err := loadNetConf(args.StdinData, args.Args)
	if err != nil {
		return err
	}

	log.Printf("at start of vlan plugin. n %v args.Args %s\n", *n, args.Args)

	// Make sure the interface for the vlan traffic exists on the node.
	parentInterface, err := netlink.LinkByName(n.Parent)
	if err != nil {
		return fmt.Errorf("the network interface %s does not exist on the node: %v", n.Parent, err)
	}

	// run the IPAM plugin and get back the config to apply
	r, err := ipam.ExecAdd(n.IPAM.Type, args.StdinData)
	if err != nil {
		return err
	}

	// release IP in case of failure
	defer func() {
		if !success {
			ipam.ExecDel(n.IPAM.Type, args.StdinData)
		}
	}()

	// Convert whatever the IPAM result was into the current Result type
	// Normally you should also process the previous result here in case
	// this plugin is part of a chain, but in practice, this plugin does
	// not get chained, or when it is chained, it is first in the chain.
	ipamResult, err := current.NewResultFromResult(r)
	if err != nil {
		return err
	}
	if len(ipamResult.IPs) == 0 {
		return errors.New("IPAM plugin returned missing IP config")
	}

	// Get the container network namespace.
	netns, err := ns.GetNS(args.Netns)
	if err != nil {
		return fmt.Errorf("failed to open netns %q: %v", args.Netns, err)
	}
	defer netns.Close()

	// Create the veth pair that links the node to the pod.
	hostInterface, containerInterface, err := setupVeth(netns, args.IfName, n.MTU, n.mac)
	if err != nil {
		return err
	}

	// Make sure the ipam settings contain the interface we want.
	result := &current.Result{
		CNIVersion: current.ImplementedSpecVersion,
		Interfaces: []*current.Interface{
			containerInterface,
			hostInterface,
		},
		IPs:    ipamResult.IPs,
		Routes: ipamResult.Routes,
		DNS:    n.DNS,
	}

	// At this point, ipamResult.IPs[0].Address is the address range for the pod-vlan subnet.
	networkAddress := ipamResult.IPs[0].Address

	podAddress, gatewayAddress, err := calculateAddresses(networkAddress, n.Gateway)
	if err != nil {
		return err
	}

	// Modify result so the first IP address can be passed to ConfigureIface.
	setupResultForIfaceConfiguration(result, podAddress, gatewayAddress, 0)

	// Configure the container hardware address and IP address(es)
	if err := netns.Do(func(_ ns.NetNS) error {
		_, _ = sysctl.Sysctl(fmt.Sprintf("net/ipv4/conf/%s/arp_notify", args.IfName), "1")
		log.Printf("configuring interface %s using %+v\n", args.IfName, result)
		// Add the IP to the interface
		if err := ipam.ConfigureIface(args.IfName, result); err != nil {
			return err
		}
		return nil
	}); err != nil {
		return err
	}

	if err = ip.EnableIP4Forward(); err != nil {
		return fmt.Errorf("failed to enable forwarding: %v", err)
	}

	hostVethLink, err := netlink.LinkByName(hostInterface.Name)
	if err != nil {
		return fmt.Errorf("failed to look up veth end %s in node: %v", hostInterface.Name, err)
	}

	// Set the gateway address on the node end of the veth pair.
	gwAddr := &netlink.Addr{IPNet: &gatewayAddress, Label: ""}
	if err := netlink.AddrAdd(hostVethLink, gwAddr); err != nil && err != syscall.EEXIST {
		return fmt.Errorf("could not add IP address to %q: %v", hostVethLink, err)
	}

	// Set up routes for the pod vlan subnet in the container
	if err := netns.Do(func(_ ns.NetNS) error {

		vethEnd, err := net.InterfaceByName(args.IfName)
		if err != nil {
			log.Printf("failed to find interface %s in container: %v", args.IfName, err)
			return err
		}
		log.Printf("found interface %s in container\n", args.IfName)

		routesToDelete, routesToAdd := calculatePodRoutes(
			&networkAddress, podAddress, gatewayAddress, vethEnd.Index)

		for _, rtd := range routesToDelete {
			log.Printf("route to delete: %+v\n", rtd)
			if err = netlink.RouteDel(&rtd); err != nil {
				return fmt.Errorf("failed to delete route %v: %v", rtd, err)
			}
		}

		for _, rta := range routesToAdd {
			log.Printf("route to add: %+v\n", rta)
			if err = netlink.RouteAdd(&rta); err != nil {
				return fmt.Errorf("failed to add route %v: %v", rta, err)
			}
			result.Routes = append(result.Routes, &types.Route{
				Dst: *rta.Dst,
				GW:  rta.Gw,
			})
		}
		return nil
	}); err != nil {
		return err
	}

	subinterfaceName := fmt.Sprintf("%s.%d", n.Parent, n.Vlan)
	_, err = netlink.LinkByName(subinterfaceName)

	if err == nil {
		// This should not happen because cmdDel has to clean up before the old pods using this interface
		// are deleted. But if it does, it is not harmless; maybe cmdDel is just about to delete this
		// interface. Better to fail now; cmdAdd will be retried.
		return fmt.Errorf("the network interface %s already exists on this node", subinterfaceName)
	}
	log.Printf("creating subinterface %s of %s\n", subinterfaceName, n.Parent)

	subinterface := &netlink.Vlan{
		LinkAttrs: netlink.LinkAttrs{
			MTU:         n.MTU,
			Name:        subinterfaceName,
			ParentIndex: parentInterface.Attrs().Index,
		},
		VlanId:       n.Vlan,
		VlanProtocol: netlink.VLAN_PROTOCOL_8021Q,
	}

	if err := netlink.LinkAdd(subinterface); err != nil {
		return fmt.Errorf("failed to create vlan-enabled subinterface: %v", err)
	}
	if err := netlink.LinkSetUp(subinterface); err != nil {
		return fmt.Errorf("failed to bring up subinterface: %v", err)
	}
	log.Printf("created subinterface %s\n", subinterfaceName)

	// Add address specified by "thisNode" to subinterface.
	var subifAddr netlink.Addr
	if n.ThisNodeIP == "" {
		// Use the parent interface's address
		addrs, err := netlink.AddrList(parentInterface, netlink.FAMILY_V4)
		if err != nil {
			return fmt.Errorf("failed to get address of parent interface: %v", err)
		}
		if len(addrs) == 1 {
			return fmt.Errorf("the parent interface %s does not have an ip address", n.Parent)
		}
		subifAddr = addrs[0]
		subifAddr.Label = subinterfaceName
	} else {
		subifIpn := net.IPNet{
			IP:   net.ParseIP(n.ThisNodeIP),
			Mask: net.CIDRMask(n.MaskOnes, 32),
		}
		subifAddr = netlink.Addr{IPNet: &subifIpn, Label: subinterfaceName}
	}

	if err := netlink.AddrAdd(subinterface, &subifAddr); err != nil && err != syscall.EEXIST {
		log.Printf("failed to set address %v on subinterface %s: %v\n", subifAddr, subinterfaceName, err)
		return err
	}

	// Set the egress qos map on the subinterface.
	// The map that this takes maps internal packet priorities to egress priorities. I think you can
	// pass the same egress priority for all internal packet priorities with 0:5 1:5 etc.
	// However, please make sure that the kernel priority that you use for the arp packets (currently,
	// this is 1) is always mapped to qos priority 0. 0 is the default value, so the below "0:5"
	// will map kernel prio 0 to qos prio 5 and all other kernel prios to qos prio 0.
	prioMap := fmt.Sprintf("0:%d", n.QosPriority)
	egressPrio := exec.Command("ip", "link", "set", subinterfaceName, "type", "vlan", "egress", prioMap)
	var out bytes.Buffer
	var stderr bytes.Buffer
	egressPrio.Stdout = &out
	egressPrio.Stderr = &stderr
	err = egressPrio.Run()
	if err != nil {
		log.Printf("failed to set egress priority using %v: %v stderr %s\n", egressPrio, err, stderr.String())
		// not treating this as a fatal error right now.
	}

	hostVethEnd, _ := net.InterfaceByName(hostInterface.Name)
	foundInterface, _ := net.InterfaceByName(subinterfaceName)
	routesToAdd := calculateNodeRoutes(&networkAddress, podAddress, n.OtherNodeIP, hostVethEnd.Index, foundInterface.Index)
	log.Printf("adding routes to node: %+v\n", routesToAdd)
	for _, rta := range routesToAdd {
		if err = netlink.RouteAdd(&rta); err != nil {
			log.Printf("failed to add route: %v: %v\n", rta, err)
			return err
		}
	}

	log.Printf("added routes, now doing arptables\n")

	// Set the arp packet priority to 1
	arpc := exec.Command("arptables", "-A", "OUTPUT", "-o", subinterfaceName, "-j", "CLASSIFY", "--set-class", "0:1")
	out.Reset()
	stderr.Reset()
	arpc.Stdout = &out
	arpc.Stderr = &stderr
	err = arpc.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", arpc, err, stderr.String())
		return err
	}

	log.Printf("added arptables entry, now doing iptables\n")

	// allow forwarding via iptables for subinterface
	fwdi := exec.Command("iptables", "-A", "FORWARD", "-w", "-i", subinterfaceName, "-j", "ACCEPT")
	out.Reset()
	stderr.Reset()
	fwdi.Stdout = &out
	fwdi.Stderr = &stderr
	err = fwdi.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", fwdi, err, stderr.String())
	}
	fwdo := exec.Command("iptables", "-A", "FORWARD", "-w", "-o", subinterfaceName, "-j", "ACCEPT")
	out.Reset()
	stderr.Reset()
	fwdo.Stdout = &out
	fwdo.Stderr = &stderr
	err = fwdo.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", fwdo, err, stderr.String())
	}

	log.Printf("added iptables\n")

	success = true
	err = types.PrintResult(result, cniVersion)
	if err != nil {
		log.Printf("PrintResult says %v\n", err)
	}

	log.Printf("at end of cmdAdd, returning %v\n", err)

	return err
}

func cmdDel(args *skel.CmdArgs) error {
	n, _, err := loadNetConf(args.StdinData, args.Args)
	_ = n
	if err != nil {
		return err
	}

	ipamDel := func() error {
		if err := ipam.ExecDel(n.IPAM.Type, args.StdinData); err != nil {
			return err
		}
		return nil
	}
	defer ipamDel()

	if args.Netns != "" {

		// Delete the extra network interface on the pod.
		err = ns.WithNetNSPath(args.Netns, func(_ ns.NetNS) error {
			var err error
			_, err = ip.DelLinkByNameAddr(args.IfName)
			if err != nil && err == ip.ErrLinkNotFound {
				return nil
			}
			return err
		})
		if err != nil {
			// I'm not sure if it's possible to get here but better to be safe, errors in cmdDel are nasty.
			_, ok := err.(ns.NSPathNotExistErr)
			if ok {
				return nil
			}
			return err
		}
	}

	subinterfaceName := fmt.Sprintf("%s.%d", n.Parent, n.Vlan)

	// Deleting the subinterface should also delete the routes associated with it.
	subif, err := netlink.LinkByName(subinterfaceName)
	if err == nil {
		// The subinterface exists, try to delete it.
		err := netlink.LinkDel(subif)
		if err != nil {
			log.Printf("failed to delete subinterface %s: %v\n", subinterfaceName, err)
			// I think you want to retry this, this should not be ignored.
			return err
		}
	}
	var out bytes.Buffer
	var stderr bytes.Buffer

	// The arptables deletion line has to match the creation line exactly.
	arpc := exec.Command("arptables", "-D", "OUTPUT", "-j", "CLASSIFY", "-o", subinterfaceName, "--set-class", "0:1")
	arpc.Stdout = &out
	arpc.Stderr = &stderr
	err = arpc.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", arpc, err, stderr.String())
	}

	// The below lines have to match what you did in creating the iptables rules in cmdAdd.
	// iptables -D returns an error if you try to delete a rule that does not exist,
	// so this just prints the error if any and continues.
	fwdi := exec.Command("iptables", "-D", "FORWARD", "-w", "-i", subinterfaceName, "-j", "ACCEPT")
	fwdi.Stdout = &out
	fwdi.Stderr = &stderr
	err = fwdi.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", fwdi, err, stderr.String())
	}

	fwdo := exec.Command("iptables", "-D", "FORWARD", "-w", "-o", subinterfaceName, "-j", "ACCEPT")
	out.Reset()
	stderr.Reset()
	fwdo.Stdout = &out
	fwdo.Stderr = &stderr
	err = fwdo.Run()
	if err != nil {
		log.Printf("failed to run command %v: %v stderr %s\n", fwdo, err, stderr.String())
	}

	return nil
}

func cmdCheck(args *skel.CmdArgs) error {
	n, _, err := loadNetConf(args.StdinData, args.Args)
	if err != nil {
		return err
	}
	netns, err := ns.GetNS(args.Netns)
	if err != nil {
		return fmt.Errorf("failed to open netns %q: %v", args.Netns, err)
	}
	defer netns.Close()

	// run the IPAM plugin and get back the config to apply
	err = ipam.ExecCheck(n.IPAM.Type, args.StdinData)
	if err != nil {
		return err
	}

	// Parse previous result.
	if n.NetConf.RawPrevResult == nil {
		return fmt.Errorf("required prevResult missing")
	}

	if err := version.ParsePrevResult(&n.NetConf); err != nil {
		return err
	}
	result, err := current.NewResultFromResult(n.PrevResult)
	if err != nil {
		return err
	}
	log.Printf("result now: %+v\n", result)

	// TODO: look at result.Interfaces, maybe also at routes

	return nil
}

func main() {
	var err error
	f, err := os.OpenFile("/var/log/node-vlan.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0640)
	if err != nil {
		log.Printf("error: cannot create log file %v\n", err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	skel.PluginMain(cmdAdd, cmdCheck, cmdDel, version.All, bv.BuildString("nodeside vlan plugin"))

}
