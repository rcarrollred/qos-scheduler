<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Sample CNI plugin

This is a sample CNI plugin.

It is intended as a demonstration of what can be done, but should not be used
in production without careful review.

If you are looking for better examples, try the sample CNI plugins here:
<https://github.com/containernetworking/plugins/tree/main>.
