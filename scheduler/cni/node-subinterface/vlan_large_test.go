// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build runasroot
// +build runasroot

package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"

	"github.com/containernetworking/cni/pkg/skel"
	"github.com/containernetworking/cni/pkg/types"
	types100 "github.com/containernetworking/cni/pkg/types/100"
	"github.com/containernetworking/plugins/pkg/ip"
	"github.com/containernetworking/plugins/pkg/ns"
	"github.com/containernetworking/plugins/pkg/testutils"

	"github.com/vishvananda/netlink"

	"github.com/containernetworking/plugins/plugins/ipam/host-local/backend/allocator"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

const (
	PODNWIF = "ethvlan"
)

type CniArgs struct {
	Vlan        int    `json:"vlan"`
	Parent      string `json:"parent"`
	Gateway     string `json:"gateway"`
	ThisNode    string `json:"thisNode"`
	OtherNode   string `json:"otherNode"`
	QosPriority int    `json:"qosPriority"`
	MaskOnes    int    `json:"maskOnes"`
}

type Net struct {
	Name       string                `json:"name"`
	CNIVersion string                `json:"cniVersion"`
	Type       string                `json:"type,omitempty"`
	IPAM       *allocator.IPAMConfig `json:"ipam"`
	Args       struct {
		Cni CniArgs `json:"cni"`
	} `json:"args"`
	RawPrevResult map[string]interface{} `json:"prevResult,omitempty"`
	PrevResult    types100.Result        `json:"-"`
}

// testCase defines the Cni parameters for a test case.
type testCase struct {
	cniVersion  string // CNI Version
	subnet      string // The IP arg
	vlan        int
	parent      string
	gateway     string
	otherNode   string
	thisNode    string
	qosPriority int
	maskOnes    int
	envArgs     map[string]string

	// expected items generated
	expGWCIDRs []string // Expected gateway addresses in CIDR form
	//AddErr020  string
	//DelErr020  string
	//AddErr010  string
	//DelErr010  string
}

// netConf() creates a NetConf structure for a test case.
func (tc testCase) netConf() *NetConf {
	return &NetConf{
		NetConf: types.NetConf{
			CNIVersion: tc.cniVersion,
			Name:       "testConfig",
			Type:       "node-vlan",
		},
		MTU:         5000,
		Vlan:        tc.vlan,
		Parent:      "fakeeth0",
		Gateway:     tc.gateway,
		OtherNodeIP: tc.otherNode,
		ThisNodeIP:  tc.thisNode,
		QosPriority: tc.qosPriority,
		MaskOnes:    tc.maskOnes,
	}
}

// Snippets for generating a JSON network configuration string.
const (
	netConfStr = `
	"cniVersion": "%s",
	"name": "testConfig",
	"type": "ndoe-vlan",
   "runtimeConfig": { "IPs": ["%s"] },
	"ipam": { "type": "static" }`

	argsFormat = `,
	"args": { "cni": { "vlan": %d, "parent": "%s", "gateway": "%s", "thisNode": "%s", "otherNode": "%s", "qosPriority": %d, "maskOnes": %d }, "IPs": "%s" }`
)

// netConfJSON() generates a JSON network configuration string
// for a test case.
func (tc testCase) netConfJSON() string {
	conf := fmt.Sprintf(netConfStr, tc.cniVersion, tc.subnet)
	conf += fmt.Sprintf(argsFormat, tc.vlan, tc.parent, tc.gateway, tc.thisNode, tc.otherNode, tc.qosPriority, tc.maskOnes, tc.subnet)
	return "{" + conf + "}"
}

var counter uint

// createCmdArgs generates network configuration and creates command
// arguments for a test case.
func (tc testCase) createCmdArgs(targetNS ns.NetNS) *skel.CmdArgs {
	conf := tc.netConfJSON()
	return &skel.CmdArgs{
		ContainerID: fmt.Sprintf("dummy-%d", counter),
		Netns:       targetNS.Path(),
		IfName:      PODNWIF, // This is the interface to use on the pod
		StdinData:   []byte(conf),
		Args:        fmt.Sprintf("IP=%s", tc.subnet),
	}
}

// createCheckCmdArgs generates network configuration and creates command
// arguments for a Check test case.
func (tc testCase) createCheckCmdArgs(targetNS ns.NetNS, config *Net) *skel.CmdArgs {

	conf, err := json.Marshal(config)
	Expect(err).NotTo(HaveOccurred())

	return &skel.CmdArgs{
		ContainerID: fmt.Sprintf("dummy-%d", counter),
		Netns:       targetNS.Path(),
		IfName:      PODNWIF,
		Args:        fmt.Sprintf("ips: %s", tc.subnet),
		StdinData:   []byte(conf),
	}
}

// expectedCIDRs determines the IPv4 in which the resulting
// addresses are expected to be contained.
func (tc testCase) expectedCIDRs() []*net.IPNet {
	var cidrsV4 []*net.IPNet
	appendSubnet := func(subnet string) {
		_, cidr, err := net.ParseCIDR(subnet)
		Expect(err).NotTo(HaveOccurred())
		cidrsV4 = append(cidrsV4, cidr)
	}
	if tc.subnet != "" {
		appendSubnet(tc.subnet)
	}
	return cidrsV4
}

func cmdCheckTest(tc testCase, targetNS ns.NetNS, testNS ns.NetNS, prevResult types.Result) error {
	confString := tc.netConfJSON()
	conf := &Net{}
	err := json.Unmarshal([]byte(confString), &conf)
	Expect(err).NotTo(HaveOccurred())

	config := make(map[string]interface{})
	confBytes, err := json.Marshal(conf)
	Expect(err).NotTo(HaveOccurred())
	err = json.Unmarshal(confBytes, &config)
	Expect(err).NotTo(HaveOccurred())
	config["name"] = "testConfig"
	config["cniVersion"] = tc.cniVersion
	config["prevResult"] = prevResult

	newBytes, err := json.Marshal(config)
	Expect(err).NotTo(HaveOccurred())
	err = json.Unmarshal(newBytes, &conf)
	Expect(err).NotTo(HaveOccurred())

	args := tc.createCheckCmdArgs(targetNS, conf)
	if args == nil {
		return fmt.Errorf("failed to create cmd args")
	}
	err = testNS.Do(func(ns.NetNS) error {
		defer GinkgoRecover()
		err := testutils.CmdCheckWithArgs(args, func() error {
			return cmdCheck(args)
		})
		Expect(err).NotTo(HaveOccurred())

		return nil
	})
	Expect(err).NotTo(HaveOccurred())

	return nil
}

func cmdDelTest(tc testCase, targetNS ns.NetNS, testNS ns.NetNS) error {
	args := tc.createCmdArgs(targetNS)
	if args == nil {
		return fmt.Errorf("failed to create cmd args")
	}
	err := testNS.Do(func(ns.NetNS) error {
		defer GinkgoRecover()
		err := testutils.CmdDelWithArgs(args, func() error {
			return cmdDel(args)
		})
		Expect(err).NotTo(HaveOccurred())

		vethlink, err := netlink.LinkByName(tc.parent)
		Expect(err).NotTo(HaveOccurred())
		routes, err := netlink.RouteList(vethlink, netlink.FAMILY_V4)
		Expect(err).NotTo(HaveOccurred())
		Expect(len(routes)).To(Equal(1))

		return nil
	})
	Expect(err).NotTo(HaveOccurred())

	return nil
}

func cmdAddTest(tc testCase, targetNS ns.NetNS, testNS ns.NetNS) (types.Result, error) {
	args := tc.createCmdArgs(targetNS)
	if args == nil {
		return nil, fmt.Errorf("failed to create cmd args")
	}
	var result *types100.Result

	fmt.Printf("running test case %+v\n", tc)

	var podGw net.IP
	var podAddr net.IPNet

	// testNS represents the node network namespace.
	err := testNS.Do(func(ns.NetNS) error {
		defer GinkgoRecover()
		r, _, err := testutils.CmdAddWithArgs(args, func() error {
			return cmdAdd(args)
		})
		fmt.Printf("cmd add with args: r %+v err %v\n", r, err)
		Expect(err).NotTo(HaveOccurred())
		resultType, err := r.GetAsVersion(tc.cniVersion)
		Expect(err).NotTo(HaveOccurred())
		result = resultType.(*types100.Result)

		podAddr = result.IPs[0].Address
		podGw = result.IPs[0].Gateway
		Expect(result.IPs[0].Gateway.String()).To(Equal(tc.gateway))

		// Expect to see:
		// subinterface with routes away from the node
		subiflink, _ := netlink.LinkByName(fmt.Sprintf("%s.%d", tc.parent, tc.vlan))
		routes, err := netlink.RouteList(subiflink, netlink.FAMILY_V4)
		Expect(err).NotTo(HaveOccurred())
		fmt.Printf("routes have been created: %+v\n", routes)
		fmt.Printf("looking for a route to %s via %s\n", tc.subnet, tc.otherNode)
		foundRouteToOtherPod := false
		for _, r := range routes {
			if r.Gw.String() == tc.otherNode {
				Expect(r.Dst.String()).To(Equal(tc.subnet))
				foundRouteToOtherPod = true
				break
			}
		}
		Expect(foundRouteToOtherPod).To(Equal(true))

		return nil
	})
	Expect(err).NotTo(HaveOccurred())
	fmt.Printf("result: %+v\n", *result)

	fmt.Printf("pod address: %+v\n", podAddr)

	// the equivalent check in the pod namespace:
	err = targetNS.Do(func(ns.NetNS) error {
		defer GinkgoRecover()
		// expect a route with link scope to the gateway
		// expect a route to the other pod via the gateway
		vethlink, err := netlink.LinkByName(PODNWIF)
		Expect(err).NotTo(HaveOccurred())
		routes, err := netlink.RouteList(vethlink, netlink.FAMILY_V4)
		Expect(err).NotTo(HaveOccurred())
		for _, r := range routes {
			fmt.Printf("after add: route %+v via %s exists in targetns\n", r, PODNWIF)
			if r.Gw == nil {
				Expect(r.Dst.IP.String()).To(Equal(podGw.String()))
			} else {
				Expect(r.Dst.String()).To(Equal(tc.subnet))
			}
		}
		return nil
	})
	Expect(err).NotTo(HaveOccurred())

	fmt.Printf("result: %+v\n", *result)
	return result, nil
}

var _ = Describe("vlan Operations", func() {
	var originalNS, targetNS ns.NetNS

	BeforeEach(func() {
		// the netns testutils can use this for netns creation so that the
		// test does not write to where the real network namespaces live.
		os.Setenv("XDG_RUNTIME_DIR", "./tmp/netns")
		// Create a new NetNS so we don't modify the host
		var err error
		originalNS, err = testutils.NewNS()
		Expect(err).NotTo(HaveOccurred())
		targetNS, err = testutils.NewNS()
		Expect(err).NotTo(HaveOccurred())

		// This sets up a fake eth0 device so we have somewhere
		// to use as a gateway for the route to the other node.
		// In networking terms, the setup here makes no sense, it is just
		// the simplest one that will make the initialization not fail.
		err = originalNS.Do(func(ns.NetNS) error {
			_, _, err = ip.SetupVeth("fakeeth0", 0, "", targetNS)
			Expect(err).NotTo(HaveOccurred())
			veth, err := netlink.LinkByName("fakeeth0")
			Expect(err).NotTo(HaveOccurred())
			err = netlink.LinkSetUp(veth)
			Expect(err).NotTo(HaveOccurred())
			// Set an address on the veth end inside the pod.
			addr := &netlink.Addr{IPNet: &net.IPNet{IP: net.ParseIP("10.10.0.1"),
				Mask: net.CIDRMask(29, 32)},
				Label: ""}
			err = netlink.AddrAdd(veth, addr)
			Expect(err).NotTo(HaveOccurred())
			return nil
		})
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		Expect(originalNS.Close()).To(Succeed())
		Expect(testutils.UnmountNS(originalNS)).To(Succeed())
		Expect(targetNS.Close()).To(Succeed())
		Expect(testutils.UnmountNS(targetNS)).To(Succeed())
	})

	// For now, only testing with cni version 1.0.0
	ver := "1.0.0"
	for i, tc := range []testCase{
		{
			subnet:      "10.1.2.0/29",
			gateway:     "10.1.2.1",
			otherNode:   "10.10.0.3",
			parent:      "fakeeth0",
			thisNode:    "10.10.0.2",
			qosPriority: 5,
			maskOnes:    29,
			vlan:        20,
			expGWCIDRs:  []string{"10.1.2.1/29"},
		},
		{
			subnet:      "10.1.2.8/29",
			gateway:     "10.1.2.9",
			otherNode:   "10.10.0.3",
			parent:      "fakeeth0",
			thisNode:    "10.10.0.2",
			vlan:        20,
			qosPriority: 4,
			maskOnes:    29,
			expGWCIDRs:  []string{"10.1.2.9/29"},
		},
	} {
		tc := tc
		i := i
		It(fmt.Sprintf("[%s] (%d) configures and deconfigures a subinterface and veth with routes with ADD/DEL", ver, i), func() {
			tc.cniVersion = ver
			prevResult, err := cmdAddTest(tc, targetNS, originalNS)
			Expect(err).NotTo(HaveOccurred())
			cmdCheckTest(tc, targetNS, originalNS, prevResult)
			cmdDelTest(tc, targetNS, originalNS)
		})
	}
})
