// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// OptimizerRetryPolicy specifies how to update optimizer settings
// on retries.
type OptimizerRetryPolicy struct {
	// MaxRetries is the maximum number of time an optimization request
	// will be retried. Retries happen when the optimizer result
	// indicates that no feasible schedule was found within the time limit.
	// +kubebuilder:default:=5
	MaxRetries int `json:"maxRetries"`

	// InitialTimeLimit is the initial value of the time limit passed
	// to the optimizer. If you set this to a value <= 0, the system
	// will set it to a nonzero default. The value is in seconds.
	// +kubebuildeR:default=300
	InitialTimeLimit int `json:"initialTimeLimit"`

	// TimeLimitIncrease is the factor by which the initial time limit will be
	// multiplied on retry. If you set this to a value < 1, the system will use 1.
	// +optional
	// +kubebuilder:default:=1
	TimeLimitIncrease int `json:"timeLimitIncrease,omitempty"`

	// InitialGapLimit is the initial value of the quality gap limit passed
	// to the optimizer. This field only has meaning for optimizers that
	// have a quality threshold. If this is supported and set, then the optimizer
	// is allowed to return as soon as it finds a solution that is at most
	// gapLimit percent worse than the optimal solution.
	// If you set both time limit and gap limit, the optimizer will still return
	// as soon as it hits the gap limit, and if it does not find a solution within
	// both the gap and time limit, it will report the request infeasible.
	// +optional
	InitialGapLimit int `json:"initialGapLimit,omitempty"`

	// GapLimitIncrease is the factor by which the initial gap limit will
	// be multiplied on retry. Has to be at least 1.0.
	// Like the gap limit, this field only has meaning when you use an optimizer that
	// supports a quality threshold.
	// +optional
	// +kubebuilder:default:=1
	GapLimitIncrease int `json:"gapLimitIncrease,omitempty"`
}

// ApplicationGroupSpec describes an application group.
// Members of an application group are identified by having a label
// application-group with value equal to this group's name.
type ApplicationGroupSpec struct {

	// MinMember says how many applications matching the label query
	// need to be either running or waiting in order for scheduling
	// to begin. The default is 1.
	// +optional
	// +kubebuilder:default:=1
	MinMember int32 `json:"minMember,omitempty"`

	// OptimizerRetryPolicy specifies the retry policy on optimizer failures.
	// +optional
	OptimizerRetryPolicy OptimizerRetryPolicy `json:"optimizerRetryPolicy,omitempty" protobuf:"bytes,1,opt,name=optimizerRetryPolicy"`
}

// ApplicationGroupStatus defines the observed state of ApplicationGroup
type ApplicationGroupStatus struct {
	Phase ApplicationGroupPhase `json:"phase,omitempty" protobuf:"bytes,1,opt,name=phase,casttype=ApplicationGroupPhase"`

	LastUpdateTime metav1.Time `json:"lastUpdateTime,omitempty" protobuf:"bytes,3,opt,name=lastUpdateTime"`

	OptimizerAttemptCount int `json:"optimizerAttemptCount"`
}

type ApplicationGroupPhase string

const (
	// The application group is waiting for there to be enough applications so
	// scheduling can begin.
	ApplicationGroupWaiting ApplicationGroupPhase = "Waiting"

	// The application group is waiting for the optimizer callback
	// to continue scheduling.
	ApplicationGroupOptimizing ApplicationGroupPhase = "Optimizing"

	// The application group has received an optimizer plan and
	// triggered scheduling. It will go back to Waiting once the applications
	// are running.
	ApplicationGroupScheduling ApplicationGroupPhase = "Scheduling"

	// Permanent failure, for example due to optimizer returning non-retryable errors.
	ApplicationGroupFailed ApplicationGroupPhase = "Failed"
)

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:shortName=ag
//+kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.phase",description="Application group status"

// ApplicationGroup is a collection of Applications that should be
// scheduled together.
type ApplicationGroup struct {
	metav1.TypeMeta `json:",inline"`

	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApplicationGroupSpec   `json:"spec,omitempty"`
	Status ApplicationGroupStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ApplicationGroupList contains a list of ApplicationGroup
type ApplicationGroupList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ApplicationGroup `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ApplicationGroup{}, &ApplicationGroupList{})
}
