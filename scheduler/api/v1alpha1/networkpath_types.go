// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type NetworkPathSpec struct {
	// The linknode (should be a NetworkEndpoint) where this path starts.
	Start LinkNode `json:"start"`
	// The linknode (should be a NetworkEndpoint) where this path ends.
	End LinkNode `json:"end"`
	// The names of the NetworkLinks along this path spec.
	Links []string `json:"links"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:shortName:nwp

// NetworkPath is a list of NetworkLinks. It connects
// endpoints, which must be NetworkEndpoints.
type NetworkPath struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec NetworkPathSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// NetworkPathList is a list of NetworkPaths.
type NetworkPathList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []NetworkPath `json:"items"`
}

func init() {
	SchemeBuilder.Register(&NetworkPath{}, &NetworkPathList{})
}
