// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"gitlab.eclipse.org/rcarrollred/qos-scheduler/scheduler/assignment"
)

type AssignmentPlanSpec struct {
	// +optional
	QoSModel assignment.QoSModel `protobuf:"bytes,1,opt,name=qoSModel,proto3" json:"qoSModel,omitempty"`
	// +optional
	OptimizerReply assignment.Reply `protobuf:"bytes,1,opt,name=reply,proto3" json:"reply,omitempty"`
	// +optional
	ReplyReceived bool `json:"replyReceived"`
}

type AssignmentPlanStatusEntry struct {
	// list of channel names
	// +optional
	Channels []string `json:"channels,omitempty"`
	// the name of the preferred node
	// +optional
	PreferredNode string `json:"preferredNode,omitempty"`
}

// AssignmentPlanStatus describes the current state of an assignment plan.
type AssignmentPlanStatus struct {
	// map pod name to its current status
	// +optional
	PodStatus map[string]AssignmentPlanStatusEntry `json:"podStatus,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:shortName=ap
//+kubebuilder:printcolumn:name="PodStatus",type="string",JSONPath=".status.podStatus",description="Status of pods"

// AssignmentPlan describes the placement of the workloads and channels of an ApplicationGroup
// on the cluster nodes.
type AssignmentPlan struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AssignmentPlanSpec   `json:"spec"`
	Status AssignmentPlanStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// AssignmentPlanList contains a list of AssignmentPlan
type AssignmentPlanList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []AssignmentPlan `json:"items"`
}

func init() {
	SchemeBuilder.Register(&AssignmentPlan{}, &AssignmentPlanList{})
}
