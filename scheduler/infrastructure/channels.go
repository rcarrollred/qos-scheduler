// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	"strings"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

const (
	DefaultVlanId = 42
)

func isChannelCanceled(channel *crd.Channel) bool {
	return channel.Status.Status == crd.ChannelCanceled
}

func isChannelUp(channel *crd.Channel) bool {
	return channel.Status.Status == crd.ChannelRunning
}

func isChannelPending(channel *crd.Channel) bool {
	return channel.Status.Status == crd.ChannelRequested ||
		channel.Status.Status == "" ||
		channel.Status.Status == crd.ChannelReviewed ||
		channel.Status.Status == crd.ChannelImplementing ||
		channel.Status.Status == crd.ChannelImplemented ||
		channel.Status.Status == crd.ChannelAck
}

func isChannelDeleted(channel *crd.Channel) bool {
	return channel.Status.Status == crd.ChannelDeleted
}

// ChannelServiceName returns the name of the service that will be generated for this channel.
func ChannelServiceName(channel crd.Channel) string {
	return fmt.Sprintf("svc-%s-%s", channel.Name, channel.Namespace)
}

// ChannelId returns an id for this channel that combines namespace and name
// into one string.
func ChannelId(channel crd.Channel) string {
	return fmt.Sprintf("%s_%s", channel.Namespace, channel.Name)
}

// ParseChannelId expects a channel id created by ChannelId() and
// returns a tuple channelName, channelNamespace.
// If parsing is not successful, the original string will be returned as the
// first tuple item and an empty string as the second item.
func ParseChannelId(channelId string) (string, string) {
	parts := strings.Split(channelId, "_")
	if len(parts) < 2 {
		return channelId, ""
	}
	if len(parts) == 2 {
		return parts[1], parts[0]
	}
	return strings.Join(parts[1:], "_"), parts[0]
}

// ChannelSourceApplication returns the name of the application where this channel starts.
func ChannelSourceApplication(channel *crd.Channel) string {
	a, exists := channel.GetLabels()["application"]
	if exists {
		return a
	}
	return ""
}

// ChannelTargetApplication returns the name of the application where this channel ends.
func ChannelTargetApplication(channel *crd.Channel) string {
	a, exists := channel.GetLabels()["to-application"]
	if exists {
		return a
	}
	return ""
}

// ServiceClass is the service class that was specified for this channel,
// such as "ASSURED".
func ServiceClass(channel *crd.Channel) crd.NetworkServiceClass {
	return channel.Spec.ServiceClass
}

// ChannelImplementation returns the network implementation that the channel will be implemented on.
// This is derived from the ServiceClass and the capabilities
// present in this cluster.
func ChannelImplementation(channel *crd.Channel) nwlib.NetworkImplementationClass {
	c, exists := channel.GetLabels()[crd.NetworkLabel]
	if exists {
		return nwlib.NetworkImplementationClass(c)
	}
	return nwlib.BasicNetwork
}

// PhysicalChannelImplementation returns the physical network implementation that the channel will be using.
// This is derived from the ChannelImplementation.
func PhysicalChannelImplementation(channel *crd.Channel) nwlib.NetworkImplementationClass {
	c, exists := channel.GetLabels()[crd.PhysicalNetworkLabel]
	if exists {
		return nwlib.NetworkImplementationClass(c)
	}
	return nwlib.NetworkImplementationClass("")
}

// Compare spec and status.
func doesChannelMeetQoS(channel *crd.Channel) string {
	if !isChannelUp(channel) {
		return "not running"
	}
	if channel.Spec.MaxDelayNanos > 0 && channel.Status.Latency > channel.Spec.MaxDelayNanos {
		return fmt.Sprintf("status latency %d larger than spec %d", channel.Status.Latency, channel.Spec.MaxDelayNanos)
	}
	return ""
}

// ResetChannelStatus resets a channel to its initial state.
func ResetChannelStatus(channel *crd.Channel) error {
	if isChannelUp(channel) {
		return fmt.Errorf("cannot reset channel %s while it is running", channel.Name)
	}
	channel.Status.Status = crd.ChannelRequested
	channel.Status.FrameSize = 0
	channel.Status.Interval = 0
	channel.Status.Latency = 0
	channel.Status.EarliestTransmissionOffset = 0
	channel.Status.LatestTransmissionOffset = 0
	channel.Status.CniMetadata = crd.ChannelCniMetadata{}
	return nil
}

// CompleteChannelSpec fills in framesize and delay if they're missing.
// It tries to estimate a good value for framesize or sendinterval
// if the bandwidth is specified, based on the assumption that
// bandwidth ~ <framesize> bytes every <sendinterval> nanoseconds.
func CompleteChannelSpec(channel *crd.Channel) {

	// There are hardware limitations around large send intervals.
	if channel.Spec.SendInterval > 10000000 {
		fmt.Printf("specified sendInterval is %d which is larger than the maximum allowed (10ms). Setting to 10ms.",
			channel.Spec.SendInterval)
		channel.Spec.SendInterval = 10000000
	}

	if channel.Spec.Framesize > 0 && channel.Spec.SendInterval > 0 {
		// bandwidth should be about:
		// channel.Spec.Framesize * 8 * 1000000000 / channel.Spec.SendInterval bits/s
		estimatedBandwidth := int64(channel.Spec.Framesize) * 8000000000 / int64(channel.Spec.SendInterval)
		if channel.Spec.MinBandwidthBits > 0 && channel.Spec.MinBandwidthBits != estimatedBandwidth {
			// TODO: maybe this should be an error. It means the bandwidth
			// in the spec is inconsistent with what we'll be sending to the
			// network controller.
			fmt.Printf("estimated bandwidth is %d bits/s, specified %d\n",
				estimatedBandwidth, channel.Spec.MinBandwidthBits)
		}
		return
	}
	// If nothing is specified, specify "500 bytes every 100us"
	if channel.Spec.MinBandwidthBits <= 0 {
		if channel.Spec.Framesize <= 0 {
			channel.Spec.Framesize = 500
		}
		if channel.Spec.SendInterval <= 0 {
			channel.Spec.SendInterval = 100000
		}
		return
	}
	if channel.Spec.Framesize <= 0 && channel.Spec.SendInterval <= 0 {
		channel.Spec.Framesize = 500
	}
	if channel.Spec.Framesize <= 0 {
		channel.Spec.Framesize = int32(channel.Spec.MinBandwidthBits * int64(channel.Spec.SendInterval) / 8000000000)
		return
	}
	if channel.Spec.SendInterval <= 0 {
		channel.Spec.SendInterval = int32(8000000000 * int64(channel.Spec.Framesize) / channel.Spec.MinBandwidthBits)
		if channel.Spec.SendInterval > 10000000 {
			channel.Spec.SendInterval = 10000000
		}
	}
}

// StartChannelRunning sets a channel's status to running if possible.
func StartChannelRunning(channel *crd.Channel) bool {
	if channel == nil {
		return false
	}
	if isChannelPending(channel) {
		channel.Status.Status = crd.ChannelRunning
		return true
	}
	return false
}

// CancelChannel cancelx a channel if it is running or pending, otherwise
// do nothing.
func CancelChannel(channel *crd.Channel) bool {
	if channel == nil {
		return false
	}
	if isChannelUp(channel) || isChannelPending(channel) {
		channel.Status.Status = crd.ChannelCanceled
		return true
	}
	return false
}

// DeleteChannel sets a channel to 'Deleted' if possible.
func DeleteChannel(channel *crd.Channel) bool {
	if channel == nil {
		return false
	}
	if isChannelUp(channel) || isChannelPending(channel) {
		return false
	}
	channel.Status.Status = crd.ChannelDeleted
	return true
}

// ChannelStatus represents the status of a channel internally.
type ChannelStatus struct {
	Name          string
	Reason        string
	AppCondition  crd.ApplicationConditionType
	ChannelStatus crd.ChannelStatusCode
}

// AreAllChannelsDeleted returns true iff all channels on the list are deleted.
func AreAllChannelsDeleted(channels *crd.ChannelList) bool {
	for _, channel := range channels.Items {
		if !isChannelDeleted(&channel) {
			return false
		}
	}
	return true
}

// EvaluateChannel gives you the channel status.
// You might want to have a more elaborate reason here so the
// next optimizer call can take the channel failure reason into account.
func EvaluateChannel(channel *crd.Channel) *ChannelStatus {
	if isChannelCanceled(channel) {
		return &ChannelStatus{Name: channel.Name, Reason: "CANCELED", AppCondition: crd.ApplicationChannelCanceled, ChannelStatus: crd.ChannelCanceled}
	}
	if isChannelPending(channel) {
		return &ChannelStatus{Name: channel.Name, Reason: "PENDING", AppCondition: crd.ApplicationOk, ChannelStatus: crd.ChannelRequested}
	}
	if isChannelDeleted(channel) {
		return &ChannelStatus{Name: channel.Name, Reason: "DELETED", AppCondition: crd.ApplicationChannelCanceled, ChannelStatus: crd.ChannelDeleted}
	}
	if !isChannelUp(channel) {
		return &ChannelStatus{Name: channel.Name, Reason: "NACK", AppCondition: crd.ApplicationChannelNack, ChannelStatus: crd.ChannelRejected}
	}
	msg := doesChannelMeetQoS(channel)
	if msg != "" {
		return &ChannelStatus{Name: channel.Name, Reason: msg, AppCondition: crd.ApplicationChannelQoS, ChannelStatus: crd.ChannelOOQOS}
	}
	if isChannelUp(channel) {
		return &ChannelStatus{Name: channel.Name, Reason: "ALLOCATED", AppCondition: crd.ApplicationOk, ChannelStatus: crd.ChannelRunning}
	}
	return &ChannelStatus{Name: channel.Name, Reason: "PENDING", AppCondition: crd.ApplicationOk, ChannelStatus: crd.ChannelRequested}
}

// NetworkEndpointsForChannel identifies the network endpoints that are the source and the target of a given channel.
// If no matching nodes are found, returns empty nodes.
func NetworkEndpointsForChannel(channel crd.Channel, nodelist crd.NetworkEndpointList) (crd.NetworkEndpoint, crd.NetworkEndpoint) {
	var sourceNode crd.NetworkEndpoint
	var targetNode crd.NetworkEndpoint
	for _, ep := range nodelist.Items {
		// channel.Spec.ChannelFrom is endpoint.Spec.NodeName
		if ep.Spec.NodeName == channel.Spec.ChannelFrom {
			sourceNode = ep
		}
		if ep.Spec.NodeName == channel.Spec.ChannelTo {
			targetNode = ep
		}
	}
	return sourceNode, targetNode
}
