// Copyright 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	require "github.com/stretchr/testify/require"
	"net"
	"testing"
)

func TestNewIPManager(t *testing.T) {
	_, err := NewIPManager("10.10.0.7/24")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
}

func TestDoesSubnetFitHere(t *testing.T) {
	mgr, err := NewIPManager("10.10.0.0/20")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
	addrFrom := net.ParseIP("10.10.0.0")
	addrTo := net.ParseIP("10.10.0.1")
	subnet, _ := mgr.doesSubnetFitHere(3, addrFrom, addrTo)
	if subnet != nil {
		t.Errorf("subnet with prefix length 4 should not fit between %s and %s and yet i got %s", addrFrom.String(), addrTo.String(), subnet.String())
	}

	addrTo = net.ParseIP("10.10.0.8")
	subnet, err = mgr.doesSubnetFitHere(3, addrFrom, addrTo)
	if subnet == nil || err != nil {
		t.Errorf("subnet with prefix length 4 should fit between %s and %s and yet i got %v", addrFrom.String(), addrTo.String(), err)
	}

}

func TestGetCidr(t *testing.T) {
	mgr, err := NewIPManager("10.10.0.0/16")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
	iprange, err := mgr.getCidr(5)
	if err != nil {
		t.Errorf("expected to get an ip range for five addresses but got %v", err)
	}
	fmt.Printf("got ip range: %s\n", iprange.String())
	if iprange.IP.String() != "10.10.0.0" {
		t.Errorf("first ip range allocated should start at manager cidr")
	}

	iprange, err = mgr.getCidr(7)
	if err != nil {
		t.Errorf("expected to get an ip range for seven addresses but got %v", err)
	}
	fmt.Printf("got ip range: %s\n", iprange.String())
}

func TestReserveCidr(t *testing.T) {
	mgr, err := NewIPManager("10.10.0.0/16")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
	subnet, err := mgr.ReserveCidr(5, "test5")
	if err != nil {
		t.Errorf("expected to be able to reserve an ip range for 5 addresses but got %v", err)
	}
	fmt.Printf("mgr map and list: %v and %v\n", mgr.cidrToIDMap, mgr.allocatedCidrs)

	entry, ok := mgr.cidrToIDMap[subnet.String()]
	if !ok {
		t.Errorf("expected an entry for subnet %s in map", subnet.String())
	}
	if entry != "test5" {
		t.Errorf("expected id test5 in map but got %s", entry)
	}

	subnet, err = mgr.ReserveCidr(6, "test6")
	if err != nil {
		t.Errorf("expected to be able to reserve an ip range for 6 addresses but got %v", err)
	}
	fmt.Printf("mgr map and list: %v and %v\n", mgr.cidrToIDMap, mgr.allocatedCidrs)

	entry, ok = mgr.cidrToIDMap[subnet.String()]
	if !ok {
		t.Errorf("expected an entry for subnet %s in map", subnet.String())
	}
	if entry != "test6" {
		t.Errorf("expected id test6 in map but got %s", entry)
	}
}

func TestRegisterCidr(t *testing.T) {
	mgr, err := NewIPManager("10.10.0.0/16")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
	err = mgr.RegisterCidr("", "")
	if err == nil {
		t.Errorf("expected error since client id is empty")
	}

	clientId := "some-client"
	err = mgr.RegisterCidr(clientId, "10.11.0.0/24")
	if err == nil {
		t.Errorf("expected error since subnet does not match manager cidr")
	}

	subnet := "10.10.1.1/29"
	err = mgr.RegisterCidr(clientId, subnet)
	require.NoError(t, err, "expected to be able to register subnet")
	require.Equal(t, 1, len(mgr.cidrToIDMap))
	require.Equal(t, subnet, mgr.cidrToIDMap[clientId])
	require.Equal(t, 1, len(mgr.allocatedCidrs))
	require.Equal(t, subnet, mgr.allocatedCidrs[0])

	err = mgr.RegisterCidr(clientId, subnet)
	require.NoError(t, err, "expected to be able to register subnet")
	require.Equal(t, 1, len(mgr.cidrToIDMap))
	require.Equal(t, subnet, mgr.cidrToIDMap[clientId])
	require.Equal(t, 1, len(mgr.allocatedCidrs))
	require.Equal(t, subnet, mgr.allocatedCidrs[0])

}

func TestReleaseCidrs(t *testing.T) {
	mgr, err := NewIPManager("10.10.0.0/16")
	if err != nil {
		t.Errorf("did not expect error return for NewIPManager")
	}
	// Just verify that releasing an unregistered client is fine.
	mgr.ReleaseCidrs("nonexistent")

	subnet, err := mgr.ReserveCidr(5, "test5")
	if err != nil {
		t.Errorf("expected to be able to reserve an ip range for 5 addresses but got %v", err)
	}
	mgr.ReleaseCidrs("nonexistent")

	entry, ok := mgr.cidrToIDMap[subnet.String()]
	if !ok {
		t.Errorf("expected an entry for subnet %s in map", subnet.String())
	}
	if entry != "test5" {
		t.Errorf("expected id test5 in map but got %s", entry)
	}

	mgr.ReleaseCidrs(entry)
	entry, ok = mgr.cidrToIDMap[subnet.String()]
	if ok {
		t.Errorf("expected no entry for subnet %s in map but got %s", subnet.String(), entry)
	}
	if len(mgr.allocatedCidrs) > 0 {
		t.Errorf("there should be no allocated cidrs now but i have %v", mgr.allocatedCidrs)
	}

	// You can register it again
	_, err = mgr.ReserveCidr(5, "test5")
	if err != nil {
		t.Errorf("expected to be able to reserve an ip range for 5 addresses but got %v", err)
	}

}
