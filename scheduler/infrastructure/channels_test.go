// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"testing"
)

func TestEvaluateChannel(t *testing.T) {
	channel := MakeChannel("s1", "test_namespace")
	status := EvaluateChannel(&channel)
	if status.ChannelStatus != crd.ChannelRequested {
		t.Errorf("pending channel should evaluate to 'requested' but was %s", status.ChannelStatus)
	}
	channel.Status.Latency = channel.Spec.MaxDelayNanos + 1
	status = EvaluateChannel(&channel)
	if status.ChannelStatus != crd.ChannelRequested {
		t.Errorf("pending channel should evaluate to 'requested' but was %s", status.ChannelStatus)
	}

	channel.Status.Status = crd.ChannelRunning
	status = EvaluateChannel(&channel)
	if status.ChannelStatus != crd.ChannelOOQOS {
		t.Errorf("channel should be out of qos but status is %s", status.ChannelStatus)
	}
}

func TestDoesChannelMeetQoS(t *testing.T) {
	channel := MakeChannel("s1", "test_namespace")
	if msg := doesChannelMeetQoS(&channel); msg == "" {
		t.Errorf("channel without status should fail qos")
	}
	channel.Status.Latency = channel.Spec.MaxDelayNanos + 1
	if msg := doesChannelMeetQoS(&channel); msg == "" {
		t.Errorf("channel has too high latency to meet qos")
	}

	channel.Status.Status = crd.ChannelRunning
	if msg := doesChannelMeetQoS(&channel); msg == "" {
		t.Errorf("channel has too high latency to meet qos")
	}

	channel.Status.Latency = channel.Spec.MaxDelayNanos - 1
	if msg := doesChannelMeetQoS(&channel); msg != "" {
		t.Errorf("channel should meet qos but %s", msg)
	}

	channel.Status.Status = crd.ChannelRejected
	if msg := doesChannelMeetQoS(&channel); msg == "" {
		t.Errorf("channel is not acked, cannot meet qos")
	}
}

func TestChannelImplementation(t *testing.T) {
	channel := MakeChannel("s1", "test_namespace")
	lb := ChannelImplementation(&channel)
	if lb != nwlib.BasicNetwork {
		t.Errorf("basic should be the default channel implementation but got %v", lb)
	}
	channel.Labels[crd.NetworkLabel] = "foo"
	lb = ChannelImplementation(&channel)
	// foo is not a valid enum value, this checks that using an invalid value
	// does not cause a panic.
	if string(lb) != "foo" {
		t.Errorf("expected channel class 'foo' but got %s", string(lb))
	}
}

func TestCompleteChannelSpec(t *testing.T) {
	scenarios := map[string][]int32{
		// scenarios are: spec framesize, spec interval, spec bandwidth,
		// expected framesize, expected interval, expected bandwidth.

		// This leaves the bandwidth unchanged even if it is inconsistent.
		"framesize and sendinterval specified": {50, 1000, 1234, 50, 1000, 1234},

		// This populates the channel spec with default values:
		// 500 bytes every 100us
		"nothing specified": {0, 0, 0, 500, 100000, 0},

		// if you specify framesize and bandwidth, the send interval gets computed.
		"framesize and bandwidth specified": {10, 0, 500, 10, 10000000, 500},

		// if you specify interval and bandwidth, the framesize gets computed.
		"interval and bandwidth specified": {0, 1000000, 1000000, 125, 1000000, 1000000},

		// if you specify only bandwidth, the framesize gets its default value and then the interval is computed.
		"only bandwidth specified": {0, 0, 5000, 500, 10000000, 5000},

		// excessive intervals are capped at 10ms
		"excessive interval": {50, 200000000, 10000, 50, 10000000, 10000},
	}

	channel := MakeChannel("s1", "default")
	for desc, values := range scenarios {
		channel.Spec.Framesize = values[0]
		channel.Spec.SendInterval = values[1]
		channel.Spec.MinBandwidthBits = int64(values[2])
		CompleteChannelSpec(&channel)
		if channel.Spec.Framesize != values[3] || channel.Spec.SendInterval != values[4] || channel.Spec.MinBandwidthBits != int64(values[5]) {
			t.Errorf("unexpected values for test case %s: got %+v, expected %d, %d, %d",
				desc, channel.Spec, values[3], values[4], values[5])
		}
	}

}

func makeNetworkEndpoint(index int) crd.NetworkEndpoint {
	return crd.NetworkEndpoint{
		Spec: crd.NetworkEndpointSpec{
			NodeName: fmt.Sprintf("k8s-name-%d", index),
			NetworkInterfaces: []crd.NetworkEndpointNetworkInterface{
				{
					Name:        "eth0",
					IPv4Address: fmt.Sprintf("10.100.10.%d", index),
				},
				{
					Name:        fmt.Sprintf("tsn-if-%d", index),
					IPv4Address: fmt.Sprintf("20.100.10.%d", index),
				},
			},
			MappedNetworkInterfaces: map[string]string{
				"tsn": fmt.Sprintf("tsn-if-%d", index),
			},
		},
	}
}

func TestNetworkEndpointsForChannel(t *testing.T) {
	endpoints := crd.NetworkEndpointList{
		Items: []crd.NetworkEndpoint{
			makeNetworkEndpoint(1),
			makeNetworkEndpoint(2),
		},
	}

	channel := crd.Channel{
		Spec: crd.ChannelSpec{
			ChannelFrom: "k8s-name-1",
			ChannelTo:   "k8s-name-2",
		},
	}
	source, target := NetworkEndpointsForChannel(channel, endpoints)
	if source.Spec.NodeName != "k8s-name-1" || target.Spec.NodeName != "k8s-name-2" {
		t.Errorf("expected network endpoint lookup to find source and target for channel %v but got source %v and target %v", channel, source, target)
	}

	channel.Spec.ChannelFrom = "tsn-name-1"
	source, target = NetworkEndpointsForChannel(channel, endpoints)
	if source.Spec.NodeName != "" {
		t.Errorf("did not expect to find source node based on tsn name but got %v", source)
	}
}

func TestResetChannelStatus(t *testing.T) {
	channel := crd.Channel{
		Spec: crd.ChannelSpec{
			ChannelFrom: "k8s-name-1",
			ChannelTo:   "k8s-name-2",
		},
	}
	err := ResetChannelStatus(&channel)
	if err != nil {
		t.Errorf("unexpected error resetting channel status: %v", err)
	}
	if channel.Status.Status != crd.ChannelRequested {
		t.Errorf("tsn channel should have been set to requested but was %v", channel.Status.Status)
	}

	// A channel that is up cannot be reset.
	channel.Status.Status = crd.ChannelRunning
	err = ResetChannelStatus(&channel)
	if err == nil {
		t.Errorf("should not have reset a running channel")
	}
}

func TestParseChannelId(t *testing.T) {
	name, namespace := ParseChannelId("foo")
	if name != "foo" || namespace != "" {
		t.Errorf("parse channel id on non-channel id string should just return the string, but got %s, %s", name, namespace)
	}
	name, namespace = ParseChannelId("foo_bar")
	if namespace != "foo" || name != "bar" {
		t.Errorf("expected namespace 'foo' but got %s and expected name 'bar' but got %s", namespace, name)
	}
	name, namespace = ParseChannelId("foo_bar_baz")
	if namespace != "foo" || name != "bar_baz" {
		t.Errorf("expected namespace 'foo' but got %s and expected name 'bar_baz' but got %s", namespace, name)
	}
}
