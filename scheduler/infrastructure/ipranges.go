// Copyright 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	"github.com/fvbommel/sortorder"
	"math"
	"net"
	"sort"

	"github.com/apparentlymart/go-cidr/cidr"
)

// An IPManager manages one range of IP addresses, such as 10.10.0.0/16
// It keeps track of subranges that have already been allocated.
// It can handle a request for a new subrange and it can release existing
// allocations.
type IPManager struct {
	// The range of addresses that this manager controls.
	network net.IPNet

	// The number of bits in addresses managed by this network.
	bits int

	// Maps a CIDR string to an identifier for the object that the CIDR
	// was allocated to.
	cidrToIDMap map[string]string

	// A sorted list of the cidrs that have been allocated.
	allocatedCidrs []string
}

func NewIPManager(cidr string) (*IPManager, error) {
	firstAddr, network, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	// For an input like 10.10.0.16, we will now have:
	// firstAdr 10.10.0.0, prefixLen 16, bits 32
	prefixLen, bits := network.Mask.Size()
	fmt.Printf("for cidr %s, got prefixLen %d and bits %d, first addr is %s\n", cidr, prefixLen, bits, firstAddr.String())
	return &IPManager{
		network:        *network,
		cidrToIDMap:    make(map[string]string),
		allocatedCidrs: make([]string, 0),
		bits:           bits,
	}, nil
}

// ReserveCidr obtains a subnet of the desired size, reserve it, and return it.
// Returns an error if there is not enough room for the new subnet.
func (i *IPManager) ReserveCidr(addressCount int, clientID string) (*net.IPNet, error) {
	subnetCandidate, err := i.getCidr(addressCount)
	if err != nil {
		return nil, err
	}
	if subnetCandidate == nil {
		return nil, fmt.Errorf("failed to allocated a subnet for %d more addresses, probably out of address space", addressCount)
	}

	i.cidrToIDMap[subnetCandidate.String()] = clientID
	err = i.insertSorted(subnetCandidate.String())
	if err != nil {
		return nil, err
	}

	return subnetCandidate, nil
}

// ReleaseCidrs releases all network ranges held under the given name.
func (i *IPManager) ReleaseCidrs(clientID string) {
	subnetsToRelease := make([]string, 0, len(i.allocatedCidrs))
	for subnet, client := range i.cidrToIDMap {
		if client == clientID {
			subnetsToRelease = append(subnetsToRelease, subnet)
		}
	}
	for _, sub := range subnetsToRelease {
		fmt.Printf("releasing subnet range %s\n", sub)
		delete(i.cidrToIDMap, sub)
		index := sort.SearchStrings(i.allocatedCidrs, sub)
		if i.allocatedCidrs[index] != sub {
			fmt.Printf("trying to release subnet %s that was not reserved?", sub)
		}
		i.allocatedCidrs = append(i.allocatedCidrs[:index], i.allocatedCidrs[index+1:]...)
	}
}

// RegisterCidr registers new subnet with IP Manager.
func (i *IPManager) RegisterCidr(clientId string, subnet string) error {

	if clientId == "" {
		return fmt.Errorf("client id must not be empty")
	}

	managerFor, err := i.IsManagerFor(subnet)
	if err != nil {
		return err
	}
	if !managerFor {
		return fmt.Errorf("subnet %s is not managed by this manager", subnet)
	}
	i.cidrToIDMap[clientId] = subnet
	i.insertSorted(subnet)
	return nil
}

func (i *IPManager) insertSorted(subnet string) error {
	index, err := i.getAllocatedCidrsIndex(subnet)
	if err != nil {
		return err
	}
	// if subnet is first, then just insert it
	if index == 0 && len(i.allocatedCidrs) == 0 {
		i.allocatedCidrs = append(i.allocatedCidrs, subnet)
		return nil
	}
	// if subnet is already allocated then do nothing
	if index == len(i.allocatedCidrs) && i.allocatedCidrs[index-1] == subnet {
		return nil
	}
	// insert new subnet into allocated cidrs slice at right position
	i.allocatedCidrs = append(i.allocatedCidrs, "")
	copy(i.allocatedCidrs[index+1:], i.allocatedCidrs[index:])
	i.allocatedCidrs[index] = subnet
	return nil
}

func (i *IPManager) IsManagerFor(subnet string) (bool, error) {

	_, sn, err := net.ParseCIDR(subnet)
	if err != nil {
		return false, err
	}

	// Check if the subnet is part of the network
	return i.network.Contains(sn.IP), nil
}

// Will a subnet with the given prefix length and starting from addrFrom end before addrTo?
func (i *IPManager) doesSubnetFitHere(prefixLen int, addrFrom net.IP, addrTo net.IP) (*net.IPNet, error) {
	fmt.Printf("does a subnet with prefix length %d and starting at %s fit before %s?\n", prefixLen, addrFrom.String(), addrTo.String())
	_, subnetCandidate, err := net.ParseCIDR(fmt.Sprintf("%s/%d", addrFrom.String(), i.bits-prefixLen))
	if err != nil {
		return nil, err
	}
	_, subnetEnd := cidr.AddressRange(subnetCandidate)
	if sortorder.NaturalLess(addrTo.String(), subnetEnd.String()) {
		// No, it does not fit
		fmt.Printf("subnet %s does not fit before  %s\n", subnetCandidate.String(), addrTo.String())
		return nil, nil
	}
	// Yes, it does fit
	return subnetCandidate, nil
}

// Returns a subrange of the range managed by this manager for the desired
// number of addresses.
// Returns a pointer to the subrange if possible, otherwise an error.
// This will reserve a range large enough for addressCount + 2.
func (i *IPManager) getCidr(addressCount int) (*net.IPNet, error) {
	prefixLen := int(math.Ceil(math.Log2(float64(addressCount + 2))))
	fmt.Printf("prefix length for %d addresses is %d\n", addressCount, prefixLen)
	firstIP := i.network.IP
	for _, cdr := range i.allocatedCidrs {
		rangeStart, fullRange, err := net.ParseCIDR(cdr)
		if err != nil {
			return nil, err
		}
		if sortorder.NaturalLess(firstIP.String(), rangeStart.String()) {
			// Would the new subnet fit between firstIP and cdr?
			subnet, err := i.doesSubnetFitHere(prefixLen, firstIP, rangeStart)
			if err != nil {
				return nil, err
			}
			if subnet != nil {
				return subnet, nil
			}
		}
		_, rangeEnd := cidr.AddressRange(fullRange)
		firstIP = cidr.Inc(rangeEnd)
		fmt.Printf("set firstIP to %s, one past the range of %s\n", firstIP.String(), cdr)
		continue
	}
	fmt.Printf("reached end of allocated cidrs\n")
	// If we got here, then just try making a network starting at firstIP
	_, subnetCandidate, err := net.ParseCIDR(fmt.Sprintf("%s/%d", firstIP.String(), i.bits-prefixLen))
	if err != nil {
		return nil, err
	}
	return subnetCandidate, nil
}

func (i *IPManager) getAllocatedCidrsIndex(subnet string) (int, error) {
	subnetFirstIP, _, err := net.ParseCIDR(subnet)
	if err != nil {
		return 0, err
	}
	for index, allocatedCidr := range i.allocatedCidrs {
		allocatedFirstIP, _, err := net.ParseCIDR(allocatedCidr)
		if err != nil {
			return 0, err
		}
		if sortorder.NaturalLess(subnetFirstIP.String(), allocatedFirstIP.String()) {
			return index, nil
		}
	}
	return len(i.allocatedCidrs), nil
}
