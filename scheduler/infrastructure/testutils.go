// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package infrastructure

import (
	"bufio"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"os"
	"strconv"
	"strings"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func makeNode(name string, labels []string) *corev1.Node {
	ret := &corev1.Node{
		ObjectMeta: metav1.ObjectMeta{Name: name, Labels: make(map[string]string)},
	}

	ret.Status.Capacity = corev1.ResourceList{
		"cpu":    *resource.NewMilliQuantity(2000, resource.DecimalSI),
		"memory": *resource.NewQuantity(8*1024*1024*1024, resource.BinarySI),
	}
	ret.Status.Allocatable = corev1.ResourceList{
		"cpu":    *resource.NewMilliQuantity(1500, resource.DecimalSI),
		"memory": *resource.NewQuantity(7*1024*1024*1024, resource.BinarySI),
	}

	for _, l := range labels {
		ret.Labels["siemens.com.qosscheduler."+l] = "true"
	}
	return ret
}

func MakeChannel(name string, namespace string) crd.Channel {
	return crd.Channel{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    map[string]string{},
		},
		Spec: crd.ChannelSpec{
			ChannelFrom:      "node1",
			ChannelTo:        "node2",
			MinBandwidthBits: 10000,
			MaxDelayNanos:    2,
		},
		Status: crd.ChannelStatus{
			Status: crd.ChannelRequested,
		},
	}
}

func MakeNetworkEndpoint(name string, interfaceNames []string, macAddresses []string) (crd.NetworkEndpoint, crd.NetworkLink) {
	interf := make([]crd.NetworkEndpointNetworkInterface, len(interfaceNames))
	for i, inf := range interfaceNames {
		interf[i] = crd.NetworkEndpointNetworkInterface{
			Name:        inf,
			MacAddress:  macAddresses[i],
			IPv4Address: "127.0.0.1",
		}
	}
	cn := crd.NetworkEndpoint{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: crd.NetworkEndpointSpec{
			NodeName:          name,
			NetworkInterfaces: interf,
		},
	}
	linkNode := crd.LinkNode{Name: name, Type: crd.COMPUTE_NODE}
	ln := crd.NetworkLink{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name + "-" + name,
			Labels: map[string]string{"linkFrom": name, "linkTo": name},
		},
		Spec: crd.NetworkLinkSpec{
			LinkFrom:      linkNode,
			LinkTo:        linkNode,
			BandWidthBits: int64(1000000),
			LatencyNanos:  int64(1),
		},
		Status: crd.NetworkLinkStatus{
			BandWidthAvailable: int64(1000000),
			LatencyNanos:       int64(1),
		},
	}
	return cn, ln
}

func MakeNetworkLink(fromName, fromType, toName, toType string, bw, latency int64) crd.NetworkLink {
	node1 := crd.LinkNode{
		Name: fromName,
		Type: crd.NodeType(fromType),
	}
	node2 := crd.LinkNode{
		Name: toName,
		Type: crd.NodeType(toType),
	}
	return crd.NetworkLink{
		TypeMeta: metav1.TypeMeta{Kind: "NetworkLink", APIVersion: "v1alpha1"},
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-%s", fromName, toName),
			Namespace: "network-test-namespace",
		},
		Spec: crd.NetworkLinkSpec{
			LinkFrom:      node1,
			LinkTo:        node2,
			BandWidthBits: bw,
			LatencyNanos:  latency,
		},
		Status: crd.NetworkLinkStatus{
			BandWidthAvailable: bw,
			LatencyNanos:       latency,
		},
	}
}

func MakeInfrastructureModel() *assignment.InfrastructureModel {
	computeNodes := make([]corev1.Node, 2)
	computeNodes[0] = *makeNode("node1", []string{"Inc"})
	computeNodes[1] = *makeNode("node2", []string{})

	links := []crd.NetworkLink{
		MakeNetworkLink("node1", "COMPUTE", "nw1", "NETWORK", 300000000, 10),
		MakeNetworkLink("node1", "COMPUTE", "node1", "COMPUTE", 300000000, 10),
		MakeNetworkLink("node2", "COMPUTE", "node2", "COMPUTE", 300000000, 10),
		MakeNetworkLink("node2", "COMPUTE", "nw1", "NETWORK", 400000000, 5),
		MakeNetworkLink("nw1", "NETWORK", "node2", "COMPUTE", 300000000, 10),
		MakeNetworkLink("nw1", "NETWORK", "node1", "COMPUTE", 400000000, 5),
		MakeNetworkLink("nw1", "NETWORK", "nw1", "NETWORK", 400000000, 5),
	}

	router := nwlib.NewRouter()
	router.Initialize(map[string]crd.NetworkLink{})
	for _, l := range links {
		router.AddNetworkLink(l)
	}
	pathSpecs, _ := router.GetNetworkPaths()
	paths := make([]crd.NetworkPath, len(pathSpecs))
	for i, p := range pathSpecs {
		paths[i] = *nwlib.NetworkPathFromSpec(p, "network-test-namespace")
	}
	ret, _ := BuildInfrastructureModel(computeNodes, paths, links, false, false)

	return ret
}

// ReadInfrastructureModelFromTextfiles reads an infrastructure model from text files.
// It expects to find files called NodeAttributes.txt, NodeLabels.txt, and
// LinkData.txt in the directory you give it. It also expects these to have
// a certain shape (same as in e.g. the semiotics test data dir).
// This code actually creates Kubernetes objects from those files and then
// converts those into an infrastructure model because that way, you get a more
// realistic idea of what the input coming from the go code is going to look like.
func ReadInfrastructureModelFromTextfiles(dirname string) (*assignment.InfrastructureModel, error) {
	computeNodeMap, networkNodeMap := readNodesFromTextfile(dirname + "NodeAttributes.txt")
	labelNodesFromTextfile(dirname+"NodeLabels.txt", computeNodeMap)
	labelNodesFromTextfile(dirname+"NodeLabels.txt", networkNodeMap)

	computeNodes := make([]corev1.Node, len(computeNodeMap))
	i := 0
	for _, node := range computeNodeMap {
		computeNodes[i] = *node
		i++
	}

	links := readNetworkLinksFromTextfile(dirname+"LinkData.txt", computeNodeMap, networkNodeMap)
	router := nwlib.NewRouter()
	router.Initialize(map[string]crd.NetworkLink{})
	for _, l := range links {
		router.AddNetworkLink(l)
	}
	pathSpecs, _ := router.GetNetworkPaths()
	paths := make([]crd.NetworkPath, len(pathSpecs))
	for i, p := range pathSpecs {
		paths[i] = *nwlib.NetworkPathFromSpec(p, "network-test-namespace")
	}
	return BuildInfrastructureModel(computeNodes, paths, links, false, false)
}

// This reads the NodeAttributes.txt file.
// The first line may be a header (starts with #).
// The other lines are of the form
// <nodeid> <cpu> <ram> <initial cpu> <initial ram> name
// all values are separated by whitespace (sometimes multiple whitespace).
// initial-cpu and initial-ram represent the initial load on the nodes.
// cpu is in millicpu. memory is in MegaBytes.
func readNodesFromTextfile(filename string) (map[int]*corev1.Node, map[int]*corev1.Node) {
	computeNodes := make(map[int]*corev1.Node)
	networkNodes := make(map[int]*corev1.Node)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 6 {
			log.Fatal(fmt.Errorf("expected at least 6 entries in line %v", pieces))
		}
		nodeid, _ := strconv.Atoi(strings.TrimSpace(pieces[0]))
		cpu, _ := strconv.Atoi(strings.TrimSpace(pieces[1]))
		memory, _ := strconv.Atoi(strings.TrimSpace(pieces[2]))
		initialCpu, _ := strconv.Atoi(strings.TrimSpace(pieces[3]))
		initialMemory, _ := strconv.Atoi(strings.TrimSpace(pieces[4]))
		nodename := strings.TrimSpace(pieces[5])
		if cpu == 0 {
			networkNodes[nodeid] = &corev1.Node{
				ObjectMeta: metav1.ObjectMeta{Name: nodename, Labels: make(map[string]string)},
			}
		} else {
			n := &corev1.Node{
				ObjectMeta: metav1.ObjectMeta{Name: nodename, Labels: make(map[string]string)},
			}

			n.Status.Capacity = corev1.ResourceList{
				"cpu":    *resource.NewMilliQuantity(int64(cpu), resource.DecimalSI),
				"memory": *resource.NewQuantity(int64(memory*1024*1024), resource.BinarySI),
			}
			n.Status.Allocatable = corev1.ResourceList{
				"cpu":    *resource.NewMilliQuantity(int64(cpu-initialCpu), resource.DecimalSI),
				"memory": *resource.NewQuantity(int64(memory-initialMemory)*1024*1024, resource.BinarySI),
			}
			computeNodes[nodeid] = n
		}
	}
	return computeNodes, networkNodes
}

func labelNodesFromTextfile(filename string, nodes map[int]*corev1.Node) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 2 {
			log.Fatal(fmt.Errorf("expected at least two entries in line %s", line))
		}
		nodeid, _ := strconv.Atoi(strings.TrimSpace(pieces[0]))
		label := strings.TrimSpace(pieces[1])
		if nodes[nodeid] != nil {
			nodes[nodeid].Labels["siemens.com.qosscheduler."+label] = "true"
		}
	}
}

// Read a LinkData text file.
// Lines are expected to be of the form
// sourcenodeid  targetnodeid  latency (in ms)   bandwidth (in kbit/s)  used bandwidth (in kbit/s)
func readNetworkLinksFromTextfile(filename string, computeNodes map[int]*corev1.Node, networkNodes map[int]*corev1.Node) []crd.NetworkLink {
	ret := make([]crd.NetworkLink, 0)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 5 {
			log.Fatal(fmt.Errorf("expected at least five entries in line %s", line))
		}
		sourceNodeId, _ := strconv.Atoi(pieces[0])
		targetNodeId, _ := strconv.Atoi(pieces[1])
		latencyFloat, _ := strconv.ParseFloat(pieces[2], 64)
		latencyns := int64(latencyFloat * 1000 * 1000)
		bandwidthkbits, _ := strconv.Atoi(pieces[3])
		usedBandwidthkbits, _ := strconv.Atoi(pieces[4])
		var sourceName, targetName string
		var sourceType, targetType string
		if computeNodes[sourceNodeId] != nil {
			sourceType = "COMPUTE"
			sourceName = computeNodes[sourceNodeId].Name
		} else {
			sourceType = "NETWORK"
			sourceName = networkNodes[sourceNodeId].Name
		}
		if computeNodes[targetNodeId] != nil {
			targetType = "COMPUTE"
			targetName = computeNodes[targetNodeId].Name
		} else {
			targetType = "NETWORK"
			targetName = networkNodes[targetNodeId].Name
		}
		link := MakeNetworkLink(sourceName, sourceType, targetName, targetType, int64(bandwidthkbits*1000), latencyns)
		link.Status.BandWidthAvailable = int64(bandwidthkbits-usedBandwidthkbits) * 1000
		ret = append(ret, link)
	}
	return ret
}
