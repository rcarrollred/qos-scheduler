// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"strings"
)

func BuildInfrastructureModel(nodes []corev1.Node, paths []crd.NetworkPath, links []crd.NetworkLink, scheduleOnMaster bool, optimizerCompatMode bool) (*assignment.InfrastructureModel, error) {
	ret := &assignment.InfrastructureModel{}

	// Determine the compute nodes based on which nodes exist in Kubernetes.
	modelNodes := getComputeNodeInfo(nodes, scheduleOnMaster)
	// Extend with network node information based on the links.
	ret.Nodes = getNetworkNodeInfo(links, modelNodes)

	var err error
	linksMap := getNetworkLinkInfo(links, ret.Nodes)
	allPaths, err := getNetworkPathInfo(paths, linksMap)
	if err != nil {
		return ret, err
	}
	ret.Links = reformatNetworkLinksMap(linksMap)
	ret.Paths, err = makePathsUndirected(allPaths)
	if err != nil {
		return ret, err
	}
	if optimizerCompatMode {
		adaptModelForOldOptimizer(ret)
	}

	return ret, err
}

func reformatNetworkLinksMap(linksMap map[string]([]*assignment.InfrastructureModel_Link)) []*assignment.InfrastructureModel_Link {
	ret := make([]*assignment.InfrastructureModel_Link, 0)
	for _, linkList := range linksMap {
		ret = append(ret, linkList...)
	}
	return ret
}

// The old optimizer does not understand the multigraph with labeled links and paths.
// Collapse links and paths.
// This will mix links and paths from different networks. Of course that is
// wrong, but there is no correct way to pass the right information to the old optimizer.
func adaptModelForOldOptimizer(model *assignment.InfrastructureModel) {
	model.Links = adaptLinksForOldOptimizer(model.Links)
	model.Paths = adaptPathsForOldOptimizer(model.Paths)
}

func adaptLinksForOldOptimizer(links []*assignment.InfrastructureModel_Link) []*assignment.InfrastructureModel_Link {
	linksMap := make(map[string]bool)
	ret := make([]*assignment.InfrastructureModel_Link, 0, len(links))
	for _, l := range links {
		key := fmt.Sprintf("%s-%s", l.SourceNode, l.TargetNode)
		_, exists := linksMap[key]
		if !exists {
			linksMap[key] = true
			ret = append(ret, l)
		}
	}
	return ret
}

func adaptPathsForOldOptimizer(paths []*assignment.InfrastructureModel_Path) []*assignment.InfrastructureModel_Path {
	pathMap := make(map[string]bool)
	ret := make([]*assignment.InfrastructureModel_Path, 0, len(paths))
	for _, p := range paths {
		key := strings.Join(p.NodeIds, ".")
		_, exists := pathMap[key]
		if !exists {
			pathMap[key] = true
			ret = append(ret, p)
		}
	}
	return ret
}

func reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < len(s)/2; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

// The optimizer currently wants paths to be specified only in one direction.
// Our system has them in both directions, and it is technically possible
// for the reverse direction for a path to not exist.
// This function returns only those paths that have a reverse, and for those
// it only returns paths from a lexicographically smaller to a lexicographically larger node.
func makePathsUndirected(paths map[string]([]*assignment.InfrastructureModel_Path)) ([]*assignment.InfrastructureModel_Path, error) {
	ret := make([]*assignment.InfrastructureModel_Path, 0, len(paths))
	for _, pathList := range paths {
		pathMap := make(map[string]bool)
		for _, p := range pathList {
			pathID := strings.Join(p.NodeIds, "_")
			pathMap[pathID] = true
		}

		for _, p := range pathList {

			pathID := strings.Join(p.NodeIds, "_")
			reversePathID := strings.Join(reverse(p.NodeIds), "_")
			exists := pathMap[reversePathID]
			if !exists {
				return ret, fmt.Errorf("path %s has no reverse path", pathID)
			}
			if strings.Compare(pathID, reversePathID) <= 0 {
				ret = append(ret, p)
			}
		}
	}
	return ret, nil
}

func getNetworkPathInfo(paths []crd.NetworkPath,
	linksMap map[string]([]*assignment.InfrastructureModel_Link)) (
	map[string]([]*assignment.InfrastructureModel_Path), error) {

	ret := make(map[string]([]*assignment.InfrastructureModel_Path))

	for _, p := range paths {
		ni, err := nwlib.NiFromNamespace(p.Namespace)
		if err != nil {
			return ret, fmt.Errorf("cannot find ni tag for namespace %s of path %s", p.Namespace, p.Name)
		}
		nodeIds := make([]string, len(p.Spec.Links)+1)
		nodeIds[0] = p.Spec.Start.Name
		links, found := linksMap[ni]
		if !found || len(links) == 0 {
			return ret, fmt.Errorf("cannot find network links for ni label %s of path %s", ni, p.Name)
		}
		for j, l := range p.Spec.Links {
			var link *assignment.InfrastructureModel_Link
			for _, lnk := range links {
				lnkName := fmt.Sprintf("%s-%s", lnk.SourceNode, lnk.TargetNode)
				if lnkName == l {
					link = lnk
					break
				}
			}
			if link == nil {
				return ret, fmt.Errorf("link %s is in path spec %s but missing from system", l, p.Name)
			}
			nodeIds[j+1] = link.TargetNode
		}
		pathsForNi, found := ret[ni]
		if !found {
			pathsForNi = make([]*assignment.InfrastructureModel_Path, 0)
		}
		ret[ni] = append(pathsForNi, &assignment.InfrastructureModel_Path{
			NodeIds:      nodeIds,
			NetworkLabel: ni,
		})
	}
	return ret, nil
}

func getNetworkLinkInfo(links []crd.NetworkLink,
	nodes []*assignment.InfrastructureModel_Node) map[string]([]*assignment.InfrastructureModel_Link) {
	ret := make(map[string]([]*assignment.InfrastructureModel_Link))
	for _, l := range links {
		networkImplementation := nwlib.NetworkLinkClass(l)
		niName := string(networkImplementation)
		var links []*assignment.InfrastructureModel_Link
		var exists bool
		links, exists = ret[niName]
		if !exists {
			links = make([]*assignment.InfrastructureModel_Link, 0)
		}
		ret[niName] = append(links, &assignment.InfrastructureModel_Link{
			SourceNode:    l.Spec.LinkFrom.Name,
			TargetNode:    l.Spec.LinkTo.Name,
			LatencyNanos:  int64(l.Spec.LatencyNanos),
			BandwidthBits: int64(l.Spec.BandWidthBits),
			NetworkLabel:  niName,
			PhysicalBase:  l.Spec.PhysicalBase,
		})
	}
	return ret
}
