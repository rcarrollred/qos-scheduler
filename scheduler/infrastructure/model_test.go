// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package infrastructure

import (
	"fmt"
	"strings"
	"testing"
)

func TestReadInfraModelFromTextfiles(t *testing.T) {
	model, err := ReadInfrastructureModelFromTextfiles("../testdata/")
	if err != nil {
		t.Errorf("error reading model from file: %v", err)
	}

	if len(model.Nodes) != 24 {
		t.Errorf("expected 24 nodes in semiotics model but found %d", len(model.Nodes))
	}
	if len(model.Links) != 64 {
		t.Errorf("expected 64 links in semiotics model but found %d", len(model.Links))
	}
	if len(model.Paths) != 312 {
		for i, p := range model.Paths {
			fmt.Printf("%d: %s\n", i, strings.Join(p.NodeIds, " -> "))
		}
		t.Errorf("expected 312 Paths in semiotics model but found %d", len(model.Paths))
	}
	found := false
	for _, node := range model.Nodes {
		if node.Id == "C15" {
			found = true
			if node.Resources.MilliCPU != 8000 {
				t.Errorf("node c15 should have 8000 millicpu but has %d", node.Resources.MilliCPU)
			}
			if len(node.Labels) != 1 || node.Labels[0] != "siemens.com.qosscheduler.MDSP" {
				t.Errorf("node c15 should be labelled MDSP but its labels are %v", node.Labels)
			}
		}
	}
	if !found {
		t.Errorf("Failed to find node C15 in semiotics model")
	}
}
