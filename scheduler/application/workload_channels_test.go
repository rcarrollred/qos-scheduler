// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"testing"
)

func TestChannelsForWorkloadChannels(t *testing.T) {
	app := MakeApplication("appname", "default", []string{"wla", "wlb"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "app1", Port: 1234},
			{Basename: "wl2", ApplicationName: "app2", Port: 1234},
		})

	wlif := getAppChannels(*app, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})
	if len(wlif) != 4 {
		t.Errorf("expected four application channels but got %d", len(wlif))
	}
	channels, err := channelsForWorkloadChannels(wlif, []*assignment.QoSAssignment_PathAssignment{}, "default")
	if err != nil {
		t.Errorf("error getting workload channels: %v", err)
	}
	if len(channels) != 4 {
		t.Errorf("expected four channels but got %d", len(channels))
	}
}
