// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// GetApplicationModel builds an assignment.ApplicationModel containing compute requirements derived
// from the applications passed in, as well as channels derived from the workload channels.
func GetApplicationModel(apps []qosschedulerv1alpha1.Application, qosMap map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass)) *assignment.ApplicationModel {
	return &assignment.ApplicationModel{
		Applications: getComputeRequirements(apps),
		Channels:     getChannels(apps, qosMap),
	}
}

// GetExpectedChannels identifies those channels that originate or terminate at a given application.
// Input parameters: an application and a list of channels.
// Returns: a list of channels originating at the application, and a list of channels terminating at the application.
func GetExpectedChannels(app qosschedulerv1alpha1.Application, channels []qosschedulerv1alpha1.Channel) (map[string]([]qosschedulerv1alpha1.Channel), map[string]([]qosschedulerv1alpha1.Channel)) {

	channelsFromPods := make(map[string]([]qosschedulerv1alpha1.Channel))
	channelsToPods := make(map[string]([]qosschedulerv1alpha1.Channel))

	for _, s := range channels {
		ta := infrastructure.ChannelTargetApplication(&s)
		if ta == app.Name {
			targetPodName := s.Spec.TargetWorkload
			if channelsToPods[targetPodName] == nil {
				channelsToPods[targetPodName] = []qosschedulerv1alpha1.Channel{s}
			} else {
				channelsToPods[targetPodName] = append(channelsToPods[targetPodName], s)
			}
		}

		src := infrastructure.ChannelSourceApplication(&s)
		if src == app.Name {
			sourcePodName := s.Spec.SourceWorkload
			if channelsFromPods[sourcePodName] == nil {
				channelsFromPods[sourcePodName] = []qosschedulerv1alpha1.Channel{s}
			} else {
				channelsFromPods[sourcePodName] = append(channelsFromPods[sourcePodName], s)
			}
		}
	}
	return channelsFromPods, channelsToPods
}

func getAppChannels(app qosschedulerv1alpha1.Application, qosMap map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass)) []*assignment.ApplicationModel_Channel {
	ret := make([]*assignment.ApplicationModel_Channel, 0)
	for _, wl := range app.Spec.Workloads {
		sourceWlName := fmt.Sprintf("%s-%s", app.Name, wl.Basename)
		for _, ch := range wl.Channels {
			targetWlName := fmt.Sprintf("%s-%s", ch.OtherWorkload.ApplicationName, ch.OtherWorkload.Basename)
			bandwidth, err := nwlib.GetQuantityValue(ch.MinBandwidth)
			if err != nil {
				fmt.Printf("failed to convert bandwidth spec to number: %v\n", err)
				continue
			}
			latency, err := nwlib.GetQuantityNanoValue(ch.MaxDelay)
			if err != nil {
				fmt.Printf("failed to convert latency spec to number: %v\n", err)
				continue
			}
			framesize, err := nwlib.GetQuantityValue(ch.Framesize)
			if err != nil {
				fmt.Printf("failed to convert Framesize spec %v to number: %v\n", ch.Framesize, err)
				continue
			}
			interval, err := nwlib.GetQuantityNanoValue(ch.SendInterval)
			if err != nil {
				fmt.Printf("failed to convert SendInterval spec to number: %v\n", err)
				continue
			}
			var port int32
			if ch.OtherWorkload.Port == 0 {
				port = int32(80)
			} else {
				port = int32(ch.OtherWorkload.Port)
			}
			var interfaceBasename string
			if ch.Basename != "" {
				interfaceBasename = ch.Basename
			} else {
				interfaceBasename = fmt.Sprintf("%s-to-%s", sourceWlName, targetWlName)
			}
			var niClass nwlib.NetworkImplementationClass
			niClass, ok := qosMap[ch.ServiceClass]
			if !ok {
				niClass = nwlib.K8sNetwork
			}
			spec := &assignment.ApplicationModel_Channel{
				SourceWorkload:    sourceWlName,
				TargetWorkload:    targetWlName,
				SourceApplication: app.Name,
				TargetApplication: ch.OtherWorkload.ApplicationName,
				BandwidthBits:     int64(bandwidth),
				LatencyNanos:      int64(latency),
				Framesize:         int32(framesize),
				SendInterval:      int32(interval),
				NetworkLabel:      string(niClass),

				TargetPort:   port,
				Basename:     interfaceBasename,
				ServiceClass: string(ch.ServiceClass),
			}
			ret = append(ret, spec)
		}
	}
	return ret
}

func getChannels(apps []qosschedulerv1alpha1.Application, qosMap map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass)) []*assignment.ApplicationModel_Channel {
	ret := make([]*assignment.ApplicationModel_Channel, 0, len(apps))
	for _, app := range apps {
		if IsEligibleForScheduling(&app) {
			ret = append(ret, getAppChannels(app, qosMap)...)
		}
	}
	return ret
}

// Get a list of application protos for the optimizer.
func getComputeRequirements(apps []qosschedulerv1alpha1.Application) []*assignment.ApplicationModel_Application {
	ret := make([]*assignment.ApplicationModel_Application, 0, len(apps))
	for _, app := range apps {
		if IsEligibleForScheduling(&app) {
			ret = append(ret, getAppComputeRequirements(app))
		}
	}
	return ret
}

// Transform an Application resource into an Application proto for the optimizer.
func getAppComputeRequirements(app qosschedulerv1alpha1.Application) *assignment.ApplicationModel_Application {
	ret := &assignment.ApplicationModel_Application{
		Id:        app.Name,
		Workloads: make([](*assignment.ApplicationModel_Workload), 0, len(app.Spec.Workloads)),
	}

	for _, wl := range app.Spec.Workloads {
		wlName := fmt.Sprintf("%s-%s", app.Name, wl.Basename)

		podspec := wl.Template
		totalMilliCPU := int32(0)
		totalMemoryBytes := int64(0)
		for _, cont := range podspec.Spec.Containers {
			r := cont.Resources // this has type ResourceRequirements
			totalMilliCPU += int32(r.Requests.Cpu().MilliValue())
			totalMemoryBytes += int64(r.Requests.Memory().Value())
		}
		// Noderesources/fit actually takes the max over the init containers, maybe
		// do that here too.
		for _, cont := range podspec.Spec.InitContainers {
			r := cont.Resources // this has type ResourceRequirements
			totalMilliCPU += int32(r.Requests.Cpu().MilliValue())
			totalMemoryBytes += int64(r.Requests.Memory().Value())
		}
		// Not using pod.Spec.Overhead yet.
		computeResources := &assignment.ComputeResources{
			MilliCPU:      totalMilliCPU,
			MemoryInBytes: totalMemoryBytes,
		}

		labels, forbiddenLabels, _ := GetLabels(wl)
		workload := &assignment.ApplicationModel_Workload{
			Id:                 wlName,
			RequestedResources: computeResources,
			RequiredLabels:     labels,
			ForbiddenLabels:    forbiddenLabels,
		}
		ret.Workloads = append(ret.Workloads, workload)
	}
	return ret
}
