// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestNewStatus(t *testing.T) {

	podPhases := [][]corev1.PodPhase{
		{}, {corev1.PodPending}, {corev1.PodRunning}, {corev1.PodSucceeded},
		{corev1.PodFailed}, {corev1.PodUnknown},
		{corev1.PodPending, corev1.PodRunning}, {corev1.PodPending, corev1.PodFailed},
		{corev1.PodPending, corev1.PodSucceeded},

		{corev1.PodRunning, corev1.PodFailed}, {corev1.PodRunning, corev1.PodSucceeded},

		{corev1.PodFailed, corev1.PodSucceeded},
	}

	// Target new phases for each test scenario.
	newPhases := []crd.ApplicationPhase{
		crd.ApplicationWaiting,
		crd.ApplicationPending,
		crd.ApplicationRunning,
		crd.ApplicationWaiting,
		crd.ApplicationFailing,
		crd.ApplicationRunning,
		crd.ApplicationPending,
		crd.ApplicationFailing,
		crd.ApplicationPending,
		crd.ApplicationFailing,
		crd.ApplicationRunning,
		crd.ApplicationFailing,
	}

	for idx, scenario := range podPhases {
		pods := &corev1.PodList{}
		for _, phase := range scenario {
			pod := &corev1.Pod{}
			pod.Status.Phase = phase
			pods.Items = append(pods.Items, *pod)
		}
		newStatus := NewStatusFromWorkloads(pods)
		if newStatus.Phase != newPhases[idx] {
			t.Errorf("for scenario %v expected newPhase %s but got %s",
				scenario, string(newPhases[idx]), string(newStatus.Phase))
		}
	}
}

func TestIsPhaseChangePermitted(t *testing.T) {
	successPairs := map[crd.ApplicationPhase][]crd.ApplicationPhase{
		crd.ApplicationWaiting: {
			crd.ApplicationWaiting,
			crd.ApplicationScheduling},
		crd.ApplicationScheduling: {
			crd.ApplicationScheduling,
			crd.ApplicationPending},
		crd.ApplicationPending: {
			crd.ApplicationPending,
			crd.ApplicationRunning,
			crd.ApplicationFailing},
		crd.ApplicationRunning: {
			crd.ApplicationRunning,
			crd.ApplicationWaiting,
			crd.ApplicationPending,
			crd.ApplicationFailing},
		crd.ApplicationFailing: {
			crd.ApplicationFailing, crd.ApplicationWaiting},
	}

	allPhases := []crd.ApplicationPhase{
		crd.ApplicationWaiting,
		crd.ApplicationScheduling,
		crd.ApplicationPending,
		crd.ApplicationRunning,
		crd.ApplicationFailing,
	}

	for _, oldPhase := range allPhases {
		allowedTo, exists := successPairs[oldPhase]
		if exists {
			toPhases := make(map[crd.ApplicationPhase]bool)
			for _, newPhase := range allowedTo {
				toPhases[newPhase] = true
			}
			for _, newPhase := range allPhases {
				permittedDesc := "should not be permitted"
				if toPhases[newPhase] {
					permittedDesc = "should be permitted"
				}
				if IsPhaseChangePermitted(oldPhase, newPhase) != toPhases[newPhase] {
					t.Errorf("phase change from %s to %s %s",
						string(oldPhase), string(newPhase), permittedDesc)
				}
			}
		} else {
			for _, newPhase := range allPhases {
				if IsPhaseChangePermitted(oldPhase, newPhase) {
					t.Errorf("phase change from %s to %s should not be permitted",
						string(oldPhase), string(newPhase))
				}
			}
		}
	}
}

func TestGetMissingPodSpecs(t *testing.T) {
	app := MakeApplication("test-app", "default", []string{"test-workload"}, []crd.WorkloadId{})
	existingPodNames := map[string][]string{
		"no-matching-pod":              {"foo"},
		"matching-pod":                 {"test-app-test-workload"},
		"no-pods":                      {},
		"several-pods-including-match": {"foo", "test-app-test-workload", "bar"},
	}
	expected := map[string]bool{
		"no-matching-pod":              true,
		"matching-pod":                 false,
		"no-pods":                      true,
		"several-pods-including-match": false,
	}

	for desc, podnames := range existingPodNames {
		existingPods := corev1.PodList{}
		for _, p := range podnames {
			pod := corev1.Pod{ObjectMeta: metav1.ObjectMeta{Name: p}}
			existingPods.Items = append(existingPods.Items, pod)
		}
		missing := GetMissingPodSpecs(*app, existingPods)
		if len(missing) == 0 && expected[desc] || len(missing) > 0 && !expected[desc] {
			t.Errorf("%s: unexpected missing pods %+v", desc, missing)
		}
		if expected[desc] {
			for _, pod := range missing {
				if pod.Metadata.Labels == nil || len(pod.Metadata.Labels) == 0 {
					t.Errorf("expected pod to be generated with labels but it wasn't")
				}
			}
		}
	}
}

func makeWorkloadSpec(nodeSelector map[string]string, affinity *corev1.Affinity) *crd.ApplicationWorkloadSpec {
	wl := &crd.ApplicationWorkloadSpec{
		Basename: "wl",
		Template: crd.QosPodTemplateSpec{
			Spec: corev1.PodSpec{
				NodeSelector: nodeSelector,
				Affinity:     affinity,
			},
		},
	}
	return wl
}

func makeAffinity(terms []corev1.NodeSelectorTerm) *corev1.Affinity {
	return &corev1.Affinity{
		NodeAffinity: &corev1.NodeAffinity{
			RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
				NodeSelectorTerms: terms,
			},
			PreferredDuringSchedulingIgnoredDuringExecution: make([]corev1.PreferredSchedulingTerm, 0),
		},
	}
}

type scenario struct {
	desc                    string
	nodeSelectorLabels      map[string]string
	affinity                *corev1.Affinity
	expectedLabels          []string
	expectedForbiddenLabels []string
	expectedValidation      []LabelValidationFailure
}

func TestValidateLabels(t *testing.T) {
	scenarios := []scenario{
		{
			desc:               "empty selectors",
			nodeSelectorLabels: map[string]string{},
			affinity:           nil,
			expectedValidation: []LabelValidationFailure{},
		},
		{
			desc:               "non-workload label",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "foo",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedValidation: []LabelValidationFailure{NOT_A_WORKLOAD_LABEL},
		},
		{
			// Two node selectors in node affinity would be OR'ed together, not supported.
			desc:               "unsupported double affinity",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic2",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedValidation: []LabelValidationFailure{MULTIPLE_NODE_SELECTOR_TERMS},
		},
		{
			desc:               "match field in node selector",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchFields: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedValidation: []LabelValidationFailure{MATCH_FIELDS_IN_NODE_SELECTOR},
		},
	}
	for _, s := range scenarios {
		wl := makeWorkloadSpec(s.nodeSelectorLabels, s.affinity)
		msgs := ValidateLabels(*wl)
		if len(msgs) != len(s.expectedValidation) {
			t.Errorf("%s: expected %d validation messages but got %v\n", s.desc, len(s.expectedValidation), msgs)
		}
		for idx, m := range msgs {
			if m != s.expectedValidation[idx] {
				t.Errorf("%s: expected validation message %s but got %s\n", s.desc, s.expectedValidation[idx], m)
			}
		}
	}
}

func TestGetLabels(t *testing.T) {
	scenarios := []scenario{
		{
			desc:                    "empty selectors",
			nodeSelectorLabels:      map[string]string{},
			affinity:                nil,
			expectedLabels:          []string{},
			expectedForbiddenLabels: []string{},
		},
		{
			desc:                    "basic node selector label",
			nodeSelectorLabels:      map[string]string{"siemens.com.qosscheduler.Inc": "true"},
			affinity:                nil,
			expectedLabels:          []string{"siemens.com.qosscheduler.Inc"},
			expectedForbiddenLabels: []string{},
		},
		{
			desc:               "basic node affinity",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedLabels:          []string{"siemens.com.qosscheduler.Mic"},
			expectedForbiddenLabels: []string{},
		},
		{
			// Two node selectors in node affinity would be OR'ed together, not supported.
			// Only use the first one.
			desc:               "unsupported double affinity",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic2",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedLabels:          []string{"siemens.com.qosscheduler.Mic"},
			expectedForbiddenLabels: []string{},
		},
		{
			// Multiple MatchExpressions in the same node selector term will be AND'ed together.
			desc:               "supported double match expression",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpExists,
						},
						{
							Key:      "siemens.com.qosscheduler.Mic2",
							Operator: corev1.NodeSelectorOpExists,
						},
					},
				},
			}),
			expectedLabels:          []string{"siemens.com.qosscheduler.Mic", "siemens.com.qosscheduler.Mic2"},
			expectedForbiddenLabels: []string{},
		},
		{
			desc:               "forbidden label",
			nodeSelectorLabels: map[string]string{},
			affinity: makeAffinity([]corev1.NodeSelectorTerm{
				{
					MatchExpressions: []corev1.NodeSelectorRequirement{
						{
							Key:      "siemens.com.qosscheduler.Mic",
							Operator: corev1.NodeSelectorOpDoesNotExist,
						},
					},
				},
			}),
			expectedLabels:          []string{},
			expectedForbiddenLabels: []string{"siemens.com.qosscheduler.Mic"},
		},
	}
	for _, s := range scenarios {
		wl := makeWorkloadSpec(s.nodeSelectorLabels, s.affinity)
		labels, forbiddenLabels, _ := GetLabels(*wl)
		if len(labels) != len(s.expectedLabels) {
			t.Errorf("%s: expected %d labels but got %v\n", s.desc, len(s.expectedLabels), labels)
		}
		for idx, lb := range labels {
			if lb != s.expectedLabels[idx] {
				t.Errorf("%s: expected label %s but got %s\n", s.desc, s.expectedLabels[idx], lb)
			}
		}
		if len(forbiddenLabels) != len(s.expectedForbiddenLabels) {
			t.Errorf("%s: expected %d forbidden labels but got %v\n", s.desc, len(s.expectedForbiddenLabels), forbiddenLabels)
		}
		for idx, lb := range forbiddenLabels {
			if lb != s.expectedForbiddenLabels[idx] {
				t.Errorf("%s: expected forbidden label %s but got %s\n", s.desc, s.expectedForbiddenLabels[idx], lb)
			}
		}
	}
}

func makePodStatus(status map[corev1.PodConditionType]corev1.ConditionStatus,
	containers []corev1.ContainerStatus,
	initContainers []corev1.ContainerStatus) corev1.PodStatus {
	ret := corev1.PodStatus{
		Phase:                 corev1.PodFailed,
		ContainerStatuses:     containers,
		InitContainerStatuses: initContainers,
		Conditions:            make([]corev1.PodCondition, 0, len(status)),
	}

	for t, c := range status {
		ret.Conditions = append(ret.Conditions, corev1.PodCondition{Type: t, Status: c})
	}

	return ret
}

type podFailureScenario struct {
	podStatus      map[corev1.PodConditionType]corev1.ConditionStatus
	containers     []corev1.ContainerStatus
	initContainers []corev1.ContainerStatus
	expectNil      bool
	expectedType   crd.ApplicationConditionType
	msg            string
}

func TestDescribePodFailure(t *testing.T) {

	scenarios := []podFailureScenario{
		{
			podStatus:    make(map[corev1.PodConditionType]corev1.ConditionStatus),
			expectNil:    false,
			expectedType: crd.ApplicationPodFailed,
			msg:          "lack of explanation does not make a pod successful",
		},
		{
			podStatus: map[corev1.PodConditionType]corev1.ConditionStatus{
				corev1.PodReady: corev1.ConditionTrue,
			},
			expectNil: true,
			msg:       "ready pod should have no failure conditions",
		},
		{
			podStatus: map[corev1.PodConditionType]corev1.ConditionStatus{
				corev1.PodReady:     corev1.ConditionTrue,
				corev1.PodScheduled: corev1.ConditionTrue,
			},
			containers: []corev1.ContainerStatus{{State: corev1.ContainerState{Terminated: &corev1.ContainerStateTerminated{}}}},
			expectNil:  true,
			msg:        "ready pod should have no failure conditions",
		},
		{
			podStatus: map[corev1.PodConditionType]corev1.ConditionStatus{
				corev1.PodScheduled: corev1.ConditionFalse,
			},
			expectNil:    false,
			expectedType: crd.ApplicationPodUnschedulable,
			msg:          "unschedulable pod should get PodUnschedulable condition",
		},
		{
			podStatus: map[corev1.PodConditionType]corev1.ConditionStatus{
				corev1.ContainersReady: corev1.ConditionFalse,
			},
			containers:   []corev1.ContainerStatus{{State: corev1.ContainerState{Terminated: &corev1.ContainerStateTerminated{}}}},
			expectNil:    false,
			expectedType: crd.ApplicationPodFailed,
			msg:          "containers not ready: epect PodFailed condition",
		},
		{
			podStatus: map[corev1.PodConditionType]corev1.ConditionStatus{
				corev1.PodInitialized: corev1.ConditionFalse,
			},
			initContainers: []corev1.ContainerStatus{{State: corev1.ContainerState{Terminated: &corev1.ContainerStateTerminated{}}}},
			expectNil:      false,
			expectedType:   crd.ApplicationNotRunning,
			msg:            "init containers terminated: epect PodNotRunning condition",
		},
	}

	for _, s := range scenarios {
		pod := corev1.Pod{
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{{Name: "c", Image: "c"}},
			},
			Status: makePodStatus(s.podStatus, s.containers, s.initContainers),
		}
		c := DescribePodFailure(pod)
		if (c == nil) != s.expectNil {
			t.Fatalf(s.msg)
		}
		if !s.expectNil && c.Type != s.expectedType {
			t.Errorf(s.msg)
		}
	}
}
