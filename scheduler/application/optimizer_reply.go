// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	"strings"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

// OptimizerRetryCode summarises the meaning of an optimizer return:
// - retry after a few seconds (no data in assignment plan yet)
// - retry with longer timeout / higher gap limit (optimizer said infeasible)
// - failed, do not retry (optimizer reported unretryable errors)
// - everything ok, no retry necessary
type OptimizerRetryCode int

const (
	NORETRYOK     OptimizerRetryCode = iota // Everything ok, no retry necessary.
	WAIT                                    // Try reading the plan again in a few seconds, it is not finished yet.
	RETRYMOD                                // Retry with longer timeout or higher gap limit (infeasible reply)
	NORETRYFAILED                           // Non-retryable failure
)

type OptimizerReply struct {
	RetryCode          OptimizerRetryCode
	Errors             []string
	Assignment         *assignment.QoSAssignment
	Model              *assignment.QoSModel
	prunedApplications map[string]bool
	prunedChannels     []*assignment.ApplicationModel_Channel
	podChannelsMap     map[string][]string
	channels           []*crd.Channel
	Reason             string
	Namespace          string
}

func (o *OptimizerReply) GetChannels() []*crd.Channel            { return o.channels }
func (o *OptimizerReply) GetPodChannelsMap() map[string][]string { return o.podChannelsMap }
func (o *OptimizerReply) GetPrunedApplications() map[string]bool { return o.prunedApplications }

// FromAssignments initializes a new OptimizerReply from optimizer output.
// The input (assignment plan) is a crd containing all of assignments, model, and errors.
// All of them are protocol buffers (QoSAssignment, QoSModel and Reply_Error).
func FromAssignments(assignmentPlan *crd.AssignmentPlan) OptimizerReply {
	ret := OptimizerReply{
		Namespace: assignmentPlan.Namespace,
		Errors:    make([]string, 0, len(assignmentPlan.Spec.OptimizerReply.ErrorMessages))}

	if !assignmentPlan.Spec.ReplyReceived {
		// The optimizer hasn't sent a reply yet, just keep waiting.
		ret.RetryCode = WAIT
		return ret
	}

	hasErrors := false
	for _, err := range assignmentPlan.Spec.OptimizerReply.ErrorMessages {
		hasErrors = true
		if strings.EqualFold(err.Kind, "timeout") {
			ret.RetryCode = RETRYMOD
			ret.Reason = "timeout"
			return ret
		}
		ret.Errors = append(ret.Errors, fmt.Sprintf("%s: %s", err.Kind, err.Details))
	}

	if hasErrors {
		ret.RetryCode = NORETRYFAILED
		return ret
	}

	if !assignmentPlan.Spec.OptimizerReply.Feasible {
		ret.RetryCode = RETRYMOD
		ret.Reason = "requested applications could not be scheduled, probably due to insufficient capacity"
		return ret
	}

	if assignmentPlan.Spec.OptimizerReply.QoSAssignment == nil {
		ret.RetryCode = RETRYMOD
		ret.Reason = "plan contains nil assignment, optimizer not finished?"
		return ret
	}

	if len(assignmentPlan.Spec.OptimizerReply.QoSAssignment.WorkloadAssignments) == 0 && len(assignmentPlan.Spec.OptimizerReply.QoSAssignment.PathAssignments) == 0 {
		ret.RetryCode = RETRYMOD
		ret.Reason = "plan contains no workload assignments"
		return ret
	}

	ret.Assignment = assignmentPlan.Spec.OptimizerReply.QoSAssignment.DeepCopy()
	ret.Model = assignmentPlan.Spec.QoSModel.DeepCopy()

	ret.RetryCode = NORETRYOK
	ret.pruneApplications()
	if len(ret.prunedApplications) == 0 {
		// This can only happen when all the assignments in the plan
		// are for apps that are no longer waiting to be scheduled.
		// This can be because the plan contained no workload assignments.
		// retrymod seems appropriate for that.
		ret.RetryCode = RETRYMOD
		ret.Reason = "plan contains no workload assignments"
	} else {
		ret.prepareChannelRequests()
	}

	return ret
}

// Add node ids to channel requests now that we have the schedule.
func (o *OptimizerReply) prepareChannelRequests() {
	o.podChannelsMap = make(map[string][]string)
	var err error
	o.channels, err = channelsForWorkloadChannels(o.prunedChannels, o.Assignment.PathAssignments, o.Namespace)
	if err != nil {
		o.Errors = []string{fmt.Sprintf("failed to convert workload channels: %v", err)}
		return
	}

	nodeIds := make(map[string]string)
	assignments := make(map[string]string)
	for _, a := range o.Assignment.WorkloadAssignments {
		assignments[a.WorkloadId] = a.NodeId
		nodeIds[a.NodeId] = a.NodeId
	}

	for _, channel := range o.channels {
		podFrom := channel.Spec.SourceWorkload
		nodeFromName := assignments[podFrom]
		if nodeFromName == "" {
			o.RetryCode = RETRYMOD
			o.Errors = []string{fmt.Sprintf("plan has no assignment for pod %s", podFrom)}
			return
		}
		nodeFrom := nodeIds[nodeFromName]
		if o.podChannelsMap[podFrom] == nil {
			o.podChannelsMap[podFrom] = make([]string, 0)
		}
		podTo := channel.Spec.TargetWorkload
		nodeToName := assignments[podTo]
		if nodeToName == "" {
			o.RetryCode = RETRYMOD
			o.Errors = []string{fmt.Sprintf("plan has no assignment for pod %s", podTo)}
			return
		}
		nodeTo := nodeIds[nodeToName]
		if o.podChannelsMap[podTo] == nil {
			o.podChannelsMap[podTo] = make([]string, 0)
		}

		channelID := channel.Name
		o.podChannelsMap[podFrom] = append(o.podChannelsMap[podFrom], channelID)
		o.podChannelsMap[podTo] = append(o.podChannelsMap[podTo], channelID)

		channel.Spec.ChannelFrom = nodeFrom
		channel.Spec.ChannelTo = nodeTo
	}
}

func (o *OptimizerReply) pruneApplications() {
	// See which applications in the model are in the plan.
	// Assume that the optimizer honours "all or nothing" planning for applications,
	// so if an application is in the plan, all its workloads will have a node assignment.
	// It would be possible to verify that here, but it should be unnecessary.
	plannedApps := make(map[string]bool)
	for _, p := range o.Assignment.WorkloadAssignments {
		plannedApps[p.ApplicationId] = true
	}
	o.prunedApplications = make(map[string]bool)
	for _, a := range o.Model.ApplicationModel.Applications {
		if plannedApps[a.Id] {
			o.prunedApplications[a.Id] = true
		}
	}

	// Prune the list of channels to request.
	// TODO: once the pathAssignments are in the optimizer reply, check against
	// them, too.
	o.prunedChannels = make([]*assignment.ApplicationModel_Channel, 0, len(o.Model.ApplicationModel.Channels))
	for _, i := range o.Model.ApplicationModel.Channels {
		if o.prunedApplications[i.SourceApplication] && o.prunedApplications[i.TargetApplication] {
			o.prunedChannels = append(o.prunedChannels, i)
		}
	}
}
