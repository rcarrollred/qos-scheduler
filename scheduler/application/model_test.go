// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"google.golang.org/protobuf/proto"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"testing"
)

func TestGetApplicationModel(t *testing.T) {
	app1 := MakeApplication("test-app", "default", []string{"test-workload"}, []crd.WorkloadId{})
	app2 := MakeApplication("test-app2", "default", []string{"test-workload2"}, []crd.WorkloadId{})
	app1.Status.Phase = crd.ApplicationWaiting
	app2.Status.Phase = crd.ApplicationWaiting
	app2.Spec.Workloads[0].Channels = []crd.NetworkChannel{}
	app1.Spec.Workloads[0].Channels = []crd.NetworkChannel{
		{
			OtherWorkload: crd.WorkloadId{Basename: "test-workload2", ApplicationName: "test-app2", Port: 1234},
			MinBandwidth:  "100M",
		},
	}
	appmodel := GetApplicationModel([]crd.Application{*app1, *app2}, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})
	if len(appmodel.Applications) != 2 {
		t.Errorf("expected 2 applications in model but got %d", len(appmodel.Applications))
	}
	if len(appmodel.Channels) != 1 {
		t.Errorf("expected 1 channel in model but got %d", len(appmodel.Channels))
	}
}

func TestReadApplicationModelFromTextfiles(t *testing.T) {
	model := ReadApplicationModelFromTextfiles("../testdata/")
	if len(model.Applications) != 1 {
		t.Errorf("expected to find one application in semiotics but found %d",
			len(model.Applications))
	}
	if len(model.Applications[0].Workloads) != 38 {
		t.Errorf("expected to find 38 workloads in semiotics but found %d",
			len(model.Applications[0].Workloads))
	}
	wl := model.Applications[0].Workloads[0]
	if wl.RequestedResources.MilliCPU != 200 {
		t.Errorf("expected millicpu requirements for first workload to be 200 but it was %d",
			wl.RequestedResources.MilliCPU)
	}
	if wl.RequestedResources.MemoryInBytes != 104857600 {
		t.Errorf("expected millicpu requirements for first workload to be 104857600 but it was %d",
			wl.RequestedResources.MemoryInBytes)
	}
	if len(model.Channels) != 112 {
		t.Errorf("expected to find 112 channels in semiotics but found %d",
			len(model.Channels))
	}
	interf := model.Channels[0]
	if interf.TargetWorkload != "A1-23" {
		t.Errorf("expected target workload A1-23 on first channel but found %s",
			interf.TargetWorkload)
	}
}

func TestGetComputeRequirements(t *testing.T) {
	app1 := MakeApplication("test-app", "default", []string{"test-workload"}, []crd.WorkloadId{})
	app1.Status.Phase = crd.ApplicationWaiting
	cr := getComputeRequirements([]crd.Application{*app1})
	if len(cr) != 1 {
		t.Errorf("expected to get one application model back but got %+v", cr)
	}
	// cr is a slice of pointers to optimizer.ApplicationModel_Application
	// I'm mainly interested in the RequestedResources proto.
	expected := &assignment.ApplicationModel_Application{Id: "test-app"}
	expected.Workloads = make([]*assignment.ApplicationModel_Workload, 1)
	expected.Workloads[0] = &assignment.ApplicationModel_Workload{
		Id: "test-app-test-workload",
		RequestedResources: &assignment.ComputeResources{
			MilliCPU:      500 + 10,
			MemoryInBytes: 2147483648 + 2*1024*1024,
		},
	}
	for _, value := range cr {
		if !proto.Equal(expected, value) {
			t.Errorf("got requirements %+v\n", value)
		}
	}

	app2 := MakeApplication("test-app", "default", []string{"test-workload2"}, []crd.WorkloadId{})
	app2.Status.Phase = crd.ApplicationWaiting
	cr = getComputeRequirements([]crd.Application{*app1, *app2})
	if len(cr) != 2 {
		t.Errorf("expected to get two applications back but got %+v", cr)
	}
	for _, value := range cr {
		for _, wlrequirements := range value.Workloads {
			if !proto.Equal(expected.Workloads[0].RequestedResources, wlrequirements.RequestedResources) {
				t.Errorf("got requirements %+v\n", wlrequirements)
			}
		}
	}
}

func TestGetChannels(t *testing.T) {
	app1 := MakeApplication("test-app", "default", []string{"test-workload"}, []crd.WorkloadId{})
	app2 := MakeApplication("test-app2", "default", []string{"test-workload2"}, []crd.WorkloadId{})
	app1.Status.Phase = crd.ApplicationWaiting
	app2.Status.Phase = crd.ApplicationWaiting
	app2.Spec.Workloads[0].Channels = []crd.NetworkChannel{}
	app1.Spec.Workloads[0].Channels = []crd.NetworkChannel{
		{
			OtherWorkload: crd.WorkloadId{Basename: "test-workload2", ApplicationName: "test-app2", Port: 1234},
			MinBandwidth:  "100M",
		},
	}

	ifs := getChannels([]crd.Application{*app1, *app2}, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})
	if len(ifs) != 1 {
		t.Errorf("Expected to find one channel but got %+v\n", ifs)
	}
	for _, interf := range ifs {
		if interf.SourceWorkload != "test-app-test-workload" || interf.SourceApplication != "test-app" {
			t.Errorf("Expected only test-app-test-workload as source but got %+v\n", interf)
		}
		if interf.TargetApplication != "test-app2" {
			t.Errorf("target application should be test-app2 but is %s", interf.TargetApplication)
		}
		if interf.BandwidthBits != 100000000 {
			t.Errorf("unexpected bandwidth on channel: %+v\n", interf)
		}
	}
}

func TestGetExpectedChannels(t *testing.T) {
	app1 := MakeApplication("test-app", "default", []string{"test-workload"}, []crd.WorkloadId{})
	app2 := MakeApplication("test-app2", "default", []string{"test-workload2"}, []crd.WorkloadId{})
	app1.Status.Phase = crd.ApplicationWaiting
	app2.Status.Phase = crd.ApplicationWaiting
	app2.Spec.Workloads[0].Channels = []crd.NetworkChannel{}
	app1.Spec.Workloads[0].Channels = []crd.NetworkChannel{
		{
			OtherWorkload: crd.WorkloadId{Basename: "test-workload2", ApplicationName: "test-app2", Port: 1234},
			MinBandwidth:  "100M",
		},
	}

	// No channels existing yet is normal, so handle it.
	fromHere, toHere := GetExpectedChannels(*app1, []crd.Channel{})
	if len(fromHere) != 0 {
		t.Errorf("expected no channel from test-app-test-workload to be found")
	}
	if len(toHere) != 0 {
		t.Errorf("expected no channel to test-app-test-workload to be found")
	}

	// Create a channel that the pod should reattach to
	existingChannel := crd.Channel{
		Spec: crd.ChannelSpec{
			SourceWorkload: "test-app-test-workload",
		},
	}
	existingChannel.Labels = map[string]string{"application": app1.Name}

	fromHere, toHere = GetExpectedChannels(*app1, []crd.Channel{existingChannel})
	if len(fromHere) != 1 || fromHere["test-app-test-workload"][0].Name != existingChannel.Name {
		t.Errorf("expected a channel from test-app-test-workload to be found but got %+v", fromHere)
	}
	if len(toHere) != 0 {
		t.Errorf("expected no channel to test-app-test-workload to be found")
	}
}
