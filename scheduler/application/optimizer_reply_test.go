// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"testing"
)

func makeQoSAssignment(apps map[string][]string) *assignment.QoSAssignment {
	ret := &assignment.QoSAssignment{WorkloadAssignments: make([]*assignment.QoSAssignment_WorkloadAssignment, 0)}
	for app, wls := range apps {
		for _, wl := range wls {
			ret.WorkloadAssignments = append(ret.WorkloadAssignments, &assignment.QoSAssignment_WorkloadAssignment{
				ApplicationId: app,
				WorkloadId:    fmt.Sprintf("%s-%s", app, wl),
				NodeId:        "node1",
			})
		}
	}
	return ret
}

type assignmentScenario struct {
	errorMessages []string
	infeasible    bool
	assignment    *assignment.QoSAssignment
	model         *assignment.ApplicationModel
	retryCode     OptimizerRetryCode
	msg           string
}

func assignmentPlanFromScenario(s assignmentScenario) *crd.AssignmentPlan {
	ap := &crd.AssignmentPlan{}
	ap.Spec.OptimizerReply.Feasible = !s.infeasible
	ap.Spec.ReplyReceived = true
	for _, err := range s.errorMessages {
		ap.Spec.OptimizerReply.ErrorMessages = append(ap.Spec.OptimizerReply.ErrorMessages, &assignment.Reply_Error{Kind: err})
	}
	if s.assignment != nil {
		ap.Spec.OptimizerReply.QoSAssignment = s.assignment
	} else {
		ap.Spec.OptimizerReply.QoSAssignment = &assignment.QoSAssignment{}
	}
	ap.Spec.QoSModel = assignment.QoSModel{
		InfrastructureModel: &assignment.InfrastructureModel{},
		ApplicationModel:    s.model,
	}
	return ap
}

func makeApplicationModelFromAssignment(a *assignment.QoSAssignment) *assignment.ApplicationModel {
	var ret assignment.ApplicationModel

	if a != nil {
		wl := make(map[string][]string)
		for _, wla := range a.WorkloadAssignments {
			if wl[wla.ApplicationId] == nil {
				wl[wla.ApplicationId] = make([]string, 0)
			}
			wl[wla.ApplicationId] = append(wl[wla.ApplicationId], wla.WorkloadId)
		}
		ret.Applications = make([]*assignment.ApplicationModel_Application, 0, len(wl))
		for appid, workloads := range wl {
			app := assignment.ApplicationModel_Application{Id: appid, Workloads: make([]*assignment.ApplicationModel_Workload, len(workloads))}
			for i, w := range workloads {
				app.Workloads[i] = &assignment.ApplicationModel_Workload{Id: w}
			}
			ret.Applications = append(ret.Applications, &app)
		}
	}

	return &ret
}

func assignmentPlanFromProto(ass *assignment.QoSAssignment,
	model *assignment.ApplicationModel) *crd.AssignmentPlan {
	ap := &crd.AssignmentPlan{}
	ap.Spec.OptimizerReply.Feasible = true
	ap.Spec.ReplyReceived = true
	ap.Spec.OptimizerReply.QoSAssignment = ass
	ap.Spec.QoSModel = assignment.QoSModel{
		InfrastructureModel: &assignment.InfrastructureModel{},
		ApplicationModel:    model,
	}
	return ap
}

func TestFromAssignments(t *testing.T) {
	a := makeQoSAssignment(map[string][]string{"app1": {"wl1"}})
	emptyModel := &assignment.ApplicationModel{}
	appModel := makeApplicationModelFromAssignment(a)
	scenarios := []assignmentScenario{
		{[]string{}, false, nil, emptyModel, RETRYMOD, "empty assignment"},
		{[]string{}, true, nil, emptyModel, RETRYMOD, "infeasible"},
		{[]string{"timeout"}, false, nil, emptyModel, RETRYMOD, "timeout"},
		{[]string{"someError"}, false, nil, emptyModel, NORETRYFAILED, "generic error"},
		{[]string{}, false, a, emptyModel, RETRYMOD, "pruning"},
		{[]string{}, false, a, appModel, NORETRYOK, "assignment"},
	}

	for _, s := range scenarios {
		reply := FromAssignments(assignmentPlanFromScenario(s))
		if reply.RetryCode != s.retryCode {
			t.Errorf("%s: expected retry code %d but got %d", s.msg, s.retryCode, reply.RetryCode)
		}
	}
}

func TestPruneApplications(t *testing.T) {
	app1 := MakeApplication("noplan", "default", []string{"wla", "wlb"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "plan1", Port: 1234},
		})
	app2 := MakeApplication("plan1", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "plan1", Port: 1234},
			{Basename: "wl2", ApplicationName: "plan1"},
			{Basename: "wla", ApplicationName: "noplan", Port: 2345},
		})
	appModel := GetApplicationModel([]crd.Application{*app1, *app2}, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})

	assignment := makeQoSAssignment(map[string][]string{"plan1": {"wl1", "wl2"}, "plan2": {"wl3"}})
	reply := FromAssignments(assignmentPlanFromProto(assignment, appModel))
	// FromAssignments calls pruneApplications
	if len(reply.prunedApplications) != 1 {
		t.Fatalf("expected one application post-pruning but got %v", reply.prunedApplications)
	}
	if _, ok := reply.prunedApplications["plan1"]; !ok {
		t.Errorf("expected only plan1 to remain after pruning but got %v", reply.prunedApplications)
	}
	if len(reply.prunedChannels) != 2 {
		t.Fatalf("expected two channels post-pruning but got %v", reply.prunedChannels)
	}
	for _, pif := range reply.prunedChannels {
		if pif.SourceApplication != "plan1" || pif.TargetApplication != "plan1" {
			t.Errorf("expected only channels on app plan1 post-pruning but got %v", pif)
		}
	}
}

func TestPruneChannels(t *testing.T) {
	app1 := MakeApplication("noplan", "default", []string{"wla", "wlb"}, []crd.WorkloadId{})
	app2 := MakeApplication("plan1", "default", []string{"wl1", "wl2"},
		[]crd.WorkloadId{
			{Basename: "wl1", ApplicationName: "plan1", Port: 1234},
			{Basename: "wl2", ApplicationName: "plan1"},
			{Basename: "wla", ApplicationName: "noplan", Port: 2345},
		})
	appModel := GetApplicationModel([]crd.Application{*app1, *app2}, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})

	assignment := makeQoSAssignment(map[string][]string{"plan1": {"wl1", "wl2"}, "plan2": {"wl3"}})
	reply := FromAssignments(assignmentPlanFromProto(assignment, appModel))
	// FromAssignments calls prepareChannelRequests
	if len(reply.channels) != 2 {
		t.Fatalf("expected 2 channel requests but got %d", len(reply.channels))
	}
	for _, s := range reply.channels {
		if s.Labels["application"] != "plan1" {
			t.Errorf("Expected channels from app plan1 to remain but found %s", s.Labels["application"])
		}
		if s.Labels["to-application"] != "plan1" {
			t.Errorf("Expected only channels to app plan1 to remain but found %s", s.Labels["to-application"])
		}

	}
}
