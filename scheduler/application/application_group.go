// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

// IsGroupPhaseChangePermitted returns true if an application group may
// change from oldPhase to newPhase.
func IsGroupPhaseChangePermitted(oldPhase, newPhase crd.ApplicationGroupPhase) bool {
	if oldPhase == newPhase {
		return true
	}
	if newPhase == "" {
		return false
	}
	switch oldPhase {
	case "":
		return true
	case crd.ApplicationGroupWaiting:
		return newPhase == crd.ApplicationGroupOptimizing
	case crd.ApplicationGroupOptimizing:
		return newPhase == crd.ApplicationGroupScheduling ||
			newPhase == crd.ApplicationGroupWaiting ||
			newPhase == crd.ApplicationGroupFailed
	case crd.ApplicationGroupScheduling:
		return newPhase == crd.ApplicationGroupWaiting ||
			newPhase == crd.ApplicationGroupFailed
	case crd.ApplicationGroupFailed:
		return false
	}
	// Shouldn't get here.
	return false
}

// IsGroupReady decides whether there are enough ready applications to satisfy
// the group's co-scheduling condition.
func IsGroupReady(appGroup *crd.ApplicationGroup, applications []crd.Application) bool {

	// Count applications by phase.
	countByPhase := make(map[crd.ApplicationPhase]int)
	for _, app := range applications {
		phase := app.Status.Phase
		countByPhase[phase]++
	}

	// If no applications are waiting, then there is nothing to do.
	// This doesn't technically mean the group isn't ready, it's just an
	// optimization.
	// Failed applications go to "Waiting" when all their pods have terminated,
	// so this will pick them up at that time.
	if countByPhase[crd.ApplicationWaiting] == 0 {
		return false
	}

	// If an application has failed, it needs to do some cleanup before it
	// goes back to 'Waiting'. The application group should wait for that to be done.
	if countByPhase[crd.ApplicationFailing] > 0 {
		return false
	}

	// Count applications in waiting, scheduling, pending, running.
	readyApplicationCount := countByPhase[crd.ApplicationWaiting] +
		countByPhase[crd.ApplicationScheduling] +
		countByPhase[crd.ApplicationPending] +
		countByPhase[crd.ApplicationRunning]

	// For now, just compare this count against MinMember.
	return readyApplicationCount >= int(appGroup.Spec.MinMember)
}

func NewPhaseFromApplications(current crd.ApplicationGroupPhase,
	apps []crd.Application) crd.ApplicationGroupPhase {

	if current != crd.ApplicationGroupScheduling {
		return current
	}

	for _, app := range apps {
		if app.Status.Phase == crd.ApplicationScheduling {
			// Stay in Scheduling until the applications are at least pending
			return crd.ApplicationGroupScheduling
		}
	}
	return crd.ApplicationGroupWaiting
}

// CanRetryOptimizing tells you whether this application group
// can make another optimizer attempt.
func CanRetryOptimizing(appGroup *crd.ApplicationGroup) bool {
	policy := appGroup.Spec.OptimizerRetryPolicy
	if policy.MaxRetries < 0 {
		return true
	}
	currentRetryCount := appGroup.Status.OptimizerAttemptCount
	return currentRetryCount < policy.MaxRetries
}

// NewLimitsForOptimizerRequest updates the time and gap limits
// for the next optimizer request based on the application group's
// retry policy.
func NewLimitsForOptimizerRequest(appGroup *crd.ApplicationGroup, model *assignment.QoSModel) {
	policy := appGroup.Spec.OptimizerRetryPolicy

	timeLimit := policy.InitialTimeLimit
	gapLimit := policy.InitialGapLimit
	timeLimitIncrease := policy.TimeLimitIncrease
	if timeLimitIncrease < 1 {
		timeLimitIncrease = 1
	}
	if timeLimit <= 0 {
		timeLimit = 300
	}
	gapLimitIncrease := policy.GapLimitIncrease
	if gapLimitIncrease < 1 {
		gapLimitIncrease = 1
	}
	currentRetryCount := appGroup.Status.OptimizerAttemptCount
	for i := 1; i <= currentRetryCount; i++ {
		timeLimit *= timeLimitIncrease
		gapLimit *= gapLimitIncrease
	}

	model.TimeLimit = int32(timeLimit)
	model.GapLimit = float64(gapLimit) / 100.0
}
