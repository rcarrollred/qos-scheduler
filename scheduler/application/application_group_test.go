// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"testing"
)

func TestIsGroupReady(t *testing.T) {
	scenarios := [][]crd.ApplicationPhase{
		{crd.ApplicationWaiting},
		{crd.ApplicationScheduling},
		{crd.ApplicationPending},
		{crd.ApplicationRunning},
		{crd.ApplicationWaiting, crd.ApplicationWaiting},
		{crd.ApplicationWaiting, crd.ApplicationScheduling},
		{crd.ApplicationWaiting, crd.ApplicationPending},
		{crd.ApplicationWaiting, crd.ApplicationRunning},
	}

	expected := []bool{false, false, false, false,
		true, true, true, true}

	appGroup := crd.ApplicationGroup{}
	appGroup.Spec.MinMember = 2

	for idx, scenario := range scenarios {
		var expectedResult string
		apps := make([]crd.Application, len(scenario))
		for i, phase := range scenario {
			apps[i] = crd.Application{}
			apps[i].Status.Phase = phase
		}
		if IsGroupReady(&appGroup, apps) != expected[idx] {
			if expected[idx] {
				expectedResult = "should have been ready"
			} else {
				expectedResult = "should not have been ready"
			}
			t.Errorf("application group with application phases %v %s",
				scenario, expectedResult)
		}
	}
}

func TestIsGroupPhaseChangePermitted(t *testing.T) {
	allPhases := []crd.ApplicationGroupPhase{
		crd.ApplicationGroupWaiting,
		crd.ApplicationGroupOptimizing,
		crd.ApplicationGroupScheduling,
		crd.ApplicationGroupFailed,
	}

	successPairs := map[crd.ApplicationGroupPhase][]crd.ApplicationGroupPhase{
		crd.ApplicationGroupWaiting: {
			crd.ApplicationGroupWaiting,
			crd.ApplicationGroupOptimizing},
		crd.ApplicationGroupOptimizing: {
			crd.ApplicationGroupWaiting,
			crd.ApplicationGroupScheduling,
			crd.ApplicationGroupFailed,
			crd.ApplicationGroupOptimizing},
		crd.ApplicationGroupScheduling: {
			crd.ApplicationGroupScheduling,
			crd.ApplicationGroupFailed,
			crd.ApplicationGroupWaiting},
		crd.ApplicationGroupFailed: {
			crd.ApplicationGroupFailed},
	}
	for _, oldPhase := range allPhases {
		allowedTo, exists := successPairs[oldPhase]
		if exists {
			toPhases := make(map[crd.ApplicationGroupPhase]bool)
			for _, newPhase := range allowedTo {
				toPhases[newPhase] = true
			}
			for _, newPhase := range allPhases {
				permittedDesc := "should not be permitted"
				if toPhases[newPhase] {
					permittedDesc = "should be permitted"
				}
				if IsGroupPhaseChangePermitted(oldPhase, newPhase) != toPhases[newPhase] {
					t.Errorf("phase change from %s to %s %s",
						string(oldPhase), string(newPhase), permittedDesc)
				}
			}
		} else {
			for _, newPhase := range allPhases {
				if IsGroupPhaseChangePermitted(oldPhase, newPhase) {
					t.Errorf("phase change from %s to %s should not be permitted",
						string(oldPhase), string(newPhase))
				}
			}
		}
	}
}

type retryScenario struct {
	maxRetries           int
	timeLimit            int
	gapLimit             int
	timeIncrease         int
	gapIncrease          int
	attemptCount         int
	expectedCanRetry     bool
	expectedNewTimeLimit int32
	expectedNewGapLimit  float64
	msg                  string
}

func appGroupForScenario(s retryScenario) *crd.ApplicationGroup {
	ret := &crd.ApplicationGroup{
		Spec: crd.ApplicationGroupSpec{
			OptimizerRetryPolicy: crd.OptimizerRetryPolicy{
				MaxRetries:        s.maxRetries,
				InitialTimeLimit:  s.timeLimit,
				InitialGapLimit:   s.gapLimit,
				TimeLimitIncrease: s.timeIncrease,
				GapLimitIncrease:  s.gapIncrease,
			},
		},
	}

	ret.Status.OptimizerAttemptCount = s.attemptCount
	return ret
}

func TestCanRetryOptimizing(t *testing.T) {
	scenarios := []retryScenario{
		{1, 10, 0, 1, 1, 0, true, 0, 0.0, "retry should be ok"},
		{0, 10, 0, 1, 1, 0, false, 0, 0.0, "retry should be rejected"},
	}
	for _, s := range scenarios {
		ok := CanRetryOptimizing(appGroupForScenario(s))
		if ok != s.expectedCanRetry {
			t.Errorf(s.msg)
		}
	}
}

func TestNewLimitsForOptimizerRequest(t *testing.T) {
	scenarios := []retryScenario{
		{1, 0, 0, 1, 1, 0, true, 300, 0.0, "time limit default"},
		{1, 20, 4, 0, 0, 0, true, 20, 0.04, "increase at least 1"},
		{3, 20, 4, 3, 2, 1, true, 60, 0.08, "increase for each retry"},
	}
	for _, s := range scenarios {
		model := &assignment.QoSModel{}
		NewLimitsForOptimizerRequest(appGroupForScenario(s), model)
		if model.TimeLimit != s.expectedNewTimeLimit {
			t.Errorf(fmt.Sprintf("test %s: expected new time limit %d but it was %d", s.msg, s.expectedNewTimeLimit, model.TimeLimit))
		}
		if model.GapLimit != s.expectedNewGapLimit {
			t.Errorf(fmt.Sprintf("test %s: expected new gap limit %f but it was %f", s.msg, s.expectedNewGapLimit, model.GapLimit))
		}
	}
}
