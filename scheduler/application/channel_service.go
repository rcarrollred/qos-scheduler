// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"siemens.com/qos-scheduler/scheduler/network/cni"
)

func getIpFromPod(pod corev1.Pod) string {
	return pod.Status.PodIP
}

func CreateServiceForChannelAndPod(channel crd.Channel, pod corev1.Pod) (*corev1.Service, *corev1.Endpoints, error) {

	var podIp string
	var err error
	if channel.Status.CniMetadata.VlanId > 0 {
		annotations := pod.Annotations[cni.NetworkSourceKey]
		if annotations != "" {
			podIp, err = cni.GetPodIPFromNetworkStatus(annotations, channel.Status.CniMetadata.VlanId)
			if err != nil {
				return nil, nil, err
			}
		}
	} else {
		podIp = getIpFromPod(pod)
	}
	if podIp == "" {
		return nil, nil, fmt.Errorf("pod %s does not have an ip address yet", pod.Name)
	}
	channelServiceName := infrastructure.ChannelServiceName(channel)
	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      channelServiceName,
			Namespace: pod.Namespace,
		},
		TypeMeta: metav1.TypeMeta{Kind: "Service", APIVersion: "v1"},
		Spec: corev1.ServiceSpec{
			// empty selector map because we'll manage the endpoints ourselves
			// ClusterIP is none because the service is headless
			ClusterIP: "None",
		},
	}

	endpoints := &corev1.Endpoints{
		ObjectMeta: metav1.ObjectMeta{
			// service and endpoints get matched via their names
			Name:      channelServiceName,
			Namespace: pod.Namespace,
		},
		Subsets: []corev1.EndpointSubset{
			{
				Addresses: []corev1.EndpointAddress{
					{IP: podIp},
				},
				Ports: []corev1.EndpointPort{
					{Port: channel.Spec.TargetPort},
				},
			},
		},
	}
	return svc, endpoints, nil
}
