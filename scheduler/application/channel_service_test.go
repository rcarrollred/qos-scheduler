// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func makeChannel() crd.Channel {
	return crd.Channel{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test-channel",
			Namespace: "default",
		},
		Spec: crd.ChannelSpec{
			ChannelFrom: "k8s-name-1",
			ChannelTo:   "k8s-name-2",
			TargetPort:  3333,
		},
	}
}

func TestCreateServiceForChannelAndPod(t *testing.T) {
	var pod corev1.Pod
	channel := makeChannel()
	_, _, err := CreateServiceForChannelAndPod(channel, pod)
	if err == nil {
		t.Fatalf("pod without ip address should have failed channel creation")
	}

	pod.Status.PodIP = "20.10.10.2"
	svc, ep, err := CreateServiceForChannelAndPod(channel, pod)
	if err != nil {
		t.Fatalf("unexpected error creating service: %v", err)
	}

	if svc.Name != ep.Name {
		t.Errorf("service and endpoints name should be the same but they are %s vs %s", svc.Name, ep.Name)
	}
	if svc.Spec.ClusterIP != "None" {
		t.Errorf("service cluster ip should be 'None' but is %s", svc.Spec.ClusterIP)
	}

	if ep.Subsets[0].Addresses[0].IP != "20.10.10.2" {
		t.Errorf("endpoints should have one subset with one ip address equal to 20.10.10.2 but it has %+v", ep.Subsets)
	}

	if ep.Subsets[0].Ports[0].Port != int32(3333) {
		t.Errorf("expected to find port 3333 on subsets but got %+v", ep.Subsets[0].Ports)
	}
}
