// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package application

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func channelsForWorkloadChannels(workloadChannels []*assignment.ApplicationModel_Channel,
	paths []*assignment.QoSAssignment_PathAssignment,
	namespace string) ([]*crd.Channel, error) {
	channels := make([]*crd.Channel, 0, 2*len(workloadChannels))
	pathMap := make(map[string]*assignment.QoSAssignment_PathAssignment)

	for _, p := range paths {
		pathMap[p.InterfaceId] = p
	}

	for _, interf := range workloadChannels {
		path, ok := pathMap[interf.Basename]
		if !ok {
			if len(paths) > 0 {
				fmt.Printf("missing a path assignment for channel %s\n", interf.Basename)
			}
			// TODO: else case logging that there are no path assignments in the optimizer reply?
		}
		_ = path
		podFrom := interf.SourceWorkload
		podTo := interf.TargetWorkload
		s := &crd.Channel{
			ObjectMeta: metav1.ObjectMeta{
				Name:      interf.Basename,
				Namespace: namespace,
				Labels: map[string]string{"application": interf.SourceApplication,
					"to-application": interf.TargetApplication,
					crd.NetworkLabel: interf.NetworkLabel,
				},
			},
			Spec: crd.ChannelSpec{
				// TODO: the path assignment currently only contains the source and target
				// nodes, so this is just a placeholder.
				// ChannelPath: convertPathAssignmentToChannelPath(*path),
				SourceWorkload:   podFrom,
				TargetWorkload:   podTo,
				TargetPort:       interf.TargetPort,
				MinBandwidthBits: int64(interf.BandwidthBits),
				MaxDelayNanos:    int32(interf.LatencyNanos),
				Framesize:        int32(interf.Framesize),
				SendInterval:     int32(interf.SendInterval),
				ServiceClass:     crd.NetworkServiceClass(interf.ServiceClass),
			},
		}
		infrastructure.CompleteChannelSpec(s)
		channels = append(channels, s)
	}
	return channels, nil
}

func ChannelsForApplication(app crd.Application, qosMap map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass)) ([]*crd.Channel, error) {
	workloadChannels := getAppChannels(app, qosMap)
	return channelsForWorkloadChannels(workloadChannels, []*assignment.QoSAssignment_PathAssignment{}, app.Namespace)
}
