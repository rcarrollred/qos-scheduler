// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package application

import (
	"bufio"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"os"
	"strconv"
	"strings"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func MakeApplication(name string, namespace string, wlnames []string,
	channels []crd.WorkloadId) *crd.Application {
	workloads := make([]crd.ApplicationWorkloadSpec, len(wlnames))
	for idx, wlname := range wlnames {
		appChannels := make([]crd.NetworkChannel, 0)
		for _, ch := range channels {
			if ch.ApplicationName == name && ch.Basename == wlname {
				continue
			}
			nwif := crd.NetworkChannel{
				Basename:      fmt.Sprintf("if%d-to-%s-%s", idx, ch.Basename, ch.ApplicationName),
				OtherWorkload: ch,
				MinBandwidth:  "100M",   // This is 100 MBits/s
				MaxDelay:      "0.005m", // This is 0.005 milliseconds aka 5 microseconds
				Framesize:     "1234",   // This is in bytes. The max is 1518 bytes for ethernet.
			}
			appChannels = append(appChannels, nwif)
		}
		workloads[idx] = crd.ApplicationWorkloadSpec{
			Basename: wlname,
			Template: crd.QosPodTemplateSpec{
				Metadata: crd.QosMeta{
					Labels: map[string]string{"podId": fmt.Sprintf("pod-%d", idx)},
				},
				Spec: corev1.PodSpec{
					Affinity: &corev1.Affinity{
						NodeAffinity: &corev1.NodeAffinity{
							RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
								NodeSelectorTerms: []corev1.NodeSelectorTerm{
									{
										MatchExpressions: []corev1.NodeSelectorRequirement{
											{
												Key:      fmt.Sprintf("test-key-%s-%s-%d", name, wlname, idx),
												Operator: corev1.NodeSelectorOpExists,
											},
										},
									},
								},
							},
						},
					},

					Containers: []corev1.Container{
						{
							Name:  fmt.Sprintf("%s-%d-container", wlname, idx),
							Image: fmt.Sprintf("%s-%d-container", wlname, idx),
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									// Half a core
									"cpu": *resource.NewMilliQuantity(500, resource.DecimalSI),
									// 2 GB memory
									"memory": *resource.NewQuantity(2*1024*1024*1024, resource.BinarySI),
								},
							},
						},
					},
					InitContainers: []corev1.Container{
						{
							Name:  fmt.Sprintf("%s-%d-initcontainer", wlname, idx),
							Image: fmt.Sprintf("%s-%d-initcontainer", wlname, idx),
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    *resource.NewMilliQuantity(10, resource.DecimalSI),
									"memory": *resource.NewQuantity(2*1024*1024, resource.BinarySI),
								},
							},
						},
					},
				},
			},
			Channels: appChannels,
		}
	}

	ret := &crd.Application{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "qos-scheduler.siemens.com/v1alpha1",
			Kind:       "Application",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    make(map[string]string),
		},
		Spec: crd.ApplicationSpec{
			Workloads: workloads,
		},
	}
	ret.Status.Phase = crd.ApplicationWaiting
	return ret
}

func ReadApplicationModelFromTextfiles(dirname string) *assignment.ApplicationModel {

	wlAppMap := readApplicationsFromTextfile(dirname + "WorkloadsAttributes.txt")
	readRequiredLabelsFromTextfile(dirname+"RequiredWorkloadLabels.txt", wlAppMap)
	readForbiddenLabelsFromTextfile(dirname+"ForbiddenWorkloadLabels.txt", wlAppMap)
	readWorkloadGroupsFromTextfile(dirname+"WorkloadGroups.txt", wlAppMap)
	apps := readInterfacesFromTextfile(dirname+"InterfaceData.txt", wlAppMap)
	ret := GetApplicationModel(apps, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})

	return ret
}

// Applications from WorkloadsAttributes.txt
// Format: workloadid  appname  cpu ram
func readApplicationsFromTextfile(filename string) map[int]*crd.Application {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	workloadsByApp := make(map[string][]crd.ApplicationWorkloadSpec)
	retMap := make(map[int]*crd.Application)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 4 {
			log.Fatal(fmt.Errorf("expected at least four fields in line %s", line))
		}
		workloadId, _ := strconv.Atoi(pieces[0])
		appName := pieces[1]
		cpu, _ := strconv.Atoi(pieces[2])
		memory, _ := strconv.Atoi(pieces[3])

		workload := crd.ApplicationWorkloadSpec{
			Basename: fmt.Sprintf("%d", workloadId),
			Template: crd.QosPodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  fmt.Sprintf("%d-container", workloadId),
							Image: fmt.Sprintf("%d-container", workloadId),
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    *resource.NewMilliQuantity(int64(cpu), resource.DecimalSI),
									"memory": *resource.NewQuantity(int64(memory*1024*1024), resource.BinarySI),
								},
							},
						},
					},
				},
			},
			Channels: make([]crd.NetworkChannel, 0),
		}
		if workloadsByApp[appName] == nil {
			workloadsByApp[appName] = make([]crd.ApplicationWorkloadSpec, 0)
		}
		workloadsByApp[appName] = append(workloadsByApp[appName], workload)
	}

	appsById := make(map[string]*crd.Application)
	for appName, workloadlist := range workloadsByApp {

		app := appsById[appName]
		if app == nil {
			app = &crd.Application{
				ObjectMeta: metav1.ObjectMeta{
					Name:      appName,
					Namespace: "default",
					Labels:    make(map[string]string),
				},
				Spec: crd.ApplicationSpec{
					Workloads: workloadlist,
				},
			}
			app.Status.Phase = crd.ApplicationWaiting
		}
		for _, wl := range workloadlist {
			id, _ := strconv.Atoi(wl.Basename)
			retMap[id] = app
		}
	}
	return retMap
}

// Format: sourceworkloadid targetworkloadid latency bandwidth
// Latency is in milliseconds, bandwidth in kbit/s.
func readInterfacesFromTextfile(filename string, wlAppMap map[int]*crd.Application) []crd.Application {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 4 {
			log.Fatal(fmt.Errorf("expected at least four fields in line %s", line))
		}
		sourceWlId, _ := strconv.Atoi(pieces[0])
		targetWlId, _ := strconv.Atoi(pieces[1])
		latency, _ := strconv.ParseFloat(pieces[2], 64)
		bandwidth, _ := strconv.Atoi(pieces[3])

		sourceApp := wlAppMap[sourceWlId]
		if sourceApp == nil {
			log.Fatalf("missing workload for interface %s\n", line)
		}
		targetApp := wlAppMap[targetWlId]
		if targetApp == nil {
			log.Fatalf("missing workload for interface %s\n", line)
		}
		idx := -1
		for i, w := range sourceApp.Spec.Workloads {
			if w.Basename == fmt.Sprint(sourceWlId) {
				idx = i
				break
			}
		}
		sourceApp.Spec.Workloads[idx].Channels = append(
			sourceApp.Spec.Workloads[idx].Channels, crd.NetworkChannel{
				OtherWorkload: crd.WorkloadId{Basename: fmt.Sprint(targetWlId),
					ApplicationName: targetApp.Name},
				MinBandwidth: fmt.Sprint(bandwidth * 1000),
				MaxDelay:     fmt.Sprintf("%fe-3", latency),
				Framesize:    "1234",
			})
	}
	appsById := make(map[string]*crd.Application)
	for _, app := range wlAppMap {
		if appsById[app.Name] == nil {
			appsById[app.Name] = app
		}
	}
	ret := make([]crd.Application, len(appsById))
	i := 0
	for _, a := range appsById {
		ret[i] = *a
		i++
	}
	return ret
}

func readRequiredLabelsFromTextfile(filename string, wlAppMap map[int]*crd.Application) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 2 {
			log.Fatal(fmt.Errorf("expected at least two fields in line %s", line))
		}
		wlId, _ := strconv.Atoi(pieces[0])
		label := pieces[1]
		app := wlAppMap[wlId]
		if app == nil {
			log.Fatalf("missing app for workload id %d", wlId)
		}
		found := false
		for i, w := range app.Spec.Workloads {
			if w.Basename == fmt.Sprint(wlId) {
				if w.Template.Spec.NodeSelector == nil {
					app.Spec.Workloads[i].Template.Spec.NodeSelector = make(map[string]string)
				}
				// Using app.Spec... here instead of w because I need to modify the pointer
				// not a copy of the object behind the pointer.
				app.Spec.Workloads[i].Template.Spec.NodeSelector[infrastructure.AddNamespaceToLabel(label)] = "true"
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("no workload found for id %d", wlId)
		}
	}
}

func readForbiddenLabelsFromTextfile(filename string, wlAppMap map[int]*crd.Application) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 2 {
			log.Fatal(fmt.Errorf("expected at least two fields in line %s", line))
		}
		wlId, _ := strconv.Atoi(pieces[0])
		label := pieces[1]
		app := wlAppMap[wlId]
		if app == nil {
			log.Fatalf("missing app for workload id %d", wlId)
		}
		found := false
		for i, w := range app.Spec.Workloads {
			if w.Basename == fmt.Sprint(wlId) {
				if w.Template.Spec.Affinity == nil {
					app.Spec.Workloads[i].Template.Spec.Affinity = &corev1.Affinity{
						NodeAffinity: &corev1.NodeAffinity{
							RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
								NodeSelectorTerms: make([]corev1.NodeSelectorTerm, 0),
							},
						},
					}
				}
				app.Spec.Workloads[i].Template.Spec.Affinity.NodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution.NodeSelectorTerms =
					append(app.Spec.Workloads[i].Template.Spec.Affinity.NodeAffinity.RequiredDuringSchedulingIgnoredDuringExecution.NodeSelectorTerms,
						corev1.NodeSelectorTerm{
							MatchExpressions: []corev1.NodeSelectorRequirement{
								{
									Key:      infrastructure.AddNamespaceToLabel(label),
									Operator: corev1.NodeSelectorOpDoesNotExist,
								}},
						})
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("no workload found for id %d", wlId)
		}
	}
}

func readWorkloadGroupsFromTextfile(filename string, wlAppMap map[int]*crd.Application) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		pieces := strings.Fields(line)
		if len(pieces) < 2 {
			log.Fatal(fmt.Errorf("expected at least two fields in line %s", line))
		}
		wlId, _ := strconv.Atoi(pieces[0])
		label := pieces[1]
		app := wlAppMap[wlId]
		if app == nil {
			log.Fatalf("missing app for workload id %d", wlId)
		}
		found := false
		for i, w := range app.Spec.Workloads {
			if w.Basename == fmt.Sprint(wlId) {
				if w.Template.Spec.Affinity == nil {
					app.Spec.Workloads[i].Template.Spec.Affinity = &corev1.Affinity{
						PodAntiAffinity: &corev1.PodAntiAffinity{
							RequiredDuringSchedulingIgnoredDuringExecution: make([]corev1.PodAffinityTerm, 0),
						},
					}
				}
				app.Spec.Workloads[i].Template.Spec.Affinity.PodAntiAffinity.RequiredDuringSchedulingIgnoredDuringExecution =
					append(app.Spec.Workloads[i].Template.Spec.Affinity.PodAntiAffinity.RequiredDuringSchedulingIgnoredDuringExecution,
						corev1.PodAffinityTerm{
							LabelSelector: &metav1.LabelSelector{
								MatchLabels: map[string]string{infrastructure.AddNamespaceToLabel(label): "true"},
							},
						})
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("no workload found for id %d", wlId)
		}
	}
}
