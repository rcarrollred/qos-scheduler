// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// ChannelReconciler is a base class for channel processing.
// This is not a full Reconciler because it does not implement
// the Reconcile() function.
type ChannelReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	// The list of network implementations that this controller
	// will process channels for.
	NetworkImplementations []nwlib.NetworkImplementationClass
}

func (c *ChannelReconciler) setupLinkHandlerMapFunc() handler.MapFunc {
	return func(lnk client.Object) []reconcile.Request {
		link, ok := lnk.(*crd.NetworkLink)
		if !ok {
			return []reconcile.Request{}
		}
		// Get channels embedded on link
		embeddedChannels := link.Status.EmbeddedChannels
		ret := make([]reconcile.Request, len(embeddedChannels))
		for i, ec := range embeddedChannels {
			channelName, channelNamespace := infrastructure.ParseChannelId(ec)
			ret[i] = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      channelName,
					Namespace: channelNamespace,
				},
			}
		}
		return ret
	}
}

func (c *ChannelReconciler) setupIpamHandlerMapFunc() handler.MapFunc {
	return func(ipm client.Object) []reconcile.Request {
		// Get labels on object
		label, exists := ipm.GetLabels()["channel"]
		if exists {
			channelName, channelNamespace := infrastructure.ParseChannelId(label)
			rec := reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      channelName,
					Namespace: channelNamespace,
				},
			}
			return []reconcile.Request{rec}
		}
		return []reconcile.Request{}
	}
}
