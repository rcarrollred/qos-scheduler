// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

// LogicalChannelReconciler is the outline of a logical channel
// controller. It delegates the actual processing to a Handler implementation.
type LogicalChannelReconciler struct {
	ChannelReconciler
	Handler LogicalChannelHandlerI
}

// Reconcile implements the main reconcile loop of the controller.
func (l *LogicalChannelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var channel crd.Channel
	err := l.Get(ctx, req.NamespacedName, &channel)
	if err != nil {
		if errors.IsNotFound(err) {
			l.Log.Info("channel has been deleted", "channel", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	networkImplementation := infrastructure.ChannelImplementation(&channel)

	// Are we interested in this channel?
	interested := false
	for _, ni := range l.NetworkImplementations {
		if networkImplementation == ni {
			interested = true
			break
		}
	}
	if !interested {
		return ctrl.Result{}, nil
	}

	// Is this channel in a status that we care about?
	status := channel.Status.Status

	l.Log.Info("encountered channel", "channel", channel.Name, "status", status)

	if status == crd.ChannelRequested {
		return l.Handler.HandleRequestedChannel(ctx, channel)
	}

	// This can have been rejected by this controller or by the physical
	// controller. It means the channel cannot be implemented with the available
	// capabilities. This basic implementation just cancels rejected channels, but
	// it would also be conceivable to retry them later.
	if status == crd.ChannelRejected {
		return l.Handler.HandleRejectedChannel(ctx, channel)
	}

	if status == crd.ChannelImplemented {
		return l.Handler.HandleImplementedChannel(ctx, channel)
	}

	if status == crd.ChannelCanceled {
		return l.Handler.HandleCanceledChannel(ctx, channel)
	}

	if status == crd.ChannelDeleted {
		return l.Handler.HandleDeletedChannel(ctx, channel)
	}
	return ctrl.Result{}, nil
}

// SetupWithManager initializes the controller and makes it watch channels.
// It also watches links that embed one of its channels as well as
// ipams that are used by one of its channels.
func (l *LogicalChannelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.Channel{}).
		Watches(&source.Kind{Type: &crd.NetworkLink{}}, handler.EnqueueRequestsFromMapFunc(l.setupLinkHandlerMapFunc())).
		Watches(&source.Kind{Type: &crd.Ipam{}}, handler.EnqueueRequestsFromMapFunc(l.setupIpamHandlerMapFunc())).
		Complete(l)
}
