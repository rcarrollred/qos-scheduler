// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package channelcontroller has utility code for writing and instantiating
// channel controllers.
// There are 'skeleton' logical and physical channel controllers that delegate
// all processing to a plugin called a ChannelHandler.
// The ChannelControllerFactory creates handlers and attaches them to skeleton
// controllers.
// You can write your own handlers and have the factory instantiate them.
// You do not have to use this package, you can write your own channel controllers
// from scratch if you prefer, but the skeleton controller and basic handlers might
// give you an outline to work from.
package channelcontroller
