// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// BaseLogicalChannelHandler is a sample implementation of a logical channel handler
// that implements all required methods but does nothing special.
type BaseLogicalChannelHandler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// HandleRequestedChannel verifies that there is enough capacity for the channel in the
// chosen network, and that a corresponding physical network exists.
// It records the physical network to use in a label on the channel and updates the
// channel status to include the path to use.
func (b *BaseLogicalChannelHandler) HandleRequestedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	networkImplementation := infrastructure.ChannelImplementation(&channel)
	potentialPaths, err := CanImplementChannel(ctx, b.Client, channel, string(networkImplementation))
	if len(potentialPaths) == 0 {
		if err != nil {
			b.Log.Info("channel cannot be implemented because ", "channel", channel.Name, "error", err)
		} else {
			b.Log.Info("channel cannot be implemented", "channel", channel.Name)
		}
		channel.Status.Status = crd.ChannelRejected
		err = b.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if err != nil {
		return ctrl.Result{}, err
	}

	physicalLabel := infrastructure.PhysicalChannelImplementation(&channel)
	physicalBase, pathIndex, err := GetPhysicalBaseForChannel(ctx, b.Client, channel, potentialPaths, string(networkImplementation), string(physicalLabel))
	if err != nil {
		b.Log.Info("channel cannot be implemented", "channel", channel.Name, "error", err)
		channel.Status.Status = crd.ChannelRejected
		err = b.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if string(physicalLabel) == "" {
		b.Log.Info("setting channel's physical label", "channel", channel.Name, "label", physicalBase)
		// The channel does not have a physical label yet.
		channel.Labels[crd.PhysicalNetworkLabel] = physicalBase
		err = b.Update(ctx, &channel)
		// Return here to avoid timing issues with the status update that follows.
		// This means the code section determining the channel will be executed twice.
		return ctrl.Result{}, err
	}

	channel.Status.CniMetadata.ChannelPath = crd.ChannelPathRef{
		Name:      potentialPaths[pathIndex].Name,
		Namespace: potentialPaths[pathIndex].Namespace,
	}

	channel.Status.Status = crd.ChannelReviewed
	err = b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleRejectedChannel updates the channel status to canceled.
func (b *BaseLogicalChannelHandler) HandleRejectedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	b.Log.Info("canceling rejected channel", "channel", channel.Name, "namespace", channel.Namespace)
	channel.Status.Status = crd.ChannelCanceled
	err := b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleImplementedChannel is just a placeholder in this implementation. All it does
// is update the channel's status to Ack.
func (b *BaseLogicalChannelHandler) HandleImplementedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// fill in cni metadata
	// update bandwidth and embeddedChannels status on network links
	channel.Status.Status = crd.ChannelAck
	err := b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleCanceledChannel updates the channel's status to Deleted. A different implementation
// could use this function to implement housekeeping on e.g. bandwidth management.
func (b *BaseLogicalChannelHandler) HandleCanceledChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	channel.Status.Status = crd.ChannelDeleted
	err := b.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleDeletedChannel is just a stub implementation in this controller.
func (b *BaseLogicalChannelHandler) HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// TODO: free resources, clean up if necessary
	// actual deletion should be done by physical operator
	return ctrl.Result{}, nil
}
