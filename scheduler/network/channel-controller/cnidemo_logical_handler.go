// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// CniDemoLogicalChannelHandler is a sample implementation of a logical channel handler
// that fills in cni metadata.
type CniDemoLogicalChannelHandler struct {
	client.Client
	Log                   logr.Logger
	Scheme                *runtime.Scheme
	NetworkAttachmentName string
}

// HandleRequestedChannel verifies that there is enough capacity for the channel in the
// chosen network, and that a corresponding physical network exists.
// It records the physical network to use in a label on the channel and updates the
// channel status to include the path to use.
func (c *CniDemoLogicalChannelHandler) HandleRequestedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	networkImplementation := infrastructure.ChannelImplementation(&channel)
	potentialPaths, err := CanImplementChannel(ctx, c.Client, channel, string(networkImplementation))
	if len(potentialPaths) == 0 {
		if err != nil {
			c.Log.Info("channel cannot be implemented because ", "channel", channel.Name, "error", err)
		} else {
			c.Log.Info("channel cannot be implemented", "channel", channel.Name)
		}
		channel.Status.Status = crd.ChannelRejected
		err = c.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if err != nil {
		return ctrl.Result{}, err
	}

	physicalLabel := infrastructure.PhysicalChannelImplementation(&channel)
	physicalBase, pathIndex, err := GetPhysicalBaseForChannel(ctx, c.Client, channel, potentialPaths, string(networkImplementation), string(physicalLabel))
	if err != nil {
		c.Log.Info("channel cannot be implemented", "channel", channel.Name, "error", err)
		channel.Status.Status = crd.ChannelRejected
		err = c.Status().Update(ctx, &channel)
		return ctrl.Result{}, err
	}
	if string(physicalLabel) == "" {
		c.Log.Info("setting channel's physical label", "channel", channel.Name, "label", physicalBase)
		// The channel does not have a physical label yet.
		channel.Labels[crd.PhysicalNetworkLabel] = physicalBase
		err = c.Update(ctx, &channel)
		// Return here to avoid timing issues with the status update that follows.
		// This means the code section determining the channel will be executed twice.
		return ctrl.Result{}, err
	}

	channel.Status.CniMetadata.ChannelPath = crd.ChannelPathRef{
		Name:      potentialPaths[pathIndex].Name,
		Namespace: potentialPaths[pathIndex].Namespace,
	}

	channel.Status.Status = crd.ChannelReviewed
	err = c.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleRejectedChannel updates the channel status to canceled.
func (c *CniDemoLogicalChannelHandler) HandleRejectedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	c.Log.Info("canceling rejected channel", "channel", channel.Name, "namespace", channel.Namespace)
	channel.Status.Status = crd.ChannelCanceled
	err := c.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

func (c *CniDemoLogicalChannelHandler) getPodAndNodeIpam(ipams []crd.Ipam) (int, int) {
	var podIndex int = -1
	var nodeIndex int = -1
	// Go through the list of ipams to see if any of them are for our pods and nodes.
	for i, ipam := range ipams {
		if ipam.Spec.Purpose != "" {
			if podIndex < 0 && ipam.Spec.Purpose == "pods" {
				podIndex = i
			} else if nodeIndex < 0 && ipam.Spec.Purpose == "nodes" {
				nodeIndex = i
			}
			if podIndex >= 0 && nodeIndex >= 0 {
				return podIndex, nodeIndex
			}
		}
	}
	return podIndex, nodeIndex
}

// HandleImplementedChannel requests two CIDR ranges for the channel (one for the pods, and one
// for the nodes). It waits until the CIDR ranges have been allocated and then adds them to
// the channel's cni metadata, then it updates the channel's status to Ack.
func (c *CniDemoLogicalChannelHandler) HandleImplementedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// Do we have the pod and node ipams for this channel yet?
	candidateIpams, err := GetIpamsForChannel(ctx, c.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}

	// Go through the list of existing ipams to see if any of them are for our pods
	// and nodes. We already know they are for the current channel because of the label
	// match in the list options above.
	podIndex, nodeIndex := c.getPodAndNodeIpam(candidateIpams.Items)
	var podIpam crd.Ipam
	var nodeIpam crd.Ipam
	haveToWaitForIpam := false
	if nodeIndex == -1 {
		haveToWaitForIpam = true
		nodeIpam = MakeIpam(channel, "nodes", 4)
		err = c.Create(ctx, &nodeIpam)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		nodeIpam = candidateIpams.Items[nodeIndex]
	}
	if podIndex == -1 {
		haveToWaitForIpam = true
		// Two pods, need a gateway address on each node and an address for each pod.
		podIpam = MakeIpam(channel, "pods", 4)
		err = c.Create(ctx, &podIpam)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		podIpam = candidateIpams.Items[podIndex]
	}
	// We know both ipams exist, but are they done yet?
	if podIpam.Status.StatusCode == crd.IpamRequested {
		// Keep waiting
		haveToWaitForIpam = true
	} else if podIpam.Status.StatusCode == crd.IpamUnfulfillable {
		// Time to give up
		return ctrl.Result{Requeue: false}, fmt.Errorf("ipam request for pods for channel %s cannot be fulfilled: %s", channel.Name, podIpam.Status.Error)
	} else if podIpam.Status.StatusCode != crd.IpamReserved {
		c.Log.Info("Unexpected ipam status", "status", podIpam.Status.StatusCode)
		haveToWaitForIpam = true
	}
	if nodeIpam.Status.StatusCode == crd.IpamRequested {
		// Keep waiting
		haveToWaitForIpam = true
	} else if nodeIpam.Status.StatusCode == crd.IpamUnfulfillable {
		// Time to give up
		return ctrl.Result{Requeue: false}, fmt.Errorf("ipam request for nodes for channel %s cannot be fulfilled: %s", channel.Name, podIpam.Status.Error)
	} else if nodeIpam.Status.StatusCode != crd.IpamReserved {
		c.Log.Info("Unexpected ipam status", "status", nodeIpam.Status.StatusCode)
		haveToWaitForIpam = true
	}
	if haveToWaitForIpam {
		// This reconciler also watches ipams, so there will be another reconciler
		// call when the ipam we're waiting for is created or its status changes.
		c.Log.Info("ipams are not ready yet, waiting")
		return ctrl.Result{}, nil
	}
	c.Log.Info("node ip range and pod ip range obtained", "nodecidr", nodeIpam.Status.IPv4Cidr,
		"podcidr", podIpam.Status.IPv4Cidr)
	// fill in cni metadata
	channel.Status.CniMetadata.IpRange = podIpam.Status.IPv4Cidr
	channel.Status.CniMetadata.NodeIpRange = nodeIpam.Status.IPv4Cidr
	if c.NetworkAttachmentName != "" {
		channel.Status.CniMetadata.NetworkAttachmentName = c.NetworkAttachmentName
		if channel.Status.CniMetadata.VlanId == 0 {
			channel.Status.CniMetadata.VlanId = rand.Intn(4094) + 1
			c.Log.Info("inserting a random vlan id in the channel cni metadata, you need to fix this", "vlanId", channel.Status.CniMetadata.VlanId)
		}
	}

	// TODO: also update bandwidth and embeddedChannels status on network links

	channel.Status.Status = crd.ChannelAck
	err = c.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleCanceledChannel updates the channel's status to Deleted.
// It also deletes any ipams that are associated with the channels so their network cidrs
// can be released.
func (c *CniDemoLogicalChannelHandler) HandleCanceledChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	candidateIpams, err := GetIpamsForChannel(ctx, c.Client, channel)
	if err != nil {
		return ctrl.Result{}, err
	}
	for _, ipam := range candidateIpams.Items {
		err = c.Delete(ctx, &ipam)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	channel.Status.Status = crd.ChannelDeleted
	err = c.Status().Update(ctx, &channel)
	return ctrl.Result{}, err
}

// HandleDeletedChannel is just a stub implementation.
func (c *CniDemoLogicalChannelHandler) HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error) {
	// actual deletion should be done by physical operator
	return ctrl.Result{}, nil
}
