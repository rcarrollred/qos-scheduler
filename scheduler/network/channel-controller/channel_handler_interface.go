// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
)

// LogicalChannelHandlerI is the interface that logical channel handlers must implement.
type LogicalChannelHandlerI interface {
	// HandleRequestedChannel should check whether there is enough capacity on the
	// logical network to handle the channel. If yes, record the physical network to use
	// using the physical-network-implementation label on the channel and set the channel
	// status to ChannelReviewed. Else, set the channel status to ChannelRejected.
	HandleRequestedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleRejectedChannel can either keep the channel around for debugging or further
	// updates from other systems, or it can just update the channel status to
	// ChannelCanceled.
	HandleRejectedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleImplementedChannel gets called after the physical controller has done the
	// configuration work necessary for meeting the channel's qos requests.
	// It should fill in the channel's CniMetadata status field with all the information
	// necessary to configure e.g. the CNI plugin that you are using, then update
	// the channel's status to ChannelAck.
	HandleImplementedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleCanceledChannel should perform cleanup and update the channel's status
	// to ChannelDeleted.
	HandleCanceledChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleDeletedChannel is another chance to perform cleanup. You should not delete
	// the channel here, leave that to the physical handler.
	HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// TODO: maybe there should be HandleAckChannel and HandleRunningChannel where we
	// look at whether the network performance is good enough for the channel.
}

// PhysicalChannelHandlerI is the interface that physical channel handlers must implement.
type PhysicalChannelHandlerI interface {
	// HandleReviewedChannel gets called after the logical handler has determined that
	// the logical network has capacity for the channel. It should determine whether
	// the physical network has enough capacity for the channel. If so, update the
	// status to ChannelImplementing. Otherwise, update the status to ChannelRejected.
	HandleReviewedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleImplementingChannel should take care of whichever network configurations you
	// need to do in order to meet the channel's requirements. These could be switch
	// configurations for example. When done, update the channel's status to ChannelImplemented.
	HandleImplementingChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)

	// HandleDeletedChannel should perform cleanup. Release resources held for this channel
	// and update your bandwidth management. Then delete the channel from Kubernetes.
	HandleDeletedChannel(ctx context.Context, channel crd.Channel) (ctrl.Result, error)
}
