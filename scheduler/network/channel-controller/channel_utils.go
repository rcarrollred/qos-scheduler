// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// GetNetworkLinks returns the links for the given network implementation.
func GetNetworkLinks(ctx context.Context, cl client.Client, networkImplementation string) ([]crd.NetworkLink, error) {
	var links crd.NetworkLinkList
	namespace := nwlib.NamespaceForNi(networkImplementation)
	err := cl.List(ctx, &links, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return links.Items, nil
}

// GetNetworkPaths returns the paths for the given network implementation.
func GetNetworkPaths(ctx context.Context, cl client.Client, networkImplementation string) ([]crd.NetworkPath, error) {
	var paths crd.NetworkPathList
	namespace := nwlib.NamespaceForNi(networkImplementation)
	err := cl.List(ctx, &paths, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return paths.Items, nil
}

// CanImplementChannel returns a list of paths where the channel could be implemented.
// The channel can only be implemented on paths that match its spec and that
// have enough capacity.
func CanImplementChannel(ctx context.Context, cl client.Client, channel crd.Channel, networkImplementation string) ([]crd.NetworkPath, error) {

	ret := make([]crd.NetworkPath, 0)
	paths, err := GetNetworkPaths(ctx, cl, networkImplementation)
	if err != nil {
		return ret, err
	}
	links, err := GetNetworkLinks(ctx, cl, networkImplementation)
	if err != nil {
		return ret, err
	}

	possiblePaths := nwlib.PathsForChannel(channel.Spec, paths)
	if len(possiblePaths) == 0 {
		return ret, fmt.Errorf("channel %s (spec %v) requires a path that does not exist in network %s", channel.Name, channel.Spec, networkImplementation)
	}
	for _, p := range possiblePaths {
		channelFits := nwlib.SufficientNetworkCapacityOnPath(p, links, channel)
		if channelFits {
			ret = append(ret, p)
		}
		// Keep looking if channelFits is false since there might be several paths.
	}
	// ret may still be empty, that just means there is no path for this channel.
	if len(ret) == 0 {
		return ret, fmt.Errorf("channel %s requires a path for which there is no capacity, spec: %v", channel.Name, channel.Spec)
	}
	return ret, nil
}

// GetPhysicalBaseForChannel identifies the physical network that the channel
// will be using.
func GetPhysicalBaseForChannel(ctx context.Context, cl client.Client,
	channel crd.Channel, potentialPaths []crd.NetworkPath, logicalNetwork string,
	preferredPhysicalLabel string) (string, int, error) {
	if len(potentialPaths) < 1 {
		return "", -1, fmt.Errorf("channel %s has no potential paths", channel.Name)
	}

	// Choose a path. If preferredPhysicalLabel is not empty, keep looking for a matching
	// path.
	for index, chosenPath := range potentialPaths {
		// What is the physical base of the chosen path.
		firstLink := chosenPath.Spec.Links[0]
		var link crd.NetworkLink

		err := cl.Get(ctx, types.NamespacedName{Name: firstLink, Namespace: chosenPath.Namespace}, &link)
		if err != nil {
			return "", -1, err
		}
		var physicalBase string
		if link.Spec.PhysicalBase != "" {
			physicalBase = link.Spec.PhysicalBase
		} else {
			// The link is physical
			physicalBase, err = nwlib.NiFromNamespace(link.Namespace)
			if err != nil {
				return "", -1, err
			}
		}
		if preferredPhysicalLabel == "" || preferredPhysicalLabel == physicalBase {
			return physicalBase, index, nil
		}
	}
	return "", -1, fmt.Errorf("failed to find a physical base for channel %s", channel.Name)
}

// GetIpamsForChannel retrieves a list of Ipam crds for a given channel.
func GetIpamsForChannel(ctx context.Context, cl client.Client, channel crd.Channel) (crd.IpamList, error) {
	var candidateIpams crd.IpamList
	channelId := infrastructure.ChannelId(channel)
	ni := infrastructure.ChannelImplementation(&channel)
	namespace := nwlib.NamespaceForNi(string(ni))
	listopts := []client.ListOption{
		client.InNamespace(namespace),
		client.MatchingLabels{"channel": channelId},
	}
	err := cl.List(ctx, &candidateIpams, listopts...)
	return candidateIpams, err
}

// MakeIpam creates and returns an ipam resource.
// It does not write the resource to Kubernetes.
func MakeIpam(channel crd.Channel, purpose string, numberOfAddresses int) crd.Ipam {
	ni := infrastructure.ChannelImplementation(&channel)
	namespace := nwlib.NamespaceForNi(string(ni))
	return crd.Ipam{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-%s", channel.Name, purpose),
			Namespace: namespace,
			Labels: map[string]string{
				"channel": infrastructure.ChannelId(channel),
			},
		},
		Spec: crd.IpamSpec{
			NumberOfAddresses: numberOfAddresses,
			Purpose:           purpose,
		},
	}
}
