// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package channelcontroller

import (
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

type PhysicalChannelReconciler struct {
	ChannelReconciler
	Handler PhysicalChannelHandlerI
}

func (p *PhysicalChannelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var channel crd.Channel
	err := p.Get(ctx, req.NamespacedName, &channel)
	if err != nil {
		if errors.IsNotFound(err) {
			p.Log.Info("channel has been deleted", "channel", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	physicalNetworkImplementation := infrastructure.PhysicalChannelImplementation(&channel)

	// Are we interested in this channel?
	interested := false
	for _, ni := range p.NetworkImplementations {
		if physicalNetworkImplementation == ni {
			p.Log.Info("i am interested in this channel", "channel", channel.Name)
			interested = true
			break
		}
	}
	if !interested {
		p.Log.Info("not interested in this channel", "channel", channel.Name)
		return ctrl.Result{}, nil
	}

	// Is this channel in a status that we care about?
	status := channel.Status.Status

	if status == crd.ChannelReviewed {
		return p.Handler.HandleReviewedChannel(ctx, channel)
	}

	if status == crd.ChannelImplementing {
		return p.Handler.HandleImplementingChannel(ctx, channel)
	}

	if status == crd.ChannelDeleted {
		return p.Handler.HandleDeletedChannel(ctx, channel)
	}
	return ctrl.Result{}, nil
}

func (p *PhysicalChannelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.Channel{}).
		Watches(&source.Kind{Type: &crd.NetworkLink{}}, handler.EnqueueRequestsFromMapFunc(p.setupLinkHandlerMapFunc())).
		Watches(&source.Kind{Type: &crd.Ipam{}}, handler.EnqueueRequestsFromMapFunc(p.setupIpamHandlerMapFunc())).
		Complete(p)

}
