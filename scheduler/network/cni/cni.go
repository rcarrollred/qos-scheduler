// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package cni contains code preparing cni plugin parameters.
package cni

import (
	"encoding/json"
	"fmt"
	"github.com/containernetworking/plugins/pkg/ip"
	"net"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	"strconv"
	"strings"
)

const (
	NetworkAttachmentKey = "k8s.v1.cni.cncf.io/networks"
	NetworkSourceKey     = "k8s.v1.cni.cncf.io/network-status"
)

type cniArgs struct {
	Vlan        int    `json:"vlan"`
	Parent      string `json:"parent"`
	Gateway     string `json:"gateway"`
	OtherNode   string `json:"otherNode"`
	ThisNode    string `json:"thisNode"`
	QoSPriority int    `json:"qosPriority"`
	MaskOnes    int    `json:"maskOnes"`
}

// NwadConfig contains a subset of the fields for the NetworkSelectionElement type
// from the multus-cni package.
type NwadConfig struct {
	Name             string   `json:"name"`
	Namespace        string   `json:"namespace"`
	Ips              []string `json:"ips"`
	InterfaceRequest string   `json:"interface"`
	Cni              cniArgs  `json:"cni-args"`
}

// NetworkStatus is the json struct that gets serialised into the network-status annotation
// on pods using multus.
type NetworkStatus struct {
	Name      string   `json:"name"`
	Interface string   `json:"interface"`
	Ips       []string `json:"ips"`
	Mac       string   `json:"mac"`
}

// GetPodIPFromNetworkStatus parses a multus network status annotation to get
// a pod's IP address within a given network.
func GetPodIPFromNetworkStatus(annotations string, vlanId int) (string, error) {
	if annotations == "" {
		return "", nil
	}
	var networks []*NetworkStatus

	if err := json.Unmarshal([]byte(annotations), &networks); err != nil {
		return "", fmt.Errorf("GetPodIPFromNetworkStatus: failed to parse pod Network Attachment Status JSON format: %v", err)
	}

	for _, n := range networks {
		if n.Interface == "" {
			continue
		}
		parts := strings.Split(n.Interface, ".")
		if len(parts) != 2 {
			continue
		}
		vlan, err := strconv.Atoi(parts[1])
		if err != nil {
			continue
		}
		if vlan == vlanId {
			if len(n.Ips) == 0 {
				continue
			}
			return n.Ips[0], nil
		}
	}

	return "", fmt.Errorf("failed to get ip from pod network status %s", annotations)
}

// duplicateNwad returns true if the two configs are duplicates of each other.
func duplicateNwad(original NwadConfig, other NwadConfig) bool {
	if original.Name != other.Name {
		return false
	}
	if original.Namespace != other.Namespace {
		return false
	}
	if original.InterfaceRequest != other.InterfaceRequest {
		return false
	}
	if original.Cni.Vlan != other.Cni.Vlan {
		return false
	}
	// This does not check all fields because /other/ might be intended as an update.
	return true
}

func getNetworkInterface(channel *crd.Channel, ep crd.NetworkEndpoint) (string, string, error) {
	// First, find out which physical network the channel is being implemented on.
	physicalNi := string(infrastructure.PhysicalChannelImplementation(channel))
	// Identify the network interface on ep that belongs to the chosen network.
	networkInterface, exists := ep.Spec.MappedNetworkInterfaces[physicalNi]
	if !exists {
		return "", "", fmt.Errorf("there is no network interface for %s on node %s", physicalNi, ep.Name)
	}
	address := ""
	for _, nwif := range ep.Spec.NetworkInterfaces {
		if nwif.Name == networkInterface {
			address = nwif.IPv4Address
			break
		}
	}
	return networkInterface, address, nil
}

// InsertNetworkAttachmentDefinition parses network attachment annotations out of
// the string provided, and inserts another network attachment definition with the
// given settings.
func InsertNetworkAttachmentDefinition(originalAnnotations string, pluginName string,
	source crd.NetworkEndpoint, target crd.NetworkEndpoint,
	channel crd.Channel, sourceOrTarget bool) (string, error) {
	var networks []*NwadConfig

	if originalAnnotations != "" {
		if err := json.Unmarshal([]byte(originalAnnotations), &networks); err != nil {
			return originalAnnotations, fmt.Errorf("InsertNetworkAttachmentDefinition: failed to parse pod Network Attachment Definition JSON format: %v", err)
		}
	} else {
		networks = make([]*NwadConfig, 0)
	}

	newconf, err := generateNetworkAttachmentDefinition(pluginName, channel, source, target, sourceOrTarget)
	if err != nil {
		return "", err
	}

	duplicateIndex := -1
	for i, n := range networks {
		if duplicateNwad(*n, *newconf) {
			duplicateIndex = i
			break
		}
	}

	if duplicateIndex > -1 {
		networks[duplicateIndex] = newconf
	} else {
		networks = append(networks, newconf)
	}

	js, err := json.Marshal(networks)
	return string(js), err
}

func generateNetworkAttachmentDefinition(pluginName string, channel crd.Channel,
	sourceEp crd.NetworkEndpoint, targetEp crd.NetworkEndpoint,
	sourceOrTarget bool) (*NwadConfig, error) {
	sourceGw, cidr, err := net.ParseCIDR(channel.Status.CniMetadata.IpRange)
	if err != nil {
		return nil, err
	}
	sourceGw = ip.NextIP(sourceGw)
	if !cidr.Contains(sourceGw) {
		return nil, fmt.Errorf("your cidr %s is not large enough for a vlan", cidr.String())
	}
	targetGw := sourceGw
	for i := 1; i <= 4; i++ {
		targetGw = ip.NextIP(targetGw)
	}
	if !cidr.Contains(targetGw) {
		return nil, fmt.Errorf("your cidr %s is not large enough for a vlan", cidr.String())
	}
	var gateway string
	if sourceOrTarget {
		gateway = sourceGw.String()
	} else {
		gateway = targetGw.String()
	}

	var sourceParent, sourceAddress, targetParent, targetAddress string
	sourceParent, sourceAddress, err = getNetworkInterface(&channel, sourceEp)
	if err != nil {
		return nil, err
	}
	targetParent, targetAddress, err = getNetworkInterface(&channel, targetEp)
	if err != nil {
		return nil, err
	}

	// Need to create an interface name that is unique for this channel,
	// otherwise you cannot have the same pod participating in more than
	// one basic channel
	var ifname string
	var parent string
	if sourceOrTarget {
		ifname = fmt.Sprintf("%s.%d", sourceParent, int(channel.Status.CniMetadata.VlanId))
		parent = sourceParent
	} else {
		ifname = fmt.Sprintf("%s.%d", targetParent, int(channel.Status.CniMetadata.VlanId))
		parent = targetParent
	}

	var otherNode string
	var thisNode string

	qosPriority := 5 // TODO: make this a parameter

	prefixLen := 0
	if channel.Status.CniMetadata.NodeIpRange != "" {
		_, nodeipNet, err := net.ParseCIDR(channel.Status.CniMetadata.NodeIpRange)
		if err != nil {
			return nil, err
		}
		prefixLen, _ = nodeipNet.Mask.Size()
		thisNodeIP, cidr, err := net.ParseCIDR(channel.Status.CniMetadata.NodeIpRange)
		if err != nil {
			return nil, err
		}
		thisNodeIP = ip.NextIP(thisNodeIP)
		otherNodeIP := thisNodeIP
		otherNodeIP = ip.NextIP(otherNodeIP)
		if !cidr.Contains(otherNodeIP) {
			return nil, fmt.Errorf("your node cidr %s is not large enough for a vlan", cidr.String())
		}
		if sourceOrTarget {
			thisNode = thisNodeIP.String()
			otherNode = otherNodeIP.String()
		} else {
			otherNode = thisNodeIP.String()
			thisNode = otherNodeIP.String()
		}
	} else {
		if sourceOrTarget {
			otherNode = targetAddress
			thisNode = sourceAddress
		} else {
			otherNode = sourceAddress
			thisNode = targetAddress
		}
	}

	channelImplementation := string(infrastructure.ChannelImplementation(&channel))
	namespace := nwlib.NamespaceForNi(channelImplementation)

	conf := &NwadConfig{
		Name:             pluginName,
		Namespace:        namespace,
		Ips:              []string{channel.Status.CniMetadata.IpRange},
		InterfaceRequest: ifname,
		Cni: cniArgs{
			Vlan:        int(channel.Status.CniMetadata.VlanId),
			Parent:      parent,
			Gateway:     gateway,
			OtherNode:   otherNode,
			ThisNode:    thisNode,
			QoSPriority: qosPriority,
			MaskOnes:    prefixLen,
		},
	}
	return conf, nil
}
