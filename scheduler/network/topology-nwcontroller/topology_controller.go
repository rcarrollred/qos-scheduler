// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package topologynw contains code for a network operator that obtains
// its topology from a NetworkTopology CRD.
package topologynw

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// TopologyReconciler watches a topology spec and creates networklinks from it.
type TopologyReconciler struct {
	base.BaseNetworkReconciler
	topologyResourceName string
}

// Overwrite existing paths with what's in the spec
func (t *TopologyReconciler) replacePaths(ctx context.Context, paths []crd.TopologyPathSpec, ni string) error {
	linkMap, err := t.GetExistingLinks(ctx, ni)
	if err != nil {
		return err
	}
	newPaths, err := nwlib.NetworkPathsFromTopologyPaths(paths, linkMap, t.Namespace)
	if err != nil {
		return err
	}

	return t.CrudPaths(ctx, newPaths, ni)
}

// If the topology resource contains paths, replace existing paths with these paths.
// Otherwise, compute paths based on the current state of the network links.
func (t *TopologyReconciler) processPaths(ctx context.Context, topology crd.NetworkTopologySpec) error {
	if len(topology.Paths) > 0 {
		return t.replacePaths(ctx, topology.Paths, topology.NetworkImplementation)
	}
	return t.ComputePaths(ctx, topology.NetworkImplementation)
}

// Take a topology resource and create/update/delete network links, compute nodes, and paths based on it.
func (t *TopologyReconciler) processTopology(ctx context.Context, topology crd.NetworkTopologySpec) error {
	existingNetworkEndpoints, err := t.GetExistingNetworkEndpoints(ctx)
	if err != nil {
		return err
	}
	if len(existingNetworkEndpoints) == 0 {
		return fmt.Errorf("there are no network endpoints, but they are needed for processing a topology")
	}
	err = nwlib.ValidateNetworkTopology(topology, existingNetworkEndpoints)
	if err != nil {
		return err
	}

	newLinks := nwlib.NetworkLinksFromTopologySpec(topology, topology.NetworkImplementation, t.Namespace)
	var physicalLinks map[string]crd.NetworkLink
	if !t.PhysicalNetwork {
		if topology.PhysicalBase == "" {
			return fmt.Errorf("the topology %s needs to declare a physical base network", t.topologyResourceName)
		}
		physicalLinks, err = t.GetExistingLinks(ctx, topology.PhysicalBase)
		if err != nil {
			return err
		}
		if len(physicalLinks) == 0 {
			return fmt.Errorf("the topology %s uses the physical network %s but there are no links for that", t.topologyResourceName, topology.PhysicalBase)
		}

		for name, link := range newLinks {
			physical, exists := physicalLinks[name]
			if !exists {
				return fmt.Errorf("the topology %s declares a logical link %s for which no physical base exists", t.topologyResourceName, name)
			}
			if !nwlib.CapabilitiesCanBeSatisfied(link, physical) {
				return fmt.Errorf("the topology %s declares a logical link %s whose network capabilities cannot be satisfied by the physical network", t.topologyResourceName, name)
			}
		}
	}

	// NetworkEndpoints only need to be updated if this reconciler manages a physical
	// network.
	if t.PhysicalNetwork {
		newNodes := topology.Nodes
		updates, err := nwlib.PatchNetworkEndpoints(existingNetworkEndpoints, newNodes, topology.NetworkImplementation)
		if err != nil {
			return err
		}
		err = t.UpdateNetworkEndpoints(ctx, updates)
		if err != nil {
			return err
		}
	}

	err = t.CrudLinks(ctx, newLinks, topology.NetworkImplementation)
	if err != nil {
		return err
	}
	return t.processPaths(ctx, topology)
}

// Reconcile is the main Reconcile loop of the controller.
// This loop gets called whenever a NetworkTopology resource is created, updated,
// or deleted. It updates NetworkLinks and NetworkPaths accordingly.
func (t *TopologyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var topology crd.NetworkTopology

	// Only interested in topology resources with a matching name and namespace.
	// It would be possible to configure the manager with only this namespace,
	// but then you couldn't attach several topology reconcilers with different
	// namespaces to the same manager.
	if t.topologyResourceName != "" && t.topologyResourceName != req.NamespacedName.Name {
		return ctrl.Result{}, nil
	}

	// It would be nice to configure the watcher for just the one namespace but we need
	// to look up links in the underlying physical namespace as well, and we don't
	// have that namespace until we get the topology.
	if t.Namespace != req.NamespacedName.Namespace {
		return ctrl.Result{}, nil
	}

	err := t.Get(ctx, req.NamespacedName, &topology)
	if err != nil && errors.IsNotFound(err) {
		// The topology has been removed.
		// TODO: decide what to do here, remove the corresponding links and paths?
		t.Log.Info("my topology is gone")
		return ctrl.Result{}, nil
	} else if err != nil {
		return ctrl.Result{}, err
	}
	ni := topology.Spec.NetworkImplementation
	if ni == "" {
		t.Log.Info("topology spec missing network implementation string", "topology", req.NamespacedName)
		return ctrl.Result{}, nil
	}
	if string(t.NetworkImplementation) != ni {
		// We're only interested in topologies that match our ni label.
		return ctrl.Result{}, nil
	}
	err = t.processTopology(ctx, topology.Spec)
	return ctrl.Result{}, err
}

// SetupWithManager initializes the controller and make it watch network topology resources.
func (t *TopologyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.NetworkTopology{}).Complete(t)
}
