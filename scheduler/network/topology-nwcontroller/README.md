<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Network Controller

This is a network controller that creates network links based
on a topology CRD which must be created by some outside entity
(such as an end user).
