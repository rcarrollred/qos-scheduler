// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package topologynw contains code for a network operator that obtains
// its topology from a NetworkTopology CRD.
package topologynw

import (
	"context"

	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// LinkReconciler watches physical network links and updates logical network
// links based on the physical link status.
type LinkReconciler struct {
	base.BaseLinkReconciler
}

func (l *LinkReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var link crd.NetworkLink

	_, err := nwlib.NiFromNamespace(req.NamespacedName.Namespace)
	if err != nil {
		// This link does not belong to us, just skip it.
		return ctrl.Result{}, nil
	}

	err = l.Get(ctx, req.NamespacedName, &link)
	if err != nil {
		if errors.IsNotFound(err) {
			// This link has been removed.
			// See if any link managed by this reconciler relied on the deleted link as its physical base.
			// If so, delete the affected link.
			affected, err := l.GetAffectedLink(ctx, req.NamespacedName)
			if err != nil {
				return ctrl.Result{}, err
			}
			if affected == nil {
				return ctrl.Result{}, nil
			}
			err = l.Delete(ctx, affected)
			return ctrl.Result{}, err
		} else {
			return ctrl.Result{}, err
		}
	}

	if link.Spec.PhysicalBase != "" {
		// This is a logical link, it is not relevant here.
		return ctrl.Result{}, nil
	}

	affected, err := l.GetAffectedLink(ctx, req.NamespacedName)
	if err != nil {
		return ctrl.Result{}, err
	}
	if affected == nil {
		return ctrl.Result{}, nil
	}

	// See if the affected link's capabilities still fit those of the physical link.
	if affected.Spec.BandWidthBits <= link.Spec.BandWidthBits &&
		affected.Spec.LatencyNanos >= link.Spec.LatencyNanos {
		return ctrl.Result{}, nil
	}

	l.Log.Info("the qos capabilities of the logical link can no longer be satisfied", "namespace", affected.Namespace, "link", affected.Name)

	// Should this do something else?

	return ctrl.Result{}, nil
}

func (l *LinkReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.NetworkLink{}).Complete(l)
}
