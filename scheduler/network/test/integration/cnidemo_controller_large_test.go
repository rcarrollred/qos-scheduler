// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/types"
   "sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
)

var _ = Describe("channel controller", func() {

	const (
		timeout  = time.Second * 20
		interval = time.Millisecond * 250
      logicalNi = "cnilog"
      physicalNi = "cniphys"
	)

	var (
		ctx             context.Context
		logicalTopology *crd.NetworkTopology
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("processing a channel", Ordered, func() {
		It("should create all the links", func() {
			logicalRouter := nwlib.MakeFakeRouter([]crd.NetworkPathSpec{})
         logicalRouter.AddPath("node-0", "node-1")
			physicalRouter := nwlib.MakeFakeRouter([]crd.NetworkPathSpec{})
         physicalRouter.AddPath("node-0", "node-1")
			Expect(setupTopologyReconcilers(ctx, logicalRouter, physicalRouter, physicalNi, logicalNi)).Should(Succeed())
			Expect(setupChannelReconcilers(ctx, physicalNi, logicalNi, "cnidemo")).Should(Succeed())
			physicalTopology := nwlib.MakeTopology(physicalNi, physicalNi, 3)
			Expect(k8sClient.Create(ctx, physicalTopology)).Should(Succeed())
			logicalTopology = nwlib.MakeTopology(logicalNi, logicalNi, 3)
			logicalTopology.Spec.PhysicalBase = physicalNi
			Expect(k8sClient.Create(ctx, logicalTopology)).Should(Succeed())

         // Wait until the links are all there.
			Eventually(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks, client.InNamespace("network-"+logicalNi+"-namespace")); err != nil {
					return 0
				}
            return len(networklinks.Items)
			}, timeout, interval).Should(Equal(len(logicalTopology.Spec.Links)))
		})
		It("should review a requested channel and note its physical network", func() {
			channel := nwlib.MakeChannel("test-cni-channel", "default", logicalNi)
			channel.Spec.ChannelFrom = "node-0"
			channel.Spec.ChannelTo = "node-1"
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			if channel.Status.Status == "" {
				channel.Status.Status = crd.ChannelRequested
				Expect(k8sClient.Status().Update(ctx, channel)).Should(Succeed())
			}
			// Channels spend a short time in status 'Reviewed', but we can verify that the
			// review happened because the physical channel implementation label must have been set.
			Eventually(func() string {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, channel)
				if err != nil {
					return ""
				}
				return string(infrastructure.PhysicalChannelImplementation(channel))
			}, timeout, interval).Should(Equal(physicalNi))
			k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, channel)
		})
		It("should implement a requested channel and request ip addresses", func() {
			var channel crd.Channel
			Eventually(func() crd.ChannelStatusCode {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, &channel)
				if err != nil {
					return crd.ChannelDeleted
				}
				return channel.Status.Status
			}, timeout, interval).Should(Equal(crd.ChannelImplemented))

         // During HandleImplementedChannel and before acking the channel, the ipams for pods and nodes
         // must have been requested.
         var ipams crd.IpamList
         var err error
         Eventually(func() int {
            ipams, err = chctrl.GetIpamsForChannel(ctx, k8sClient, channel)
            if err != nil { return 0 }
            return len(ipams.Items)
         }, timeout, interval).Should(Equal(2))
		})
      It("should wait until the ipams are assigned and then note the ip addresses on the channel cni metadata", func() {
			var channel crd.Channel
			k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, &channel)
         var ipams crd.IpamList
         var err error
         ipams, err = chctrl.GetIpamsForChannel(ctx, k8sClient, channel)
         Expect(err).NotTo(HaveOccurred())
         Expect(len(ipams.Items)).To(Equal(2))

         // Update one of the ipams to reserved but leave the other alone. Verify that the channel
         // does not progress to Ack.
         ipams.Items[0].Status.StatusCode = crd.IpamReserved
         if ipams.Items[0].Spec.Purpose == "pods" {
         ipams.Items[0].Status.IPv4Cidr = "10.10.0.0/29"
         } else {
         ipams.Items[0].Status.IPv4Cidr = "10.10.1.0/29"
         }
         err = k8sClient.Status().Update(ctx, &ipams.Items[0])
         Expect(err).NotTo(HaveOccurred())
         Consistently(func() crd.ChannelStatusCode {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, &channel)
				if err != nil {
					return crd.ChannelDeleted
				}
				return channel.Status.Status
			}, timeout, interval).Should(Equal(crd.ChannelImplemented))

         // Update the second ipam and verify that the channel progresses to Ack.
         ipams.Items[1].Status.StatusCode = crd.IpamReserved
         if ipams.Items[1].Spec.Purpose == "pods" {
         ipams.Items[1].Status.IPv4Cidr = "10.10.0.0/29"
         } else {
         ipams.Items[1].Status.IPv4Cidr = "10.10.1.0/29"
         }
         err = k8sClient.Status().Update(ctx, &ipams.Items[1])
         Expect(err).NotTo(HaveOccurred())
         Eventually(func() crd.ChannelStatusCode {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, &channel)
				if err != nil {
					return crd.ChannelDeleted
				}
				return channel.Status.Status
			}, timeout, interval).Should(Equal(crd.ChannelAck))

         // Verify that the channel cni metadata contains the cidrs specified in the ipams now.
         Expect(channel.Status.CniMetadata.IpRange).Should(Equal("10.10.0.0/29"))
         Expect(channel.Status.CniMetadata.NodeIpRange).Should(Equal("10.10.1.0/29"))
      })
      It("should delete the ipams when the channel is canceled", func() {
			var channel crd.Channel
         var err error
			k8sClient.Get(ctx, types.NamespacedName{Name: "test-cni-channel", Namespace: "default"}, &channel)
         var ipams crd.IpamList
         ipams, err = chctrl.GetIpamsForChannel(ctx, k8sClient, channel)
         Expect(err).NotTo(HaveOccurred())
         Expect(len(ipams.Items)).To(Equal(2))
         channel.Status.Status = crd.ChannelCanceled
         err = k8sClient.Status().Update(ctx, &channel)
         Expect(err).NotTo(HaveOccurred())

         Eventually(func() int {
            ipams, err = chctrl.GetIpamsForChannel(ctx, k8sClient, channel)
            if err != nil { return -1 }
            return len(ipams.Items)
         }, timeout, interval).Should(Equal(0)) 
      })
	})
})
