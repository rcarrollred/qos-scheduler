// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

   "k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("topology crd network controller", func() {

	const (
		timeout  = time.Second * 25
		interval = time.Millisecond * 250
	)
	var (
		ctx context.Context
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("processing a topology config", Ordered, func() {
		It("should refuse to create links without a physical base", func() {
			topo := nwlib.MakeTopology("fail-topology", "BASIC", 3)
         topo.Spec.PhysicalBase = "Nonexistent"
			Expect(k8sClient.Create(ctx, topo)).Should(Succeed())
			Consistently(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks); err != nil {
					return 1
				}
				count := 0
				for _, nwl := range networklinks.Items {
					if string(nwlib.NetworkLinkClass(nwl)) == "BASIC" {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(0))
		})
		It("should create links when a physical base exists", func() {
			topo := nwlib.MakeTopology("test-topology", "BASIC", 3)
         topo.Spec.PhysicalBase = "K8S"
			Expect(k8sClient.Create(ctx, topo)).Should(Succeed())
			Eventually(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks); err != nil {
					return 0
				}
				count := 0
				for _, nwl := range networklinks.Items {
					if string(nwlib.NetworkLinkClass(nwl)) == "BASIC" {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(len(topo.Spec.Links)))
		})
		It("should update network links", func() {
			topo := nwlib.MakeTopology("update-topology", "BASIC", 3)
         topo.Spec.PhysicalBase = "K8S"
			topo.Spec.Links[1].Capabilities = crd.TopologyLinkCapabilities{
            BandWidthBits: "5M",
            LatencyNanos: "10e5",
         }
			Expect(k8sClient.Create(ctx, topo)).Should(Succeed())
         updatedLinkName := nwlib.MakeNetworkLinkName(topo.Spec.Links[1].Source, topo.Spec.Links[1].Target)
			Eventually(func() bool {
				var updatedLink crd.NetworkLink
            if err := k8sClient.Get(ctx, types.NamespacedName{Name: updatedLinkName, Namespace: "network-basic-namespace"},
               &updatedLink); err != nil {
					return false
				}
            return updatedLink.Spec.BandWidthBits == int64(5000000) && updatedLink.Spec.LatencyNanos == int64(1000000000000000)
			}, timeout, interval).Should(Equal(true))
		})
      It("should pass updates to physical links on to the logical links", func() {
         var topo crd.NetworkTopology
         err := k8sClient.Get(ctx, types.NamespacedName{Name: "update-topology", Namespace: "network-basic-namespace"}, &topo)
         Expect(err).NotTo(HaveOccurred())
         linkToTest := topo.Spec.Links[0]
         testLinkName := nwlib.MakeNetworkLinkName(linkToTest.Source, linkToTest.Target)
         physicalNamespace := nwlib.NamespaceForNi(topo.Spec.PhysicalBase)
         var physicalLink crd.NetworkLink
         err = k8sClient.Get(ctx, types.NamespacedName{Name: testLinkName, Namespace: physicalNamespace}, &physicalLink)
         Expect(err).NotTo(HaveOccurred())

         // Modify the physical link and reduce its bandwidth.
         physicalLink.Spec.BandWidthBits = int64(40000)
         err = k8sClient.Update(ctx, &physicalLink)
         Expect(err).NotTo(HaveOccurred())

         // Currently nothing will happen
			Consistently (func() bool {
				var updatedLink crd.NetworkLink
            err := k8sClient.Get(ctx, types.NamespacedName{Name: testLinkName, Namespace: "network-basic-namespace"}, &updatedLink)
            return err == nil
			}, timeout, interval).Should(Equal(true))
      })
	})
})
