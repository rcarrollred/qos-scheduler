// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gexec"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
   chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
	basicnw "siemens.com/qos-scheduler/scheduler/network/topology-nwcontroller"
	k8snw "siemens.com/qos-scheduler/scheduler/network/k8s-nwcontroller"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	//+kubebuilder:scaffold:imports
)

var (
	cfg       *rest.Config
	k8sClient client.Client
   k8sManager ctrl.Manager
	testEnv   *envtest.Environment
	k8sReader client.Reader
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)
	_, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Network Controller Suite", reporterConfig)
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "..", "..", "..", "helm", "qos-scheduler", "crds")},
		ErrorIfCRDPathMissing: true,
	}
	err := crd.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	cfg, err := testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	//+kubebuilder:scaffold:scheme

	k8sManager, err = ctrl.NewManager(cfg, ctrl.Options{
		Scheme:                 scheme.Scheme,
		MetricsBindAddress:     "0",
		HealthProbeBindAddress: "0",
	})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sManager).NotTo(BeNil())
	k8sClient = k8sManager.GetClient()

	By("setting up controllers")
	baseReconciler := base.BaseNetworkReconciler{
		Client:                 k8sManager.GetClient(),
		Log:                    ctrl.Log.WithName("controllers").WithName("network"),
		Scheme:                 k8sManager.GetScheme(),
		NetworkImplementation:  nwlib.BasicNetwork,
		Router:                 nwlib.NewRouter(),
      // This has to match the network for the topology reconciler.
      Namespace:              "network-basic-namespace",
		PhysicalNetwork:        false,
	}
	// This is the controller that generates network links
	// from a topology crd.
	topologyReconciler := &basicnw.TopologyReconciler{
		BaseNetworkReconciler: baseReconciler,
	}
	err = topologyReconciler.SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

   baseLinkReconciler := base.BaseLinkReconciler{
		Client:                 k8sManager.GetClient(),
		Log:                    ctrl.Log.WithName("controllers").WithName("network"),
		Scheme:                 k8sManager.GetScheme(),
		NetworkImplementation:  nwlib.BasicNetwork,
   }
   topologyLinkReconciler := &basicnw.LinkReconciler{
      BaseLinkReconciler: baseLinkReconciler,
   }
   err = topologyLinkReconciler.SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	k8sReconciler := &k8snw.K8sNetworkReconciler{
		BaseNetworkReconciler: base.BaseNetworkReconciler{
			Client:                 k8sManager.GetClient(),
			Log:                    ctrl.Log.WithName("controllers").WithName("network-k8s"),
			Scheme:                 k8sManager.GetScheme(),
			NetworkImplementation:  nwlib.K8sNetwork,
			Router:                 nwlib.NewRouter(),
         Namespace:              "network-k8s-namespace",
			PhysicalNetwork:        true,
		},
      LoopbackRouter: *nwlib.NewRouter(),
	}
	err = k8sReconciler.SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	channelReconciler := &k8snw.K8sChannelReconciler{
		ChannelReconciler: chctrl.ChannelReconciler{
			Client:                 k8sManager.GetClient(),
			Log:                    ctrl.Log.WithName("channel-controllers").WithName("network-k8s"),
			Scheme:                 k8sManager.GetScheme(),
			NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.K8sNetwork, nwlib.Loopback},
		},
	}
	err = channelReconciler.SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctrl.SetupSignalHandler())
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
		gexec.KillAndWait(4 * time.Second)

		// Teardown the test environment once controller is finished.
		// Otherwise from Kubernetes 1.21+, teardown timeouts waiting on
		// kube-apiserver to return
		err := testEnv.Stop()
		Expect(err).ToNot(HaveOccurred())
	}()

	k8sReader = k8sManager.GetAPIReader()
	Expect(k8sClient).NotTo(BeNil())
	By("creating cluster infrastructure")
	ctx := context.Background()
   basicns := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-basic-namespace"}}
	err = k8sClient.Create(ctx, basicns)
	Expect(err).NotTo(HaveOccurred())
   k8sns := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-k8s-namespace"}}
	err = k8sClient.Create(ctx, k8sns)
	Expect(err).NotTo(HaveOccurred())
   lbns := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-loopback-namespace"}}
	err = k8sClient.Create(ctx, lbns)
	Expect(err).NotTo(HaveOccurred())
   phys := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-physical-namespace"}}
	err = k8sClient.Create(ctx, phys)
	Expect(err).NotTo(HaveOccurred())
   logical := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-logical-namespace"}}
	err = k8sClient.Create(ctx, logical)
	Expect(err).NotTo(HaveOccurred())
   cphys := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-cniphys-namespace"}}
	err = k8sClient.Create(ctx, cphys)
	Expect(err).NotTo(HaveOccurred())
   clogical := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-cnilog-namespace"}}
	err = k8sClient.Create(ctx, clogical)
	Expect(err).NotTo(HaveOccurred())

	node := &v1.Node{ObjectMeta: metav1.ObjectMeta{Name: "node-0"}}
	err = k8sClient.Create(ctx, node)
	Expect(err).NotTo(HaveOccurred())
	node = &v1.Node{ObjectMeta: metav1.ObjectMeta{Name: "node-1"}}
	err = k8sClient.Create(ctx, node)
	Expect(err).NotTo(HaveOccurred())
	node = &v1.Node{ObjectMeta: metav1.ObjectMeta{Name: "node-2"}}
	err = k8sClient.Create(ctx, node)
	Expect(err).NotTo(HaveOccurred())
	cn := crd.NetworkEndpoint{ObjectMeta: metav1.ObjectMeta{Name: "node-0"},
		Spec: crd.NetworkEndpointSpec{
			NodeName: "node-0",
			NetworkInterfaces: []crd.NetworkEndpointNetworkInterface{
				{
					Name:        "eth0",
					IPv4Address: "172.18.0.2",
				},
			},
		},
	}
	err = k8sClient.Create(ctx, &cn)
	Expect(err).NotTo(HaveOccurred())
	cn = crd.NetworkEndpoint{ObjectMeta: metav1.ObjectMeta{Name: "node-1"},
		Spec: crd.NetworkEndpointSpec{
			NodeName: "node-1",
			NetworkInterfaces: []crd.NetworkEndpointNetworkInterface{
				{
					Name:        "eth0",
					IPv4Address: "172.18.0.3",
				},
			},
		},
	}
	err = k8sClient.Create(ctx, &cn)
	Expect(err).NotTo(HaveOccurred())
	cn = crd.NetworkEndpoint{ObjectMeta: metav1.ObjectMeta{Name: "node-2"},
		Spec: crd.NetworkEndpointSpec{
			NodeName: "node-2",
			NetworkInterfaces: []crd.NetworkEndpointNetworkInterface{
				{
					Name:        "eth0",
					IPv4Address: "172.18.0.4",
				},
			},
		},
	}
	err = k8sClient.Create(ctx, &cn)
	Expect(err).NotTo(HaveOccurred())
	err = k8sClient.Get(ctx, types.NamespacedName{Name: "node-0"}, node)
	Expect(err).NotTo(HaveOccurred())
	node.Status.Addresses = append(node.Status.Addresses, v1.NodeAddress{Type: "InternalIP", Address: "172.18.0.2"})
	err = k8sClient.Status().Update(ctx, node)
	Expect(err).NotTo(HaveOccurred())
	err = k8sClient.Get(ctx, types.NamespacedName{Name: "node-1"}, node)
	Expect(err).NotTo(HaveOccurred())
	node.Status.Addresses = append(node.Status.Addresses, v1.NodeAddress{Type: "InternalIP", Address: "172.18.0.3"})
	err = k8sClient.Status().Update(ctx, node)
	err = k8sClient.Get(ctx, types.NamespacedName{Name: "node-2"}, node)
	Expect(err).NotTo(HaveOccurred())
	node.Status.Addresses = append(node.Status.Addresses, v1.NodeAddress{Type: "InternalIP", Address: "172.18.0.4"})
	err = k8sClient.Status().Update(ctx, node)
	Expect(err).NotTo(HaveOccurred())

})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	testEnv.Stop()
})
