// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
   v1 "k8s.io/api/core/v1"
   "k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("k8s network controller", func() {

	const (
		timeout  = time.Second * 100
		interval = time.Millisecond * 250
	)
	var (
		ctx context.Context
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("initializing a network in a cluster", Ordered, func() {
		It("should create network links between nodes", func() {
         Eventually(func() bool {
            var namespace v1.Namespace
            if err := k8sClient.Get(ctx, types.NamespacedName{Name: "network-loopback-namespace"}, &namespace); err != nil { return false }
            if err := k8sClient.Get(ctx, types.NamespacedName{Name: "network-k8s-namespace"}, &namespace); err != nil { return false }
            return true
         }, timeout, interval).Should(Equal(true))
			Eventually(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks); err != nil {
					return 0
				}
				count := 0
				for _, nwl := range networklinks.Items {
					if nwlib.NetworkLinkClass(nwl) == nwlib.K8sNetwork {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(6))
			Eventually(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks); err != nil {
					return 0
				}
				count := 0
				for _, nwl := range networklinks.Items {
					if nwlib.NetworkLinkClass(nwl) == nwlib.Loopback {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(3))
		})

		It("should create paths from the links it found", func() {
			Eventually(func() int {
				var networkPaths crd.NetworkPathList
				if err := k8sClient.List(ctx, &networkPaths); err != nil {
					return 0
				}
				count := 0
				for _, nwp := range networkPaths.Items {
					if nwlib.NetworkPathClass(nwp) == nwlib.Loopback {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(3))
			Eventually(func() int {
				var networkPaths crd.NetworkPathList
				if err := k8sClient.List(ctx, &networkPaths); err != nil {
					return 0
				}
				count := 0
				for _, nwp := range networkPaths.Items {
					if nwlib.NetworkPathClass(nwp) == nwlib.K8sNetwork {
						count++
					}
				}
				return count
			}, timeout, interval).Should(Equal(12))
		})

	})

})
