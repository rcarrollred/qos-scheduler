// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
   "context"
   ctrl "sigs.k8s.io/controller-runtime"

   "siemens.com/qos-scheduler/scheduler/network/base"
   nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
   basicnw "siemens.com/qos-scheduler/scheduler/network/topology-nwcontroller"
   chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
)

func setupTopologyReconcilers(ctx context.Context, logicalRouter nwlib.RouterI, physicalRouter nwlib.RouterI, niPhysical string, niLogical string) error { 
   physicalTopologyReconciler := &basicnw.TopologyReconciler{
      BaseNetworkReconciler: base.BaseNetworkReconciler{
         Client:                k8sManager.GetClient(),
         Log:                   ctrl.Log.WithName("topology").WithName(niPhysical),
         Scheme:                k8sManager.GetScheme(),
         NetworkImplementation: nwlib.NetworkImplementationClass(niPhysical),
         Router:                physicalRouter,
         Namespace:             "network-"+niPhysical+"-namespace",
         PhysicalNetwork:       true,
      },
   }
   err := physicalTopologyReconciler.SetupWithManager(k8sManager)
   if err != nil {
      return err
   }

   logicalTopologyReconciler := &basicnw.TopologyReconciler{
      BaseNetworkReconciler: base.BaseNetworkReconciler{
         Client:                k8sManager.GetClient(),
         Log:                   ctrl.Log.WithName("topology").WithName(niLogical),
         Scheme:                k8sManager.GetScheme(),
         NetworkImplementation: nwlib.NetworkImplementationClass(niLogical),
         Router:                logicalRouter,
         Namespace:             "network-"+niLogical+"-namespace",
         PhysicalNetwork:       false,
      },
   }
   err = logicalTopologyReconciler.SetupWithManager(k8sManager)
   if err != nil {
      return err
   }
   return nil
}

func setupChannelReconcilers(ctx context.Context, cniPhysical string, cniLogical string, pluginName string) error {
   logicalChannelReconciler := &chctrl.LogicalChannelReconciler{
      ChannelReconciler: chctrl.ChannelReconciler{
         Client:                 k8sManager.GetClient(),
         Log:                    ctrl.Log.WithName("channels").WithName(cniLogical),
         Scheme:                 k8sManager.GetScheme(),
         NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.NetworkImplementationClass(cniLogical)},
      },
   }
   factory := chctrl.NewChannelControllerFactory()
   err := factory.AddPluginToLogicalChannelController(pluginName, map[string]string{}, logicalChannelReconciler)
   if err != nil {
      return err
   }
   err = logicalChannelReconciler.SetupWithManager(k8sManager)
   if err != nil {
      return err
   }

   physicalChannelReconciler := &chctrl.PhysicalChannelReconciler{
      ChannelReconciler: chctrl.ChannelReconciler{
         Client:                 k8sManager.GetClient(),
         Log:                    ctrl.Log.WithName("channels").WithName(cniPhysical),
         Scheme:                 k8sManager.GetScheme(),
         NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.NetworkImplementationClass(cniPhysical)},
      },
   }
   err = factory.AddPluginToPhysicalChannelController(pluginName, map[string]string{}, physicalChannelReconciler)
   if err != nil {
      return err
   }
   err = physicalChannelReconciler.SetupWithManager(k8sManager)
   if err != nil {
      return err
   }
   return nil
}

