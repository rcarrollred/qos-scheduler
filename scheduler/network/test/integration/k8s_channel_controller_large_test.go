// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("k8s channel controller", func() {

	const (
		timeout  = time.Second * 20
		interval = time.Millisecond * 250
	)

	var (
		ctx context.Context
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("processing a channel", func() {
		It("should acknowledge a requested channel", func() {
			channel := nwlib.MakeChannel("requested-channel", "default", string(nwlib.Loopback))
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "requested-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			channel.Status = crd.ChannelStatus{
				Status: crd.ChannelRequested,
			}
			Expect(k8sClient.Status().Update(ctx, channel)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "requested-channel", Namespace: "default"}, channel)
				if err != nil {
					return false
				}
				return channel.Status.Status == crd.ChannelAck
			}, timeout, interval).Should(Equal(true))
		})
		It("should delete a canceled channel", func() {
			channel := nwlib.MakeChannel("canceled-channel", "default", string(nwlib.Loopback))
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "canceled-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			channel.Status = crd.ChannelStatus{
				Status: crd.ChannelCanceled,
			}
			Expect(k8sClient.Status().Update(ctx, channel)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "canceled-channel", Namespace: "default"}, channel)
				return err != nil && errors.IsNotFound(err)
			}, timeout, interval).Should(Equal(true))
		})
		It("should leave all other channels alone", func() {
			channel := nwlib.MakeChannel("other-channel", "default", string(nwlib.Loopback))
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "other-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			channel.Status = crd.ChannelStatus{
				Status: crd.ChannelRunning,
			}
			Expect(k8sClient.Status().Update(ctx, channel)).Should(Succeed())
			// Wait for the status change to take effect.
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "other-channel", Namespace: "default"}, channel)
				if err != nil {
					return false
				}
				return channel.Status.Status == crd.ChannelRunning
			}, timeout, interval).Should(Equal(true))

			// And then verify nothing changes afterwards.
			Consistently(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "other-channel", Namespace: "default"}, channel)
				if err != nil {
					return false
				}
				return channel.Status.Status == crd.ChannelRunning
			}, timeout, interval).Should(Equal(true))
		})
	})

})
