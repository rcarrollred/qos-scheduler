// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/types"
   "sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("channel controller", func() {

	const (
		timeout  = time.Second * 20
		interval = time.Millisecond * 250
      niLogical = "logical"
      niPhysical = "physical"
	)

	var (
		ctx             context.Context
		logicalTopology *crd.NetworkTopology
	)

	JustBeforeEach(func() {
		ctx = context.Background()
	})

	Describe("processing a channel", Ordered, func() {
		It("should create all the paths", func() {
			logicalRouter := nwlib.MakeFakeRouter([]crd.NetworkPathSpec{})
         logicalRouter.AddPath("node-0", "node-1")
			physicalRouter := nwlib.MakeFakeRouter([]crd.NetworkPathSpec{})
         physicalRouter.AddPath("node-0", "node-1")
			Expect(setupTopologyReconcilers(ctx, logicalRouter, physicalRouter, niPhysical, niLogical)).Should(Succeed())
			Expect(setupChannelReconcilers(ctx, niPhysical, niLogical, "base")).Should(Succeed())
			physicalTopology := nwlib.MakeTopology(niPhysical, niPhysical, 3)
			Expect(k8sClient.Create(ctx, physicalTopology)).Should(Succeed())
			logicalTopology = nwlib.MakeTopology(niLogical, niLogical, 3)
			logicalTopology.Spec.PhysicalBase = niPhysical
			Expect(k8sClient.Create(ctx, logicalTopology)).Should(Succeed())

         // Wait until the links are all there.
			Eventually(func() int {
				var networklinks crd.NetworkLinkList
				if err := k8sClient.List(ctx, &networklinks, client.InNamespace("network-"+niLogical+"-namespace")); err != nil {
					return 0
				}
            return len(networklinks.Items)
			}, timeout, interval).Should(Equal(len(logicalTopology.Spec.Links)))
		})
		It("should review a requested channel and note its physical network", func() {
			channel := nwlib.MakeChannel("test-channel", "default", niLogical)
			channel.Spec.ChannelFrom = "node-0"
			channel.Spec.ChannelTo = "node-1"
			Expect(k8sClient.Create(ctx, channel)).Should(Succeed())
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-channel", Namespace: "default"}, channel)
				return err == nil
			}, timeout, interval).Should(Equal(true))
			if channel.Status.Status == "" {
				channel.Status.Status = crd.ChannelRequested
				Expect(k8sClient.Status().Update(ctx, channel)).Should(Succeed())
			}
			// Channels spend a short time in status 'Reviewed', but we can verify that the
			// review happened because the physical channel implementation label must have been set.
			Eventually(func() string {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-channel", Namespace: "default"}, channel)
				if err != nil {
					return ""
				}
				return string(infrastructure.PhysicalChannelImplementation(channel))
			}, timeout, interval).Should(Equal(niPhysical))
			k8sClient.Get(ctx, types.NamespacedName{Name: "test-channel", Namespace: "default"}, channel)
		})
		It("should implement a requested channel", func() {
			// There is an 'implementing' status but the test channel will be in it for too
			// short a time for testing. It is fine if we find the channel in status Implemented or Ack.
         // TODO: verify that the cni metadata has been filled in.
			var channel crd.Channel
			Eventually(func() crd.ChannelStatusCode {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: "test-channel", Namespace: "default"}, &channel)
				if err != nil {
					return crd.ChannelDeleted
				}
				return channel.Status.Status
			}, timeout, interval).Should(Or(Equal(crd.ChannelImplemented), Equal(crd.ChannelAck)))
		})
	})

})
