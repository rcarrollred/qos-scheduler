// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build topology
// +build topology

package main

import (
	"flag"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"os"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	nwc "siemens.com/qos-scheduler/scheduler/network/topology-nwcontroller"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"strings"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup topology network controller")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(crd.AddToScheme(scheme))
}

func main() {
	var metricsAddr string
	var probeAddr string
	var networkImplementations string
	var isPhysical bool
	var channelController string
   var networkAttachmentName string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.StringVar(&networkImplementations, "network-implementations", "BASIC", "The network implementation tags that this network controller should handle")
	flag.BoolVar(&isPhysical, "physical-network", false, "Whether this operator manages physical or logical networks")
	flag.StringVar(&channelController, "channel-controller", "", "The name of the channel controller to load (currently 'base' or 'cnidemo'.")
   flag.StringVar(&networkAttachmentName, "network-attachment-name", "", "The name of the network attachment definition resource to use with multus cni plugins, if any")

	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	nis := strings.Split(networkImplementations, ",")
	namespaces := make([]string, len(nis))
	for i, ni := range nis {
		namespaces[i] = nwlib.NamespaceForNi(ni)
	}

	// NewManager is an alias for manager.New
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
		// NewCache: cache.MultiNamespacedCacheBuilder(namespaces),
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager for topology network reconciler")
		os.Exit(1)
	}
	for i, ni := range nis {
		nwClass := nwlib.NetworkImplementationClass(ni)
		baseReconciler := base.BaseNetworkReconciler{
			Client:                mgr.GetClient(),
			Log:                   ctrl.Log.WithName("controllers").WithName("topology"),
			Scheme:                mgr.GetScheme(),
			NetworkImplementation: nwClass,
			Namespace:             namespaces[i],
			Router:                nwlib.NewRouter(),
			PhysicalNetwork:       isPhysical,
		}
		topology := nwc.TopologyReconciler{
			BaseNetworkReconciler: baseReconciler,
		}

		if err = topology.SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to setup topology network reconciler")
			os.Exit(1)
		}

		if !isPhysical {
			// Logical networks have to watch the underlying physical networks
			// for link changes.
			baseLinkReconciler := base.BaseLinkReconciler{
				Client:                mgr.GetClient(),
				Log:                   ctrl.Log.WithName("controllers").WithName("topology"),
				Scheme:                mgr.GetScheme(),
				NetworkImplementation: nwClass,
			}

			topologyLinkReconciler := nwc.LinkReconciler{
				BaseLinkReconciler: baseLinkReconciler,
			}

			if err = topologyLinkReconciler.SetupWithManager(mgr); err != nil {
				setupLog.Error(err, "unable to setup topology link reconciler")
				os.Exit(1)
			}
		}
		if channelController != "" {
			factory := chctrl.NewChannelControllerFactory()
			if isPhysical {
				channelReconciler := chctrl.PhysicalChannelReconciler{
					ChannelReconciler: chctrl.ChannelReconciler{
						Client:                 mgr.GetClient(),
						Log:                    ctrl.Log.WithName("controllers").WithName("topology"),
						Scheme:                 mgr.GetScheme(),
						NetworkImplementations: []nwlib.NetworkImplementationClass{nwClass},
					},
				}
				err = factory.AddPluginToPhysicalChannelController(channelController,
                map[string]string{}, &channelReconciler)
				if err != nil {
					setupLog.Error(err, "unable to create channel controller")
					os.Exit(1)
				}
				if err = channelReconciler.SetupWithManager(mgr); err != nil {
					setupLog.Error(err, "unable to setup channel reconciler")
					os.Exit(1)
				}
			} else {
				channelReconciler := chctrl.LogicalChannelReconciler{
					ChannelReconciler: chctrl.ChannelReconciler{
						Client:                 mgr.GetClient(),
						Log:                    ctrl.Log.WithName("controllers").WithName("topology"),
						Scheme:                 mgr.GetScheme(),
						NetworkImplementations: []nwlib.NetworkImplementationClass{nwClass},
					},
				}
				err = factory.AddPluginToLogicalChannelController(channelController,
                map[string]string{"network-attachment-name": networkAttachmentName},
                &channelReconciler)
				if err != nil {
					setupLog.Error(err, "unable to create channel controller")
					os.Exit(1)
				}
				if err = channelReconciler.SetupWithManager(mgr); err != nil {
					setupLog.Error(err, "unable to setup channel reconciler")
					os.Exit(1)
				}
			}
		}
	}

	setupLog.Info("starting topology network operator")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running topology network operator")
		os.Exit(1)
	}
}
