// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build k8s
// +build k8s

package main

import (
	"flag"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"os"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
	k8s "siemens.com/qos-scheduler/scheduler/network/k8s-nwcontroller"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup k8s network controller")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(crd.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var probeAddr string
	var networkImplementations string
	var isPhysical bool
	var channelController string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.StringVar(&networkImplementations, "network-implementations", "K8S,LOOPBACK", "This flag has no effect and only exists to keep helm happy")
	flag.BoolVar(&isPhysical, "physical-network", true, "This flag should always be true but it exists to keep helm happy")
	flag.StringVar(&channelController, "channel-controller", "", "This flag has no effect on this controller")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager for k8s network controller")
		os.Exit(1)
	}
	baseForK8s := base.BaseNetworkReconciler{
		Client:                mgr.GetClient(),
		Log:                   ctrl.Log.WithName("controllers").WithName("k8s"),
		Scheme:                mgr.GetScheme(),
		NetworkImplementation: nwlib.K8sNetwork,
		Router:                nwlib.NewRouter(),
		Namespace:             nwlib.NamespaceForNi(string(nwlib.K8sNetwork)),
		PhysicalNetwork:       isPhysical,
	}
	k8sReconciler := k8s.K8sNetworkReconciler{
		BaseNetworkReconciler: baseForK8s,
		LoopbackRouter:        *nwlib.NewRouter(),
	}
	if err = k8sReconciler.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup fallback network controller")
		os.Exit(1)
	}

	channelReconciler := k8s.K8sChannelReconciler{
		ChannelReconciler: chctrl.ChannelReconciler{
			Client:                 mgr.GetClient(),
			Log:                    ctrl.Log.WithName("channelControllers").WithName("k8s"),
			Scheme:                 mgr.GetScheme(),
			NetworkImplementations: []nwlib.NetworkImplementationClass{nwlib.K8sNetwork, nwlib.Loopback},
		},
	}
	if err = channelReconciler.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to setup fallback channel controller")
		os.Exit(1)
	}

	setupLog.Info("starting k8s network operator")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running k8s network operator")
		os.Exit(1)
	}
}
