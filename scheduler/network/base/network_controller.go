// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package base contains reusable code for network operators.
package base

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// The BaseNetworkReconciler implements a few generally
// useful functions for network controllers.
// This is not a full Reconciler because it does not implement the Reconcile()
// function
type BaseNetworkReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme

	// The namespace where the links and paths managed by this operator live.
	Namespace string

	// If not nil, can be used to build network paths from links.
	Router nwlib.RouterI

	// The network implementation that this controller
	// is responsible for.
	NetworkImplementation nwlib.NetworkImplementationClass

	// True when this controller manages physical networks.
	// False when this controller manages logical networks.
	PhysicalNetwork bool
}

// GetExistingLinks retrieves all the links for the given network implementation.
func (b *BaseNetworkReconciler) GetExistingLinks(ctx context.Context, ni string) (map[string]crd.NetworkLink, error) {
	var links crd.NetworkLinkList
	namespace := nwlib.NamespaceForNi(ni)
	err := b.List(ctx, &links, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return nwlib.MakeNetworkLinksMap(links.Items), nil
}

// GetExistingPaths retrieves all paths for the given network implementation.
func (b *BaseNetworkReconciler) GetExistingPaths(ctx context.Context, ni string) (map[string]crd.NetworkPath, error) {
	var existingPaths crd.NetworkPathList
	namespace := nwlib.NamespaceForNi(ni)
	err := b.List(ctx, &existingPaths, client.InNamespace(namespace))
	if err != nil {
		return nil, err
	}
	return nwlib.MakeNetworkPathsMap(existingPaths.Items), nil
}

// GetPathsFromSourceToTarget retrieves all paths for the given network implementation
// that go from the sourceNode to the targetNode.
func (b *BaseNetworkReconciler) GetPathsFromSourceToTarget(ctx context.Context, ni string, sourceNode string, targetNode string) (map[string]crd.NetworkPath, error) {
	var paths crd.NetworkPathList
	namespace := nwlib.NamespaceForNi(ni)
	opts := []client.ListOption{
		client.InNamespace(namespace),
		client.MatchingLabels{"path-start": sourceNode},
		client.MatchingLabels{"path-end": targetNode},
	}
	err := b.List(ctx, &paths, opts...)
	if err != nil {
		return nil, err
	}
	return nwlib.MakeNetworkPathsMap(paths.Items), nil
}

// GetExistingNetworkEndpoints retrieves all network endpoints.
func (b *BaseNetworkReconciler) GetExistingNetworkEndpoints(ctx context.Context) (map[string]crd.NetworkEndpoint, error) {
	var endpoints crd.NetworkEndpointList
	err := b.List(ctx, &endpoints)
	if err != nil {
		return nil, err
	}
	return nwlib.MakeNetworkEndpointsMap(endpoints.Items), nil
}

// UpdateNetworkEndpoints applies network endpoint updates to the cluster.
func (b *BaseNetworkReconciler) UpdateNetworkEndpoints(ctx context.Context, updatedEndpoints []crd.NetworkEndpoint) error {
	for _, ep := range updatedEndpoints {
		err := b.Update(ctx, &ep)
		if err != nil {
			return err
		}
	}
	return nil
}

// CrudLinks takes a list of new links and compares them to the links that exist in the cluster to
// derive a list of create, update, delete actions.
// Then applies those creates, updates, and deletes.
func (b *BaseNetworkReconciler) CrudLinks(ctx context.Context, newLinks map[string]crd.NetworkLink, ni string) error {
	existingLinks, err := b.GetExistingLinks(ctx, ni)
	if err != nil {
		return err
	}
	create, update, del := nwlib.PatchNetworkLinks(existingLinks, newLinks)
	for _, l := range create {
		b.Log.Info("CrudLinks: creating", "link", l.Name, "namespace", l.Namespace)
		err = b.Create(ctx, &l)
		if err != nil {
			return err
		}
		if b.Router != nil {
			b.Log.Info("CrudLinks: adding link to router", "link", l.Name, "namespace", l.Namespace)
			b.Router.AddNetworkLink(l)
		}
	}
	for _, l := range update {
		link := existingLinks[l.Name]
		link.Spec = l.Spec
		err = b.Update(ctx, &link)
		if err != nil {
			return err
		}
	}
	for _, l := range del {
		err = b.Delete(ctx, &l)
		if err != nil {
			return err
		}
		if b.Router != nil {
			b.Router.DeleteNetworkLink(l)
		}
	}
	return nil
}

// CrudPaths takes a list of new paths and compares them to the paths that exist in the cluster to
// derive a list of create, update, delete actions.
// Then applies those creates, updates, and deletes.
func (b *BaseNetworkReconciler) CrudPaths(ctx context.Context, newPaths map[string]crd.NetworkPath, ni string) error {
	pathMap, err := b.GetExistingPaths(ctx, ni)
	if err != nil {
		return err
	}

	create, update, del := nwlib.PatchNetworkPaths(pathMap, newPaths)
	for _, p := range create {
		err = b.Create(ctx, &p)
		if err != nil {
			return err
		}
	}

	for _, p := range update {
		oldPath := pathMap[p.Name]
		oldPath.Spec = p.Spec
		err = b.Update(ctx, &oldPath)
		if err != nil {
			return err
		}
	}

	for _, p := range del {
		err = b.Delete(ctx, &p)
		if err != nil {
			return err
		}
	}
	return nil
}

// ComputePaths computes paths from existing links and applies them.
func (b *BaseNetworkReconciler) ComputePaths(ctx context.Context, ni string) error {
	links, err := b.GetExistingLinks(ctx, ni)
	if err != nil {
		return err
	}
	if b.Router == nil {
		return fmt.Errorf("must have a router")
	}
	if !b.Router.IsInitialized() {
		err = b.Router.Initialize(links)
		if err != nil {
			return err
		}
	}
	paths, err := b.Router.GetNetworkPaths()
	if err != nil {
		return err
	}

	namespace := nwlib.NamespaceForNi(ni)

	newPathsMap := make(map[string]crd.NetworkPath)
	for _, p := range paths {
		path := nwlib.NetworkPathFromSpec(p, namespace)
		newPathsMap[path.Name] = *path
	}
	return b.CrudPaths(ctx, newPathsMap, ni)
}
