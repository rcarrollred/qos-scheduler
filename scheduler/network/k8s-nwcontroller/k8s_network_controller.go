// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package k8snw contains a network controller that manages the default Kubernetes
// network connecting all nodes.
package k8snw

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/network/base"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

const (
	// The network label for the k8s network.
	K8SNI = string(nwlib.K8sNetwork)
	// The label for loopback links and channels.
	LBNI = string(nwlib.Loopback)
)

// K8sNetworkReconciler watches Kubernetes nodes and creates network links and paths for them.
type K8sNetworkReconciler struct {
	base.BaseNetworkReconciler
	k8sTopology    crd.NetworkTopologySpec
	initialized    bool
	LoopbackRouter nwlib.Router
}

func (k *K8sNetworkReconciler) scanNodes(ctx context.Context) error {
	var k8sNodes corev1.NodeList
	err := k.List(ctx, &k8sNodes)
	if err != nil {
		return err
	}
	var networkEndpoints crd.NetworkEndpointList
	err = k.List(ctx, &networkEndpoints)
	if err != nil {
		return err
	}
	newTopology, err := k.topologyFromKubernetesNodes(k8sNodes, networkEndpoints)
	if err != nil {
		return err
	}
	k.k8sTopology = newTopology
	existingNetworkEndpoints, err := k.GetExistingNetworkEndpoints(ctx)
	if err != nil {
		return err
	}
	updates, err := nwlib.PatchNetworkEndpoints(existingNetworkEndpoints, newTopology.Nodes, K8SNI)
	if err != nil {
		return err
	}
	err = k.UpdateNetworkEndpoints(ctx, updates)
	if err != nil {
		return err
	}
	newLinks := nwlib.NetworkLinksFromTopologySpec(k.k8sTopology, K8SNI, k.Namespace)
	err = k.CrudLinks(ctx, newLinks, K8SNI)
	if err != nil {
		return err
	}
	newLoopbackLinks := k.loopbackLinks(networkEndpoints)
	err = k.CrudLinks(ctx, newLoopbackLinks, LBNI)
	if err != nil {
		return err
	}
	return nil
}

func (k *K8sNetworkReconciler) handleModifiedNetworkEndpoint(ctx context.Context, node corev1.Node) error {
	var eps crd.NetworkEndpointList
	var ep crd.NetworkEndpoint
	err := k.List(ctx, &eps)
	found := false
	if err != nil {
		return err
	}
	for _, e := range eps.Items {
		if e.Spec.NodeName == node.Name {
			ep = e
			found = true
			break
		}
	}
	// Need to wait until there is a network endpoint for this node.
	if !found {
		return fmt.Errorf("no network endpoint exists yet for node %s", node.Name)
	}

	addr := doesNetworkEndpointMatchK8sNode(ep, node)
	if addr != "" {
		// The address exists and it matches. Make sure it is in the mapped network interfaces.
		var nwif string
		for _, nw := range ep.Spec.NetworkInterfaces {
			if nw.IPv4Address == addr {
				nwif = nw.Name
			}
		}
		if nwif == "" {
			return fmt.Errorf("network endpoint %s has no network interface with ip address %s", ep.Name, addr)
		}

		nwifName, exists := ep.Spec.MappedNetworkInterfaces[K8SNI]
		if exists {
			if nwifName != nwif {
				return fmt.Errorf("cannot update network interface name for k8s network on network endpoint %s because it is already set", ep.Name)
			}
		}
		ep.Spec.MappedNetworkInterfaces[K8SNI] = nwif
		err = k.Update(ctx, &ep)
		if err != nil {
			return err
		}
	} else {
		return fmt.Errorf("kubernetes node %s and network endpoint  %s have mismatched addresses", node.Name, ep.Name)
	}

	// create a loopback link if this is a new node
	loopbackLinkName := nwlib.MakeNetworkLinkName(node.Name, node.Name)
	var link crd.NetworkLink
	namespace := nwlib.NamespaceForNi(LBNI)
	err = k.Get(ctx, types.NamespacedName{Name: loopbackLinkName, Namespace: namespace}, &link)
	if err != nil {
		// TODO: extract a function for this
		if errors.IsNotFound(err) {
			k.Log.Info("loopback link does not exist yet, creating", "link", loopbackLinkName)
			link = crd.NetworkLink{
				ObjectMeta: metav1.ObjectMeta{
					Name:      loopbackLinkName,
					Namespace: namespace,
				},
				Spec: crd.NetworkLinkSpec{
					LinkFrom:      crd.LinkNode{Name: node.Name, Type: crd.COMPUTE_NODE},
					LinkTo:        crd.LinkNode{Name: node.Name, Type: crd.COMPUTE_NODE},
					BandWidthBits: nwlib.DefaultBandwidth,
					LatencyNanos:  nwlib.DefaultLatency,
				},
			}
			err = k.Create(ctx, &link)
		}
	}
	return err
}

func (k *K8sNetworkReconciler) handleDeletedNode(ctx context.Context, name string) error {
	var eps crd.NetworkEndpointList
	var ep crd.NetworkEndpoint
	err := k.List(ctx, &eps)
	found := false
	if err != nil {
		return err
	}
	for _, e := range eps.Items {
		if e.Spec.NodeName == name {
			ep = e
			found = true
			break
		}
	}
	// Delete the k8sni network from the corresponding network endpoint
	if found {
		nwlib.DeleteNetworkImplementation(&ep, K8SNI)
		err = k.Update(ctx, &ep)
		if err != nil {
			k.Log.Error(err, "failed to remove network map entry for K8SNI from network endpoint")
		}
	}
	// Delete the links that touched this node
	var nwlist crd.NetworkLinkList
	err = k.List(ctx, &nwlist)
	if err != nil {
		return err
	}
	for _, l := range nwlist.Items {
		if string(nwlib.NetworkLinkClass(l)) != K8SNI && string(nwlib.NetworkLinkClass(l)) != LBNI {
			continue
		}
		if l.Spec.LinkFrom.Name == name || l.Spec.LinkTo.Name == name {
			err = k.Delete(ctx, &l)
			if err != nil {
				k.Log.Error(err, "failed to delete a network link")
			}
		}
	}
	return err
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networklinks,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networklinks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkpaths,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkendpoints,verbs=get;list;update;patch
//+kubebuilder:rbac:groups="",resources=nodes,verbs=get;list;watch

// Reconcile gets called whenever a Kubernetes Node is created, updated,
// or deleted. It updates NetworkLinks and NetworkPaths accordingly.
func (k *K8sNetworkReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	if k.Router == nil {
		k.Log.Error(fmt.Errorf("a k8s reconciler must have a non-nil router"), "configure this reconciler with a router")
	}

	// Do a global scan of all nodes on startup.
	// There should be a separate controller that watches for network implementation tags and also takes care
	// of calling this controller so it can re-scan from time to time.
	if !k.initialized {
		k.initialized = true
		err := k.scanNodes(ctx)
		if err != nil {
			k.initialized = false
			return ctrl.Result{}, err
		}
		err = k.ComputePaths(ctx, K8SNI)
		if err != nil {
			k.initialized = false
			return ctrl.Result{}, err
		}
		lblinks, err := k.GetExistingLinks(ctx, LBNI)
		if err != nil {
			return ctrl.Result{}, err
		}
		if !k.LoopbackRouter.IsInitialized() {
			err = k.LoopbackRouter.Initialize(lblinks)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
		lbpaths, err := k.LoopbackRouter.GetNetworkPaths()
		if err != nil {
			return ctrl.Result{}, err
		}
		lbnamespace := nwlib.NamespaceForNi(LBNI)
		newPathsMap := make(map[string]crd.NetworkPath)
		for _, p := range lbpaths {
			path := nwlib.NetworkPathFromSpec(p, lbnamespace)
			newPathsMap[path.Name] = *path
		}
		err = k.CrudPaths(ctx, newPathsMap, LBNI)
		if err != nil {
			k.initialized = false
			return ctrl.Result{}, err
		}
	}

	var node corev1.Node
	err := k.Get(ctx, req.NamespacedName, &node)
	if err != nil && errors.IsNotFound(err) {
		// The node has been removed.
		err := k.handleDeletedNode(ctx, req.NamespacedName.Name)
		return ctrl.Result{}, err
	} else if err != nil {
		return ctrl.Result{}, err
	}

	err = k.handleModifiedNetworkEndpoint(ctx, node)
	return ctrl.Result{}, err
}

// SetupWithManager initializes the controller and make it watch Kubernetes nodes.
func (k *K8sNetworkReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&corev1.Node{}).Complete(k)
}
