// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package k8snw contains a network controller that manages the default
// Kubernetes network connecting all nodes.
package k8snw

import (
	"context"

	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	chctrl "siemens.com/qos-scheduler/scheduler/network/channel-controller"
)

// The K8sChannelReconciler is a combination logical and physical
// channel operator that only handles the basic k8s and loopback channels,
// which have no settings or special properties.
type K8sChannelReconciler struct {
	chctrl.ChannelReconciler
}

// Reconcile gets called whenever a Channel is created, modified,
// or deleted. It acknowledges the channel.
// TODO: also make it adopt channels for which no controller exists
func (k *K8sChannelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var channel crd.Channel
	err := k.Get(ctx, req.NamespacedName, &channel)

	if err != nil {
		if errors.IsNotFound(err) {
			k.Log.Info("channel has been deleted", "channel", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	// Are we interested in this channel?
	// This controller can act as both a logical and a physical channel controller.
	// You might have a logical network on top of k8s with its own channel controller,
	// and then this controller here would only handle the physical side. Or you could
	// have channels that just use the k8s network all the way, and then this controller
	// would handle both the logical and the physical side of those channels.
	// And for loopback channels, this controller always handles all the steps.
	cl := infrastructure.ChannelImplementation(&channel)
	pcl := infrastructure.PhysicalChannelImplementation(&channel)
	k.Log.Info("got a channel", "channel", channel.Name, "class", cl, "labels", channel.GetLabels())
	interested_logical := false
	interested_physical := false
	for _, ni := range k.NetworkImplementations {
		if cl == ni {
			k.Log.Info("i am interested in this channel", "channel", channel.Name)
			interested_logical = true
			break
		}
		if pcl == ni {
			k.Log.Info("i am interested in this channel", "channel", channel.Name)
			interested_physical = true
		}
	}
	if !interested_logical && !interested_physical {
		k.Log.Info("i am not interested in this channel", "channel", channel.Name)
		return ctrl.Result{}, nil
	}

	// Now look at the channel status to check whether we need to do anything with it.
	// This goes through all the normal channel status and it covers both the
	// status transitions that are normally handled by logical and physical controllers.
	// The "logical" transitions will only be handled if interested_logical is true.
	// The transitions are:
	// Requested -> Reviewed (logical)
	// Reviewed -> Implemented (physical)
	// Implemented -> Ack (logical)
	switch channel.Status.Status {
	case crd.ChannelRequested:
		if interested_logical {
			channel.Status.Status = crd.ChannelReviewed
			err = k.Status().Update(ctx, &channel)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
	case crd.ChannelReviewed:
		channel.Status.Status = crd.ChannelImplemented
		err = k.Status().Update(ctx, &channel)
		if err != nil {
			return ctrl.Result{}, err
		}
	case crd.ChannelImplemented:
		if interested_logical {
			channel.Status.Status = crd.ChannelAck
			err = k.Status().Update(ctx, &channel)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
	case crd.ChannelCanceled:
		err = k.Delete(ctx, &channel)
		if err != nil {
			return ctrl.Result{}, err
		}
	case crd.ChannelDeleted:
		err = k.Delete(ctx, &channel)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (k *K8sChannelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).For(&crd.Channel{}).Complete(k)
}
