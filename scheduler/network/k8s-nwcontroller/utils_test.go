// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package k8snw

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestTopologyFromKubernetesNodes(t *testing.T) {
	k8sNodes := corev1.NodeList{
		Items: []corev1.Node{},
	}
	endpoints := crd.NetworkEndpointList{
		Items: []crd.NetworkEndpoint{},
	}
	reconciler := K8sNetworkReconciler{}
	topologySpec, err := reconciler.topologyFromKubernetesNodes(k8sNodes, endpoints)
	if err != nil {
		t.Errorf("unexpected error getting a topology from kubernetes nodes: %v", err)
	}
	fmt.Printf("got topology spec: %+v", topologySpec)
}
