// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package k8snw

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func doesNetworkEndpointMatchK8sNode(endpoint crd.NetworkEndpoint, k8sNode corev1.Node) string {
	var nodeIpAddress string
	for _, addr := range k8sNode.Status.Addresses {
		if addr.Type == "InternalIP" {
			nodeIpAddress = addr.Address
			break
		}
	}
	if nodeIpAddress == "" {
		return ""
	}
	// Look through the IP addresses of the network interfaces on the endpoint until
	// we find one that equals the node's internal IP.
	for _, nwif := range endpoint.Spec.NetworkInterfaces {
		if nwif.IPv4Address == nodeIpAddress {
			return nwif.IPv4Address
		}
		// TODO: also scan alternate addresses here.
	}
	return ""
}

func (k *K8sNetworkReconciler) loopbackLinks(endpoints crd.NetworkEndpointList) map[string]crd.NetworkLink {
	ret := make(map[string]crd.NetworkLink, len(endpoints.Items))
	for _, ep := range endpoints.Items {
		loopbackLinkName := nwlib.MakeNetworkLinkName(ep.Name, ep.Name)
		lbnamespace := nwlib.NamespaceForNi(LBNI)
		ret[loopbackLinkName] = crd.NetworkLink{
			ObjectMeta: metav1.ObjectMeta{
				Name:      loopbackLinkName,
				Namespace: lbnamespace,
			},
			Spec: crd.NetworkLinkSpec{
				LinkFrom:      crd.LinkNode{Name: ep.Name, Type: crd.COMPUTE_NODE},
				LinkTo:        crd.LinkNode{Name: ep.Name, Type: crd.COMPUTE_NODE},
				BandWidthBits: nwlib.DefaultBandwidth,
				LatencyNanos:  nwlib.DefaultLatency,
			},
		}
	}
	return ret
}

func (k *K8sNetworkReconciler) topologyFromKubernetesNodes(k8sNodes corev1.NodeList, endpoints crd.NetworkEndpointList) (crd.NetworkTopologySpec, error) {
	endpointsMap := nwlib.MakeNetworkEndpointsMap(endpoints.Items)
	nodeCount := len(k8sNodes.Items)
	newTopology := crd.NetworkTopologySpec{
		NetworkImplementation: K8SNI,
		Nodes:                 make([]crd.TopologyNodeSpec, nodeCount),
		Links:                 make([]crd.TopologyLinkSpec, 0, nodeCount*nodeCount),
	}
	for i, n := range k8sNodes.Items {
		// Skip nodes that aren't running. The Phase can be an empty string,
		// so this doesn't just compare against NodeRunning.
		if n.Status.Phase == corev1.NodePending || n.Status.Phase == corev1.NodeTerminated {
			continue
		}
		matchingEP, exists := endpointsMap[n.Name]
		if !exists {
			// This should get retried, maybe the endpoint wasn't ready yet.
			return newTopology, fmt.Errorf("failed to find a network endpoint for kubernetes node %s", n.Name)
		}
		addr := doesNetworkEndpointMatchK8sNode(matchingEP, n)
		if addr == "" {
			return newTopology, fmt.Errorf("network endpoint %s should match kubernetes node %s but the ip addresses do not match", matchingEP.Name, n.Name)
		}
		newTopology.Nodes[i] = crd.TopologyNodeSpec{
			Name:      n.Name,
			IPAddress: addr,
			Type:      crd.COMPUTE_NODE,
		}
	}
	// create links between any pair of nodes
	for _, n := range newTopology.Nodes {
		for _, m := range newTopology.Nodes {
			if n.Name == m.Name {
				continue
			}
			link := crd.TopologyLinkSpec{
				Source: n.Name,
				Target: m.Name,
				// Capacity: map[string]string // TODO: make this a default capacity for k8s links
			}
			newTopology.Links = append(newTopology.Links, link)
		}
	}
	return newTopology, nil
}
