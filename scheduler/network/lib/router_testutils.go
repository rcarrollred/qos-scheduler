// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package lib

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

type FakeRouter struct {
	initialized bool
	paths       []crd.NetworkPathSpec
}

func (f *FakeRouter) ValidateNetworkPaths() error { return nil }

func (f *FakeRouter) DeleteNetworkLink(link crd.NetworkLink) error { return nil }

func (f *FakeRouter) AddNetworkLink(link crd.NetworkLink) error { return nil }

func (f *FakeRouter) Initialize(links map[string]crd.NetworkLink) error {
	f.initialized = true
	return nil
}

func (f *FakeRouter) IsInitialized() bool { return f.initialized }

func (f *FakeRouter) GetNetworkPaths() ([]crd.NetworkPathSpec, error) {
	return f.paths, nil
}

func MakeFakeRouter(paths []crd.NetworkPathSpec) *FakeRouter {
	return &FakeRouter{
		initialized: true,
		paths:       paths,
	}
}

func (f *FakeRouter) AddPath(sourceNode string, targetNode string) {
	path := crd.NetworkPathSpec{
		Start: crd.LinkNode{Name: sourceNode},
		End:   crd.LinkNode{Name: targetNode},
		Links: []string{fmt.Sprintf("%s-%s", sourceNode, targetNode)},
	}
	f.paths = append(f.paths, path)
}
