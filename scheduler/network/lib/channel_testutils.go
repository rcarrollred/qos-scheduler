// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package lib

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

func MakeChannel(name string, namespace string, ni string) *crd.Channel {
	spec := makeChannelSpec()
	return &crd.Channel{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels: map[string]string{
				crd.NetworkLabel: ni,
			},
		},
		Spec: spec,
	}
}

func makeChannelSpec() crd.ChannelSpec {
	return crd.ChannelSpec{
		ChannelFrom:    "local",
		ChannelTo:      "local",
		SourceWorkload: "from-wl",
		TargetWorkload: "to-wl",
		TargetPort:     3333,
	}
}
