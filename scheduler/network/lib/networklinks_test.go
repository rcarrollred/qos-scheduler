// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func MakeNetworkLink(fromName, fromType, toName, toType string, bw, latency int64) crd.NetworkLink {
	node1 := crd.LinkNode{
		Name: fromName,
		Type: crd.NodeType(fromType),
	}
	node2 := crd.LinkNode{
		Name: toName,
		Type: crd.NodeType(toType),
	}
	return crd.NetworkLink{
		TypeMeta: metav1.TypeMeta{Kind: "NetworkLink", APIVersion: "v1alpha1"},
		ObjectMeta: metav1.ObjectMeta{
			Name: MakeNetworkLinkName(fromName, toName),
		},
		Spec: crd.NetworkLinkSpec{
			LinkFrom:      node1,
			LinkTo:        node2,
			BandWidthBits: bw,
			LatencyNanos:  latency,
		},
		Status: crd.NetworkLinkStatus{
			BandWidthAvailable: bw,
			LatencyNanos:       latency,
		},
	}
}

func TestPatchNetworkLinks_allNew(t *testing.T) {
	newLinks := []crd.NetworkLink{
		MakeNetworkLink("a", "COMPUTE", "b", "COMPUTE", 10000, 1000),
		MakeNetworkLink("b", "COMPUTE", "c", "COMPUTE", 10000, 1000),
	}
	c, u, d := PatchNetworkLinks(MakeNetworkLinksMap([]crd.NetworkLink{}), MakeNetworkLinksMap(newLinks))

	if len(c) != len(newLinks) {
		t.Errorf("expected %d new links but got %d", len(newLinks), len(c))
	}
	if len(u) != 0 {
		t.Errorf("expected no updates but got %v", u)
	}
	if len(d) != 0 {
		t.Errorf("expected no deletions but got %v", d)
	}
}

func TestPatchNetworkLinks_deletions(t *testing.T) {
	newLinks := []crd.NetworkLink{}
	oldLinks := []crd.NetworkLink{
		MakeNetworkLink("b", "COMPUTE", "c", "COMPUTE", 10000, 1000),
	}
	oldMap := MakeNetworkLinksMap(oldLinks)
	c, u, d := PatchNetworkLinks(oldMap, MakeNetworkLinksMap(newLinks))

	if len(c) != 0 {
		t.Errorf("expected no new links but got %v", c)
	}
	if len(u) != 0 {
		t.Errorf("expected no updates but got %v", u)
	}
	if len(d) != 1 {
		t.Errorf("expected one deletion but got %v", d)
	}
	_, present := oldMap[d[0].Name]
	if !present {
		t.Errorf("expected %v to be deleted but got %v", oldMap, d)
	}
}

func TestPatchNetworkLinks_updates(t *testing.T) {
	newLinks := []crd.NetworkLink{
		MakeNetworkLink("a", "COMPUTE", "b", "COMPUTE", 10000, 3000),
		MakeNetworkLink("b", "COMPUTE", "c", "COMPUTE", 10000, 2000),
	}
	oldLinks := []crd.NetworkLink{
		MakeNetworkLink("b", "COMPUTE", "c", "COMPUTE", 10000, 1000),
	}
	oldMap := MakeNetworkLinksMap(oldLinks)
	newMap := MakeNetworkLinksMap(newLinks)
	_, u, d := PatchNetworkLinks(oldMap, newMap)
	if len(u) != 1 {
		t.Errorf("expected one update but got %v", u)
	}
	if len(d) != 0 {
		t.Errorf("expected no deletions but got %v", d)
	}
	_, present := oldMap[u[0].Name]
	if !present {
		t.Errorf("expected %v to be updated but got %v", oldMap, u)
	}
	if u[0].Spec.LatencyNanos != 2000 {
		t.Errorf("expected %v to be updated to latency 2000 but got %v", oldMap, u)
	}
}
