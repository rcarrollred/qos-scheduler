// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestInitialize(t *testing.T) {
	links := []crd.NetworkLink{
		MakeNetworkLink("a", "COMPUTE", "b", "COMPUTE", 10000, 1000),
		MakeNetworkLink("b", "COMPUTE", "a", "COMPUTE", 20000, 2000),
	}
	lm := MakeNetworkLinksMap(links)
	router := NewRouter()
	err := router.Initialize(lm)
	if err != nil {
		t.Errorf("unexpected error during router initialization: %v", err)
	}
}

func TestGetNetworkPaths(t *testing.T) {
	router := NewRouter()

	_, err := router.GetNetworkPaths()
	if err == nil {
		t.Errorf("uninitialized router should not return paths")
	}

	links := []crd.NetworkLink{
		MakeNetworkLink("a", "COMPUTE", "b", "COMPUTE", 10000, 1000),
		MakeNetworkLink("b", "COMPUTE", "a", "COMPUTE", 20000, 2000),
	}
	lm := MakeNetworkLinksMap(links)
	err = router.Initialize(lm)
	if err != nil {
		t.Errorf("unexpected error during router initialization: %v", err)
	}
	paths, err := router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}

	if len(paths) != 2 {
		t.Errorf("expected two paths but got %+v", paths)
	}

	// Add a loopback link to the router and request paths again.
	router.AddNetworkLink(MakeNetworkLink("a", "COMPUTE", "a", "COMPUTE", 40000, 4))
	paths, err = router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}
	if len(paths) != 3 {
		t.Errorf("expected three paths but got %+v", paths)
	}

	// Add a link to a network node and request paths again (expect failure
	// because the reverse link is missing)
	router.AddNetworkLink(MakeNetworkLink("a", "COMPUTE", "c", "NETWORK", 100, 400))
	_, err = router.GetNetworkPaths()
	if err == nil {
		t.Errorf("expected an error because of missing reverse path")
	}

	// Add the missing link, now expect the same three paths as before because
	// the new target is not a compute node.
	router.AddNetworkLink(MakeNetworkLink("c", "NETWORK", "a", "COMPUTE", 100, 400))
	paths, err = router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}
	if len(paths) != 3 {
		t.Errorf("expected three paths but got %+v", paths)
	}

	// Add another link so there is a new path to another compute node.
	router.AddNetworkLink(MakeNetworkLink("c", "NETWORK", "d", "COMPUTE", 100, 400))
	router.AddNetworkLink(MakeNetworkLink("d", "COMPUTE", "c", "NETWORK", 100, 400))
	paths, err = router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}
	if len(paths) != 7 {
		t.Errorf("expected seven paths but got %+v", paths)
	}
}

func TestGetNetworkPaths_FullyConnected(t *testing.T) {
	router := NewRouter()
	links := []crd.NetworkLink{
		MakeNetworkLink("c1", "COMPUTE", "c1", "COMPUTE", 100, 10),
		MakeNetworkLink("c1", "COMPUTE", "c2", "COMPUTE", 100, 10),
		MakeNetworkLink("c1", "COMPUTE", "kind-control-plane", "COMPUTE", 100, 10),
		MakeNetworkLink("c2", "COMPUTE", "c1", "COMPUTE", 100, 10),
		MakeNetworkLink("c2", "COMPUTE", "c2", "COMPUTE", 100, 10),
		MakeNetworkLink("c2", "COMPUTE", "kind-control-plane", "COMPUTE", 100, 10),
		MakeNetworkLink("kind-control-plane", "COMPUTE", "c1", "COMPUTE", 100, 10),
		MakeNetworkLink("kind-control-plane", "COMPUTE", "c2", "COMPUTE", 100, 10),
		MakeNetworkLink("kind-control-plane", "COMPUTE", "kind-control-plane", "COMPUTE", 100, 10),
	}
	lm := MakeNetworkLinksMap(links)
	err := router.Initialize(lm)
	if err != nil {
		t.Errorf("unexpected error in initialization: %v", err)
	}

	paths, err := router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}

	// There should be three loopback paths plus two paths between
	// every pair of distinct nodes, for a total of 15.
	if len(paths) != 15 {
		router.dumpNetworkPaths()
		t.Errorf("expected 15 paths but got %d", len(paths))
	}

	err = router.ValidateNetworkPaths()
	if err != nil {
		t.Errorf("should have got a valid set of network paths but got %v", err)
	}
}

func TestGetNetworkPaths_LargeTopology(t *testing.T) {
	router := NewRouter()

	links := []crd.NetworkLink{
		MakeNetworkLink("c1", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c1", "COMPUTE", 200, 5),
		MakeNetworkLink("n1", "NETWORK", "n1", "NETWORK", 200, 5),
		MakeNetworkLink("c1", "COMPUTE", "c1", "COMPUTE", 200, 5),

		MakeNetworkLink("c2", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c2", "COMPUTE", 200, 5),
		MakeNetworkLink("c2", "COMPUTE", "c2", "COMPUTE", 200, 5),

		MakeNetworkLink("c3", "COMPUTE", "n1", "NETWORK", 100, 10),
		MakeNetworkLink("n1", "NETWORK", "c3", "COMPUTE", 200, 5),
		MakeNetworkLink("c3", "COMPUTE", "c3", "COMPUTE", 200, 5),

		MakeNetworkLink("c4", "COMPUTE", "n2", "NETWORK", 100, 10),
		MakeNetworkLink("n2", "NETWORK", "c4", "COMPUTE", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("c4", "COMPUTE", "c4", "COMPUTE", 200, 5),

		MakeNetworkLink("c6", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c6", "COMPUTE", 200, 5),
		MakeNetworkLink("n3", "NETWORK", "n3", "NETWORK", 200, 5),
		MakeNetworkLink("c6", "COMPUTE", "c6", "COMPUTE", 200, 5),

		MakeNetworkLink("c7", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c7", "COMPUTE", 200, 5),
		MakeNetworkLink("c7", "COMPUTE", "c7", "COMPUTE", 200, 5),

		MakeNetworkLink("c5", "COMPUTE", "n3", "NETWORK", 100, 10),
		MakeNetworkLink("n3", "NETWORK", "c5", "COMPUTE", 200, 5),
		MakeNetworkLink("c5", "COMPUTE", "c5", "COMPUTE", 200, 5),

		MakeNetworkLink("n1", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n1", "NETWORK", 200, 5),
		MakeNetworkLink("n3", "NETWORK", "n2", "NETWORK", 200, 5),
		MakeNetworkLink("n2", "NETWORK", "n3", "NETWORK", 200, 5),
	}
	lm := MakeNetworkLinksMap(links)
	err := router.Initialize(lm)
	if err != nil {
		t.Errorf("unexpected error in initialization: %v", err)
	}

	paths, err := router.GetNetworkPaths()
	if err != nil {
		t.Errorf("unexpected error getting network paths: %v", err)
	}

	// There are seven compute nodes and there is one path between each
	// pair of compute nodes. Thus there need to be 49 paths.
	if len(paths) != 49 {
		t.Errorf("expected 49 paths but got %d: %+v", len(paths), paths)

	}
}
