// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

const (
	// DefaultBandwidth for network links.
	DefaultBandwidth = 1000000000
	// DefaultLatency for network links.
	DefaultLatency = 100000
)

// ValidateNetworkTopology examines a topology spec and verifies that all the nodes it declares
// to be 'compute' actually have a counterpart in the list of network endpoints.
// Also verifies that all links in the topology refer to nodes that exist.
func ValidateNetworkTopology(topology crd.NetworkTopologySpec, nodesMap map[string]crd.NetworkEndpoint) error {
	topologyNodesMap := make(map[string]bool)
	for _, tn := range topology.Nodes {
		topologyNodesMap[tn.Name] = true
		if tn.Type == crd.COMPUTE_NODE {
			_, exists := nodesMap[tn.Name]
			if !exists {
				return fmt.Errorf("topology declares a compute node named %s but there is no matching node in the cluster", tn.Name)
			}
			// TODO: also verify the ip address or macaddress if specified
		}
	}
	// TODO: for a logical network, also verify all the nodes it specifies have a physical counterpart?
	// or simply ignore node declarations in logical networks

	topologyLinksMap := make(map[string]bool)
	for _, l := range topology.Links {
		if !topologyNodesMap[l.Source] {
			return fmt.Errorf("topology contains link starting at a node %s which does not exist", l.Source)
		}
		if !topologyNodesMap[l.Target] {
			return fmt.Errorf("topology contains link ending at a node %s which does not exist", l.Target)
		}
		linkName := MakeNetworkLinkName(l.Source, l.Target)
		topologyLinksMap[linkName] = true
	}

	for _, p := range topology.Paths {
		for i, n := range p.Nodes {
			if !topologyNodesMap[n] {
				return fmt.Errorf("topology contains a path going via a node %s which does not exist", n)
			}
			if i > 0 {
				link := fmt.Sprintf("%s_%s", p.Nodes[i-1], p.Nodes[i])
				if !topologyLinksMap[link] {
					return fmt.Errorf("topology contains a path going from node %s to node %s but there is no link for that", p.Nodes[i-1], p.Nodes[i])
				}
			}
		}
	}
	return nil
}

// NetworkLinksFromTopologySpec creates network link objects from a topology spec
// and returns them.
func NetworkLinksFromTopologySpec(spec crd.NetworkTopologySpec, ni string, namespace string) map[string]crd.NetworkLink {
	lns := make(map[string]crd.LinkNode)
	for _, n := range spec.Nodes {
		lns[n.Name] = crd.LinkNode{Name: n.Name}
	}
	links := make(map[string]crd.NetworkLink)

	for _, l := range spec.Links {
		var bw int64
		var latency int64
		var err error
		bw, err = GetQuantityValue(l.Capabilities.BandWidthBits)
		if err != nil {
			fmt.Printf("failed to convert bandwidth spec to number: %v\n", err)
		}
		if bw == int64(0) {
			bw = DefaultBandwidth
		}
		latency, err = GetQuantityNanoValue(l.Capabilities.LatencyNanos)
		if err != nil {
			fmt.Printf("failed to convert latency spec to number: %v\n", err)
		}
		if latency <= int64(0) {
			latency = DefaultLatency
		}

		if l.Capabilities.OtherCapabilities != nil {
			for capa := range l.Capabilities.OtherCapabilities {
				fmt.Printf("the link capability %s is not supported yet\n", capa)
			}
		}

		newName := MakeNetworkLinkName(lns[l.Source].Name, lns[l.Target].Name)
		links[newName] = crd.NetworkLink{
			ObjectMeta: metav1.ObjectMeta{
				Name:      newName,
				Namespace: namespace,
			},
			Spec: crd.NetworkLinkSpec{
				LinkFrom:      lns[l.Source],
				LinkTo:        lns[l.Target],
				BandWidthBits: bw,
				LatencyNanos:  latency,
				PhysicalBase:  spec.PhysicalBase,
			},
		}
	}
	return links
}

// NetworkPathsFromTopologyPaths creates network path objects from topology path specs.
func NetworkPathsFromTopologyPaths(specs []crd.TopologyPathSpec,
	links map[string]crd.NetworkLink, namespace string) (
	map[string]crd.NetworkPath, error) {
	linkNodeMap := make(map[string]crd.LinkNode)
	for _, l := range links {
		linkNodeMap[l.Spec.LinkFrom.Name] = l.Spec.LinkFrom
		linkNodeMap[l.Spec.LinkTo.Name] = l.Spec.LinkTo
	}
	ret := make(map[string]crd.NetworkPath)

	for _, p := range specs {
		path, err := networkPathFromTopologyPathSpec(p, linkNodeMap, namespace)
		if err != nil {
			return nil, err
		}
		ret[path.Name] = *path
	}
	return ret, nil
}

func networkPathFromTopologyPathSpec(spec crd.TopologyPathSpec, links map[string]crd.LinkNode, namespace string) (*crd.NetworkPath, error) {
	if len(spec.Nodes) < 2 {
		return nil, fmt.Errorf("need at least two nodes on a path spec")
	}
	startNode := spec.Nodes[0]
	endNode := spec.Nodes[len(spec.Nodes)-1]

	startLinkNode, exists := links[startNode]
	if !exists {
		return nil, fmt.Errorf("topology path spec uses nonexistent start node %s", startNode)
	}
	endLinkNode, exists := links[endNode]
	if !exists {
		return nil, fmt.Errorf("topology path spec uses nonexistent end node %s", endNode)
	}

	pathLinks := make([]string, len(spec.Nodes)-1)
	currentNode := startLinkNode
	for i := 1; i < len(spec.Nodes)-1; i++ {
		targetNode := spec.Nodes[i]
		targetLinkNode, exists := links[targetNode]
		if !exists {
			return nil, fmt.Errorf("topology path spec contains nonexistent node %s", targetNode)
		}
		// TODO: verify that the link exists.
		pathLinks = append(pathLinks, MakeNetworkLinkName(currentNode.Name, targetNode))
		currentNode = targetLinkNode
	}

	pathSpec := crd.NetworkPathSpec{
		Start: startLinkNode,
		End:   endLinkNode,
		Links: pathLinks,
	}

	return NetworkPathFromSpec(pathSpec, namespace), nil
}

// NetworkPathFromSpec creates a network path object from a path spec.
func NetworkPathFromSpec(pathSpec crd.NetworkPathSpec, namespace string) *crd.NetworkPath {
	return &crd.NetworkPath{
		ObjectMeta: metav1.ObjectMeta{
			Name:      MakeNetworkPathName(pathSpec),
			Namespace: namespace,
			Labels: map[string]string{
				"path-start": pathSpec.Start.Name,
				"path-end":   pathSpec.End.Name,
			},
		},
		Spec: pathSpec,
	}
}
