// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package lib

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

func MakeTopology(name string, niTag string, nodeCount int) *crd.NetworkTopology {
	spec := makeTopologySpec(niTag, nodeCount)
	namespace := NamespaceForNi(niTag)
	return &crd.NetworkTopology{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: spec,
	}
}

func MakeNetworkEndpointsForTopology(topology crd.NetworkTopologySpec) []crd.NetworkEndpoint {
	ret := make([]crd.NetworkEndpoint, 0, len(topology.Nodes))
	for _, n := range topology.Nodes {
		if n.Type != crd.COMPUTE_NODE {
			continue
		}
		ret = append(ret, crd.NetworkEndpoint{
			ObjectMeta: metav1.ObjectMeta{Name: n.Name},
			Spec: crd.NetworkEndpointSpec{
				NodeName: n.Name,
				NetworkInterfaces: []crd.NetworkEndpointNetworkInterface{
					{
						Name: "eth0",
					},
				},
			},
		})
	}
	return ret
}

func makeTopologySpec(niTag string, nodeCount int) crd.NetworkTopologySpec {
	nodes := makeTopologyNodeSpecs(nodeCount)
	links := makeTopologyLinkSpecs(nodes)
	return crd.NetworkTopologySpec{
		NetworkImplementation: niTag,
		Nodes:                 nodes,
		Links:                 links,
	}
}

func makeTopologyNodeSpecs(count int) []crd.TopologyNodeSpec {
	ret := make([]crd.TopologyNodeSpec, count)
	for i := 0; i < count; i++ {
		ret[i] = crd.TopologyNodeSpec{
			Name: fmt.Sprintf("node-%d", i),
			Type: crd.COMPUTE_NODE,
		}
	}
	return ret
}

func makeTopologyLinkSpecs(nodes []crd.TopologyNodeSpec) []crd.TopologyLinkSpec {
	ret := make([]crd.TopologyLinkSpec, 0, 2*len(nodes))
	doneMap := make(map[string]bool)
	for i, n := range nodes {

		pred := (i + len(nodes) - 1) % len(nodes)
		succ := (i + 1) % len(nodes)
		key := fmt.Sprintf("node-%d-%s", pred, n.Name)
		if !doneMap[key] {
			ret = append(ret, crd.TopologyLinkSpec{
				Target: n.Name,
				Source: fmt.Sprintf("node-%d", pred),
			})
			doneMap[key] = true
		}
		key = fmt.Sprintf("%s-node-%d", n.Name, succ)
		if !doneMap[key] {
			ret = append(ret, crd.TopologyLinkSpec{
				Source: n.Name,
				Target: fmt.Sprintf("node-%d", succ),
			})
			doneMap[key] = true
		}
	}
	return ret
}
