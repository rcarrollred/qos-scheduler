// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"
	"github.com/go-logr/logr"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
	"strings"
	"sync"
)

// Map target node to paths for getting there.
type pathMap map[string][]*crd.NetworkPathSpec

// Map source node to the paths that start from it.
type routesMap map[string]*pathMap

// Router maintains paths based on network link information.
// All the links in a router should use the same network label.
type Router struct {
	routes             routesMap
	networklinks       map[string](*crd.NetworkLink)
	linknodes          map[string](*crd.LinkNode)
	linksAdded         bool
	linksDeleted       bool
	initializationLock sync.Mutex
	initialized        bool
	Log                logr.Logger
}

// RouterI exists so that I can implement a fake router for tests.
type RouterI interface {
	ValidateNetworkPaths() error
	DeleteNetworkLink(link crd.NetworkLink) error
	AddNetworkLink(link crd.NetworkLink) error
	Initialize(links map[string]crd.NetworkLink) error
	IsInitialized() bool
	GetNetworkPaths() ([]crd.NetworkPathSpec, error)
}

// NewRouter instantiates a router.
func NewRouter() *Router {
	return &Router{
		routes:       make(map[string]*pathMap),
		networklinks: make(map[string](*crd.NetworkLink)),
		linksAdded:   false,
		linksDeleted: false,
		Log:          ctrl.Log.WithName("network router"),
	}
}

func (r *Router) dumpNetworkPaths() {
	for source, routesmap := range r.routes {
		fmt.Printf("paths from %s: \n", source)
		for target, pm := range *routesmap {
			fmt.Printf("to %s: \n", target)
			for _, p := range pm {
				fmt.Printf("path %q\n", p)
			}
		}
	}
}

// ValidateNetworkPaths verifies that the current collection of network
// paths is valid.
// Reasons for paths being invalid include
//   - the router has not been initialized
//   - not every path has a reverse; **** HM: this check is obsolete and to be removed in next versions ****
//   - there is a path where consecutive links do not match up (link i+1 does not
//     start from the endpoint of link i)
func (r *Router) ValidateNetworkPaths() error {
	if !r.initialized {
		return fmt.Errorf("setup error: router has not been initialized")
	}
	for source, pathmap := range r.routes { // paths is a pathMap
		for target, paths := range *pathmap { // path is a list of crd.NetworkPathSpec
			// Does every path have a valid link list?
			for _, p := range paths {
				for i, l := range p.Links {
					if i == 0 {
						continue
					}
					link, exists := r.networklinks[l]
					if !exists {
						return fmt.Errorf("link %s on path %v is not known to this router",
							l, *p)
					}
					prevlink := r.networklinks[p.Links[i-1]]
					if link.Spec.LinkFrom.Name != prevlink.Spec.LinkTo.Name {
						return fmt.Errorf("links %s and %s on path %v do not match up",
							prevlink.Name, l, *p)
					}
				}
			}

			// Does the reverse of every path exist?
			reversePathMap, exists := r.routes[target]
			if !exists {
				return fmt.Errorf("expected path map to %s to exist", target)
			}
			reversePaths := (*reversePathMap)[source]
			if reversePaths == nil {
				return fmt.Errorf("expected reverse path from %s to %s to exist", target, source)
			}
			if len(paths) != len(reversePaths) {
				r.dumpNetworkPaths()
				return fmt.Errorf("expected same number of paths from %s to %s as in the other direction", source, target)
			}
		}
	}
	return nil
}

// This is basically breadth-first search because it finds all non-repeating paths between any
// pair of nodes. It's not fast. The number of outputs is up to n! when n is the number of
// nodes, if the graph is fully connected.
func (r *Router) buildNetworkPaths() {

	if len(r.networklinks) == 0 {
		r.Log.Info("setup: router has no network links yet")
		return
	}

	if !r.linksAdded && !r.linksDeleted {
		r.Log.Info("no links have been added or deleted since last call, could skip rebuilding paths")
		// return // TODO: re-enable the early return here
	}

	// Link deletion will mark the routes map as stale. This piece of code will then drop
	// the old routes map before recomputing the routes. This is easier than removing all affected
	// routes from the map, and it is done lazily here, only when a new routes map is requested.
	if r.linksDeleted {
		r.initializationLock.Lock()
		defer r.initializationLock.Unlock()
		r.routes = make(map[string]*pathMap)
		r.linksDeleted = false
	}

	r.linksAdded = false

	// Separate the network links in our collection into loopback and non-loopback links.
	nonLoopbackLinks := make([]*crd.NetworkLink, 0, len(r.networklinks))
	loopbackLinks := make([]*crd.NetworkLink, 0, len(r.networklinks))
	for _, link := range r.networklinks {
		if link.Spec.LinkFrom.Name != link.Spec.LinkTo.Name {
			nonLoopbackLinks = append(nonLoopbackLinks, link)
			newRoute := &crd.NetworkPathSpec{
				Start: link.Spec.LinkFrom,
				End:   link.Spec.LinkTo,
				Links: []string{link.Name},
			}
			r.addRoute(newRoute)
		} else {
			loopbackLinks = append(loopbackLinks, link)
		}
	}

	// Create a dictionary of all paths that already exist
	pathDict := map[string]crd.NetworkPathSpec{}
	for _, pathsFrom := range r.routes {
		for _, pathsTo := range *pathsFrom {
			for _, path := range pathsTo {
				pathDict[MakeNetworkPathName(*path)] = *path
			}
		}
	}

	// Go over all links repeatedly and see if any of them can be used to extend any
	// of the existing paths. Stop when there are no path extensions left.
	// Only loop-free paths will be created, so the procedure will terminate.
	var foundPathsToExtend bool
	for {
		foundPathsToExtend = false
		for _, linkToAdd := range nonLoopbackLinks {
			from := linkToAdd.Spec.LinkFrom
			to := linkToAdd.Spec.LinkTo

			// Look for existing paths that end in 'from' and that don't yet contain 'to'
			pathsToExtend := make(map[string][](*crd.NetworkPathSpec))
			for source, paths := range r.routes { // paths is a pathMap
				if (*paths)[from.Name] != nil { // paths[from] is a list of NetworkPath that end at 'from'
					for _, path := range (*paths)[from.Name] {
						skipThis := false
						// Make sure to skip loopback paths so that the function is idempotent.
						if len(path.Links) == 1 {
							if path.Start.Name == path.End.Name {
								skipThis = true
								break
							}
						}
						// We're only looking for paths without loops, so none can
						// be longer than the number of nodes.
						if len(path.Links) >= len(r.linknodes) {
							skipThis = true
							break
						}
						// Is the node called 'to' already on any of the links in this path?
						for _, link := range path.Links {
							// look up link in our directory
							l, exists := r.networklinks[link]
							if !exists {
								skipThis = true
								break
							}
							if l.Spec.LinkFrom.Name == to.Name || l.Spec.LinkTo.Name == to.Name {
								skipThis = true
								break
							}
						}
						if skipThis {
							continue
						}
						// Check if the new path already exists
						candidatePath := appendLinkToPath(*path, *linkToAdd)
						candidateName := MakeNetworkPathName(candidatePath)
						_, exists := pathDict[candidateName]
						if !exists {
							if pathsToExtend[source] == nil {
								pathsToExtend[source] = []*crd.NetworkPathSpec{path}
							} else {
								pathsToExtend[source] = append(pathsToExtend[source], path)
							}
						}
					}
				}
			}
			if len(pathsToExtend) > 0 {
				foundPathsToExtend = true
			}
			for _, pathToExtend := range pathsToExtend {
				for _, p := range pathToExtend {
					newPath := appendLinkToPath(*p, *linkToAdd)
					pathDict[MakeNetworkPathName(newPath)] = newPath
					r.addRoute(&newPath)
				}
			}
		}
		if !foundPathsToExtend {
			break
		}
	}

	// Now add the loopbacklinks as paths.
	for _, link := range loopbackLinks {
		newRoute := &crd.NetworkPathSpec{
			Start: link.Spec.LinkFrom,
			End:   link.Spec.LinkTo,
			Links: []string{link.Name},
		}
		r.addRoute(newRoute)
	}
}

// GetNetworkPaths returns all paths whose endpoints are compute nodes.
// If links have been added to or removed from the router since
// the last time routes were computed, routes will be recomputed.
func (r *Router) GetNetworkPaths() ([]crd.NetworkPathSpec, error) {
	if !r.IsInitialized() {
		return nil, fmt.Errorf("cannot get network paths before initialization is complete")
	}
	// This is expensive but idempotent.
	r.buildNetworkPaths()
	if err := r.ValidateNetworkPaths(); err != nil {
		// Make sure we rebuild the paths the next time.
		r.linksAdded = true
		return nil, err
	}
	ret := make([]crd.NetworkPathSpec, 0, 2*len(r.routes))
	for source, paths := range r.routes { // paths is a pathMap
		sourcelink, exists := r.linknodes[source]
		if !exists {
			return ret, fmt.Errorf("missing link node named %s", source)
		}
		if sourcelink.Type != crd.COMPUTE_NODE {
			continue
		}
		for target, pathlist := range *paths { // pathlist is a list of NetworkPath
			targetlink, exists := r.linknodes[target]
			if !exists {
				return ret, fmt.Errorf("missing link node named %s", target)
			}
			if targetlink.Type != crd.COMPUTE_NODE {
				continue
			}
			for _, p := range pathlist {
				ret = append(ret, *p)
			}
		}
	}
	return ret, nil
}

// IsInitialized tells you whether this router has been initialized with
// a set of network links yet.
func (r *Router) IsInitialized() bool {
	return r.initialized
}

// Initialize should be called on startup. It takes a map of network link name
// to network link and builds paths from them.
func (r *Router) Initialize(links map[string]crd.NetworkLink) error {
	r.initializationLock.Lock()
	defer r.initializationLock.Unlock()
	r.routes = make(map[string]*pathMap)
	r.networklinks = make(map[string](*crd.NetworkLink))
	r.linknodes = make(map[string](*crd.LinkNode))
	r.linksAdded = true
	for _, link := range links {
		r.insertLink(link)
	}
	r.buildNetworkPaths()
	r.initialized = true
	return nil
}

// AddNetworkLink adds a network link to the router.
// This does not trigger a recomputation of paths but if the link
// was new, a flag is set on the router indicating that new paths
// may need to be added.
func (r *Router) AddNetworkLink(link crd.NetworkLink) error {
	if !r.IsInitialized() {
		return fmt.Errorf("router has not been initialized")
	}
	r.insertLink(link)
	return nil
}

// DeleteNetworkLink deletes a network link from the router.
// Does not trigger a recomputation of paths but if the
// link exists and gets removed, a flag is set on the router
// indicating that paths need to be updated.
func (r *Router) DeleteNetworkLink(link crd.NetworkLink) error {
	if !r.IsInitialized() {
		return fmt.Errorf("router has not been initialized")
	}
	r.deleteLink(link)
	return nil
}

// addRoute adds a route to the routing table.
// Returns true if the route was added, false if it already existed.
func (r *Router) addRoute(newRoute *crd.NetworkPathSpec) bool {
	if r.routes[newRoute.Start.Name] == nil {
		pm := pathMap(map[string][]*crd.NetworkPathSpec{
			newRoute.End.Name: {newRoute},
		})
		r.routes[newRoute.Start.Name] = &pm
		return true
	}
	if (*r.routes[newRoute.Start.Name])[newRoute.End.Name] == nil {
		(*r.routes[newRoute.Start.Name])[newRoute.End.Name] = []*crd.NetworkPathSpec{newRoute}
		return true
	}
	key := strings.Join(newRoute.Links, ":")
	for _, existing := range (*r.routes[newRoute.Start.Name])[newRoute.End.Name] {
		desc := strings.Join(existing.Links, ":")
		if key == desc {
			return false
		}
	}
	(*r.routes[newRoute.Start.Name])[newRoute.End.Name] = append((*r.routes[newRoute.Start.Name])[newRoute.End.Name], newRoute)
	return true
}

// Add a link to the router. Returns true if the link
// already existed, false otherwise.
func (r *Router) insertLink(link crd.NetworkLink) bool {
	_, exists := r.networklinks[link.Name]
	if exists {
		return true
	}
	r.networklinks[link.Name] = &link
	_, exists = r.linknodes[link.Spec.LinkFrom.Name]
	if !exists {
		r.linknodes[link.Spec.LinkFrom.Name] = &link.Spec.LinkFrom
	}
	_, exists = r.linknodes[link.Spec.LinkTo.Name]
	if !exists {
		r.linknodes[link.Spec.LinkTo.Name] = &link.Spec.LinkTo
	}
	r.linksAdded = true
	return false
}

// Delete a link from the router. Returns true if the link
// existed and was removed, false otherwise.
func (r *Router) deleteLink(link crd.NetworkLink) bool {
	_, exists := r.networklinks[link.Name]
	if !exists {
		return false
	}
	delete(r.networklinks, link.Name)
	// there is no need to delete from linknodes, since for all we know,
	// those still exist and moreover, they do not matter for the paths.
	r.linksDeleted = true
	return true
}
