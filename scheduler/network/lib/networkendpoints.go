// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// MakeNetworkEndpointsMap returns a map of network endpoint name to network endpoint.
func MakeNetworkEndpointsMap(cn []crd.NetworkEndpoint) map[string]crd.NetworkEndpoint {
	ret := make(map[string]crd.NetworkEndpoint)
	for _, n := range cn {
		ret[n.Name] = n
	}
	return ret
}

// DeleteNetworkImplementation removes entries for a network implementation from one network endpoint.
func DeleteNetworkImplementation(cn *crd.NetworkEndpoint, networkImplementation string) {
	delete(cn.Spec.MappedNetworkInterfaces, networkImplementation)
}

func patchNetworkEndpointSpec(oldNode crd.NetworkEndpointSpec, newNode crd.TopologyNodeSpec, networkImplementation string) (crd.NetworkEndpointSpec, error) {
	patch := oldNode
	if patch.MappedNetworkInterfaces == nil {
		patch.MappedNetworkInterfaces = make(map[string]string)
	}
	if newNode.MACAddress == "" && newNode.IPAddress == "" {
		return patch, nil
	}
	nwifName, exists := patch.MappedNetworkInterfaces[networkImplementation]
	// If the interface is already mapped to this network implementation,
	// only accept a new mac or ip address if it wasn't already set.
	if exists {
		var nwif *crd.NetworkEndpointNetworkInterface
		for _, nw := range patch.NetworkInterfaces {
			if nw.Name == nwifName {
				nwif = &nw
				break
			}
		}
		if nwif == nil {
			return patch, fmt.Errorf("node %s specifies the network interface %s for network %s but that network interface does not exist", patch.NodeName, nwifName, networkImplementation)
		}
		if newNode.MACAddress != "" {
			if nwif.MacAddress != newNode.MACAddress {
				return patch, fmt.Errorf("topology update specifies that mac address of network interface %s on node %s should change from %s to %s", nwifName, patch.NodeName, nwif.MacAddress, newNode.MACAddress)
			} else if nwif.MacAddress == "" {
				nwif.MacAddress = newNode.MACAddress
			}
		}
		if newNode.IPAddress != "" {
			if nwif.IPv4Address != newNode.IPAddress {
				return patch, fmt.Errorf("topology update specifies that ip address of network interface %s on node %s should change from %s to %s", nwifName, patch.NodeName, nwif.IPv4Address, newNode.IPAddress)
			} else if nwif.IPv4Address == "" {
				nwif.IPv4Address = newNode.IPAddress
			}
		}
		return patch, nil
	}
	// There is no mapped network interface for networkImplementation yet, create one
	var nwif *crd.NetworkEndpointNetworkInterface
	for _, nw := range patch.NetworkInterfaces {
		if newNode.MACAddress != "" && newNode.MACAddress == nw.MacAddress {
			nwif = &nw
			break
		}
		if nwif == nil && newNode.IPAddress != "" && newNode.IPAddress == nw.IPv4Address {
			nwif = &nw
			break
		}
	}
	if nwif == nil {
		return patch, fmt.Errorf("failed to find a network interface matching %v on node %s", newNode, patch.NodeName)
	}
	patch.MappedNetworkInterfaces[networkImplementation] = nwif.Name
	return patch, nil
}

// PatchNetworkEndpoints returns a list of network endpoints that should be updated based on the information
// in newNodes.
// Updates can only be made to the MappedNetworkInterfaces field,
// and only to those map entries whose key matches networkImplementation.
// Moreover, fields like MacAddress or IPAddress can only be set if they are
// currently empty.
// An error is returned if updates would not be permissible.
func PatchNetworkEndpoints(oldNodes map[string]crd.NetworkEndpoint, newNodes []crd.TopologyNodeSpec, networkImplementation string) ([]crd.NetworkEndpoint, error) {
	patch := make([]crd.NetworkEndpoint, 0, len(newNodes))
	for _, newNode := range newNodes {
		if newNode.Type != crd.COMPUTE_NODE {
			continue
		}
		matches := findMatchingNetworkEndpoints(oldNodes, newNode, networkImplementation)
		if len(matches) > 1 {
			return nil, fmt.Errorf("found multiple network endpoint matches for %v: %v", newNode, matches)
		}
		if len(matches) == 0 {
			continue
		}

		patchme, err := patchNetworkEndpointSpec(matches[0].Spec, newNode, networkImplementation)
		if err != nil {
			return nil, err
		}
		matches[0].Spec = patchme
		patch = append(patch, matches[0])
	}

	return patch, nil
}

// Return network endpoints matching n.
// The match is made based on the Kubernetes name of the network endpoint.
// The expected number of endpoints returned is 0 or 1. More than 1 matching network endpoint means that
// the topology node cannot be matched uniquely to a network endpoint, and that usually indicates
// an error in the topology.
// 0 nodes being returned is normal if n does not correspond to a Kubernetes node.
func findMatchingNetworkEndpoints(cn map[string]crd.NetworkEndpoint, n crd.TopologyNodeSpec, networkImplementation string) []crd.NetworkEndpoint {
	ret := make([]crd.NetworkEndpoint, 0, 1)
	for _, c := range cn {
		if c.Spec.NodeName == n.Name {
			ret = append(ret, c)
			break
		}
	}
	return ret
}
