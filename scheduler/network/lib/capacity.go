// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// SufficientNetworkCapacityOnPath returns true iff the path's current network capacity is enough for the requests
// of the channel.
// This takes the links that the path can use as the second argument so that this
// method can implement just the logic without requiring a Kubernetes client.
// This function does not verify that the path's start and end points match the channel,
// it only verifies capacities along the path.
func SufficientNetworkCapacityOnPath(path crd.NetworkPath, links []crd.NetworkLink, channel crd.Channel) bool {
	// For each link in the path
	for _, l := range path.Spec.Links {
		// Look up the link in the list
		found := false
		for _, lnk := range links {
			if l == lnk.Name {
				found = true
				// Compare the channel's request to the link's current capacity.
				var latencyNanos int64
				var bandwidthAvailableBitsPerSecond int64
				if lnk.Status.LatencyNanos > 0 {
					latencyNanos = lnk.Status.LatencyNanos
				} else {
					latencyNanos = lnk.Spec.LatencyNanos
				}
				// TODO: there needs to be a way to detect whether this value is 0 because
				// the status has never been set or because the bandwidth is exhausted.
				// Maybe set it to -1 to indicate no more bandwidth.
				if lnk.Status.BandWidthAvailable != 0 {
					bandwidthAvailableBitsPerSecond = lnk.Status.BandWidthAvailable
				} else {
					bandwidthAvailableBitsPerSecond = lnk.Spec.BandWidthBits
				}

				if channel.Spec.MaxDelayNanos > 0 && int64(channel.Spec.MaxDelayNanos) < latencyNanos {
					return false
				}
				if channel.Spec.MinBandwidthBits > 0 && int64(channel.Spec.MinBandwidthBits) > bandwidthAvailableBitsPerSecond {
					return false
				}
				// Not checking framesize etc here because there is not currently a status or spec field
				// for those on the networklinks.
			}
		}
		// If the link was not found, return false.
		if !found {
			return false
		}
	}
	return true
}
