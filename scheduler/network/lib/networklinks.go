// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// NetworkLinkClass returns the network implementation class for a network link.
func NetworkLinkClass(nwlink crd.NetworkLink) NetworkImplementationClass {
	ni, err := NiFromNamespace(nwlink.Namespace)
	if err != nil {
		return BasicNetwork
	}
	return NetworkImplementationClass(ni)
}

// NetworkPathClass returns the network implementation class for a network path.
func NetworkPathClass(nwpath crd.NetworkPath) NetworkImplementationClass {
	ni, err := NiFromNamespace(nwpath.Namespace)
	if err != nil {
		return BasicNetwork
	}
	return NetworkImplementationClass(ni)
}

// MakeNetworkLinkName returns the name of a network link based on its endpoints.
// The network implementation is not included in the name because
// it implied by the namespace.
func MakeNetworkLinkName(source string, target string) string {
	return fmt.Sprintf("%s-%s", conformantString(source), conformantString(target))
}

// MakeNetworkLinksMap returns a map of network link name to link resource.
func MakeNetworkLinksMap(links []crd.NetworkLink) map[string]crd.NetworkLink {
	ret := make(map[string]crd.NetworkLink)
	for _, l := range links {
		ret[l.Name] = l
	}
	return ret
}

// PatchNetworkLinks figures out how to apply a topology update to network links.
// oldLinks are the network links currently in existence. newLinks come from
// a topology update. Return lists of links to create, update, delete in order
// for the topology to match newLinks.
func PatchNetworkLinks(oldLinks map[string]crd.NetworkLink, newLinks map[string]crd.NetworkLink) (
	[]crd.NetworkLink, []crd.NetworkLink, []crd.NetworkLink) {
	linksToCreate := make([]crd.NetworkLink, 0, len(newLinks))
	linksToUpdate := make([]crd.NetworkLink, 0, len(newLinks))
	linksToDelete := make([]crd.NetworkLink, 0, len(oldLinks))

	for newName, newLink := range newLinks {
		_, found := oldLinks[newName]
		if !found {
			linksToCreate = append(linksToCreate, newLink)
			continue
		}
		linksToUpdate = append(linksToUpdate, newLink)
	}
	for oldName, oldLink := range oldLinks {
		_, found := newLinks[oldName]
		if !found {
			linksToDelete = append(linksToDelete, oldLink)
		}
	}
	return linksToCreate, linksToUpdate, linksToDelete
}

// CapabilitiesCanBeSatisfied returns true if the physical link's capabilities are sufficient for those of
// the logical link.
// This only checks the spec capabilities (the total resources), not the currently
// available capabilities.
func CapabilitiesCanBeSatisfied(logical crd.NetworkLink, physical crd.NetworkLink) bool {
	if logical.Spec.BandWidthBits > physical.Spec.BandWidthBits {
		return false
	}
	if logical.Spec.LatencyNanos <= 0 {
		return true
	}
	return logical.Spec.LatencyNanos >= physical.Spec.LatencyNanos
}
