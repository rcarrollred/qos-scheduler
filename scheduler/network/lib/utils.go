// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package lib contains code that can be reused by all network operators.
package lib

import (
	"fmt"
	"strings"

	"k8s.io/apimachinery/pkg/api/resource"
)

// NetworkImplementationClass identifies a network.
// All network resources modeled inside a cluster (links,
// paths, channels) are labelled with a network
// implementation class.
// You can define your own network implementation classes
// by picking a name and setting up network links and
// paths for your class. A network operator usually
// automates this.
type NetworkImplementationClass string

const (
	// Predefined network implementation class for the network that
	// connects all Kubernetes nodes in a cluster by default.
	// This network implementation class always exists.
	K8sNetwork NetworkImplementationClass = "K8S"
	// This network class is only for links from a node to itself.
	// Those links exist only in the Loopback network but they are valid
	// for all service classes.
	Loopback NetworkImplementationClass = "LOOPBACK"
	// Network implementation class for links or channels that are missing
	// an implementation class.
	BasicNetwork NetworkImplementationClass = "BASIC"
)

// NamespaceForNi returns the namespace that will have been generated for this ni tag.
func NamespaceForNi(ni string) string {
	return strings.ToLower(fmt.Sprintf("network-%s-namespace", ni))
}

// NiFromNamespace retrieves the network implementation from a namespace if possible
// and returns it.
func NiFromNamespace(namespace string) (string, error) {
	parts := strings.Split(namespace, "-")
	if len(parts) != 3 || parts[0] != "network" || parts[2] != "namespace" {
		return "", fmt.Errorf("string %s does not conform to network namespace naming convention", namespace)
	}
	return strings.ToUpper(parts[1]), nil
}

// Return a version of input that is probably ok to use in a Kubernetes name
// (all lowercase, no underscores).
func conformantString(input string) string {
	return strings.Replace(strings.ToLower(input), "_", "-", -1)
}

func GetQuantityValue(quantitySpec string) (int64, error) {
	if quantitySpec == "" {
		return int64(0), nil
	}
	q, err := resource.ParseQuantity(quantitySpec)
	if err != nil {
		return int64(0), err
	}
	return q.Value(), nil
}

func GetQuantityNanoValue(quantitySpec string) (int64, error) {
	if quantitySpec == "" {
		return int64(0), nil
	}
	q, err := resource.ParseQuantity(quantitySpec)
	if err != nil {
		return int64(0), err
	}
	scaled := q.ScaledValue(resource.Scale(int32(resource.Nano)))
	return scaled, nil
}
