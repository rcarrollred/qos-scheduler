// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package lib

import (
	"fmt"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"testing"
)

func TestNetworkLinksFromTopologySpec(t *testing.T) {
	topo := MakeTopology("topo", "testnet", 2)
	links := NetworkLinksFromTopologySpec(topo.Spec, "testnet", "default")
	if len(links) != len(topo.Spec.Links) {
		t.Errorf("should have got %d links out of topology but got %d",
			len(topo.Spec.Links), len(links))
		fmt.Printf("topology: %v\n", topo.Spec.Links)
		fmt.Printf("nw links: %v\n", links)
	}
	firstTopologyLink := topo.Spec.Links[0]
	expectedName := MakeNetworkLinkName(firstTopologyLink.Source, firstTopologyLink.Target)
	nwlink, exists := links[expectedName]
	if !exists {
		t.Errorf("expected to find a link with name %s but the generated links are %v", expectedName, links)
	}
	if nwlink.Spec.BandWidthBits != DefaultBandwidth {
		t.Errorf("expected bandwidth to be %d but it is %d", DefaultBandwidth, nwlink.Spec.BandWidthBits)
	}
	if nwlink.Spec.LatencyNanos != DefaultLatency {
		t.Errorf("expected latency to be %d but it is %d", DefaultLatency, nwlink.Spec.LatencyNanos)
	}
}

func TestNetworkPathsFromTopologyPaths(t *testing.T) {
	topo := MakeTopology("topo", "testnet", 3)
	links := NetworkLinksFromTopologySpec(topo.Spec, "testnet", "default")
	topoPaths := []crd.TopologyPathSpec{
		{
			Nodes: []string{"node-1", "node-2"},
		},
		{
			Nodes: []string{"node-2", "node-1"},
		},
	}
	paths, err := NetworkPathsFromTopologyPaths(topoPaths, links, "default")
	if err != nil {
		t.Errorf("unexpected error transforming paths: %v", err)
	}
	if len(paths) != len(topoPaths) {
		t.Errorf("expected to get %d paths but got %d", len(topoPaths), len(paths))
	}
}

type pathTestSpec struct {
	nodes       []string
	expectError bool
	description string
}

func TestNetworkPathFromTopologyPathSpec(t *testing.T) {
	topo := MakeTopology("topo", "testnet", 3)
	links := NetworkLinksFromTopologySpec(topo.Spec, "testnet", "default")
	linkNodeMap := make(map[string]crd.LinkNode)
	for _, l := range links {
		linkNodeMap[l.Spec.LinkFrom.Name] = l.Spec.LinkFrom
		linkNodeMap[l.Spec.LinkTo.Name] = l.Spec.LinkTo
	}

	pathTestSpecs := []pathTestSpec{
		{
			nodes:       []string{},
			expectError: true,
			description: "empty nodes list is not a valid path spec",
		},
		{
			nodes:       []string{"node-1"},
			expectError: true,
			description: "nodes list of length 1 is not a valid path spec",
		},
		{
			nodes:       []string{"node-1", "abc"},
			expectError: true,
			description: "all nodes on list must also be in the links map",
		},
		{
			nodes:       []string{"node-1", "node-2"},
			expectError: false,
			description: "a path list based on existing links is ok",
		},
	}

	for _, p := range pathTestSpecs {
		path, err := networkPathFromTopologyPathSpec(crd.TopologyPathSpec{Nodes: p.nodes}, linkNodeMap, "default")
		if err != nil && !p.expectError {
			t.Errorf("unexpected error %v for test that %s", err, p.description)
		} else if err == nil && p.expectError {
			t.Errorf("expected error for test that %s but got none", p.description)
		} else if path != nil {
			if len(p.nodes)-1 != len(path.Spec.Links) {
				t.Errorf("expected %d links on path spec but got %d in test that %s", len(p.nodes)-1, len(path.Spec.Links), p.description)
			}
		}
	}

}
