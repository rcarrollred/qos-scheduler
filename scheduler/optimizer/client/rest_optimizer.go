// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package optimizerclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	retryablehttp "github.com/hashicorp/go-retryablehttp"
	"net/http"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

// RestOptimizer calls the optimizer rest service.
type RestOptimizer struct {
	URL        string
	MaxRetries int
}

// OptimizationRequest combines a QoSModel with some metadata
// necessary for identifying the result config map correctly.
type OptimizationRequest struct {
	Model            *assignment.QoSModel `json:"model" protobuf:"bytes,1,name=model"`
	ApplicationGroup string               `json:"applicationGroup"`
	Namespace        string               `json:"namespace"`
}

// Cancel cancels a running optimization.
// RestOptimizer does not support cancellation.
func (o *RestOptimizer) Cancel(string, string) error {
	return nil
}

// Optimize performs the REST call.
// If it returns nil then it has called the optimizer and the result
// will appear in the status map eventually.
// If it returns an error then the error will say what went wrong.
// This can be caused by the optimizer being busy, or by transient network
// issues, both of which are retryable.
func (o *RestOptimizer) Optimize(model *assignment.QoSModel, applicationGroup string, namespace string) error {
	client := retryablehttp.NewClient()
	// Limit retries. Can also set the timeout and a backoff function
	// Outside of the rest service being down or temporarily unreachable, there is
	// no scenario where retries make sense here. A failure to find a plan will
	// not be communicated via the http status code.
	client.RetryMax = o.MaxRetries

	request := OptimizationRequest{
		Model:            model,
		ApplicationGroup: applicationGroup,
		Namespace:        namespace,
	}

	json, err := json.Marshal(request)
	if err != nil {
		fmt.Printf("failed to marshal request: %v\n", err)
		return err
	}

	req, _ := retryablehttp.NewRequest(http.MethodPost, o.URL, bytes.NewBuffer(json))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(req)
	// Do() returns an error if there was a network connectivity issue.
	// A 4xx or 5xx status code does not cause an error, have to check for those separately.
	// The optimizer service can return a 409 when there's a lock conflict.
	if err != nil {
		fmt.Printf("error response from rest request: %v\n", err)
		return err
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("optimizer call returned not-ok status %v\n", resp.StatusCode)
		return fmt.Errorf("optimizer call returned %s", http.StatusText(resp.StatusCode))
	}
	return nil
}
