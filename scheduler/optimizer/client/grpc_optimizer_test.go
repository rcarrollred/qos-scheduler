// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package optimizerclient

import (
	"context"
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"io"
	"siemens.com/qos-scheduler/scheduler/assignment"
	clientmock "siemens.com/qos-scheduler/scheduler/optimizer/client/mocks"
	"testing"
)

type mockClientFactory struct {
	ctrl *gomock.Controller
}

func (f *mockClientFactory) newSchedulerClient() (assignment.SchedulerClient, error) {
	return clientmock.NewMockSchedulerClient(f.ctrl), nil
}

func TestOptimizer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	optimizer := &GrpcOptimizer{
		numClients:    1,
		kubeClient:    nil,
		clients:       make([]*grpcClientWrapper, 1),
		clientFactory: &mockClientFactory{ctrl: ctrl},
	}

	err := optimizer.initializeConnections(context.TODO())
	if err != nil {
		t.Errorf("unexpected error initializing mock connections: %v", err)
	}
}

func TestSchedule(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	client := clientmock.NewMockSchedulerClient(ctrl)
	stream := clientmock.NewMockScheduler_ScheduleClient(ctrl)
	client.EXPECT().Schedule(gomock.Any(), gomock.Any()).Return(stream, nil)
	wrapper := &grpcClientWrapper{
		schedulerClient: client,
		model:           &assignment.QoSModel{},
	}
	err := wrapper.Schedule()
	if err != nil {
		t.Errorf("unexpected error calling Schedule(): %v", err)
	}
	if wrapper.stream != stream {
		t.Errorf("expected stream to be saved to client")
	}
	if wrapper.cancel == nil {
		t.Errorf("cancel should not be nil after successful Schedule() call")
	}
}

func TestOptimize(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	client := clientmock.NewMockSchedulerClient(ctrl)
	stream := clientmock.NewMockScheduler_ScheduleClient(ctrl)

	optimizer := &GrpcOptimizer{
		numClients:    1,
		kubeClient:    nil,
		clients:       []*grpcClientWrapper{&grpcClientWrapper{schedulerClient: client}},
		clientFactory: &mockClientFactory{ctrl: ctrl},
	}

	client.EXPECT().Schedule(gomock.Any(), gomock.Any()).Return(stream, nil)
	err := optimizer.Optimize(&assignment.QoSModel{}, "testgroup", "testns")
	if err != nil {
		t.Errorf("unexpected error calling Optimize(): %v", err)
	}

	err = optimizer.Optimize(&assignment.QoSModel{}, "testgroup", "testns")
	if err == nil {
		t.Errorf("should have got a DuplicateRequest error")
	}
	_, ok := err.(DuplicateRequest)
	if !ok {
		t.Errorf("error should have been DuplicateRequest but was %v", err)
	}

	err = optimizer.Optimize(&assignment.QoSModel{}, "testgroup2", "testns")
	if err == nil {
		t.Errorf("should have got a NoAvailableSlots error")
	}
	_, ok = err.(NoAvailableSlots)
	if !ok {
		t.Errorf("error should have been NoAvailableSlots but was %v", err)
	}
}

func TestCancel(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	client := clientmock.NewMockSchedulerClient(ctrl)
	stream := clientmock.NewMockScheduler_ScheduleClient(ctrl)
	wrapper := &grpcClientWrapper{schedulerClient: client}

	optimizer := &GrpcOptimizer{
		numClients:    1,
		kubeClient:    nil,
		clients:       []*grpcClientWrapper{wrapper},
		clientFactory: &mockClientFactory{ctrl: ctrl},
	}

	client.EXPECT().Schedule(gomock.Any(), gomock.Any()).Return(stream, nil)
	err := optimizer.Optimize(&assignment.QoSModel{}, "testgroup", "testns")
	if err != nil {
		t.Errorf("unexpected error calling Optimize(): %v", err)
	}

	err = optimizer.Cancel("othergroup", "testns")
	if err != nil {
		t.Errorf("canceling a nonexistent request should not return an error")
	}

	err = optimizer.Cancel("testgroup", "testns")
	if err != nil {
		t.Errorf("canceling an ongoing request should work")
	}

	// Now it should be ok to call optimize again for this group.
	client.EXPECT().Schedule(gomock.Any(), gomock.Any()).Return(stream, nil)
	err = optimizer.Optimize(&assignment.QoSModel{}, "testgroup", "testns")
	if err != nil {
		t.Errorf("unexpected error calling Optimize(): %v", err)
	}
	wrapper.assignmentPlan = &assignment.Reply{}
	err = optimizer.Cancel("testgroup", "testns")
	if err == nil {
		t.Errorf("should not be able to cancel a request that already has a reply")
	}
}

func TestPoll(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	client := clientmock.NewMockSchedulerClient(ctrl)
	stream := clientmock.NewMockScheduler_ScheduleClient(ctrl)
	client.EXPECT().Schedule(gomock.Any(), gomock.Any()).Return(stream, nil)
	wrapper := &grpcClientWrapper{
		schedulerClient: client,
		model:           &assignment.QoSModel{},
	}
	err := wrapper.Schedule()
	if err != nil {
		t.Errorf("unexpected error calling Schedule(): %v", err)
	}
	rep := &assignment.Reply{}
	stream.EXPECT().Recv().Return(rep, nil)
	stream.EXPECT().Context().Return(context.TODO())
	reply, err := wrapper.Poll()
	if reply == nil || err != nil {
		t.Errorf("expected non-nil return and no error for Poll() where reply is returned but got %v", err)
	}

	stream.EXPECT().Recv().Return(nil, io.EOF)
	reply, err = wrapper.Poll()
	if reply != nil || err != nil {
		t.Errorf("expected nil reply for Poll() when EOF is returned but got %v", err)
	}

	stream.EXPECT().Recv().Return(nil, fmt.Errorf("fake error"))
	reply, err = wrapper.Poll()
	if reply != nil || err == nil {
		t.Errorf("expected error return for Poll() when error is returned but got %v", err)
	}

}

func TestCheckGrpcClients(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	optimizer := &GrpcOptimizer{
		numClients:    1,
		kubeClient:    nil,
		clients:       make([]*grpcClientWrapper, 1),
		clientFactory: &mockClientFactory{ctrl: ctrl},
	}

	err := optimizer.initializeConnections(context.TODO())
	if err != nil {
		t.Errorf("unexpected error initializing mock connections: %v", err)
	}

	optimizer.checkGrpcClients(context.TODO())

	optimizer.clients[0].assignmentPlan = &assignment.Reply{}
	optimizer.checkGrpcClients(context.TODO())
	if optimizer.clients[0].assignmentPlan == nil {
		t.Errorf("checkGrpcClients should not have reset assignment plan to nil because done was false")
	}

	optimizer.clients[0].done = true
	optimizer.checkGrpcClients(context.TODO())
	if optimizer.clients[0].assignmentPlan != nil {
		t.Errorf("checkGrpcClients should have reset assignment plan to nil because done was true")
	}

	stream := clientmock.NewMockScheduler_ScheduleClient(ctrl)
	optimizer.clients[0].stream = stream
	rep := &assignment.Reply{}
	stream.EXPECT().Recv().Return(rep, nil)
	optimizer.clients[0].model = &assignment.QoSModel{}
	optimizer.checkGrpcClients(context.TODO())
	if optimizer.clients[0].model == nil {
		t.Errorf("checkGrpcClients should not have reset model to nil")
	}
	if optimizer.clients[0].assignmentPlan != rep {
		t.Errorf("assignment plan should have been written to client")
	}
}
