// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package optimizerclient contains code for clients of optimizer services.
// Such clients implement the Optimize() function, usually by sending a request
// to an optimizer service, waiting for the reply, and saving the reply into
// an AssignmentPlan resource.
package optimizerclient

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func WriteOptimizerResultToPlan(ctx context.Context, reply *assignment.Reply, model *assignment.QoSModel,
	applicationGroup string, namespace string, cl client.Client) error {
	apKey := types.NamespacedName{
		Name:      fmt.Sprintf("%s-assignment-plan", applicationGroup),
		Namespace: namespace,
	}

	// This happens in tests outside of Kubernetes
	if cl == nil {
		fmt.Printf("no kubernetes. i am supposed to write errors %v and model to assignment plan %v\n",
			reply.ErrorMessages, apKey)
		return nil
	}

	ap := &crd.AssignmentPlan{}
	err := cl.Get(ctx, apKey, ap)

	if err != nil {
		fmt.Printf("Failed to find assignment plan identified by %+v\n", apKey)
		return err
	}

	ap.Spec.ReplyReceived = true

	// This happens when there was a timeout in the optimizer itself.
	if reply == nil {
		ap.Spec.OptimizerReply = assignment.Reply{}
		ap.Spec.OptimizerReply.ErrorMessages = []*assignment.Reply_Error{{Kind: "timeout", Details: "optimizer request timed out"}}
	} else {
		ap.Spec.OptimizerReply = *reply.DeepCopy()
	}

	ap.Spec.QoSModel = *model.DeepCopy()

	err = cl.Update(ctx, ap)
	if err != nil {
		fmt.Printf("failed to update assignment plan: %v\n", err)
		return err
	}
	fmt.Printf("wrote errors and model to assignment plan %s\n", ap.Name)
	return nil
}
