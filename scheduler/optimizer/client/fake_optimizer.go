// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package optimizerclient

import (
	"context"
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"siemens.com/qos-scheduler/scheduler/assignment"
)

// EmptyOptimizer does nothing. This is for tests where I inject a fake
// optimizer plan.
type EmptyOptimizer struct {
	LastCall       map[string]*assignment.QoSModel
	ReceivedCancel map[string]bool
}

func (o *EmptyOptimizer) Optimize(model *assignment.QoSModel, applicationGroup string, namespace string) error {
	if o.LastCall == nil {
		o.LastCall = make(map[string]*assignment.QoSModel)
	}
	o.LastCall[applicationGroup] = model
	return nil
}

func (o *EmptyOptimizer) Cancel(applicationGroup string, namespace string) error {
	if o.ReceivedCancel == nil {
		o.ReceivedCancel = make(map[string]bool)
	}
	o.ReceivedCancel[applicationGroup] = true
	return nil
}

// FakeOptimizer implements a round-robin scheduler that ignores node capabilities
// completely. It is really just for testing.
// This avoids external calls but it still needs a Kubernetes client
// for creating the assignment plan.
type FakeOptimizer struct {
	client.Client
	Ctx context.Context
}

// Cancel implementats cancellation of an optimizer request.
// The FakeOptimizer does not support cancellation.
func (o *FakeOptimizer) Cancel(string, string) error { return nil }

// Optimize obtains a workload assignment and writes it to an assignment plan.
func (o *FakeOptimizer) Optimize(model *assignment.QoSModel, applicationGroup string, namespace string) error {
	assignment, err := o.optimizeAndReturn(model)

	if err != nil {
		return err
	}

	return WriteOptimizerResultToPlan(o.Ctx, assignment, model, applicationGroup,
		namespace, o.Client)
}

// OptimizeAndReturn just does a round-robin schedule.
func (o *FakeOptimizer) optimizeAndReturn(model *assignment.QoSModel) (*assignment.Reply, error) {

	ret := assignment.Reply{}
	ass := &assignment.QoSAssignment{WorkloadAssignments: make([]*assignment.QoSAssignment_WorkloadAssignment, 0)}

	lastNodeIndex := 0
	for _, app := range model.ApplicationModel.Applications {
		fmt.Printf("application %s: \n", app.Id)
		for _, wl := range app.Workloads {
			decision := lastNodeIndex
			fmt.Printf("I choose node %s for pod %s. Good luck!\n", model.InfrastructureModel.Nodes[decision].Id, wl.Id)
			ass.WorkloadAssignments = append(ass.WorkloadAssignments, &assignment.QoSAssignment_WorkloadAssignment{
				ApplicationId: app.Id,
				WorkloadId:    wl.Id,
				NodeId:        model.InfrastructureModel.Nodes[decision].Id,
			})
			for model.InfrastructureModel.Nodes[lastNodeIndex].NodeType != assignment.InfrastructureModel_Node_COMPUTE {
				lastNodeIndex = (lastNodeIndex + 1) % len(model.InfrastructureModel.Nodes)
			}
		}
	}

	ret.QoSAssignment = ass

	return &ret, nil
}
