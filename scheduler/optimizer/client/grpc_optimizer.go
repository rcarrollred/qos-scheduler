// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package optimizerclient

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"siemens.com/qos-scheduler/scheduler/assignment"
	k8sclient "sigs.k8s.io/controller-runtime/pkg/client"
)

// GrpcOptimizer calls the optimizer grpc service.
// It will maintain up to numClients grpc clients. Each client can
// make, and then wait for, one grpc request.
// The optimizer will collect information from clients that have
// received their replies.
type GrpcOptimizer struct {
	numClients              int
	kubeClient              k8sclient.Client
	clients                 []*grpcClientWrapper
	clientFactory           clientFactory
	schedulerTimeoutSeconds int
}

// A grpcClientWrapper contains a scheduler client and some data for the calls
// to make and where to store the results.
type grpcClientWrapper struct {
	schedulerClient assignment.SchedulerClient
	model           *assignment.QoSModel
	// This is a plan that has been retrieved from the server but that still needs to
	// be written back to Kubernetes.
	assignmentPlan          *assignment.Reply
	applicationGroupName    string
	namespace               string
	cancel                  context.CancelFunc
	stream                  assignment.Scheduler_ScheduleClient
	schedulerTimeoutSeconds int
	// This will be set to true when the assignment plan is ready to be written
	// to Kubernetes.
	done bool
}

// NewGrpcOptimizer creates a grpc optimizer and initializes the grpc connections for its clients.
func NewGrpcOptimizer(kubeClient k8sclient.Client, tls bool, caFile string, serverAddr string, serverHostOverride string, numClients int, timeout int) (*GrpcOptimizer, error) {
	ret := &GrpcOptimizer{
		numClients: numClients,
		kubeClient: kubeClient,
		clients:    make([]*grpcClientWrapper, numClients),
		clientFactory: &grpcClientFactory{
			tls:                tls,
			caFile:             caFile,
			serverAddr:         serverAddr,
			serverHostOverride: serverHostOverride,
		},
		schedulerTimeoutSeconds: timeout,
	}
	return ret, ret.initializeConnections(context.Background())
}

// This function is called periodically and it goes over all existing grpc clients.
// It will attempt to write any assignment plans received to Kubernetes,
// and it will poll waiting clients for new replies.
func (g *GrpcOptimizer) checkGrpcClients(ctx context.Context) {
	for _, cl := range g.clients {
		// Try to write pending assignment plan to Kubernetes.
		if cl.done {
			if cl.assignmentPlan != nil {
				err := WriteOptimizerResultToPlan(ctx, cl.assignmentPlan,
					cl.model, cl.applicationGroupName, cl.namespace, g.kubeClient)
				if err != nil {
					fmt.Printf("failed to write optimizer result to assignment plan: %v\n", err)
					if k8serrors.IsNotFound(err) {
						// Very likely the application group has been deleted, best to stop here.
						cl.reset()
					}
					// All other errors will be retried.
				} else {
					// All done, this client is ready for the next request.
					cl.reset()
				}
				continue
			}
			cl.reset()
			continue
		}

		// If model is nil, this client is not waiting for a reply.
		if cl.model == nil {
			continue
		}
		reply, err := cl.Poll()
		if err == nil && reply != nil {
			cl.assignmentPlan = reply
			cl.done = false
			continue
		}
		if err != nil {
			fmt.Printf("Poll() returned an error: %v\n", err)
			cl.assignmentPlan = &assignment.Reply{
				ErrorMessages: []*assignment.Reply_Error{
					&assignment.Reply_Error{
						Kind:    "Grpc Failure",
						Details: err.Error(),
					},
				},
			}
			continue
		} else {
			// a nil, nil reply means we got an eof and the stream is done
			// or we ran out of time waiting.
			// This client wrapper needs to be kept alive until the assignment
			// plan has been written to Kubernetes.
			cl.done = true
		}
	}
}

func (g *GrpcOptimizer) initializeConnections(ctx context.Context) error {
	for i := 0; i < g.numClients; i++ {
		schedulerClient, err := g.clientFactory.newSchedulerClient()
		if err != nil {
			return FailedToConnect{err: err}
		}
		g.clients[i] = &grpcClientWrapper{
			schedulerClient:         schedulerClient,
			schedulerTimeoutSeconds: g.schedulerTimeoutSeconds,
		}
	}
	go func() {
		schedulerWaitingTicker := time.NewTicker(time.Second * 1)
		for range schedulerWaitingTicker.C {
			g.checkGrpcClients(ctx)
		}
	}()
	return nil
}

func (g *grpcClientWrapper) reset() {
	g.applicationGroupName = ""
	g.namespace = ""
	g.model = nil
	g.assignmentPlan = nil
	g.done = false
}

// Schedule calls the Schedule() grpc method and saves a pointer to the
// grpc stream on the client wrapper.
func (g *grpcClientWrapper) Schedule() error {
	if g.model == nil {
		return fmt.Errorf("grpc client wrapper cannot schedule anything without a model")
	}
	var err error
	var ctx context.Context
	ctx, g.cancel = context.WithTimeout(context.Background(),
		time.Duration(g.schedulerTimeoutSeconds)*time.Second)
	g.stream, err = g.schedulerClient.Schedule(ctx, g.model)
	if err != nil {
		fmt.Printf("Schedule returned error: %v\n", err)
	}
	return err
}

// Cancel cancels the underlying grpc call, if any, then resets this client
// so it can take a new request.
func (g *grpcClientWrapper) Cancel() {
	g.cancel()
	g.reset()
}

// Poll retrieves data from the grpc stream associated with this wrapper.
// It returns a pointer to the reply if there is one, otherwise nil.
// A return of nil, nil indicates that the stream is finished.
// Currently the protocol is:
// The server may send early replies. A reply sent on the stream
// is always a complete Reply protocol buffer. An early reply may
// be less good than the final reply, but can be used if the call
// runs out of time otherwise.
// When the server is done computing a plan, it sends the final
// reply on the stream. After this, stream.Recv() will return EOF.
// There are no other messages being sent on the stream.
// The client wrapper will save early replies and will write them back
// to Kubernetes when either EOF has been received (indicating the last
// reply was the final one) or a timeout occurs.
func (g *grpcClientWrapper) Poll() (*assignment.Reply, error) {
	if g.stream == nil {
		g.Cancel()
		return nil, fmt.Errorf("cannot Poll with a nil stream")
	}
	reply, err := g.stream.Recv()
	if reply != nil {
		fmt.Printf("stream.Recv() returned reply\n")
		return reply, nil
	}
	if err == io.EOF {
		fmt.Printf("stream %s is done\n", g.applicationGroupName)
		return nil, nil
	}
	defer g.cancel()
	if err != nil {
		// We got here because Recv() and hence RecvMsg() returned so it is safe
		// to inspect the client's Context.
		streamCtxErr := g.stream.Context().Err()
		if streamCtxErr != nil {
			if errors.Is(streamCtxErr, context.DeadlineExceeded) {
				fmt.Printf("deadline exceeded on optimizer call for application group %s\n", g.applicationGroupName)
				// Return nil error here so that if a reply has been received before
				// the deadline was reached, it will be used.
				return nil, nil
			} else if errors.Is(streamCtxErr, context.Canceled) {
				fmt.Printf("optimizer call for application group %s was canceled\n", g.applicationGroupName)
			} else {
				fmt.Printf("stream context error is %v\n", streamCtxErr)
			}
			return nil, streamCtxErr
		} else {
			fmt.Printf("stream %s got an error %v\n", g.applicationGroupName, err)
		}
		return nil, err
	}
	// If we get here, then err == nil and reply == nil.
	// That is probably an error.
	return nil, fmt.Errorf("nil reply received from stream Recv()")
}

// Cancel implementats cancellation of ongoing optimizations.
// If an optimization call is ongoing for the application group, cancel it.
func (g *GrpcOptimizer) Cancel(applicationGroup string, namespace string) error {
	for _, c := range g.clients {
		if c.model != nil && c.applicationGroupName == applicationGroup && c.namespace == namespace {
			// If this has a reply then we might be already writing that one back, so let it finish.
			if c.assignmentPlan != nil {
				return fmt.Errorf("already received reply for %s/%s, too late to cancel", namespace, applicationGroup)
			}
			fmt.Printf("Canceling request for %s/%s\n", namespace, applicationGroup)
			c.Cancel()
			return nil
		}
	}
	// No matching request found.
	return nil
}

// Optimize finds a free grpc client and asks it to call Schedule().
// If it returns nil then it has performed the call and will be listening to the results stream.
// Errors can be caused by the optimizer service being busy, or by transient network issues.
// It is up to the caller of Optimize() to decide whether and when to retry.
func (g *GrpcOptimizer) Optimize(model *assignment.QoSModel, applicationGroupName string, namespace string) error {
	// Are we already running a client for this request?
	for _, c := range g.clients {
		if c.model != nil && c.applicationGroupName == applicationGroupName && c.namespace == namespace {
			return DuplicateRequest{namespace, applicationGroupName}
		}
	}

	// Do we have a free grpc client?
	var freeClient *grpcClientWrapper
	for _, c := range g.clients {
		if c.model == nil && c.assignmentPlan == nil {
			freeClient = c
			break
		}
	}
	if freeClient == nil {
		return NoAvailableSlots{}
	}

	freeClient.model = model
	freeClient.applicationGroupName = applicationGroupName
	freeClient.namespace = namespace
	err := freeClient.Schedule()
	if err != nil {
		freeClient.reset()
		return err
	}
	return nil
}
