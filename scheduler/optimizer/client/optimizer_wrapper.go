// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package optimizerclient

import (
	"fmt"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

// Optimizer is a thing that optimizes pod/node assignments.
type Optimizer interface {
	// Optimize takes a QosModel and creates an assignment plan
	// with workload assignments.
	// It would be possible to change this so it sends possible nodes for each pod.
	Optimize(model *assignment.QoSModel, applicationGroup string, namespace string) error

	// Attempt to cancel an ongoing optimization.
	// Not all optimization systems support this operation. Those
	// that do not will simply return nil without blocking.
	Cancel(applicationGroup string, namespace string) error

	// Maybe in the future there can be an incremental option where the optimizer also
	// gets an existing assignment with information as to which pod/node pairings can
	// be re-computed if necessary.
}

// NoAvailableSlots is what the Optimizer returns when it is
// currently unable to take additional requests.
// This error is retryable.
type NoAvailableSlots struct{}

func (n NoAvailableSlots) Error() string {
	return "The optimizer is busy and cannot take any requests at the moment"
}

// FailedToConnect happens when the Optimizer fails to connect to its
// backend.
// This error is not retryable.
type FailedToConnect struct {
	err error
}

func (f FailedToConnect) Error() string {
	return fmt.Sprintf("Failed to connect to backend: %v", f.err)
}

// DuplicateRequest is what the Optimizer returns when there is already
// a client working on a request for the same application group.
// This error is not retryable.
type DuplicateRequest struct {
	applicationGroup string
	namespace        string
}

func (d DuplicateRequest) Error() string {
	return fmt.Sprintf("Already working on an optimization request for %s/%s", d.namespace, d.applicationGroup)
}
