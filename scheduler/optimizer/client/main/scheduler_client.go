// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"google.golang.org/protobuf/encoding/protojson"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"log"
	"os"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
	oclient "siemens.com/qos-scheduler/scheduler/optimizer/client"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"time"
)

var (
	tls                = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	caFile             = flag.String("ca_file", "", "The file containing the CA root cert file")
	serverAddr         = flag.String("addr", "localhost:50051", "The server address in the format of host:port")
	serverHostOverride = flag.String("server_host_override", "x.test.example.com", "The server name used to verify the hostname returned by the TLS handshake")
	localFile          = flag.String("localinput", "", "A local file to load a model input from")
	timeout            = flag.Int("timeout", 3, "How long to wait for optimizer server replies")
)

func main() {
	flag.Parse()
	if *tls {
		if *caFile == "" {
			*caFile = ("x509/ca_cert.pem")
		}
	}

	scheme := runtime.NewScheme()
	clientgoscheme.AddToScheme(scheme)
	crd.AddToScheme(scheme)

	var k8scl client.Client
	config, err := config.GetConfig()
	if err != nil {
		log.Printf("failed to get a kubernetes config, i hope you are just debugging.")
		k8scl = nil
	} else {
		k8scl, err = client.New(config, client.Options{Scheme: scheme})
		if err != nil {
			log.Fatalf("Failed to get kubernetes client: %v", err)
		}
	}

	optimizerClient, err := oclient.NewGrpcOptimizer(k8scl, *tls, *caFile, *serverAddr, *serverHostOverride, 1, *timeout)
	if err != nil {
		log.Fatalf("failed to initialize optimizer client: %v", err)
	}

	optimizationRequest := &assignment.QoSModel{}
	if *localFile != "" {
		log.Printf("try to load a qos model from local file %s", *localFile)
		modelReadJSON, err := os.ReadFile(*localFile)
		if err != nil {
			log.Fatalf("failed to load model from local file: %v", err)
		}
		err = protojson.Unmarshal(modelReadJSON, optimizationRequest)
		if err != nil {
			log.Fatalf("%v", err)
		}
		if optimizationRequest.ApplicationModel == nil {
			log.Fatalf("application model is nil")
		}

		log.Printf("loaded model: %+v\n", optimizationRequest.ApplicationModel.Applications)
	}

	err = optimizerClient.Optimize(optimizationRequest, "testgroup", "testns")
	if err != nil {
		log.Printf("failed to call scheduler: %v", err)
	}
	log.Printf("waiting to exit\n")
	for i := 0; i < 10; {
		time.Sleep(1 * time.Second)
	}
}
