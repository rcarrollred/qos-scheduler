// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package main contains a grpc server stub.
// This can take Schedule() calls for testing, but it only ever produces
// empty assignments.
package main

import (
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	"net"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

var (
	tls      = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile = flag.String("cert_file", "", "The TLS cert file")
	keyFile  = flag.String("key_file", "", "The TLS key file")
	port     = flag.Int("port", 50051, "The server port")
)

type sampleSchedulerServer struct {
	assignment.UnimplementedSchedulerServer
}

func newServer() *sampleSchedulerServer {
	s := &sampleSchedulerServer{}
	return s
}

func (sampleSchedulerServer) Schedule(model *assignment.QoSModel, stream assignment.Scheduler_ScheduleServer) error {
	log.Printf("got a request with model %+v\n", model.ApplicationModel.Applications)
	// fill in the model with an assignment of some sort
	reply := &assignment.Reply{
		Feasible: true,
		QoSAssignment: &assignment.QoSAssignment{
			WorkloadAssignments: []*assignment.QoSAssignment_WorkloadAssignment{},
			PathAssignments:     []*assignment.QoSAssignment_PathAssignment{},
		},
	}
	stream.Send(reply)
	return nil
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	if *tls {
		if *certFile == "" {
			*certFile = "x509/server_cert.pem"
		}
		if *keyFile == "" {
			*keyFile = "x509/server_key.pem"
		}
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		if err != nil {
			log.Fatalf("Failed to generate credentials: %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}

	// I think this is the maximum number of simultaneous rpcs per connection
	// Not sure if there's a built-in way to limit the number of connections;
	// may need to implement our own interceptor for this.
	// On the other hand, we control the client, and can make sure it only creates
	// one connection, so this is not something we have to fix right now.
	opts = append(opts, grpc.MaxConcurrentStreams(1))
	opts = append(opts, grpc.NumStreamWorkers(2))
	grpcServer := grpc.NewServer(opts...)
	assignment.RegisterSchedulerServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}
