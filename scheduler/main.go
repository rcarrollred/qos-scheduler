// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"os"
	"strings"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/controllers"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	optimizer "siemens.com/qos-scheduler/scheduler/optimizer/client"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(qosschedulerv1alpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var scheduleOnMasterNode bool
	var probeAddr string
	var customScheduler string
	var optimizerService string
	var serviceClassMap string
	var optimizerCompatMode bool
	var optimizerRetryDelay int
	var optimizerTimeout int
	var ipv4Cidrs string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&customScheduler, "custom-scheduler", "qos-scheduler", "The name of the scheduler to set on pods generated from workloads.")
	flag.BoolVar(&scheduleOnMasterNode, "schedule-on-master-node", false,
		"Allow the optimizer to schedule workloads on the master node. Your workloads must have a toleration for the master taint.")
	flag.StringVar(&optimizerService, "optimizer-service", "http://controllers-optimizer-service.controllers-system/optimize", "The URL of the optimizer service.")
	flag.StringVar(&serviceClassMap, "service-class-map", "BESTEFFORT:BASIC,ASSURED:TSN", "A mapping of channel service classes to implementations")
	flag.BoolVar(&optimizerCompatMode, "optimizer-compat-mode", true, "run with an old optimizer")
	flag.IntVar(&optimizerRetryDelay, "optimizer-retry-delay", int(5), "how long to wait before retrying failed optimizer calls")
	flag.IntVar(&optimizerTimeout, "optimizer-timeout", int(5), "how long to wait for replies to an optimizer request")
	flag.StringVar(&ipv4Cidrs, "ipv4-cidrs", "", "the ipv4 cidrs that are available to use")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "63c9a0d6.siemens.com",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	qosImplMap := map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass){}
	impls := strings.Split(serviceClassMap, ",")
	if len(impls) == 0 {
		setupLog.Info("no service class implementation mapping specified; all workload interfaces will be implemented using basic channels")
	} else {
		for _, imp := range impls {
			parts := strings.Split(imp, ":")
			if len(parts) != 2 {
				setupLog.Info("could not parse implementation map, skipping", "map", imp)
				continue
			}
			setupLog.Info("channels implementation map line added: ", "class", parts[0], "implementation", parts[1])
			qosImplMap[qosschedulerv1alpha1.NetworkServiceClass(parts[0])] = nwlib.NetworkImplementationClass(parts[1])
		}
	}

	channels := &controllers.ChannelReconciler{
		Client:               mgr.GetClient(),
		Log:                  ctrl.Log.WithName("controllers").WithName("Channel"),
		Scheme:               mgr.GetScheme(),
		Reader:               mgr.GetAPIReader(),
		QosImplementationMap: qosImplMap,
	}
	if err = channels.SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Channel")
		os.Exit(1)
	}

	if err = (&controllers.ApplicationReconciler{
		Client:        mgr.GetClient(),
		Log:           ctrl.Log.WithName("controllers").WithName("Application"),
		Scheme:        mgr.GetScheme(),
		SchedulerName: customScheduler,
		Reader:        mgr.GetAPIReader(),
		Recorder:      mgr.GetEventRecorderFor("application"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Application")
		os.Exit(1)
	}

	optimizerClient, err := optimizer.NewGrpcOptimizer(mgr.GetClient(), false, "", optimizerService, "", 1, optimizerTimeout)
	if err != nil {
		setupLog.Error(err, "unable to create grpc optimizer client")
		os.Exit(1)
	}

	if err = (&controllers.ApplicationGroupReconciler{
		Client:               mgr.GetClient(),
		Log:                  ctrl.Log.WithName("controllers").WithName("ApplicationGroup"),
		Scheme:               mgr.GetScheme(),
		Channels:             channels,
		OptimizerCompatMode:  optimizerCompatMode,
		OptimizerClient:      optimizerClient,
		QosImplementationMap: qosImplMap,
		Recorder:             mgr.GetEventRecorderFor("application-group"),
		Reader:               mgr.GetAPIReader(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApplicationGroup")
		os.Exit(1)
	}

	if err = (&controllers.AssignmentPlanReconciler{
		Client:              mgr.GetClient(),
		Log:                 ctrl.Log.WithName("controllers").WithName("ApplicationGroup"),
		Scheme:              mgr.GetScheme(),
		OptimizerClient:     optimizerClient,
		OptimizerRetryDelay: optimizerRetryDelay,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "AssignmentPlan")
		os.Exit(1)
	}

	if err = (&controllers.IpamReconciler{
		Client:         mgr.GetClient(),
		Log:            ctrl.Log.WithName("controllers").WithName("ApplicationGroup"),
		Scheme:         mgr.GetScheme(),
		Ipv4CidrRanges: strings.Split(ipv4Cidrs, ","),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Ipam")
		os.Exit(1)
	}

	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
