// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	oclient "siemens.com/qos-scheduler/scheduler/optimizer/client"
)

// AssignmentPlanReconciler watches for assignment plans. If it sees any that have ReplyReceived
// set to false, it starts an optimizer request for them.
type AssignmentPlanReconciler struct {
	client.Client
	Log                 logr.Logger
	Scheme              *runtime.Scheme
	OptimizerClient     oclient.Optimizer
	OptimizerRetryDelay int
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=assignmentplans,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=assignmentplans/status,verbs=get;update;patch

// Reconcile aims to move the state of the cluster towards the desired state.
func (r *AssignmentPlanReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	plan := &qosschedulerv1alpha1.AssignmentPlan{}
	err := r.Get(ctx, req.NamespacedName, plan)
	if err != nil {
		if errors.IsNotFound(err) {
			// Assignment plans get deleted when the corresponding application group goes out of scope,
			// this is normal.
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if plan.Spec.ReplyReceived {
		// We're only interested in plans that have ReplyReceived false.
		return ctrl.Result{}, nil
	}

	controller := metav1.GetControllerOf(plan)
	if controller == nil {
		return ctrl.Result{}, fmt.Errorf("assignment plan %s is not owned by an application group", plan.Name)
	}

	r.Log.Info("should send assignment plan to optimizer", "plan", plan.Name)
	err = r.OptimizerClient.Optimize(&plan.Spec.QoSModel, controller.Name, req.NamespacedName.Namespace)

	if err != nil {
		r.Log.Error(err, "optimization call failed")
		requeueAfter := time.Second * time.Duration(r.OptimizerRetryDelay)
		if _, ok := err.(oclient.NoAvailableSlots); ok {
			r.Log.Info("will retry after a while", "while", requeueAfter)
			return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, err
		}
		if _, ok := err.(oclient.FailedToConnect); ok {
			r.Log.Info("this cannot be retried")
			return ctrl.Result{Requeue: false}, err
		}
		if _, ok := err.(oclient.DuplicateRequest); ok {
			r.Log.Info("this makes no sense to retry")
			return ctrl.Result{}, nil
		}
		r.Log.Info("unexpected error, will retry and hope for the best")
		return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, err

	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *AssignmentPlanReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&qosschedulerv1alpha1.AssignmentPlan{}).Complete(r)
}
