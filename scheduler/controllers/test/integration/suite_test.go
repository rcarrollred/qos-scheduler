// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gexec"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	qosctrl "siemens.com/qos-scheduler/scheduler/controllers"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	optimizer "siemens.com/qos-scheduler/scheduler/optimizer/client"
	//+kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

var (
	cfg                *rest.Config
	k8sClient          client.Client
	testEnv            *envtest.Environment
	k8sReader          client.Reader
	k8sScheme          *runtime.Scheme
	emptyOptimizer     *optimizer.EmptyOptimizer
	channelsController *qosctrl.ChannelReconciler
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	_, reporterConfig := GinkgoConfiguration()

	RunSpecs(t, "Controller Suite", reporterConfig)
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "..", "..", "..", "helm", "qos-scheduler", "charts", "qos-crds", "crds")},
		ErrorIfCRDPathMissing: true,
	}

	k8sScheme = scheme.Scheme
	err := qosschedulerv1alpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	cfg, err := testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	//+kubebuilder:scaffold:scheme

	k8sManager, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme:                 scheme.Scheme,
		MetricsBindAddress:     "0",
		HealthProbeBindAddress: "0",
	})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sManager).NotTo(BeNil())

	By("setting up controllers")

	channelsController = &qosctrl.ChannelReconciler{
		Client: k8sManager.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("Channel"),
		Scheme: k8sManager.GetScheme(),
		Reader: k8sManager.GetAPIReader(),
	}
	err = channelsController.SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	emptyOptimizer = &optimizer.EmptyOptimizer{}

	err = (&qosctrl.ApplicationGroupReconciler{
		Client:          k8sManager.GetClient(),
		Log:             ctrl.Log.WithName("controllers").WithName("ApplicationGroup"),
		Scheme:          k8sManager.GetScheme(),
		Channels:        channelsController,
		Recorder:        k8sManager.GetEventRecorderFor("application-group"),
		Reader:          k8sManager.GetAPIReader(),
		OptimizerClient: emptyOptimizer,
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	err = (&qosctrl.ApplicationReconciler{
		Client:        k8sManager.GetClient(),
		Log:           ctrl.Log.WithName("controllers").WithName("Application"),
		Scheme:        k8sManager.GetScheme(),
		SchedulerName: "qos-scheduler",
		Reader:        k8sManager.GetAPIReader(),
		Recorder:      k8sManager.GetEventRecorderFor("application"),
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	err = (&qosctrl.AssignmentPlanReconciler{
		Client:              k8sManager.GetClient(),
		Log:                 ctrl.Log.WithName("controllers").WithName("AssignmentPlan"),
		Scheme:              k8sManager.GetScheme(),
		OptimizerClient:     emptyOptimizer,
		OptimizerRetryDelay: int(1),
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	err = (&qosctrl.IpamReconciler{
		Client:         k8sManager.GetClient(),
		Log:            ctrl.Log.WithName("controllers").WithName("AssignmentPlan"),
		Scheme:         k8sManager.GetScheme(),
		Ipv4CidrRanges: []string{"10.0.0.0/24"},
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctrl.SetupSignalHandler())
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
		gexec.KillAndWait(4 * time.Second)

		// Teardown the test environment once controller is finished.
		// Otherwise from Kubernetes 1.21+, teardown timeouts waiting on
		// kube-apiserver to return
		err := testEnv.Stop()
		Expect(err).ToNot(HaveOccurred())
	}()

	k8sClient = k8sManager.GetClient()
	k8sReader = k8sManager.GetAPIReader()
	Expect(k8sClient).NotTo(BeNil())

	By("creating cluster infrastructure")
	ns := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "test-namespace"}}
	err = k8sClient.Create(context.Background(), ns)
	Expect(err).NotTo(HaveOccurred())
	ns = &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "network-test-namespace"}}
	err = k8sClient.Create(context.Background(), ns)
	Expect(err).NotTo(HaveOccurred())
	node := &v1.Node{ObjectMeta: metav1.ObjectMeta{Name: "node1"}}
	err = k8sClient.Create(context.Background(), node)
	Expect(err).NotTo(HaveOccurred())
	node = &v1.Node{ObjectMeta: metav1.ObjectMeta{Name: "node2"}}
	err = k8sClient.Create(context.Background(), node)
	Expect(err).NotTo(HaveOccurred())
	nwl1 := infrastructure.MakeNetworkLink("node1", "COMPUTE", "switch1", "NETWORK", 100, 5)
	nwl2 := infrastructure.MakeNetworkLink("switch1", "NETWORK", "node1", "COMPUTE", 100, 2)
	nwl3 := infrastructure.MakeNetworkLink("node1", "COMPUTE", "node1", "COMPUTE", 400, 1)
	Expect(k8sClient.Create(context.Background(), &nwl1)).Should(Succeed())
	Expect(k8sClient.Create(context.Background(), &nwl2)).Should(Succeed())
	Expect(k8sClient.Create(context.Background(), &nwl3)).Should(Succeed())
	nwl1 = infrastructure.MakeNetworkLink("node2", "COMPUTE", "switch1", "NETWORK", 100, 5)
	nwl2 = infrastructure.MakeNetworkLink("switch1", "NETWORK", "node2", "COMPUTE", 100, 2)
	nwl3 = infrastructure.MakeNetworkLink("node2", "COMPUTE", "node2", "COMPUTE", 400, 1)
	Expect(k8sClient.Create(context.Background(), &nwl1)).Should(Succeed())
	Expect(k8sClient.Create(context.Background(), &nwl2)).Should(Succeed())
	Expect(k8sClient.Create(context.Background(), &nwl3)).Should(Succeed())
	ep1 := &qosschedulerv1alpha1.NetworkEndpoint{
		ObjectMeta: metav1.ObjectMeta{Name: "node1"},
		Spec: qosschedulerv1alpha1.NetworkEndpointSpec{
			NodeName: "node1",
			NetworkInterfaces: []qosschedulerv1alpha1.NetworkEndpointNetworkInterface{
				{
					Name:        "eth0",
					IPv4Address: "10.100.0.1",
				},
			},
			MappedNetworkInterfaces: map[string]string{
				"tsn": "eth0",
			},
		},
	}
	ep2 := &qosschedulerv1alpha1.NetworkEndpoint{
		ObjectMeta: metav1.ObjectMeta{Name: "node2"},
		Spec: qosschedulerv1alpha1.NetworkEndpointSpec{
			NodeName: "node2",
			NetworkInterfaces: []qosschedulerv1alpha1.NetworkEndpointNetworkInterface{
				{
					Name:               "eth0",
					IPv4Address:        "10.100.0.2",
					AlternateAddresses: []string{"10.100.0.2"},
				},
			},
			MappedNetworkInterfaces: map[string]string{
				"tsn": "eth0",
			},
		},
	}
	Expect(k8sClient.Create(context.Background(), ep1)).Should(Succeed())
	Expect(k8sClient.Create(context.Background(), ep2)).Should(Succeed())

})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	testEnv.Stop()
})
