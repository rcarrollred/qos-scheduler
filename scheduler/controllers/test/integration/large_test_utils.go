// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/gomega"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

func advanceToSchedulingPhase(name string, namespace string) error {
	var app qosscheduler.Application
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: name, Namespace: namespace}, &app); err != nil {
		return err
	}
	app.Status.Phase = qosscheduler.ApplicationScheduling
	return k8sClient.Status().Update(context.Background(), &app)
}

func advanceToRunningPhase(name string, namespace string) error {
	var app qosscheduler.Application
	if err := k8sReader.Get(context.Background(), types.NamespacedName{Name: name, Namespace: namespace}, &app); err != nil {
		return err
	}
	app.Status.Phase = qosscheduler.ApplicationRunning
	return k8sClient.Status().Update(context.Background(), &app)
}

func readApp(name string, namespace string, app *qosscheduler.Application) error {
	return k8sReader.Get(context.Background(), types.NamespacedName{Name: name, Namespace: namespace}, app)
}

func createChannel(channelName string, fromApplication string, toApplication string, applicationGroupName string, namespace string) error {
	channel := &qosscheduler.Channel{
		ObjectMeta: metav1.ObjectMeta{
			Name:      channelName,
			Namespace: namespace,
			Labels: map[string]string{"application": fromApplication,
				qosscheduler.NetworkLabel:     "TSN",
				"to-application":    toApplication,
				"application-group": applicationGroupName},
		},
		Spec: qosscheduler.ChannelSpec{
			ChannelFrom:      "node1",
			ChannelTo:        "node2",
			SourceWorkload:   fmt.Sprintf("%s-test-%s", fromApplication, "basename"),
			TargetWorkload:   fmt.Sprintf("%s-test-%s", toApplication, "basename-2"),
			MinBandwidthBits: 10,
			MaxDelayNanos:    2,
			ServiceClass:     qosscheduler.ServiceClassAssured,
		},
	}
	return k8sClient.Create(context.Background(), channel)
}

func readChannel(channelName string, namespace string, channel *qosscheduler.Channel) error {
	return k8sReader.Get(context.Background(), types.NamespacedName{Name: channelName, Namespace: namespace}, channel)
}

func getChannel(channelName string, namespace string, channel *qosscheduler.Channel) error {
	return k8sClient.Get(context.Background(), types.NamespacedName{Name: channelName, Namespace: namespace}, channel)
}

func createIpam(name string, namespace string, addressCount int) error {
   ipamSpec := qosscheduler.IpamSpec{
      NumberOfAddresses: addressCount,
   }
	ipam := &qosscheduler.Ipam{
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
		},
      Spec: ipamSpec,
	}
	return k8sClient.Create(context.Background(), ipam)
}

func createApplication(name string, namespace string) *qosscheduler.Application {
	wlSpec := qosscheduler.ApplicationWorkloadSpec{
		Basename: "test-basename",
		Template: qosscheduler.QosPodTemplateSpec{
			Spec: v1.PodSpec{
				NodeSelector: map[string]string{
					"foo": "bar",
				},
				Affinity: &v1.Affinity{
					NodeAffinity: &v1.NodeAffinity{
						RequiredDuringSchedulingIgnoredDuringExecution: &v1.NodeSelector{
							NodeSelectorTerms: []v1.NodeSelectorTerm{
								{
									MatchExpressions: []v1.NodeSelectorRequirement{
										{
											Key:      fmt.Sprintf("test-key-%s-wl1", name),
											Operator: v1.NodeSelectorOpExists,
										},
									},
								},
							},
						},
					},
				},
				Containers: []v1.Container{
					{
						Name:  "test-container-wl1",
						Image: "test-container",
					},
				},
			},
		},
		Channels: []qosscheduler.NetworkChannel{},
	}
	wl2Spec := qosscheduler.ApplicationWorkloadSpec{
		Basename: "test-basename-2",
		Template: qosscheduler.QosPodTemplateSpec{
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:  "test-container-wl2",
						Image: "test-container",
					},
				},
			},
			Metadata: qosscheduler.QosMeta{
				Labels: map[string]string{
					"foo": "bar",
				},
			},
		},
		Channels: []qosscheduler.NetworkChannel{},
	}
	app := &qosscheduler.Application{
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Labels:      map[string]string{"application-group": "ag"},
			Annotations: map[string]string{"annotation1": "annotation-value1"},
		},
		Spec: qosscheduler.ApplicationSpec{
			Workloads: []qosscheduler.ApplicationWorkloadSpec{
				wlSpec,
				wl2Spec,
			},
		},
	}
	return app
}

func getApplication(lookupKey types.NamespacedName, app *qosscheduler.Application) bool {
	return k8sClient.Get(context.Background(), lookupKey, app) == nil
}

func createApplicationGroup(name string, namespace string) error {
	appGroup := &qosscheduler.ApplicationGroup{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: qosscheduler.ApplicationGroupSpec{
			MinMember: 2,
			OptimizerRetryPolicy: qosscheduler.OptimizerRetryPolicy{
				MaxRetries:        1,
				InitialTimeLimit:  2,
				InitialGapLimit:   4,
				TimeLimitIncrease: 2,
				GapLimitIncrease:  1,
			},
		},
	}
	return k8sClient.Create(context.Background(), appGroup)
}

func waitUntilGroupExists(name string, namespace string) {
	Eventually(func() error {
		var group qosscheduler.ApplicationGroup
		if err := k8sClient.Get(context.Background(), getGroupKey(name, namespace), &group); err != nil {
			return err
		}
		if err := readGroup(name, namespace, &group); err != nil {
			return err
		}
		if group.Status.Phase != "" {
			return nil
		}
		return fmt.Errorf("application group %s not ready yet", name)
	}, time.Second*10, time.Millisecond*250).Should(BeNil())
}

func awaitOptimizingPhase(name string, namespace string) error {
	var group qosscheduler.ApplicationGroup
	if err := readGroup(name, namespace, &group); err != nil {
		return err
	}
	var plan qosscheduler.AssignmentPlan
	if err := readPlan(name, namespace, &plan); err != nil {
		return err
	}
	if group.Status.Phase == qosscheduler.ApplicationGroupOptimizing {
		return nil
	}
	return fmt.Errorf("group %s is in state %s instead of optimizing", name, group.Status.Phase)
}

func getGroupKey(name string, namespace string) types.NamespacedName {
	return types.NamespacedName{Name: name, Namespace: namespace}
}

func getPlanKey(name string, namespace string) types.NamespacedName {
	return types.NamespacedName{
		Name:      name + "-assignment-plan",
		Namespace: namespace,
	}
}

func readGroup(name string, namespace string, group *qosscheduler.ApplicationGroup) error {
	return k8sReader.Get(context.Background(), getGroupKey(name, namespace), group)
}

func readPlan(name string, namespace string, plan *qosscheduler.AssignmentPlan) error {
	return k8sClient.Get(context.Background(), getPlanKey(name, namespace), plan)
}
