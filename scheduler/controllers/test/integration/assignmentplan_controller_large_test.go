// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

var _ = Describe("Assignment plan controller", func() {

	const (
		namespace = "test-namespace"

		timeout  = time.Second * 20
		interval = time.Millisecond * 250
	)
	var (
		ctx                  context.Context
		applicationGroupName string
	)

	BeforeEach(func() {
		applicationGroupName = ""
	})

	JustBeforeEach(func() {
		ctx = context.Background()
		createApplicationGroup(applicationGroupName, namespace)
	})

	AfterEach(func() {
		var group qosscheduler.ApplicationGroup
		if err := readGroup(applicationGroupName, namespace, &group); err == nil {
			k8sClient.Delete(ctx, &group)
		}
	})

	When("an assignment plan has not received a reply yet", func() {
		BeforeEach(func() {
			applicationGroupName = "assignment-ag-noreply"
		})
		It("must send an optimizer request", func() {
			var group qosscheduler.ApplicationGroup
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: applicationGroupName, Namespace: namespace}, &group)
				return err == nil
			}, timeout, interval).Should(Equal(true))

			apKey := getPlanKey(applicationGroupName, namespace)
			plan := qosscheduler.AssignmentPlan{
				ObjectMeta: metav1.ObjectMeta{
					Name:      apKey.Name,
					Namespace: namespace,
					Labels:    map[string]string{"application-group": applicationGroupName},
				},
				TypeMeta: metav1.TypeMeta{Kind: "AssignmentPlan", APIVersion: "v1alpha1"},
				Spec: qosscheduler.AssignmentPlanSpec{
					QoSModel: assignment.QoSModel{
						InfrastructureModel: &assignment.InfrastructureModel{},
						ApplicationModel:    &assignment.ApplicationModel{},
					},
					OptimizerReply: assignment.Reply{},
					ReplyReceived:  false,
				},
			}
			err := ctrl.SetControllerReference(&group, &plan, k8sScheme)
			Expect(err).NotTo(HaveOccurred())
			Expect(k8sClient.Create(ctx, &plan)).Should(Succeed())
			Eventually(func() bool {
				return emptyOptimizer.LastCall[applicationGroupName] != nil
			}, timeout, interval).Should(Equal(true))
		})
	})
	When("an assignment plan has already received a reply", func() {
		BeforeEach(func() {
			applicationGroupName = "assignment-ag-reply-exists"
		})
		It("must not send another optimizer request", func() {
			var group qosscheduler.ApplicationGroup
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: applicationGroupName, Namespace: namespace}, &group)
				return err == nil
			}, timeout, interval).Should(Equal(true))

			apKey := getPlanKey(applicationGroupName, namespace)
			plan := qosscheduler.AssignmentPlan{
				ObjectMeta: metav1.ObjectMeta{
					Name:      apKey.Name,
					Namespace: namespace,
					Labels:    map[string]string{"application-group": applicationGroupName},
				},
				TypeMeta: metav1.TypeMeta{Kind: "AssignmentPlan", APIVersion: "v1alpha1"},
				Spec: qosscheduler.AssignmentPlanSpec{
					QoSModel: assignment.QoSModel{
						InfrastructureModel: &assignment.InfrastructureModel{},
						ApplicationModel:    &assignment.ApplicationModel{},
					},
					OptimizerReply: assignment.Reply{},
					ReplyReceived:  true,
				},
			}
			err := ctrl.SetControllerReference(&group, &plan, k8sScheme)
			Expect(err).NotTo(HaveOccurred())
			Expect(k8sClient.Create(ctx, &plan)).Should(Succeed())
			Consistently(func() bool {
				return emptyOptimizer.LastCall[applicationGroupName] != nil
			}, timeout, interval).Should(Equal(false))
		})
	})
})
