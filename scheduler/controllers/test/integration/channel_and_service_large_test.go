// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("Channel and service requests in the controllers", func() {

	const (
		timeout   = time.Second * 10
		interval  = time.Millisecond * 250
		namespace = "default"
	)

	var (
		applicationGroupName string
		ctx                  context.Context
	)

	BeforeEach(func() {
		applicationGroupName = ""
	})

	JustBeforeEach(func() {
		if applicationGroupName == "" {
			return
		}
		ctx = context.Background()
		createApplicationGroup(applicationGroupName, namespace)

		app1Name := fmt.Sprintf("%s-app-1", applicationGroupName)
		app2Name := fmt.Sprintf("%s-app-2", applicationGroupName)
		app1 := *application.MakeApplication(app1Name, namespace, []string{"workload"},
			[]qosscheduler.WorkloadId{
				{
					ApplicationName: app2Name,
					Basename:        "workload",
					Port:            3333,
				},
			})
		app1.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app1)

		app2 := *application.MakeApplication(app2Name, namespace, []string{"workload"},
			[]qosscheduler.WorkloadId{
				{
					ApplicationName: app1Name,
					Basename:        "workload",
					Port:            4444,
				},
			})
		app2.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app2)
	})

	AfterEach(func() {
		if applicationGroupName == "" {
			return
		}
		var group qosscheduler.ApplicationGroup
		if err := readGroup(applicationGroupName, namespace, &group); err == nil {
			k8sClient.Delete(ctx, &group)
		}
	})

	When("the assignment plan contains channels", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-with-channels" })

		It("should create channel requests", func() {
			app1Name := applicationGroupName + "-app-1"
			app2Name := applicationGroupName + "-app-2"
			Eventually(func() error {
				var app qosscheduler.Application
				if err := k8sClient.Get(context.Background(), types.NamespacedName{Name: app1Name, Namespace: namespace}, &app); err != nil {
					return err
				}
				if app.Status.Phase != "" {
					return nil
				}
				return fmt.Errorf("application %s not ready yet", app1Name)
			}, timeout, interval).Should(BeNil())

			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))

			// create a valid assignment plan with channels.
			ass := assignment.QoSAssignment{
				WorkloadAssignments: []*assignment.QoSAssignment_WorkloadAssignment{
					{
						ApplicationId: app1Name,
						WorkloadId:    app1Name + "-workload",
						NodeId:        "node1",
					},
					{
						ApplicationId: app2Name,
						WorkloadId:    app2Name + "-workload",
						NodeId:        "node2",
					},
				},
				PathAssignments: []*assignment.QoSAssignment_PathAssignment{
					{
						InterfaceId: "if0-to-workload-" + app2Name,
						SourceNode:  "node1",
						TargetNode:  "node2",
					},
					{
						InterfaceId: "if0-to-workload-" + app1Name,
						SourceNode:  "node2",
						TargetNode:  "node1",
					},
				},
			}
			applications := &qosscheduler.ApplicationList{}
			Expect(k8sClient.List(ctx, applications)).Should(Succeed())
			model := &assignment.QoSModel{
				ApplicationModel: application.GetApplicationModel(applications.Items,
					map[qosscheduler.NetworkServiceClass](nwlib.NetworkImplementationClass){}),
				InfrastructureModel: infrastructure.MakeInfrastructureModel(),
			}
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.QoSModel = *model.DeepCopy()
			plan.Spec.OptimizerReply.QoSAssignment = &ass
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = true
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())

			// This verifies that the optimizer plan has been received and turned into
			// a schedule.
			Eventually(func() error {
				if err := readPlan(applicationGroupName, namespace, &plan); err != nil {
					return err
				}
				if len(plan.Status.PodStatus) == 0 {
					return fmt.Errorf("schedule is empty")
				}
				return nil
			}, 2*timeout, interval).Should(BeNil())

			// Verify that channels have been created for the right node connections and with the right target ports.
			Eventually(func() error {
				var channel1 qosscheduler.Channel
				if err := k8sClient.Get(context.Background(), types.NamespacedName{
					Name: "if0-to-workload-" + app2Name, Namespace: namespace}, &channel1); err != nil {
					return err
				}
				if channel1.Spec.TargetPort != 3333 {
					return fmt.Errorf("channel1 should have had a target port of 3333 but its spec is %+v", channel1.Spec)
				}
				var channel2 qosscheduler.Channel
				if err := k8sClient.Get(context.Background(), types.NamespacedName{
					Name: "if0-to-workload-" + app1Name, Namespace: namespace}, &channel2); err != nil {
					return err
				}
				if channel2.Spec.TargetPort != 4444 {
					return fmt.Errorf("channel2 should have had a target port of 4444 but its spec is %+v", channel2.Spec)
				}
				return nil
			}, timeout, interval).Should(BeNil())

		})
	})
})
