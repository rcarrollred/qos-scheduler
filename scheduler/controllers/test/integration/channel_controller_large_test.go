// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

var _ = Describe("Channel controller", func() {

	const (
		namespace            = "test-namespace"
		applicationGroupName = "ag"

		timeout  = time.Second * 25
		interval = time.Millisecond * 250
	)
	var (
		applicationName string
		ctx             context.Context
	)

	BeforeEach(func() {
		applicationName = ""
	})

	JustBeforeEach(func() {
		ctx = context.Background()
		if applicationName == "" {
			return
		}
		// This application has to exist, otherwise the channel called
		// this-channel-should-remain will be deleted.
		var otherApp qosscheduler.Application
		err := readApp("other-application", namespace, &otherApp)
		if err != nil {
			x := createApplication("other-application", namespace)
			Expect(k8sClient.Create(ctx, x)).Should(Succeed())
			otherApp = *x
		}

		// Channels connect two applications, or two workloads
		// from the same application.
		targetAppName := fmt.Sprintf("to-%s", applicationName)
		toApp := createApplication(targetAppName, namespace)
		Eventually(func() bool {
			err := k8sClient.Create(ctx, toApp)
			if err == nil {
				return true
			} else {
				return errors.IsAlreadyExists(err)
			}
		}, timeout, interval).Should(BeTrue())
		Eventually(func() qosscheduler.ApplicationPhase {
			err := readApp(targetAppName, namespace, toApp)
			if err != nil {
				return qosscheduler.ApplicationFailing
			}
			return toApp.Status.Phase
		}, timeout, interval).Should(Equal(qosscheduler.ApplicationWaiting))
		Expect(advanceToSchedulingPhase(targetAppName, namespace)).Should(Succeed())

		Eventually(func() qosscheduler.ApplicationPhase {
			err := readApp(targetAppName, namespace, toApp)
			if err != nil {
				return qosscheduler.ApplicationFailing
			}
			return toApp.Status.Phase
		}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationScheduling),
			Equal(qosscheduler.ApplicationPending)))

		app := createApplication(applicationName, namespace)
		Eventually(func() bool {
			err := k8sClient.Create(ctx, app)
			if err == nil {
				return true
			} else {
				return errors.IsAlreadyExists(err)
			}
		}, timeout, interval).Should(BeTrue())
		Eventually(func() qosscheduler.ApplicationPhase {
			err := readApp(applicationName, namespace, app)
			if err != nil {
				return qosscheduler.ApplicationFailing
			}
			return app.Status.Phase
		}, timeout, interval).Should(Equal(qosscheduler.ApplicationWaiting))
		Expect(advanceToSchedulingPhase(applicationName, namespace)).Should(Succeed())

		Eventually(func() qosscheduler.ApplicationPhase {
			err := readApp(applicationName, namespace, app)
			if err != nil {
				return qosscheduler.ApplicationFailing
			}
			return app.Status.Phase
		}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationScheduling),
			Equal(qosscheduler.ApplicationPending)))

		var channel qosscheduler.Channel
		channelName := fmt.Sprintf("%s-channel", applicationName)
		if err = readChannel(channelName, namespace, &channel); err != nil {
			Expect(createChannel(channelName, applicationName, targetAppName, applicationGroupName, namespace)).Should(Succeed())
		}
		channelName = "this-channel-should-remain"
		if err = readChannel(channelName, namespace, &channel); err != nil {
			Expect(createChannel(channelName, otherApp.Name, otherApp.Name, applicationGroupName, namespace)).Should(Succeed())
		}
		channelName = fmt.Sprintf("%s-maybe-cancel", applicationName)
		if err = readChannel(channelName, namespace, &channel); err != nil {
			Expect(createChannel(channelName, applicationName, targetAppName, applicationGroupName, namespace)).Should(Succeed())
		}
	})

	When("an application requires a channel but it is NACKed", func() {

		BeforeEach(func() { applicationName = "app-with-nack-channel" })

		It("must fail", func() {
			channelName := fmt.Sprintf("%s-channel", applicationName)
			var channel qosscheduler.Channel
			Expect(readChannel(channelName, namespace, &channel)).Should(Succeed())
			channel.Status = qosscheduler.ChannelStatus{
				Status: qosscheduler.ChannelRejected,
			}
			Expect(k8sClient.Status().Update(ctx, &channel)).Should(Succeed())
			var app qosscheduler.Application
			Eventually(func() qosscheduler.ApplicationPhase {
				err := readApp(applicationName, namespace, &app)
				if err != nil {
					return qosscheduler.ApplicationRunning
				}
				return app.Status.Phase
			}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationFailing),
				Equal(qosscheduler.ApplicationWaiting)))

			// sometimes you already have both the CANCEL and the NACK and sometimes
			// just the NACK.
			Expect(len(app.Status.Conditions)).Should(Or(Equal(1), Equal(2)))
			Expect(app.Status.Conditions[0].Type).Should(Or(
				Equal(qosscheduler.ApplicationChannelNack),
				Equal(qosscheduler.ApplicationChannelCanceled)))

		})
	})
	When("an application requires a channel and it goes out of qos", func() {

		BeforeEach(func() { applicationName = "app-with-ooqos-channel" })

		It("must fail", func() {
			channelName := fmt.Sprintf("%s-channel", applicationName)
			var channel qosscheduler.Channel
			Expect(readChannel(channelName, namespace, &channel)).Should(Succeed())
			channel.Status = qosscheduler.ChannelStatus{
				Latency: 5,

				Status: qosscheduler.ChannelRunning,
			}
			Expect(k8sClient.Status().Update(ctx, &channel)).Should(Succeed())

			var app qosscheduler.Application
			Expect(readApp(applicationName, namespace, &app)).Should(Succeed())

			Eventually(func() qosscheduler.ApplicationPhase {
				err := readApp(applicationName, namespace, &app)
				if err != nil {
					return qosscheduler.ApplicationPending
				}
				return app.Status.Phase
			}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationFailing),
				Equal(qosscheduler.ApplicationWaiting)))
			Expect(len(app.Status.Conditions)).Should(Or(Equal(1), Equal(2)))
			Expect(app.Status.Conditions[0].Type).Should(Or(
				Equal(qosscheduler.ApplicationChannelQoS),
				Equal(qosscheduler.ApplicationChannelCanceled)))

			// All the pods need to be terminated.
			// This happens for all channel failures, could also verify it in the other
			// test cases.
         podNames := getPodNames(app)
         var pod v1.Pod
			Eventually(func() int {
				podCount := 0
				for _, pn := range podNames {
					if k8sClient.Get(ctx, types.NamespacedName{Name: pn, Namespace: namespace}, &pod) == nil {
						podCount++
					}
				}
				return podCount
			}, timeout, interval).Should(Equal(0))

		})
	})
	When("an application requires a channel that gets deleted", func() {

		BeforeEach(func() { applicationName = "app-with-deleted-channel" })

		It("must not crash", func() {
			// No further guarantees---channels aren't supposed to be deleted while
			// an application group references them.
			channelName := fmt.Sprintf("%s-channel", applicationName)
			var channel qosscheduler.Channel
			Expect(readChannel(channelName, namespace, &channel)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, &channel)).Should(Succeed())

			Eventually(func() qosscheduler.ApplicationPhase {
				var app qosscheduler.Application
				err := readApp(applicationName, namespace, &app)
				if err != nil {
					return qosscheduler.ApplicationFailing
				}
				return app.Status.Phase
			}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationPending),
				Equal(qosscheduler.ApplicationWaiting)))
		})
	})
	When("an application using a channel gets deleted", func() {

		BeforeEach(func() { applicationName = "deleted-app" })

		It("must cancel and delete the channel", func() {
			channelName := fmt.Sprintf("%s-channel", applicationName)
			var channel qosscheduler.Channel
			Expect(readChannel(channelName, namespace, &channel)).Should(Succeed())

			var app qosscheduler.Application
			Expect(readApp(applicationName, namespace, &app)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, &app)).Should(Succeed())

			Eventually(func() qosscheduler.ChannelStatusCode {
				err := readChannel(channelName, namespace, &channel)
				if err != nil {
					if errors.IsNotFound(err) {
						return qosscheduler.ChannelDeleted
					}
					return qosscheduler.ChannelRequested
				}
				return channel.Status.Status
			}, timeout, interval).Should(Equal(qosscheduler.ChannelDeleted))

		})
	})
})
