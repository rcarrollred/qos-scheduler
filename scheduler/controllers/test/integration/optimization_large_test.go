// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

var _ = Describe("Optimization in the applicationgroup controller", func() {
	const (
		timeout   = time.Second * 10
		interval  = time.Millisecond * 250
		namespace = "default"
	)

	var (
		applicationGroupName string
		ctx                  context.Context
	)

	BeforeEach(func() {
		applicationGroupName = ""
	})

	JustBeforeEach(func() {
		if applicationGroupName == "" {
			return
		}
		ctx = context.Background()
		createApplicationGroup(applicationGroupName, namespace)

		appName := fmt.Sprintf("%s-app-1", applicationGroupName)
		app := *application.MakeApplication(appName, namespace, []string{"workload"}, []qosscheduler.WorkloadId{})
		app.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app)

		appName = fmt.Sprintf("%s-app-2", applicationGroupName)
		app = *application.MakeApplication(appName, namespace, []string{"workload"}, []qosscheduler.WorkloadId{})
		app.Labels["application-group"] = applicationGroupName
		k8sClient.Create(ctx, &app)
	})

	AfterEach(func() {
		if applicationGroupName == "" {
			return
		}
		var group qosscheduler.ApplicationGroup
		if err := readGroup(applicationGroupName, namespace, &group); err == nil {
			k8sClient.Delete(ctx, &group)
		}
	})

	When("an Application Group waits for an optimizer result", func() {
		BeforeEach(func() {
			applicationGroupName = "opt-ag-waiting"
		})
		It("should wait for the optimizer assignments", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())
			// Verify the group stays in that phase.
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))
		})
	})

	When("the optimizer returns nonretryable errors", func() {

		BeforeEach(func() {
			applicationGroupName = "opt-ag-nonretryable"
		})

		It("should go to state failure", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())

			// create assignment plan with error messages
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			fmt.Printf("read assignment plan for group %s\n", applicationGroupName)
			plan.Spec.OptimizerReply.ErrorMessages = []*assignment.Reply_Error{{Kind: "sample optimizer error", Details: "sample optimizer error"}}
			plan.Spec.ReplyReceived = true
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())

			Eventually(func() int {
				var planRead qosscheduler.AssignmentPlan
				if err := readPlan(applicationGroupName, namespace, &planRead); err != nil {
					return 0
				}
				return len(planRead.Spec.OptimizerReply.ErrorMessages)
			}, timeout, interval).Should(Equal(1))

			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupOptimizing
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupFailed))
		})

	})

	When("the optimizer returns an empty assignment", func() {

		BeforeEach(func() {
			applicationGroupName = "opt-ag-emptyassignment"
		})

		It("should retry optimizing", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			// create a valid assignment plan with empty assignments.
			ass := assignment.QoSAssignment{
				WorkloadAssignments: []*assignment.QoSAssignment_WorkloadAssignment{}}
			applications := &qosscheduler.ApplicationList{}
			Expect(k8sClient.List(ctx, applications)).Should(Succeed())
			model := &assignment.QoSModel{
				ApplicationModel: application.GetApplicationModel(applications.Items,
					map[qosscheduler.NetworkServiceClass](nwlib.NetworkImplementationClass){}),
				InfrastructureModel: infrastructure.MakeInfrastructureModel(),
			}
			plan.Spec.QoSModel = *model.DeepCopy()
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = true
			plan.Spec.OptimizerReply.QoSAssignment = &ass
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))
		})
	})

	When("the optimizer returns a non-empty valid assignment", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-valid" })

		It("should proceed to scheduling and then to optimizing", func() {
			waitUntilGroupExists("opt-ag-valid", namespace)
			appName := applicationGroupName + "-app-1"
			Eventually(func() error {
				var app qosscheduler.Application
				if err := k8sClient.Get(context.Background(), types.NamespacedName{Name: appName, Namespace: namespace}, &app); err != nil {
					return err
				}
				if app.Status.Phase != "" {
					return nil
				}
				return fmt.Errorf("application %s not ready yet", appName)
			}, timeout, interval).Should(BeNil())

			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))

			// create a valid assignment plan with assignments.
			ass := assignment.QoSAssignment{
				WorkloadAssignments: []*assignment.QoSAssignment_WorkloadAssignment{
					{
						ApplicationId: appName,
						WorkloadId:    appName + "-workload",
						NodeId:        "node-1",
					},
				},
			}
			applications := &qosscheduler.ApplicationList{}
			// This finds applications that this group doesn't control. That's ok
			// here, the code has to deal with the plan having diverged from reality.
			Expect(k8sClient.List(ctx, applications)).Should(Succeed())
			model := &assignment.QoSModel{
				ApplicationModel: application.GetApplicationModel(applications.Items,
					map[qosscheduler.NetworkServiceClass](nwlib.NetworkImplementationClass){}),
				InfrastructureModel: infrastructure.MakeInfrastructureModel(),
			}
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.QoSModel = *model.DeepCopy()
			plan.Spec.OptimizerReply.QoSAssignment = &ass
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = true
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())

			// This verifies that the optimizer plan has been received and turned into
			// a schedule.
			Eventually(func() error {
				if err := readPlan(applicationGroupName, namespace, &plan); err != nil {
					return err
				}
				if len(plan.Status.PodStatus) == 0 {
					return fmt.Errorf("schedule is empty")
				}
				return nil
			}, timeout, interval).Should(BeNil())

			// The plan only contains one of the two apps, so the controller
			// will go from scheduling back to optimizing. Because it has reset
			// the optimizer plan, it will then go back and forth between optimizing
			// and waiting.
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				var group qosscheduler.ApplicationGroup
				if err := readGroup(applicationGroupName, namespace, &group); err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationGroupOptimizing),
				Equal(qosscheduler.ApplicationGroupWaiting)))

			// Even though the optimizer plan has been reset, the schedule (which is used
			// by the scheduler) is not.
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			Expect(plan.Status.PodStatus).ShouldNot(BeEmpty())
		})
	})
	When("the application group has a non-zero retry count and gets a successful schedule", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-reset-attempt-count" })

		It("should reset the retry count and proceed to scheduling and then to waiting", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())
			var group qosscheduler.ApplicationGroup
			Expect(readGroup(applicationGroupName, namespace, &group)).Should(Succeed())
			group.Status.OptimizerAttemptCount++
			group.Status.LastUpdateTime = metav1.Now()
			Expect(k8sClient.Status().Update(context.Background(), &group)).Should(Succeed())

			ass := assignment.QoSAssignment{
				WorkloadAssignments: []*assignment.QoSAssignment_WorkloadAssignment{
					{
						ApplicationId: applicationGroupName + "-app-1",
						WorkloadId:    applicationGroupName + "-app-1-workload",
						NodeId:        "node-1",
					},
					{
						ApplicationId: applicationGroupName + "-app-2",
						WorkloadId:    applicationGroupName + "-app-2-workload",
						NodeId:        "node-1",
					},
				},
			}
			applications := &qosscheduler.ApplicationList{}
			Expect(k8sClient.List(ctx, applications)).Should(Succeed())
			model := &assignment.QoSModel{
				ApplicationModel: application.GetApplicationModel(applications.Items,
					map[qosscheduler.NetworkServiceClass](nwlib.NetworkImplementationClass){}),
				InfrastructureModel: infrastructure.MakeInfrastructureModel(),
			}
			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.QoSModel = *model.DeepCopy()
			plan.Spec.OptimizerReply.QoSAssignment = &ass
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = true
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())
			Eventually(func() int {
				err := readGroup(applicationGroupName, namespace, &group)
				if err != nil {
					return -1
				}
				return group.Status.OptimizerAttemptCount
			}, time.Second*60, interval).Should(Equal(0))
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := readGroup(applicationGroupName, namespace, &group)
				if err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Or(Equal(qosscheduler.ApplicationGroupWaiting),
				Equal(qosscheduler.ApplicationGroupScheduling)))
		})
	})
	When("the application group gets a retryable failure", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-retryable" })

		It("should increment the retry count, send a new optimizer request, and return to Optimizing", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())

			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = false
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())

			Eventually(func() bool {
				err := readPlan(applicationGroupName, namespace, &plan)
				if err != nil {
					return false
				}
				if plan.Spec.OptimizerReply.Feasible {
					return false
				}
				return true
			}, timeout, interval).Should(Equal(true))

			var group qosscheduler.ApplicationGroup
			Eventually(func() int {
				err := readGroup(applicationGroupName, namespace, &group)
				if err != nil {
					return 0
				}
				return group.Status.OptimizerAttemptCount
			}, time.Second*30, interval).Should(Equal(1))

			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := readGroup(applicationGroupName, namespace, &group)
				if err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupOptimizing))

			// This is how you can see the optimizer parameters were modified for the retry.
			Eventually(func() int {
				if emptyOptimizer.LastCall[applicationGroupName] == nil {
					return 0
				}
				return int(emptyOptimizer.LastCall[applicationGroupName].TimeLimit)
			}, timeout, interval).Should(Equal(4))
		})
	})
	When("the application group gets a retryable failure but is out of retries", func() {
		BeforeEach(func() { applicationGroupName = "opt-ag-out-of-retries" })

		It("should go to state failed", func() {
			Eventually(func() error {
				return awaitOptimizingPhase(applicationGroupName, namespace)
			}, timeout, interval).Should(BeNil())

			var group qosscheduler.ApplicationGroup
			Expect(readGroup(applicationGroupName, namespace, &group)).Should(Succeed())
			group.Status.OptimizerAttemptCount = 1
			group.Status.LastUpdateTime = metav1.Now()
			Expect(k8sClient.Status().Update(context.Background(), &group)).Should(Succeed())

			var plan qosscheduler.AssignmentPlan
			Expect(readPlan(applicationGroupName, namespace, &plan)).Should(Succeed())
			plan.Spec.ReplyReceived = true
			plan.Spec.OptimizerReply.Feasible = false
			Expect(k8sClient.Update(ctx, &plan)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := readGroup(applicationGroupName, namespace, &group)
				if err != nil {
					return qosscheduler.ApplicationGroupWaiting
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupFailed))
			Expect(group.Status.OptimizerAttemptCount).Should(Equal(1))
		})
	})
	When("the optimizer never returns", func() {
		var timeoutName string
		BeforeEach(func() {
			applicationGroupName = "" // avoid the normal setup that creates apps.
			timeoutName = "opt-ag-timeout"
			ctx = context.Background()
			Expect(createApplicationGroup(timeoutName, namespace)).Should(Succeed())
		})
		It("should go back to waiting", func() {
			waitUntilGroupExists("opt-ag-timeout", namespace)
			var group qosscheduler.ApplicationGroup
			Expect(readGroup(timeoutName, namespace, &group)).Should(Succeed())
			group.Status.LastUpdateTime = metav1.Now()
			Expect(k8sClient.Status().Update(context.Background(), &group)).Should(Succeed())
			Eventually(func() qosscheduler.ApplicationGroupPhase {
				err := readGroup(timeoutName, namespace, &group)
				if err != nil {
					return qosscheduler.ApplicationGroupFailed
				}
				return group.Status.Phase
			}, timeout, interval).Should(Equal(qosscheduler.ApplicationGroupWaiting))
		})
	})
})
