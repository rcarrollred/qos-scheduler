// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build integration
// +build integration

package integration

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
//	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
//	ctrl "sigs.k8s.io/controller-runtime"

	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
//	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

var _ = Describe("IPAM controller", func() {

	const (
		namespace = "test-namespace"

		timeout  = time.Second * 60
		interval = time.Millisecond * 250
	)
	var (
		ctx                  context.Context
		ipamName string
	)

	BeforeEach(func() {
		ipamName = ""
	})

	JustBeforeEach(func() {
		ctx = context.Background()
		createIpam(ipamName, namespace, 4)
	})

	When("an ipam is created", func() {
		BeforeEach(func() {
			ipamName = "ipam-basic"
		})
		It("must reserve an ip range", func() {
			var ipam qosscheduler.Ipam
			Eventually(func() bool {
				err := k8sClient.Get(ctx, types.NamespacedName{Name: ipamName, Namespace: namespace}, &ipam)
            if err != nil { return false }
				return ipam.Status.StatusCode == qosscheduler.IpamReserved && ipam.Status.IPv4Cidr != ""
			}, timeout, interval).Should(Equal(true))

		})
	})
})
