// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"context"
	"fmt"
	"strings"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func populateAssignmentsInPlan(reply *application.OptimizerReply, ap *crd.AssignmentPlan) {
	if ap.Status.PodStatus == nil {
		ap.Status.PodStatus = make(map[string]crd.AssignmentPlanStatusEntry)
	}
	podChannelsMap := reply.GetPodChannelsMap()

	for _, workloadAssignment := range reply.Assignment.WorkloadAssignments {
		pk := fmt.Sprintf("%s/%s", ap.Namespace, workloadAssignment.WorkloadId)
		channels, ok := podChannelsMap[workloadAssignment.WorkloadId]
		if !ok || channels == nil {
			channels = []string{}
		}
		statusEntry := crd.AssignmentPlanStatusEntry{
			PreferredNode: workloadAssignment.NodeId,
			Channels:      channels,
		}
		ap.Status.PodStatus[pk] = statusEntry
	}
}

// GetApplicationGroup returns the name of the application group in charge
// of reconciling an object, if any.
func GetApplicationGroup(o client.Object) string {
	// Once an application group has reached its quorum, it takes ownership of
	// the applications in it. Thus even if you remove the application-group label
	// after this point, the application group can still see the application.
	controller := metav1.GetControllerOf(o)
	if controller != nil {
		return controller.Name
	}
	labels := o.GetLabels()
	agname, exists := labels["application-group"]
	if !exists {
		return ""
	}
	return agname
}

func (r *ApplicationReconciler) getPods(ctx context.Context, app *crd.Application) (*v1.PodList, error) {
	pods := &v1.PodList{}
	err := r.List(ctx, pods, client.InNamespace(app.Namespace),
		client.MatchingFields{".metadata.controller": app.Name})
	if err != nil {
		return nil, err
	}
	return pods, nil
}

func (r *ApplicationReconciler) getChannels(ctx context.Context, app string, namespace string) (*crd.ChannelList, error) {
	channels := &crd.ChannelList{}
	err := r.List(ctx, channels, client.InNamespace(namespace), client.MatchingFields{".metadata.Application": app})
	if err != nil {
		return nil, err
	}
	return channels, nil
}

// A channel is ready for deletion when both its endpoint applications are missing
// or in state Failing.
func (r *ApplicationReconciler) isChannelReadyForDeletion(ctx context.Context, channel *crd.Channel) (bool, error) {
	var app crd.Application
	sourceApp := infrastructure.ChannelSourceApplication(channel)
	if sourceApp != "" {
		err := r.Get(ctx, types.NamespacedName{Name: sourceApp, Namespace: channel.Namespace}, &app)
		if err != nil {
			if !errors.IsNotFound(err) {
				return false, err
			}
		} else {
			if app.Status.Phase != crd.ApplicationFailing {
				return false, nil
			}
		}
	}
	targetApp := infrastructure.ChannelTargetApplication(channel)
	if targetApp != "" {
		err := r.Get(ctx, types.NamespacedName{Name: targetApp, Namespace: channel.Namespace},
			&app)
		if err != nil {
			if !errors.IsNotFound(err) {
				return false, err
			}
		} else {
			if app.Status.Phase != crd.ApplicationFailing {
				return false, nil
			}
		}
	}
	return true, nil
}

func (r *ApplicationReconciler) recordFailures(ctx context.Context,
	app *crd.Application, conditions []crd.ApplicationCondition) {

	if len(conditions) == 0 {
		return
	}
	var record strings.Builder
	for _, c := range conditions {
		fmt.Fprintf(&record, "%s was in state %s (reason: %s); ", c.Entity, c.Type, c.Reason)
	}
	r.Recorder.Event(app, "Normal", "Channel or pod failure", record.String())
}

// This makes it so you can tell which application conditions are from the most
// recent run (those are the only ones whose status is true).
func (r *ApplicationReconciler) resetApplicationConditions(ctx context.Context, app *crd.Application) error {
	for _, c := range app.Status.Conditions {
		if c.Status == v1.ConditionTrue {
			c.Status = v1.ConditionFalse
		}
	}
	return nil
}
