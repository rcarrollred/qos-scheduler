// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Collect metrics on custom resources.
// For now, this just counts objects by namespace and state.

package controllers

import (
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

// Arrays are never const, but this is the next best thing.
var applicationGroupPhases = [...]crd.ApplicationGroupPhase{
	crd.ApplicationGroupWaiting,
	crd.ApplicationGroupOptimizing,
	crd.ApplicationGroupScheduling,
	crd.ApplicationGroupFailed}

var applicationPhases = [...]crd.ApplicationPhase{
	crd.ApplicationWaiting,
	crd.ApplicationScheduling,
	crd.ApplicationPending,
	crd.ApplicationRunning,
	crd.ApplicationFailing,
}

var channelClusterStatus = [...]crd.ChannelStatusCode{
	crd.ChannelRequested,
	crd.ChannelReviewed,
	crd.ChannelImplementing,
	crd.ChannelImplemented,
	crd.ChannelRunning,
	crd.ChannelRejected,
	crd.ChannelCanceled,
	crd.ChannelOOQOS,
	crd.ChannelDeleted,
}

func exportApplicationGroupMetrics(ag crd.ApplicationGroup) {
	for _, p := range applicationGroupPhases {
		if ag.Status.Phase == p {
			applicationGroups.WithLabelValues(ag.Namespace, ag.Name, string(p)).Set(float64(1))
		} else {
			applicationGroups.WithLabelValues(ag.Namespace, ag.Name, string(p)).Set(float64(0))
		}
	}
}

func recordApplicationGroupDeleted(name string, namespace string) {
	for _, p := range applicationGroupPhases {
		applicationGroups.WithLabelValues(namespace, name, string(p)).Set(float64(0))
	}
}

func exportApplicationMetrics(a crd.Application) {
	for _, p := range applicationPhases {
		if a.Status.Phase == p {
			applications.WithLabelValues(a.Namespace, a.Name, string(p)).Set(float64(1))
		} else {
			applications.WithLabelValues(a.Namespace, a.Name, string(p)).Set(float64(0))
		}
	}
}

func recordApplicationDeleted(name string, namespace string) {
	for _, p := range applicationPhases {
		applications.WithLabelValues(namespace, name, string(p)).Set(float64(0))
	}
}

func exportChannelMetrics(s crd.Channel) {
	for _, cs := range channelClusterStatus {
		if s.Status.Status == cs {
			channels.WithLabelValues(s.Namespace, s.Name, string(s.Spec.ServiceClass), string(cs)).Set(float64(1))
		}
	}
}

func recordChannelDeleted(name string, namespace string) {
	for _, cs := range channelClusterStatus {
		channels.WithLabelValues(namespace, name, "-1", string(cs)).Set(float64(0))
	}
}
