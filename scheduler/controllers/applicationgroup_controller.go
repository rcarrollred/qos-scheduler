// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package controllers contains the applicationgroup and application operators.
package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	oclient "siemens.com/qos-scheduler/scheduler/optimizer/client"
)

const (
	applicationGroupKey = ".metadata.application-group"
)

// ApplicationGroupReconciler reconciles an ApplicationGroup object
type ApplicationGroupReconciler struct {
	client.Client
	Log                  logr.Logger
	Scheme               *runtime.Scheme
	Channels             *ChannelReconciler
	Reader               client.Reader
	Recorder             record.EventRecorder
	QosImplementationMap map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass)
	// If true, nodes with the master taint will be passed to the optimizer.
	// If you do that, all application workloads that you schedule need to
	// tolerate the master taint.
	ScheduleOnMaster bool
	// Compat mode creates an infrastructure model that is compatible with the old (cpp) optimizer.
	// The old infrastructure models have at most one link between any pair of nodes, they do not
	// support multiple links with different network labels.
	// compat mode is for testing only, the results will not be what you expect when you
	// have more than one network.
	OptimizerCompatMode bool
	OptimizerClient     oclient.Optimizer
}

func (r *ApplicationGroupReconciler) updateStatus(ctx context.Context,
	newPhase qosschedulerv1alpha1.ApplicationGroupPhase,
	appGroup *qosschedulerv1alpha1.ApplicationGroup) error {

	if appGroup.Status.Phase == newPhase {
		return nil
	}

	var freshAg qosschedulerv1alpha1.ApplicationGroup
	err := r.Reader.Get(ctx, types.NamespacedName{Name: appGroup.Name, Namespace: appGroup.Namespace}, &freshAg)
	if err != nil {
		r.Log.Error(err, "unable to read application")
		return err
	}

	if freshAg.Status.Phase == newPhase {
		return nil
	}

	if !application.IsGroupPhaseChangePermitted(freshAg.Status.Phase, newPhase) {
		err := fmt.Errorf("changing status from %s to %s is not possible",
			string(freshAg.Status.Phase), string(newPhase))
		r.Log.Error(err, "Unable to update application group status")
		return err
	}

	oldPhase := freshAg.Status.Phase
	freshAg.Status.Phase = newPhase
	freshAg.Status.LastUpdateTime = metav1.Now()
	if err := r.Status().Update(ctx, &freshAg); err != nil {
		return fmt.Errorf("cannot change ag %s from state %s to %s: %v", freshAg.Name, oldPhase, newPhase, err)
	}
	r.Recorder.Event(&freshAg, "Normal", "phase change", fmt.Sprintf("changed phase from %s to %s", oldPhase, newPhase))
	return nil
}

func (r *ApplicationGroupReconciler) processOptimizingState(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup) (qosschedulerv1alpha1.ApplicationGroupPhase, int64, applyStateTransitionFn, error) {

	ap, err := r.getAssignmentPlan(ctx, appGroup.Name, appGroup.Namespace)
	if err != nil {
		return appGroup.Status.Phase, 0, nopStateTransitionFunction, err
	}

	reply := application.FromAssignments(ap)

	var optMsg string
	if len(ap.Spec.OptimizerReply.ErrorMessages) > 0 {
		optMsg = fmt.Sprintf("reason: %s (%v)", reply.Reason, reply.Errors)
	} else {
		optMsg = reply.Reason
	}

	switch reply.RetryCode {

	case application.NORETRYOK:
		return qosschedulerv1alpha1.ApplicationGroupScheduling, 0, makeGroupApplyFn(ctx, appGroup, &reply, r.applyOptimizerPlan), nil

	case application.WAIT:
		timeSpentWaiting := time.Since(appGroup.Status.LastUpdateTime.Time)
		timelimit := appGroup.Spec.OptimizerRetryPolicy.InitialTimeLimit * 3
		if timelimit <= 0 {
			timelimit = 60
		}
		r.Log.Info("seconds spent waiting for optimizer so far, limit", "time", timeSpentWaiting.Seconds(), "limit", timelimit)
		if timeSpentWaiting.Seconds() > float64(timelimit) {
			optimizerTimeouts.WithLabelValues(appGroup.Namespace, appGroup.Name).Inc()
			r.Recorder.Event(appGroup, "Warning", "optimizer timeout", "timed out waiting for optimizer")
			// Send a cancellation for this request.
			r.Log.Info("canceling pending optimizer request", "appGroup", appGroup.Name)
			err := r.OptimizerClient.Cancel(appGroup.Name, appGroup.Namespace)
			if err != nil {
				r.Log.Error(err, "cancellation attempt returned error")
			}
			return qosschedulerv1alpha1.ApplicationGroupWaiting, 60, nopStateTransitionFunction, nil
		}
		// This means the optimizer isn't done optimizing yet. Retry with timeout.
		r.Log.Info("optimizer assignment is not done yet, wait", "ag", appGroup.Name)
		return qosschedulerv1alpha1.ApplicationGroupOptimizing, 2, nopStateTransitionFunction, silentError{}

	case application.RETRYMOD:
		optimizerFailures.WithLabelValues(appGroup.Namespace, appGroup.Name).Inc()
		if !application.CanRetryOptimizing(appGroup) {
			r.Recorder.Event(appGroup, "Warning", "out of retries", fmt.Sprintf("out of retries (max %d, retry caused by %s)", appGroup.Spec.OptimizerRetryPolicy.MaxRetries, optMsg))
			return qosschedulerv1alpha1.ApplicationGroupFailed, 2, nopStateTransitionFunction, nil
		}
		r.Recorder.Event(appGroup, "Normal", "optimizer retry", optMsg)
		optimizerRetries.WithLabelValues(appGroup.Namespace, appGroup.Name).Inc()
		if err := r.prepareOptimizerRetry(ctx, appGroup); err != nil {
			return appGroup.Status.Phase, 2, nopStateTransitionFunction, err
		}
		return qosschedulerv1alpha1.ApplicationGroupWaiting, 5, nopStateTransitionFunction, nil

	case application.NORETRYFAILED:
		optimizerFailures.WithLabelValues(appGroup.Namespace, appGroup.Name).Inc()
		r.Recorder.Event(appGroup, "Warning", "nonretryable optimizer failures", optMsg)
		return qosschedulerv1alpha1.ApplicationGroupFailed, 0, nopStateTransitionFunction, nil

	default:
		r.Log.Error(fmt.Errorf("mystery return value %v from optimizer", reply.RetryCode), "application group failed")
		return qosschedulerv1alpha1.ApplicationGroupFailed, 0, nopStateTransitionFunction, nil
	}
}

func (r *ApplicationGroupReconciler) applyOptimizerPlan(ctx context.Context, appGroup *qosschedulerv1alpha1.ApplicationGroup, reply *application.OptimizerReply) error {

	if reply == nil || reply.RetryCode != application.NORETRYOK {
		return fmt.Errorf("applyOptimizerPlan called even though reply was %v", reply)
	}
	appGroup.Status.OptimizerAttemptCount = 0
	if err := r.Status().Update(ctx, appGroup); err != nil {
		r.Log.Info("failed to update status", "err", err)
		return err
	}

	// Requesting the channels during a retry will reset the
	// status of previously-failed channels to REQUESTED.
	// This has to happen before publishWorkloadAssignments,
	// as the latter will put the applications back into their
	// Scheduling phases. At that point, the channels they need
	// must have been re-requested already, otherwise the applications
	// will go straight back to ChannelFailed.
	err := r.initializeChannels(ctx, appGroup, reply.GetChannels())
	if err != nil {
		return err
	}

	err = r.publishWorkloadAssignments(ctx, appGroup, reply)
	r.Log.Info("publishing workload assignments", "err", err, "ag", appGroup.Name)
	if err != nil {
		return err
	}

	return nil
}

// Initialize the channel CRDs and send them to the channel controller.
// The channel controller will fill in further data and then save the
// channel objects in Kubernetes. Further processing will depend
// on their implementation type.
func (r *ApplicationGroupReconciler) initializeChannels(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup,
	channels []*qosschedulerv1alpha1.Channel) error {

	r.Log.Info("initializing channels", "channelCount", len(channels), "applicationgroup", appGroup.Name)

	for _, channel := range channels {
		channel.Namespace = appGroup.Namespace
		channel.Labels["application-group"] = appGroup.Name
	}
	return r.Channels.createChannels(ctx, channels)
}

func (r *ApplicationGroupReconciler) publishWorkloadAssignments(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup, reply *application.OptimizerReply) error {

	applications, err := r.getApplications(ctx, appGroup.Namespace, appGroup.Name)
	if err != nil {
		return err
	}
	ap, err := r.getAssignmentPlan(ctx, appGroup.Name, appGroup.Namespace)
	if err != nil {
		return err
	}

	// Write the whole plan (assignments and channel requests) to the assignment plan. The channel requirements
	// are important. Without them, pods would start running before their channels
	// are ready.
	populateAssignmentsInPlan(reply, ap)
	err = r.Status().Update(ctx, ap)
	if err != nil {
		r.Log.Error(err, "Failed to update assignment plan")
		return err
	}

	var freshApp qosschedulerv1alpha1.Application
	for _, app := range applications.Items {
		// Applications with no plan entry will be left Waiting.
		for pruned := range reply.GetPrunedApplications() {
			if pruned == app.Name {
				r.Reader.Get(ctx, types.NamespacedName{Name: app.Name, Namespace: app.Namespace}, &freshApp)
				if freshApp.Status.Phase == qosschedulerv1alpha1.ApplicationWaiting {
					freshApp.Status.Phase = qosschedulerv1alpha1.ApplicationScheduling
					if err := r.Status().Update(ctx, &freshApp); err != nil {
						r.Log.Error(err, "Failed to update application status to scheduling")
						// continue, try to update other applications
						// If the optimizer gets called again, those applications that remain
						// Waiting now will get picked up then.
					}
					r.Recorder.Event(&freshApp, "Normal", "phase change", "changed from Waiting to Scheduling")
				}
			}
		}
	}
	return nil
}

func (r *ApplicationGroupReconciler) processWaitingState(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup) (
	qosschedulerv1alpha1.ApplicationGroupPhase, applyStateTransitionFn, error) {

	// See if any other application group is waiting for the optimizer.
	otherApplicationGroups := &qosschedulerv1alpha1.ApplicationGroupList{}
	err := r.List(ctx, otherApplicationGroups)
	if err != nil {
		r.Log.Info("failed to look up other application groups", "err", err)
	} else {
		for _, ag := range otherApplicationGroups.Items {
			if ag.Name == appGroup.Name && ag.Namespace == appGroup.Namespace {
				continue
			}
			if ag.Status.Phase == qosschedulerv1alpha1.ApplicationGroupOptimizing {
				r.Recorder.Event(appGroup, "Normal", "requeue", fmt.Sprintf("application group %s/%s is currently optimizing, so I will requeue", ag.Namespace, ag.Name))
				return appGroup.Status.Phase, nopStateTransitionFunction,
					fmt.Errorf("waiting for other application group to finish optimizing")
			}
		}
	}

	applications, err := r.getApplications(ctx, appGroup.Namespace, appGroup.Name)
	if err != nil {
		return appGroup.Status.Phase, nopStateTransitionFunction, err
	}

	if !application.IsGroupReady(appGroup, applications.Items) {
		return appGroup.Status.Phase, nopStateTransitionFunction, nil
	}

	return qosschedulerv1alpha1.ApplicationGroupOptimizing,
		makeGroupApplyFn(ctx, appGroup, nil, r.prepareOptimizerCall), nil
}

func (r *ApplicationGroupReconciler) prepareOptimizerCall(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup, _ *application.OptimizerReply) error {

	applications, err := r.getApplications(ctx, appGroup.Namespace, appGroup.Name)
	if err != nil {
		return err
	}

	// Take ownership of the applications here.
	for _, app := range applications.Items {
		controller := metav1.GetControllerOf(&app)
		if controller != nil && controller.Name == appGroup.Name {
			continue
		}
		var freshApp qosschedulerv1alpha1.Application
		err = r.Reader.Get(ctx, types.NamespacedName{Name: app.Name, Namespace: app.Namespace}, &freshApp)
		if err != nil {
			r.Log.Error(err, "Failed to re-read application for ownership update")
			return err
		}
		err = ctrl.SetControllerReference(appGroup, &freshApp, r.Scheme)
		if err != nil {
			r.Log.Error(err, "Failed to take ownership of application")
			return err
		}
		err = r.Update(ctx, &freshApp)
		if err != nil {
			r.Log.Error(err, "Failed to take ownership of application")
			return err
		}
	}

	// Prepare input data for the optimizer.
	// Get an inventory of workloads (pods and network channels) across all
	// waiting applications belonging to this group.
	applicationModel := application.GetApplicationModel(applications.Items, r.QosImplementationMap)

	// This is just for information right now.
	for _, app := range applications.Items {
		if !application.IsEligibleForScheduling(&app) {
			continue
		}
		for _, c := range app.Status.Conditions {
			if c.Status == corev1.ConditionTrue {
				r.Log.Info("should take app failure condition into account", "appgroup",
					appGroup.Name, "app", app.Name, "reason", c.Reason)
			}
		}
	}

	// Take a snapshot of the network and nodes before calling the optimizer.
	// This is where there's a possibility for a race condition if you
	// schedule two application groups at the same time.
	nodes := &corev1.NodeList{}
	err = r.List(ctx, nodes)
	if err != nil {
		return err
	}
	paths := &qosschedulerv1alpha1.NetworkPathList{}
	err = r.List(ctx, paths)
	if err != nil {
		return err
	}
	links := &qosschedulerv1alpha1.NetworkLinkList{}
	err = r.List(ctx, links)
	if err != nil {
		return err
	}
	infraModel, err := infrastructure.BuildInfrastructureModel(nodes.Items, paths.Items, links.Items, r.ScheduleOnMaster, r.OptimizerCompatMode)
	if err != nil {
		r.Log.Error(err, "Failed to build infrastructure model")
		return err
	}
	if r.OptimizerCompatMode {
		r.Log.Info("Running in optimizer compat mode. Only one link will be used between any pair of nodes. This mode is for testing only.")
	}

	qosModel := &assignment.QoSModel{
		InfrastructureModel: infraModel,
		ApplicationModel:    applicationModel,
	}
	application.NewLimitsForOptimizerRequest(appGroup, qosModel)
	return r.populateOptimizerRequest(ctx, appGroup, qosModel)
}

func (r *ApplicationGroupReconciler) processSchedulingState(ctx context.Context, appGroup *qosschedulerv1alpha1.ApplicationGroup) (qosschedulerv1alpha1.ApplicationGroupPhase, applyStateTransitionFn, error) {
	applications, err := r.getApplications(ctx, appGroup.Namespace, appGroup.Name)
	if err != nil {
		return appGroup.Status.Phase, nopStateTransitionFunction, err
	}

	newPhase := application.NewPhaseFromApplications(appGroup.Status.Phase, applications.Items)
	return newPhase, nopStateTransitionFunction, nil
}

// When you return an error from Reconcile() it gets logged rather verbosely.
// Sometimes I want to return an error from an ApplyFn just to signal that a retry
// is necessary but I need no further logging. That's what a silentError does.
type silentError struct{}

func (s silentError) Error() string { return "error for triggering retries only" }

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applicationgroups,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applicationgroups/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applicationgroups/finalizers,verbs=update
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applications,verbs=get;list;watch;update
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applications/status,verbs=get;list;watch;update
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=assignmentplans,verbs=create;get;list;watch;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkpaths,verbs=get;list;watch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networklinks,verbs=get;list;watch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkendpoints,verbs=get;list
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=assignmentplans/status,verbs=get;list;update
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch;update
//+kubebuilder:rbac:groups="",resources=nodes,verbs=get;list

// Reconcile is part of the main Kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *ApplicationGroupReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("applicationgroup", req.NamespacedName)

	var err error
	// Look up the application group that the current notification is for.
	appGroup := &qosschedulerv1alpha1.ApplicationGroup{}
	err = r.Get(ctx, req.NamespacedName, appGroup)
	if err != nil {
		if errors.IsNotFound(err) {
			recordApplicationGroupDeleted(req.NamespacedName.Name, req.NamespacedName.Namespace)
			// Cancel outstanding optimization requests if any.
			r.OptimizerClient.Cancel(req.NamespacedName.Name, req.NamespacedName.Namespace)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if appGroup.Status.Phase == "" {
		err = r.updateStatus(ctx, qosschedulerv1alpha1.ApplicationGroupWaiting, appGroup)
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	exportApplicationGroupMetrics(*appGroup)

	var newState qosschedulerv1alpha1.ApplicationGroupPhase
	var callFn applyStateTransitionFn
	var requeueDelay int64

	switch appGroup.Status.Phase {

	// Permanent failure is a terminal state.
	// The application group does not cancel apps when it enters the failing state.
	// It just gives up on scheduling new ones.
	case qosschedulerv1alpha1.ApplicationGroupFailed:
		return ctrl.Result{}, nil

	case qosschedulerv1alpha1.ApplicationGroupWaiting:
		newState, callFn, err = r.processWaitingState(ctx, appGroup)

	case qosschedulerv1alpha1.ApplicationGroupOptimizing:
		newState, requeueDelay, callFn, err = r.processOptimizingState(ctx, appGroup)
		if newState != qosschedulerv1alpha1.ApplicationGroupOptimizing {
			r.Log.Info("processOptimizing call returned, entering new state", "err", err, "ag", appGroup.Name, "newstate", newState)
		}

	case qosschedulerv1alpha1.ApplicationGroupScheduling:
		newState, callFn, err = r.processSchedulingState(ctx, appGroup)

	default:
		log.Info("Unrecognized phase in application group", "phase", appGroup.Status.Phase)
		return ctrl.Result{}, nil
	}
	requeueAfter := time.Second * time.Duration(requeueDelay)

	if err != nil {
		if _, ok := err.(silentError); ok {
			return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, nil
		}
		return ctrl.Result{RequeueAfter: requeueAfter}, err
	}
	err = callFn()
	if err != nil {
		r.Log.Info("callFn returned error", "err", err, "ag", appGroup.Name)
		if _, ok := err.(silentError); ok {
			return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, nil
		}
		return ctrl.Result{RequeueAfter: requeueAfter}, err
	}
	ret := r.updateStatus(ctx, newState, appGroup)
	return ctrl.Result{}, ret
}

// SetupWithManager sets up the controller with the Manager.
func (r *ApplicationGroupReconciler) SetupWithManager(mgr ctrl.Manager) error {

	// Index applications by their application group if they have one.
	if err := mgr.GetFieldIndexer().IndexField(context.Background(),
		&qosschedulerv1alpha1.Application{},
		applicationGroupKey,
		func(app client.Object) []string {
			agname := GetApplicationGroup(app)
			if agname == "" {
				return nil
			}
			return []string{agname}
		}); err != nil {
		return err
	}

	// Map the watch events (which refer to Applications) to keys of ApplicationGroups
	// (because that's what this controller operates on).
	mapFn := func(a client.Object) []reconcile.Request {
		applicationGroupName := GetApplicationGroup(a)
		if applicationGroupName != "" {
			return []reconcile.Request{
				{NamespacedName: types.NamespacedName{
					Name:      applicationGroupName,
					Namespace: a.GetNamespace(),
				}},
			}
		} else {
			// The application doesn't specify an application group, so it's not
			// subject to coscheduling. Nothing for the group reconciler to do.
			// You could consider applications like this part of a trivial group,
			// but it seems like an unnecessary complication.
			// This does mean applications without a group never
			// end up doing anything.
			return []reconcile.Request{}
		}
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&qosschedulerv1alpha1.ApplicationGroup{}).
		Watches(&source.Kind{Type: &qosschedulerv1alpha1.Application{}},
			handler.EnqueueRequestsFromMapFunc(mapFn)).
		Complete(r)
}
