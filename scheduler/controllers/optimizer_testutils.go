// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

//go:build mock
// +build mock

package controllers

import (
	qosscheduler "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

func makeApplicationModel() *assignment.ApplicationModel {
	apps := make([]qosscheduler.Application, 2)
	apps[0] = *application.MakeApplication("app1", "default", []string{"wl1-1", "wl1-2"},
		[]qosscheduler.WorkloadId{{Basename: "wl3-1", ApplicationName: "app2"},
			{Basename: "wl1-2", ApplicationName: "app1"}})
	apps[1] = *application.MakeApplication("app2", "default", []string{"wl3-1"},
		[]qosscheduler.WorkloadId{})

	return application.GetApplicationModel(apps, map[qosscheduler.NetworkServiceClass](nwlib.NetworkImplementationClass){})
}

// MakeQoSModel creates a QoSModel for testing.
func MakeQoSModel() *assignment.QoSModel {
	return &assignment.QoSModel{
		InfrastructureModel: infrastructure.MakeInfrastructureModel(),
		ApplicationModel:    makeApplicationModel(),
	}
}

// MakeQoSModelFromTextfiles reads data from .txt files.
func MakeQoSModelFromTextfiles(dirname string) *assignment.QoSModel {
	infraModel, _ := infrastructure.ReadInfrastructureModelFromTextfiles(dirname)
	return &assignment.QoSModel{
		InfrastructureModel: infraModel,
		ApplicationModel:    application.ReadApplicationModelFromTextfiles(dirname),
	}
}
