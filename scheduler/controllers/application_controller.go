// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
)

// ApplicationReconciler reconciles an Application object
type ApplicationReconciler struct {
	client.Client
	Log           logr.Logger
	Scheme        *runtime.Scheme
	SchedulerName string
	Reader        client.Reader
	Recorder      record.EventRecorder
}

func (r *ApplicationReconciler) updateStatus(ctx context.Context,
	newStatus crd.ApplicationStatus, app *crd.Application) error {

	if app.Status.Phase == newStatus.Phase {
		return nil
	}

	var freshApp crd.Application
	r.Reader.Get(ctx, types.NamespacedName{Name: app.Name, Namespace: app.Namespace}, &freshApp)

	if !application.IsPhaseChangePermitted(app.Status.Phase, newStatus.Phase) {
		return fmt.Errorf("changing status from %s to %s is not possible",
			string(app.Status.Phase), string(newStatus.Phase))
	}

	if freshApp.Status.Phase == newStatus.Phase {
		return nil
	}

	oldPhase := freshApp.Status.Phase
	freshApp.Status.Phase = newStatus.Phase

	// Append conditions. Merging would also be an option, though it doesn't seem
	// to be supported for custom resources.
	if len(newStatus.Conditions) > 0 {
		if len(freshApp.Status.Conditions) == 0 {
			freshApp.Status.Conditions = newStatus.Conditions
		} else {
			freshApp.Status.Conditions = append(freshApp.Status.Conditions, newStatus.Conditions...)
		}
	}
	if err := r.Status().Update(ctx, &freshApp); err != nil {
		return err
	}
	if len(newStatus.Conditions) > 0 {
		r.recordFailures(ctx, &freshApp, newStatus.Conditions)
	}
	r.Recorder.Event(&freshApp, "Normal", "phase change", fmt.Sprintf("changed from %s to %s",
		oldPhase, newStatus.Phase))
	return nil
}

func (r *ApplicationReconciler) cancelChannels(ctx context.Context, app string, namespace string) error {
	// This finds both outgoing and incoming channels for this application.
	channels, err := r.getChannels(ctx, app, namespace)
	if err != nil {
		return err
	}
	for _, channel := range channels.Items {
		if infrastructure.CancelChannel(&channel) {
			r.Log.Info("canceling channel because application is going down", "channel", channel.Name)
			if err := r.Status().Update(ctx, &channel); err != nil {
				r.Log.Error(err, "Failed to cancel channel")
				// try canceling the other channels anyway
				continue
			}
		}
	}
	return nil
}

func (r *ApplicationReconciler) deleteAppChannels(ctx context.Context, app *crd.Application) error {
	r.Recorder.Event(app, "Normal", "deleting channels", "Deleting channels after cancelling them")
	return r.deleteChannels(ctx, app.Name, app.Namespace)
}

func (r *ApplicationReconciler) deleteChannels(ctx context.Context, app string, namespace string) error {
	channels, err := r.getChannels(ctx, app, namespace)
	if err != nil {
		return err
	}
	for _, channel := range channels.Items {
		var channelToDelete crd.Channel
		r.Reader.Get(ctx, types.NamespacedName{Name: channel.Name, Namespace: channel.Namespace}, &channelToDelete)
		ready, err := r.isChannelReadyForDeletion(ctx, &channelToDelete)
		if err != nil {
			continue
		}
		if !ready {
			r.Log.Info("app asked for channel to be deleted but it is not ready yet", "app", app, "channel", channel.Name)
			continue
		}
		if infrastructure.DeleteChannel(&channelToDelete) {
			r.Recorder.Event(&channelToDelete, "Normal", "deleting channel", "Deleting channel because its endpoint applications have terminated or failed")
			if err := r.Status().Update(ctx, &channelToDelete); err != nil {
				r.Log.Error(err, "Failed to delete channel")
				// try to delete the other channels anyway
				continue
			}
		} else {
			// This should not be reachable.
			r.Log.Info("not deleting channel because it is in a non-failing state",
				"channel", channel.Name, "app", app, "state", channelToDelete.Status.Status)
		}
	}
	return nil
}

func (r *ApplicationReconciler) cancelPods(ctx context.Context,
	app *crd.Application) error {
	pods, err := r.getPods(ctx, app)
	if err != nil {
		r.Log.Error(err, "failed to get pods for canceling")
		return err
	}
	for _, pod := range pods.Items {
		err = r.Delete(ctx, &pod)
		if err != nil {
			r.Log.Error(err, "failed to delete pod", "pod", pod.Name)
			// Keep deleting even when there are errors.
		}
	}
	return err // If there were errors deleting pods, return the last one.
}

func (r *ApplicationReconciler) cancelApp(ctx context.Context, app *crd.Application) error {
	err1 := r.cancelPods(ctx, app)
	err2 := r.cancelChannels(ctx, app.Name, app.Namespace)
	if err1 != nil {
		return err1
	}
	return err2
}

func (r *ApplicationReconciler) createPods(ctx context.Context, app *crd.Application) error {
	annotations := app.GetAnnotations()
	pods, err := r.getPods(ctx, app)
	if err != nil {
		return err
	}
	podSpecs := application.GetMissingPodSpecs(*app, *pods)

	for name, templateSpec := range podSpecs {
		pod := &corev1.Pod{Spec: templateSpec.Spec}
		pod.Namespace = app.Namespace
		pod.Name = name
		pod.Labels = map[string]string{"application-group": GetApplicationGroup(app)}
		for key, value := range templateSpec.Metadata.Labels {
			if key != "application-group" {
				pod.Labels[key] = value
			}
		}
		if len(annotations) > 0 {
			if pod.Annotations == nil {
				pod.Annotations = make(map[string]string)
			}
			for k, v := range annotations {
				pod.Annotations[k] = v
			}
		}
		if r.SchedulerName != "" && pod.Spec.SchedulerName == "" {
			pod.Spec.SchedulerName = r.SchedulerName
		}
		if pod.Spec.Hostname == "" {
			pod.Spec.Hostname = name
		}
		if err := ctrl.SetControllerReference(app, pod, r.Scheme); err != nil {
			return err
		}
		if err := r.Create(ctx, pod); err != nil {
			return err
		}
	}
	return nil
}

func (r *ApplicationReconciler) processPendingState(ctx context.Context,
	app *crd.Application) (crd.ApplicationStatus, applyStateTransitionFn, error) {

	channels, err := r.getChannels(ctx, app.Name, app.Namespace)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationPending}, nopStateTransitionFunction, err
	}
	conditions := application.DescribeChannelFailures(channels)
	if len(conditions) > 0 {
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing, Conditions: conditions},
			makeApplyFn(ctx, app, r.cancelApp), nil
	}

	// Waiting for pods to be scheduled.
	pods, err := r.getPods(ctx, app)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationPending}, nopStateTransitionFunction, err
	}

	// While pods are being created, it's possible they're not all in Kubernetes yet.
	if len(pods.Items) == 0 {
		return crd.ApplicationStatus{Phase: crd.ApplicationPending}, nopStateTransitionFunction, nil
	}
	missingPods := application.GetMissingPodSpecs(*app, *pods)
	if len(missingPods) > 0 {
		return crd.ApplicationStatus{Phase: crd.ApplicationPending}, nopStateTransitionFunction, nil
	}

	newStatus := application.NewStatusFromWorkloads(pods)
	// If the application is terminating, make sure to cancel the other pods and channels.
	if newStatus.Phase == crd.ApplicationFailing {
		return *newStatus, makeApplyFn(ctx, app, r.cancelApp), nil
	}
	if newStatus.Phase == crd.ApplicationWaiting {
		// This is a rare special case where all pods went to 'succeeded' really fast.
		// cancelApp needs to be called to clean up channels, then switch the app to the Failing
		// phase where it can wait for the channels to be cancelled and deleted.
		// As elsewhere, this isn't a failure as such, but close enough.
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, makeApplyFn(ctx, app, r.cancelApp), nil
	}
	return *newStatus, nopStateTransitionFunction, nil
}

func (r *ApplicationReconciler) processRunningState(ctx context.Context,
	app *crd.Application) (crd.ApplicationStatus, applyStateTransitionFn, error) {

	channels, err := r.getChannels(ctx, app.Name, app.Namespace)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationRunning}, nopStateTransitionFunction, err
	}
	conditions := application.DescribeChannelFailures(channels)
	if len(conditions) > 0 {
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing, Conditions: conditions},
			makeApplyFn(ctx, app, r.cancelApp), nil
	}

	pods, err := r.getPods(ctx, app)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationRunning}, nopStateTransitionFunction, err
	}

	missingPods := application.GetMissingPodSpecs(*app, *pods)
	if len(missingPods) > 0 {
		var podnames strings.Builder
		for name := range missingPods {
			podnames.WriteString(name + ", ")
		}
		r.Recorder.Event(app, "Normal", "missing pods", fmt.Sprintf("these pods have workload specs but are not in kubernetes: %s", podnames.String()))
		// Create these pods. If they've been created before, they should still
		// have a schedule entry and will end up on the same node as before. Otherwise
		// this will just leave the pods pending, which is probably the best we can do
		// here until partial scheduling is supported.
		return crd.ApplicationStatus{Phase: crd.ApplicationPending}, makeApplyFn(ctx, app, r.createPods), nil
	}

	newStatus := application.NewStatusFromWorkloads(pods)

	if newStatus.Phase == crd.ApplicationWaiting {
		if infrastructure.AreAllChannelsDeleted(channels) {
			return *newStatus, nopStateTransitionFunction, nil
		}
		// This isn't really a 'Failing' state so much as a 'Terminating' one. Then again,
		// to get here, all your pods must have entered the 'Succeeded' state, which is
		// weird for workloads that are supposed to run forever. So 'Failing' is probably
		// the right word.
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, makeApplyFn(ctx, app, r.cancelApp), nil
	}

	if newStatus.Phase == crd.ApplicationFailing {
		return *newStatus, makeApplyFn(ctx, app, r.cancelApp), nil
	}

	// The application is running, not failing, make sure the channel services exist.

	// get channels for which this application is the targetApp
	channelsUsingApp, err := r.getChannels(ctx, app.Name, app.Namespace)
	if err != nil {
		// If this is a transient error, it makes sense to retry.
		return crd.ApplicationStatus{Phase: crd.ApplicationRunning}, nopStateTransitionFunction, err
	}
	for _, s := range channelsUsingApp.Items {
		ta := infrastructure.ChannelTargetApplication(&s)
		if ta == app.Name {
			r.Log.Info("app is target for channel", "app", app.Name, "channel", s.Name)
		}

		targetPodName := types.NamespacedName{Namespace: app.Namespace, Name: s.Spec.TargetWorkload}
		var targetPod corev1.Pod
		err := r.Get(ctx, targetPodName, &targetPod)
		if err != nil {
			r.Log.Error(err, "target pod of channel does not appear to exist yet")
			return crd.ApplicationStatus{Phase: crd.ApplicationRunning}, nopStateTransitionFunction, err
		}

		service, endpoints, err := application.CreateServiceForChannelAndPod(s, targetPod)
		if err != nil {
			return crd.ApplicationStatus{Phase: crd.ApplicationRunning}, nopStateTransitionFunction, err
		}

		var svc corev1.Service
		err = r.Get(ctx, types.NamespacedName{Name: service.Name, Namespace: app.Namespace}, &svc)
		if err != nil {
			if !errors.IsNotFound(err) {
				r.Log.Error(err, "Failed to retrieve service")
				continue
			}
			r.Log.Info("service does not exist yet, creating it and its endpointslice", "service", service.Name)

			if err = ctrl.SetControllerReference(app, service, r.Scheme); err != nil {
				r.Log.Error(err, "unable to make application a controller of service")
				continue
			}
			if err = ctrl.SetControllerReference(app, endpoints, r.Scheme); err != nil {
				r.Log.Error(err, "unable to make application a controller of endpoints")
				continue
			}

			err = r.Create(ctx, service)
			if err != nil {
				r.Log.Error(err, "Failed to create service")
				continue
			}
			err = r.Create(ctx, endpoints)
			if err != nil {
				r.Log.Error(err, "Failed to create endpoints")
				continue
			}

		} else { // the service already exists, double check the endpoints
			r.Log.Info("service already exists", "service", svc.Name)
			if err = ctrl.SetControllerReference(app, service, r.Scheme); err != nil {
				r.Log.Error(err, "unable to make application a controller of service")
				continue
			}
			var existingEp corev1.Endpoints
			err = r.Get(ctx, types.NamespacedName{Name: service.Name, Namespace: app.Namespace}, &existingEp)
			if err != nil && errors.IsNotFound(err) {
				// The endpoints don't even exist any more, just recreate.
				err = r.Create(ctx, endpoints)
				if err != nil {
					r.Log.Error(err, "Failed to create endpoints")
					continue
				}
			} else if err != nil {
				r.Log.Error(err, "Failed to obtain endpoints")
				continue
			}
			err = r.Update(ctx, endpoints)
			if err != nil {
				r.Log.Error(err, "Failed to update endpoints")
				continue
			}
			if err = ctrl.SetControllerReference(app, endpoints, r.Scheme); err != nil {
				r.Log.Error(err, "unable to make application a controller of endpoints")
				continue
			}
		}
	}

	return *newStatus, nopStateTransitionFunction, nil
}

func (r *ApplicationReconciler) processFailingState(ctx context.Context,
	app *crd.Application) (crd.ApplicationStatus, applyStateTransitionFn, error) {

	pods, err := r.getPods(ctx, app)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, nopStateTransitionFunction, err
	}

	channels, err := r.getChannels(ctx, app.Name, app.Namespace)
	if err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, nopStateTransitionFunction, err
	}

	// The application can leave the failing state when all pods have terminated.
	if len(pods.Items) == 0 {
		// Only go back to waiting once all channels have been deleted.
		if infrastructure.AreAllChannelsDeleted(channels) {
			return crd.ApplicationStatus{Phase: crd.ApplicationWaiting}, nopStateTransitionFunction, nil
		} else {
			return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, makeApplyFn(ctx, app, r.deleteAppChannels), nil
		}
	}
	return crd.ApplicationStatus{Phase: crd.ApplicationFailing}, nopStateTransitionFunction, nil
}

func (r *ApplicationReconciler) processSchedulingState(ctx context.Context,
	app *crd.Application) (crd.ApplicationStatus, applyStateTransitionFn, error) {
	if err := r.resetApplicationConditions(ctx, app); err != nil {
		return crd.ApplicationStatus{Phase: crd.ApplicationScheduling}, nopStateTransitionFunction, err
	}
	return crd.ApplicationStatus{Phase: crd.ApplicationPending}, makeApplyFn(ctx, app, r.createPods), nil
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applications,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applications/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=applications/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch;update

// Reconcile is part of the main Kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// This function is called when there is a change to an Application or to
// a Pod with an OwnerReference to an Application. It is also called when
// there is a change to a Channel that is labelled with an Application.
func (r *ApplicationReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("application", req.NamespacedName)

	app := crd.Application{}
	err := r.Get(ctx, req.NamespacedName, &app)
	if err != nil {
		// If this is a not found error and there are channels referencing
		// this application, cancel them.
		if errors.IsNotFound(err) {
			serr := r.cancelChannels(ctx, req.NamespacedName.Name, req.NamespacedName.Namespace)
			derr := r.deleteChannels(ctx, req.NamespacedName.Name, req.NamespacedName.Namespace)

			recordApplicationDeleted(req.NamespacedName.Name, req.NamespacedName.Namespace)
			if serr != nil {
				return ctrl.Result{}, serr
			}
			if derr != nil {
				return ctrl.Result{}, derr
			}
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if app.Status.Phase == "" {
		if err := r.updateStatus(ctx, crd.ApplicationStatus{Phase: crd.ApplicationWaiting}, &app); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	if GetApplicationGroup(&app) == "" {
		log.Info("Applications need an application group label", "application", app.Name)
		return ctrl.Result{}, nil
	}

	exportApplicationMetrics(app)

	var newStatus crd.ApplicationStatus
	var callFn applyStateTransitionFn

	switch app.Status.Phase {
	case crd.ApplicationWaiting:
		// Nothing to do; the application group will update our phase.
		return ctrl.Result{}, nil
	case crd.ApplicationScheduling:
		newStatus, callFn, err = r.processSchedulingState(ctx, &app)
	case crd.ApplicationPending:
		newStatus, callFn, err = r.processPendingState(ctx, &app)
	case crd.ApplicationRunning:
		newStatus, callFn, err = r.processRunningState(ctx, &app)
	case crd.ApplicationFailing:
		newStatus, callFn, err = r.processFailingState(ctx, &app)
	default:
		r.Log.Info("Unrecognized phase in application", "phase", app.Status.Phase)
		return ctrl.Result{}, nil
	}

	if err != nil {
		return ctrl.Result{}, err
	}
	err = callFn()
	if err != nil {
		return ctrl.Result{}, err
	}
	err = r.updateStatus(ctx, newStatus, &app)
	if err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ApplicationReconciler) SetupWithManager(mgr ctrl.Manager) error {

	r.Log.WithValues("application", "manager")

	// Index pods by their owner when that owner is an Application.
	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &corev1.Pod{}, ".metadata.controller",
		func(rawObj client.Object) []string {
			pod := rawObj.(*corev1.Pod)
			owner := metav1.GetControllerOf(pod)
			if owner == nil {
				return nil
			}

			if owner.Kind != "Application" || owner.APIVersion != "qos-scheduler.siemens.com/v1alpha1" {
				return nil
			}
			// return the owner if it's of the right Kind and api version.
			return []string{owner.Name}
		}); err != nil {
		return err
	}

	// Index channels by the applications that need them.
	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &crd.Channel{},
		".metadata.Application", func(rawObj client.Object) []string {
			channel := rawObj.(*crd.Channel)
			ret := make([]string, 0, 2)
			applicationName := infrastructure.ChannelSourceApplication(channel)
			if applicationName != "" {
				ret = append(ret, applicationName)
			}
			targetAppName := infrastructure.ChannelTargetApplication(channel)
			if targetAppName != "" {
				ret = append(ret, targetAppName)
			}
			return ret
		}); err != nil {
		return err
	}

	// When watching channels, send them to Reconcile under the corresponding application names.
	mapFn := func(a client.Object) []reconcile.Request {
		channel := a.(*crd.Channel)
		applicationName := infrastructure.ChannelSourceApplication(channel)
		targetAppName := infrastructure.ChannelTargetApplication(channel)
		ret := make([]reconcile.Request, 0, 2)
		if applicationName != "" {
			fromRequest := types.NamespacedName{
				Name:      applicationName,
				Namespace: a.GetNamespace(),
			}
			ret = append(ret, reconcile.Request{NamespacedName: fromRequest})
		}
		if targetAppName != "" {
			toRequest := types.NamespacedName{
				Name:      targetAppName,
				Namespace: a.GetNamespace(),
			}
			ret = append(ret, reconcile.Request{NamespacedName: toRequest})
		}
		return ret
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&crd.Application{}).
		Owns(&corev1.Pod{}). // The application owns the pods that it creates.
		Watches(&source.Kind{Type: &crd.Channel{}}, handler.EnqueueRequestsFromMapFunc(mapFn)).
		Complete(r)
}
