// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	"siemens.com/qos-scheduler/scheduler/assignment"
)

func (r *ApplicationGroupReconciler) getApplications(ctx context.Context,
	namespace string, name string) (*qosschedulerv1alpha1.ApplicationList, error) {
	ret := &qosschedulerv1alpha1.ApplicationList{}
	err := r.List(ctx, ret, client.InNamespace(namespace),
		client.MatchingFields{applicationGroupKey: name})
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func getAssignmentPlanLookupKey(appGroupName string, namespace string) types.NamespacedName {
	apName := fmt.Sprintf("%s-assignment-plan", appGroupName)
	return types.NamespacedName{Name: apName, Namespace: namespace}
}

// Ensure that an assignment plan exists for this application group and qos model.
// If none exists, one will be created. If one exists, its spec will be modified.
// The spec will be updated to reflect that this is an optimizer request.
// The assignment plan reconciler will pick up this request, work on it, and fill
// in updates to the spec and status if a plan has been found.
// Then the scheduler will use the status to assign pods to nodes.
func (r *ApplicationGroupReconciler) populateOptimizerRequest(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup,
	qosModel *assignment.QoSModel) error {

	plan, err := r.getAssignmentPlan(ctx, appGroup.Name, appGroup.Namespace)
	if err != nil && errors.IsNotFound(err) {
		// This is expected on the first optimizer call.
		apKey := getAssignmentPlanLookupKey(appGroup.Name, appGroup.Namespace)
		createdAp := &qosschedulerv1alpha1.AssignmentPlan{
			ObjectMeta: metav1.ObjectMeta{
				Name:      apKey.Name,
				Namespace: appGroup.Namespace,
				Labels:    map[string]string{"application-group": appGroup.Name},
			},
			TypeMeta: metav1.TypeMeta{Kind: "AssignmentPlan", APIVersion: "v1alpha1"},
			Spec: qosschedulerv1alpha1.AssignmentPlanSpec{
				QoSModel:       *qosModel.DeepCopy(),
				OptimizerReply: assignment.Reply{},
				ReplyReceived:  false,
			},
		}
		if err = ctrl.SetControllerReference(appGroup, createdAp, r.Scheme); err != nil {
			r.Log.Error(err, "unable to make application group an owner of assignment plan")
			return err
		}
		return r.Create(ctx, createdAp)
	}
	if err != nil {
		r.Log.Error(err, "Failed to get assignment plan, but it exists.")
		return err
	}
	plan.Spec.QoSModel = *qosModel.DeepCopy()
	plan.Spec.OptimizerReply = assignment.Reply{}
	plan.Spec.ReplyReceived = false
	err = ctrl.SetControllerReference(appGroup, plan, r.Scheme)
	if err != nil {
		return err
	}
	return r.Update(ctx, plan)
}

func (r *ApplicationGroupReconciler) getAssignmentPlan(ctx context.Context,
	appGroupName string, namespace string) (*qosschedulerv1alpha1.AssignmentPlan, error) {
	apKey := getAssignmentPlanLookupKey(appGroupName, namespace)
	ap := &qosschedulerv1alpha1.AssignmentPlan{}
	err := r.Reader.Get(ctx, apKey, ap)
	return ap, err
}

func (r *ApplicationGroupReconciler) resetOptimizerRequest(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup) error {
	plan, err := r.getAssignmentPlan(ctx, appGroup.Name, appGroup.Namespace)
	if err != nil {
		return err
	}
	plan.Spec.ReplyReceived = false
	return r.Update(ctx, plan)
}

func (r *ApplicationGroupReconciler) prepareOptimizerRetry(ctx context.Context,
	appGroup *qosschedulerv1alpha1.ApplicationGroup) error {
	if !application.CanRetryOptimizing(appGroup) {
		optimizerExhausted.WithLabelValues(appGroup.Namespace, appGroup.Name).Inc()
		return fmt.Errorf("optimizer returned a retryable error but the max retry count %d was exhausted for application group %s",
			appGroup.Spec.OptimizerRetryPolicy.MaxRetries, appGroup.Name)
	}

	appGroup.Status.OptimizerAttemptCount++
	appGroup.Status.LastUpdateTime = metav1.Now()
	if err := r.Status().Update(ctx, appGroup); err != nil {
		return err
	}
	r.Log.Info("resetting optimizer reply because of an optimizer retry", "applicationgroup", appGroup.Name)
	return r.resetOptimizerRequest(ctx, appGroup)
}
