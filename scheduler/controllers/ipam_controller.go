// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: 2023 Siemens AG

// Watches ipams.

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// IpamReconciler reconciles Ipam requests
type IpamReconciler struct {
	client.Client
	Log            logr.Logger
	Scheme         *runtime.Scheme
	Reader         client.Reader
	Ipv4CidrRanges []string
	IPManagers     []*infrastructure.IPManager
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=ipams,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=ipams/status,verbs=get;update;patch

// Reconcile aims to move the state of the cluster towards the desired state.
func (r *IpamReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	fmt.Printf("reconciler called for %+v\n", req.NamespacedName)

	if len(r.IPManagers) == 0 {
		err := r.initialize(ctx)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	if len(r.IPManagers) == 0 {
		return ctrl.Result{}, fmt.Errorf("no ip v4 ranges specified, cannot handle ipam requests")
	}

	ipam := &qosschedulerv1alpha1.Ipam{}
	err := r.Get(ctx, req.NamespacedName, ipam)
	if err != nil {
		if errors.IsNotFound(err) {
			// The ipam has been deleted, release its ranges.
			ipamName := fmt.Sprintf("%s/%s", req.NamespacedName.Namespace, req.NamespacedName.Name)
			for _, ipmgr := range r.IPManagers {
				ipmgr.ReleaseCidrs(ipamName)
			}
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if ipam.Status.StatusCode == "" {
		ipam.Status.StatusCode = qosschedulerv1alpha1.IpamRequested
		err = r.Status().Update(ctx, ipam)
		return ctrl.Result{}, err
	}

	// A new ipam request: attempt to fulfill it.
	if ipam.Status.StatusCode == qosschedulerv1alpha1.IpamRequested {
		if ipam.Status.IPv4Cidr != "" {
			r.Log.Info("not processing ipam request because the cidr is already filled in; this is not supported")
			ipam.Status.StatusCode = qosschedulerv1alpha1.IpamUnfulfillable
			err = r.Status().Update(ctx, ipam)
			return ctrl.Result{}, err
		}
		if ipam.Spec.NumberOfAddresses < 1 {
			r.Log.Info("cannot request fewer than 1 ip address")
			ipam.Status.StatusCode = qosschedulerv1alpha1.IpamUnfulfillable
			err = r.Status().Update(ctx, ipam)
			return ctrl.Result{}, err
		}
		ipamName := fmt.Sprintf("%s/%s", req.NamespacedName.Namespace, req.NamespacedName.Name)
		for _, ipmgr := range r.IPManagers {
			if ipmgr == nil {
				continue
			} // This can happen due to initialization errors
			subnet, err := ipmgr.ReserveCidr(ipam.Spec.NumberOfAddresses, ipamName)
			if err == nil && subnet != nil {
				r.Log.Info("found subnet range", "subnet", subnet.String(), "addressCount", ipam.Spec.NumberOfAddresses)
				ipam.Status.IPv4Cidr = subnet.String()
				ipam.Status.StatusCode = qosschedulerv1alpha1.IpamReserved
				err = r.Status().Update(ctx, ipam)
				return ctrl.Result{}, err
			}
		}
		// If we get here, none of the ip managers had space for us.
		r.Log.Info("failed to allocate address range for ipam", "ipam", req.NamespacedName)
		ipam.Status.StatusCode = qosschedulerv1alpha1.IpamUnfulfillable
		err = r.Status().Update(ctx, ipam)
		return ctrl.Result{}, err
	}

	// The other ipam status are not relevant here.
	return ctrl.Result{}, nil
}

func (r *IpamReconciler) initialize(ctx context.Context) error {
	// TODO: add a mutex to avoid entering this function twice
	err := r.initializeIPManagers()
	if err != nil {
		return err
	}
	err = r.initializeExistingIpams(ctx)
	if err != nil {
		return err
	}

	return nil
}

// Populate Managers with already reserved CIDR ranges
// Iterate over existing IPAMs to do so
func (r *IpamReconciler) initializeExistingIpams(ctx context.Context) error {
	ipams := &qosschedulerv1alpha1.IpamList{}
	err := r.List(ctx, ipams)
	if err != nil {
		r.Log.Error(err, "failed to look up ipams")
	} else {
		for _, ipam := range ipams.Items {
			clientId := ipam.GetLabels()["channel"]
			if clientId == "" {
				return fmt.Errorf("ipam %s/%s has no channel label", ipam.Namespace, ipam.Name)
			}
			subnet := ipam.Status.IPv4Cidr
			if subnet != "" {
				for _, ipmgr := range r.IPManagers {
					isManagerForSubnet, err := ipmgr.IsManagerFor(subnet)
					if err != nil {
						return err
					}
					if isManagerForSubnet {
						err := ipmgr.RegisterCidr(clientId, subnet)
						if err != nil {
							return err
						}
					}
				}
			}
		}
	}
	return nil
}

func (r *IpamReconciler) initializeIPManagers() error {
	if len(r.IPManagers) > 0 {
		return fmt.Errorf("cannot initialize the ip managers list twice")
	}
	if len(r.Ipv4CidrRanges) == 0 {
		return fmt.Errorf("no ipv4 cidr ranges specified")
	}
	r.IPManagers = make([]*infrastructure.IPManager, len(r.Ipv4CidrRanges))
	var err error
	for i, cidr := range r.Ipv4CidrRanges {
		r.IPManagers[i], err = infrastructure.NewIPManager(cidr)
		if err != nil {
			return err
		}
	}
	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *IpamReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&qosschedulerv1alpha1.Ipam{}).Complete(r)
}
