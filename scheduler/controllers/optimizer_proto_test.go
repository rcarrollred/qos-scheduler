// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package controllers

import (
	"encoding/json"
	"os"
	"testing"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/proto"

	"siemens.com/qos-scheduler/scheduler/assignment"
	optimizer "siemens.com/qos-scheduler/scheduler/optimizer/client"
)

func TestSaveSemioticsModelAsText(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModelFromTextfiles("../testdata/")

	f, err := os.OpenFile("testdata/semiotics.json",
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}
	options := protojson.MarshalOptions{
		Multiline: true,
		Indent:    "  ",
	}
	f.WriteString(options.Format(modelp))
}

func TestSaveSemioticsModel(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModelFromTextfiles("../testdata/")

	out, err := proto.Marshal(modelp)
	if err != nil {
		t.Errorf("%v", err)
	}
	err = os.WriteFile("testdata/semiotics.save", out, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestSaveOptimizerRequest(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModel()

	request := optimizer.OptimizationRequest{
		Model:            modelp,
		ApplicationGroup: "ag",
		Namespace:        "default",
	}

	f, err := os.OpenFile("testdata/optrequest.json",
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}
	jsonBytes, _ := json.MarshalIndent(request, "", "  ")
	f.WriteString(string(jsonBytes))

	modelReadJSON, err := os.ReadFile("testdata/optrequest.json")
	if err != nil {
		t.Errorf("%v", err)
	}

	requestRead := optimizer.OptimizationRequest{}
	err = json.Unmarshal(modelReadJSON, &requestRead)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestSaveQoSModelAsJson(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModel()

	f, err := os.OpenFile("testdata/qosmodel.json",
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}
	options := protojson.MarshalOptions{
		Multiline: true,
		Indent:    "  ",
	}
	f.WriteString(options.Format(modelp))

	modelReadJSON, err := os.ReadFile("testdata/qosmodel.json")
	if err != nil {
		t.Errorf("%v", err)
	}

	modelRead := &assignment.QoSModel{}
	err = protojson.Unmarshal(modelReadJSON, modelRead)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestSaveQoSModelAsText(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModel()

	f, err := os.OpenFile("testdata/qosmodel.txt",
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}

	options := prototext.MarshalOptions{
		Multiline: true,
		Indent:    "  ",
	}
	f.WriteString(options.Format(modelp))

	modelReadTxt, err := os.ReadFile("testdata/qosmodel.txt")
	if err != nil {
		t.Errorf("%v", err)
	}

	modelRead := &assignment.QoSModel{}
	err = prototext.Unmarshal([]byte(modelReadTxt), modelRead)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestSaveQoSModelAsFile(t *testing.T) {
	t.Skip("Temporarily disabled")
	modelp := MakeQoSModel()

	out, err := proto.Marshal(modelp)
	if err != nil {
		t.Errorf("%v", err)
	}
	err = os.WriteFile("testdata/qosmodel.save", out, 0666)
	if err != nil {
		t.Errorf("%v", err)
	}

	modelReadBin, err := os.ReadFile("testdata/qosmodel.save")
	if err != nil {
		t.Errorf("%v", err)
	}

	modelRead := &assignment.QoSModel{}
	err = proto.Unmarshal(modelReadBin, modelRead)
	if err != nil {
		t.Errorf("%v", err)
	}
}
