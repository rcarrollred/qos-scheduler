// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Watches channels.

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	qosschedulerv1alpha1 "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/infrastructure"
	"siemens.com/qos-scheduler/scheduler/network/cni"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// ChannelReconciler handles the interaction between channel and applicationgroup resources.
// If you are looking for actual channel processing, look in the subpackages of network.
type ChannelReconciler struct {
	client.Client
	Log                  logr.Logger
	Scheme               *runtime.Scheme
	Reader               client.Reader
	QosImplementationMap map[qosschedulerv1alpha1.NetworkServiceClass](nwlib.NetworkImplementationClass)
}

//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=networkendpoints,verbs=get;list;watch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=channels,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=channels/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=qos-scheduler.siemens.com,resources=channels/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=endpoints,verbs=get;list;watch;create;update;patch;delete

// Reconcile aims to move the state of the cluster towards the desired state.
func (r *ChannelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	channel := &qosschedulerv1alpha1.Channel{}
	err := r.Get(ctx, req.NamespacedName, channel)
	if err != nil {
		if errors.IsNotFound(err) {
			recordChannelDeleted(req.NamespacedName.Name, req.NamespacedName.Namespace)
			// The channel was probably deleted.
			// This shouldn't happen unless the channel had already been canceled before,
			// otherwise applications have no easy way to notice this.
			// other cleanup work, such as releasing the ip range associated with the channel's
			// vlan, happens when the channel is cancelled, because at that point the channel
			// metadata is still accessible in Kubernetes.
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	// The channel's status is empty when it is first created, just set it to Requested.
	if channel.Status.Status == "" {
		infrastructure.ResetChannelStatus(channel)
		return ctrl.Result{}, r.Status().Update(ctx, channel)
	}

	exportChannelMetrics(*channel)

	// When a channel's status goes to ACK, do some housekeeping, then acknowledge the channel.
	// TODO: consider moving this code to channel-controller, but if you do that, then
	// channel controllers have to be allowed to modify pods.
	if channel.Status.Status == qosschedulerv1alpha1.ChannelAck {
		// Does the channel request cni parameters?
		if channel.Status.CniMetadata.NetworkAttachmentName != "" {
			sourcePod, targetPod, err := r.getPodsForChannel(ctx, channel)
			if err != nil {
				return ctrl.Result{}, err
			}
			sourceNode, targetNode, err := r.getNetworkEndpointsForChannel(ctx, channel)
			if err != nil {
				return ctrl.Result{}, err
			}

			var podAnnotations = ""
			if sourcePod.Annotations == nil {
				sourcePod.Annotations = make(map[string]string)
			} else {
				podAnnotations = sourcePod.Annotations[cni.NetworkAttachmentKey]
			}
			nwad, err := cni.InsertNetworkAttachmentDefinition(podAnnotations,
				channel.Status.CniMetadata.NetworkAttachmentName, sourceNode, targetNode, *channel, true)
			if err != nil {
				return ctrl.Result{}, err
			}
			sourcePod.Annotations[cni.NetworkAttachmentKey] = nwad
			err = r.Update(ctx, sourcePod)
			if err != nil {
				return ctrl.Result{}, err
			}

			podAnnotations = ""
			if targetPod.Annotations == nil {
				targetPod.Annotations = make(map[string]string)
			} else {
				podAnnotations = targetPod.Annotations[cni.NetworkAttachmentKey]
			}
			nwad, err = cni.InsertNetworkAttachmentDefinition(podAnnotations,
				channel.Status.CniMetadata.NetworkAttachmentName, sourceNode, targetNode, *channel, false)
			if err != nil {
				return ctrl.Result{}, err
			}
			targetPod.Annotations[cni.NetworkAttachmentKey] = nwad
			err = r.Update(ctx, targetPod)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
		infrastructure.StartChannelRunning(channel)
		r.Log.Info("updating channel because it has been allocated", "channel", *channel)
		err = r.Status().Update(ctx, channel)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	if channel.Status.Status == qosschedulerv1alpha1.ChannelDeleted {
		err = r.Delete(ctx, channel)
		return ctrl.Result{}, err
	}

	// The scheduler will check the channel status in Permit(). It looks up the channels
	// that a pod waits for in the optimizer assignment plan, and then it looks at the
	// channel status by using its channel crd client. The pod can only proceed if all channels
	// that it depends on have been allocated.
	return ctrl.Result{}, nil
}

func (r *ChannelReconciler) createChannels(ctx context.Context, channels []*qosschedulerv1alpha1.Channel) error {
	if len(channels) == 0 {
		return nil
	}
	for _, channel := range channels {
		if channel == nil {
			continue
		}
		channel.Status.Status = qosschedulerv1alpha1.ChannelRequested
		if channel.Labels == nil {
			return fmt.Errorf("expected channel %s to have a %s label", channel.Name, qosschedulerv1alpha1.NetworkLabel)
		}

		if channel.Spec.ChannelFrom == channel.Spec.ChannelTo {
			channel.Labels[qosschedulerv1alpha1.NetworkLabel] = string(nwlib.Loopback)
		} else {
			_, ok := channel.Labels[qosschedulerv1alpha1.NetworkLabel]
			if !ok {
				return fmt.Errorf("expected channel %s to have a %s label", channel.Name, qosschedulerv1alpha1.NetworkLabel)
			}
		}

		// Create the channel resources. If there is already a channel resource with the same
		// name and whose state is 'deleted', overwrite it.
		err := r.recreateChannel(ctx, *channel)
		if err != nil {
			return err
		}
	}
	return nil
}

// This creates the channel if it does not exist. If the channel already exists and
// was in state Deleted, it will be recreated. Otherwise, the existing channel
// is left unchanged.
func (r *ChannelReconciler) recreateChannel(ctx context.Context, channel qosschedulerv1alpha1.Channel) error {
	var existingChannel qosschedulerv1alpha1.Channel
	err := r.Get(ctx, types.NamespacedName{Name: channel.Name, Namespace: channel.Namespace}, &existingChannel)
	if errors.IsNotFound(err) {
		return r.Create(ctx, &channel)
	} else if err != nil {
		return err
	}
	if existingChannel.Spec.ChannelFrom != channel.Spec.ChannelFrom ||
		existingChannel.Spec.ChannelTo != channel.Spec.ChannelTo {
		return fmt.Errorf("duplicate channel name %s/%s", channel.Namespace, channel.Name)
	}

	// TODO: check if any channel parameters have changed? That should only be possible if
	// the corresponding application was removed and recreated, and that should have caused
	// the channel to be deleted, so it seems best not to modify the channel here.

	if existingChannel.Status.Status != qosschedulerv1alpha1.ChannelDeleted {
		// This isn't an error, it's possible the channel was already in the REQUESTED state
		// and we're now in a requeue for instance.
		return nil
	}

	// A channel with the name we want already exists but is in state DELETED.
	// Delete it from Kubernetes.
	r.Log.Info("deleting channel", "channel", existingChannel)
	err = r.Delete(ctx, &existingChannel)
	if err != nil {
		return err
	}
	return r.Create(ctx, &channel)
}

func (r *ChannelReconciler) getNetworkEndpointsForChannel(ctx context.Context, channel *qosschedulerv1alpha1.Channel) (qosschedulerv1alpha1.NetworkEndpoint, qosschedulerv1alpha1.NetworkEndpoint, error) {
	eps := &qosschedulerv1alpha1.NetworkEndpointList{}
	err := r.List(ctx, eps)
	if err != nil {
		return qosschedulerv1alpha1.NetworkEndpoint{}, qosschedulerv1alpha1.NetworkEndpoint{}, err
	}

	var sourceNode qosschedulerv1alpha1.NetworkEndpoint
	var targetNode qosschedulerv1alpha1.NetworkEndpoint

	for _, ep := range eps.Items {
		if ep.Name == channel.Spec.ChannelFrom {
			sourceNode = ep
		}
		if ep.Name == channel.Spec.ChannelTo {
			targetNode = ep
		}
	}
	return sourceNode, targetNode, nil
}

func (r *ChannelReconciler) getPodsForChannel(ctx context.Context, channel *qosschedulerv1alpha1.Channel) (*corev1.Pod, *corev1.Pod, error) {
	sourceApplicationName := infrastructure.ChannelSourceApplication(channel)
	targetApplicationName := infrastructure.ChannelTargetApplication(channel)
	if sourceApplicationName == "" {
		return nil, nil, fmt.Errorf("missing source application for channel %v", channel)
	}
	if targetApplicationName == "" {
		return nil, nil, fmt.Errorf("missing target application for channel %v", channel)
	}
	pods := &corev1.PodList{}
	err := r.List(ctx, pods, client.InNamespace(channel.Namespace),
		client.MatchingFields{".metadata.controller": sourceApplicationName})
	if err != nil {
		return nil, nil, err
	}
	var sourcePod *corev1.Pod
	for _, pod := range pods.Items {
		if channel.Spec.SourceWorkload == pod.Name {
			sourcePod = &pod
			break
		}
	}
	var targetPod *corev1.Pod
	err = r.List(ctx, pods, client.InNamespace(channel.Namespace),
		client.MatchingFields{".metadata.controller": targetApplicationName})
	if err != nil {
		return nil, nil, err
	}
	for _, pod := range pods.Items {
		if channel.Spec.TargetWorkload == pod.Name {
			targetPod = &pod
			break
		}
	}
	return sourcePod, targetPod, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ChannelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&qosschedulerv1alpha1.Channel{}).Complete(r)
}
