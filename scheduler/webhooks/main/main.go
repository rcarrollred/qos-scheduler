// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog/v2"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/webhooks"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

var (
	runtimeScheme = runtime.NewScheme()
	cl            client.Client
	ctx           context.Context
)

func init() {
	_ = v1.AddToScheme(runtimeScheme)
	_ = crd.AddToScheme(runtimeScheme)
}

func main() {

	klog.InitFlags(nil)
	var port string
	flag.StringVar(&port, "port", ":443", "Port")
	flag.Parse()

	config, err := config.GetConfig()
	if err != nil {
		log.Fatalf("failed to get a kubernetes config: %v", err)
	}
	cl, err = client.New(config, client.Options{Scheme: runtimeScheme})
	if err != nil {
		log.Fatalf("failed to get a kubernetes client: %v", err)
	}
	ctx = context.Background()

	http.HandleFunc("/validate-workload-interfaces", ServeValidateWorkloadChannels)
	http.HandleFunc("/health", ServeHealth)

	// listens to clear text unless TLS env var is set to "true"
	if os.Getenv("TLS") == "true" {
		cert := "/webhook/certs/tls.crt"
		key := "/webhook/certs/tls.key"
		log.Fatal(http.ListenAndServeTLS(port, cert, key, nil))
	} else {
		log.Fatal(http.ListenAndServe(port, nil))
	}
}

// ServeHealth returns 200 when things are good
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "OK")
}

// ServeValidateWorkloadChannels validates an admission request and then
// writes an admission response to 'w'
func ServeValidateWorkloadChannels(w http.ResponseWriter, r *http.Request) {
	admissionReview, err := parseRequest(*r)
	if err != nil {
		klog.V(2).ErrorS(err, "failed to parse validation request")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	validateWorkloadChannels(admissionReview)

	w.Header().Set("Content-Type", "application/json")
	jout, err := json.Marshal(admissionReview)
	if err != nil {
		e := fmt.Sprintf("could not marshal admission review: %v", err)
		klog.V(2).ErrorS(err, "failed to marshal admission review")
		http.Error(w, e, http.StatusInternalServerError)
		return
	}

	klog.V(2).Infof("sending response: %s", jout)
	fmt.Fprintf(w, "%s", jout)
}

func validateWorkloadChannels(ar *v1.AdmissionReview) {
	klog.V(2).Infof("received an admission request %+v\n", ar.Request)

	applicationResource := metav1.GroupVersionResource{Group: "qos-scheduler.siemens.com", Version: "v1alpha1", Resource: "applications"}
	ar.Response = &v1.AdmissionResponse{
		UID:     ar.Request.UID,
		Allowed: false,
		Result: &metav1.Status{
			Message: "missing status",
		},
	}

	if ar.Request.Resource != applicationResource {
		klog.V(2).Info("admission request did not specify an application resource but a %v", ar.Request.Resource)
		ar.Response.Allowed = false
		ar.Response.Result = &metav1.Status{
			Message: fmt.Sprintf("admission request did not specify an application resource but a %v", ar.Request.Resource),
		}
		return
	}

	application := crd.Application{}
	decoder, err := admission.NewDecoder(runtimeScheme)
	if err != nil {
		klog.Error(err)
		ar.Response.Allowed = false
		ar.Response.Result = &metav1.Status{
			Message: fmt.Sprintf("failed to get decoder: %v", err),
		}
		return
	}
	err = decoder.Decode(admission.Request{AdmissionRequest: *ar.Request}, &application)
	if err != nil {
		klog.Error(err)
		ar.Response.Allowed = false
		ar.Response.Result = &metav1.Status{
			Message: fmt.Sprintf("failed to parse application out of admission request: %v", err),
		}
		return
	}

	allApplications := crd.ApplicationList{}
	err = cl.List(ctx, &allApplications, client.InNamespace(application.Namespace))
	if err != nil {
		klog.Error(err)
		ar.Response.Allowed = false
		ar.Response.Result = &metav1.Status{
			Message: fmt.Sprintf("failed to obtain applications: %v", err),
		}
		return
	}

	allChannels := crd.ChannelList{}
	err = cl.List(ctx, &allChannels, client.InNamespace(application.Namespace))
	if err != nil {
		klog.Error(err)
		ar.Response.Allowed = false
		ar.Response.Result = &metav1.Status{
			Message: fmt.Sprintf("failed to obtain channels: %v", err),
		}
		return
	}

	allowed, statusmsg := webhooks.ValidateWorkloadChannels(application, allApplications.Items, allChannels.Items)
	ar.Response.Allowed = allowed
	ar.Response.Result = &metav1.Status{Message: statusmsg}
	klog.V(2).Infof("admission response: %+v", *ar.Response)
}

// parseRequest extracts an AdmissionReview from an http.Request if possible
func parseRequest(r http.Request) (*v1.AdmissionReview, error) {
	if r.Header.Get("Content-Type") != "application/json" {
		return nil, fmt.Errorf("Content-Type: %q should be %q",
			r.Header.Get("Content-Type"), "application/json")
	}

	bodybuf := new(bytes.Buffer)
	bodybuf.ReadFrom(r.Body)
	body := bodybuf.Bytes()

	if len(body) == 0 {
		return nil, fmt.Errorf("admission request body is empty")
	}

	var a v1.AdmissionReview

	if err := json.Unmarshal(body, &a); err != nil {
		return nil, fmt.Errorf("could not parse admission review request: %v", err)
	}

	if a.Request == nil {
		return nil, fmt.Errorf("admission review can't be used: Request field is nil")
	}

	return &a, nil
}
