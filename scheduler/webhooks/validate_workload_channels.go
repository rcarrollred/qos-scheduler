// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package webhooks contains webhooks for the qos-scheduler system.
package webhooks

import (
	"fmt"

	"k8s.io/apimachinery/pkg/api/validation"
	"k8s.io/klog/v2"
	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
	"siemens.com/qos-scheduler/scheduler/application"
	nwlib "siemens.com/qos-scheduler/scheduler/network/lib"
)

// ValidateWorkloadChannels examines a list of channels and checks
// for reasons why they might not be deployable.
// Currently, it verifies that channel names are unique per application,
// and that channel names are valid Kubernetes resource names.
func ValidateWorkloadChannels(app crd.Application, existingApplications []crd.Application, existingChannels []crd.Channel) (bool, string) {
	klog.V(2).Infof("examining application %s for workload channel validation", app.Name)
	channels, err := application.ChannelsForApplication(app,
		map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})
	if err != nil {
		return false, fmt.Sprintf("error getting channels for application: %v", err)
	}
	thisAppChannels := make(map[string]bool)
	for _, channel := range channels {
		ok := validateChannelName(channel.Name)
		if !ok {
			return ok, fmt.Sprintf("the name for the channel %s is invalid", channel.Name)
		}
		_, ok = thisAppChannels[channel.Name]
		if ok {
			return false, fmt.Sprintf("the application %s has more than one channel named %s", app.Name, channel.Name)
		}
		thisAppChannels[channel.Name] = true

		// This compares the new channel's name against those of existing channels
		ok, msg := validateChannelUniqueness(*channel, existingChannels)
		if !ok {
			return ok, msg
		}

	}
	// Now compare the new channel's name against the channels other applications in this namespace
	// could create.
	for _, a := range existingApplications {
		if a.Namespace != app.Namespace {
			continue
		}
		if a.Name == app.Name {
			continue
		}
		ok, msg := validateApplicationChannelUniqueness(thisAppChannels, a)
		if !ok {
			return ok, msg
		}
	}
	return true, ""
}

// Reject a channel name if it is not a valid Kubernetes resource name.
func validateChannelName(name string) bool {
	errorList := validation.NameIsDNSSubdomain(name, false)
	if len(errorList) > 0 {
		fmt.Printf("error list: %v\n", errorList)
		klog.V(2).Infof("rejecting channel name %s because %v", name, errorList)
		return false
	}
	return true
}

// Reject if any of the channels in the map have the same name as a channel that would be created for app.
func validateApplicationChannelUniqueness(thisAppChannels map[string]bool, app crd.Application) (bool, string) {
	channels, err := application.ChannelsForApplication(app, map[crd.NetworkServiceClass](nwlib.NetworkImplementationClass){})
	if err != nil {
		return false, fmt.Sprintf("failed to get channels for application: %v", err)
	}
	for _, s := range channels {
		_, ok := thisAppChannels[s.Name]
		if ok {
			return false, fmt.Sprintf("application %s also declares a channel named %s", app.Name, s.Name)
		}
	}
	return true, ""
}

// Reject if channel has a duplicate in existingChannels.
func validateChannelUniqueness(channel crd.Channel, existingChannels []crd.Channel) (bool, string) {
	for _, s := range existingChannels {
		if s.Namespace != channel.Namespace {
			continue
		}
		if channel.Name == s.Name {
			// Is this actually the same channel? Compare the source and target workloads
			if channel.Spec.SourceWorkload == s.Spec.SourceWorkload &&
				channel.Spec.TargetWorkload == s.Spec.TargetWorkload {
				continue
			}
			klog.V(2).Infof("rejecting channel %s from %s to %s because it would have the same name as existing channel from %s to %s",
				channel.Name, channel.Spec.SourceWorkload, channel.Spec.TargetWorkload, s.Spec.SourceWorkload, s.Spec.TargetWorkload)
			return false, fmt.Sprintf("rejecting channel %s from %s to %s because it would have the same name as existing channel from %s to %s",
				channel.Name, channel.Spec.SourceWorkload, channel.Spec.TargetWorkload, s.Spec.SourceWorkload, s.Spec.TargetWorkload)
		}
	}
	return true, ""
}
