<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Web Hooks

This package contains webhooks for validating custom resources.

For example, channel names are derived from the workload interface specs but must
obey Kubernetes naming rules. The workload interface validator makes sure
you get feedback on potential problems (name too long or contains forbidden characters)
when you create your application.
