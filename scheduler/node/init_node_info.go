// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

// Package node contains code for the node daemonset, which maintains
// the network endpoint information.
package node

import (
	"context"
	"fmt"
	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netlink/nl"
	v1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"

	crd "siemens.com/qos-scheduler/scheduler/api/v1alpha1"
)

type NodeInterfaceFinder struct {
	client.Client
	Scheme *runtime.Scheme
}

func (n *NodeInterfaceFinder) listInterfaces() ([]crd.NetworkEndpointNetworkInterface, error) {
	links, err := netlink.LinkList()
	if err != nil {
		return nil, err
	}
	ret := make([]crd.NetworkEndpointNetworkInterface, 0, len(links))
	for _, l := range links {
		if l.Attrs().Name == "lo" {
			// Skip the loopback interface
			continue
		}
		addrList, err := netlink.AddrList(l, nl.FAMILY_V4)
		if err != nil {
			fmt.Printf("failed to get addresses for link %s: %v\n",
				l.Attrs().Name, err)
			continue
		}
		if len(addrList) == 0 {
			continue
		}
		alternates := make([]string, len(addrList))
		if len(addrList) > 1 {
			fmt.Printf("interface %s has %d ip addresses, using the first one: %s\n",
				l.Attrs().Name, len(addrList), addrList[0].IP.To4())
			for idx, addr := range addrList {
				alternates[idx] = addr.IP.To4().String()
			}
		} else {
			alternates[0] = addrList[0].IP.To4().String()
		}
		cnni := crd.NetworkEndpointNetworkInterface{
			Name:               l.Attrs().Name,
			MacAddress:         l.Attrs().HardwareAddr.String(),
			IPv4Address:        alternates[0],
			AlternateAddresses: alternates,
		}
		ret = append(ret, cnni)
	}
	// TODO: link also has LinkStatistics which includes packages sent etc
	return ret, nil
}

func (n *NodeInterfaceFinder) getCurrentNodeName() (string, error) {
	if n.Client == nil {
		fmt.Println("Cannot get current node name outside of kubernetes")
		return "noNode", nil
	}
	pod := &v1.Pod{}
	myPodName := os.Getenv("POD_NAME")
	myNamespace := os.Getenv("POD_NAMESPACE")
	err := n.Get(context.Background(), types.NamespacedName{Name: myPodName, Namespace: myNamespace}, pod)
	if err != nil {
		fmt.Printf("failed to find pod %s\n", myPodName)
		return "", err
	}
	return pod.Spec.NodeName, nil
}

func (n *NodeInterfaceFinder) InitializeNodeInfo() error {
	// Look at this node's network interfaces
	interfaces, err := n.listInterfaces()
	if err != nil {
		return err
	}
	nodeName, err := n.getCurrentNodeName()
	if err != nil {
		return err
	}
	cn := crd.NetworkEndpoint{
		ObjectMeta: metav1.ObjectMeta{
			Name: nodeName,
		},
		Spec: crd.NetworkEndpointSpec{
			NodeName:          nodeName,
			NetworkInterfaces: interfaces,
		},
	}
	if n.Client == nil {
		fmt.Printf("Missing k8s client. I should create a NetworkEndpoint resource %+v\n", cn)
		return nil
	}
	var existingNode crd.NetworkEndpoint
	err = n.Client.Get(context.Background(), types.NamespacedName{Name: nodeName}, &existingNode)
	if err != nil {
		if kerrors.IsNotFound(err) {
			if err = n.Create(context.Background(), &cn); err != nil {
				return err
			}
		} else {
			return err
		}
	} else {
		// Update existingNode with the interfaces
		existingNode.Spec.NetworkInterfaces = interfaces
		if err = n.Update(context.Background(), &existingNode); err != nil {
			fmt.Printf("failed to update node with interfaces: %+v\n", err)
			return err
		}
	}
	return nil
}
