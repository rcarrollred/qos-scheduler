#!/bin/bash
# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0


# This is how you bring up a local kind cluster. 
# Afterwards, install components in the cluster using
# start_local_demo.sh

set -euo pipefail

# This is the path from where this is being called from to where the script lives.
SCRIPTHOME=$(dirname $(realpath "$0"))
DOCKERCONFIGJSON=${DOCKERCONFIGJSON:-"$HOME/.docker/config.json"}

# When you set this, make sure you've compiled the code for a compatible
# version of Kubernetes. If this uses v1.21.1, then you need to get your
# images from a branch where the scheduler was built with the right
# Kubernetes libraries version 21.
NODEK8SVERSION=${NODEK8SVERSION:-v1.26.6}

CONFIGDIR="demo"

sed --follow-symlinks -i "s/image: kindest\/node:v[[:digit:]]\+.[[:digit:]]\+.[[:digit:]]\+/image: kindest\/node:$NODEK8SVERSION/" $SCRIPTHOME/config/${CONFIGDIR}/cluster-config-hybrid.yaml

echo "creating ${CONFIGDIR} qos-scheduling cluster using Kubernetes version $NODEK8SVERSION"

kind create cluster --config $SCRIPTHOME/config/${CONFIGDIR}/cluster-config-hybrid.yaml

# Wait until the nodes are ready. This can take a while.
echo "Waiting up to five minutes for nodes to be ready."
kubectl wait --for=condition=ready --timeout=300s node -l kubernetes.io/hostname=cloud

# label the nodes with their Kubernetes node names for easier use with
# optimizer-compatible labels
# put additional labels to selected nodes
export NODE_LABEL_PREFIX="siemens.com.qosscheduler"
export NODES=$(kubectl get no -o jsonpath="{.items[*].metadata.name}")
for n in $NODES 
do
  kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.$n=
  if [ $n = "ipc1" ];then kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.sensor=;fi  
  if [ $n = "ipc2" ];then kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.actuator=;fi  
done
