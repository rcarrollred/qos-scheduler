<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# The CI/CD pipeline

The pipeline uses a kaniko container
for all non-hermetic steps (i.e. steps that require access to the
internet, usually because they install packages or download modules).

The pipeline goes through the following steps, each of which is
described in more detail below.

Some more complex steps have subdirectories in `container`, others
are implemented completely in the `.gitlab-ci.yml` file.

1. dev-container
2. lint
3. test
4. build
5. collect-configs
6. deploy

## Global settings

The `.gitlab-ci.yml` file has some global variables. Most of them
are just the names of the docker containers that get built, but a few
are trickier:

`GOENV_VERSION`  is the version of the goenv container that builds of the `main` branch should use. `latest` is probably fine here but if
the `latest` image is broken, you may want to pin goenv_version to a different tag to get a viable build.

`K8SVERSION` is the Kubernetes version your code is tied to. Switching
this will not create code for the given version automatically; you need to run the `hack/maybe-update-deps.sh` script first, then check in the changes it makes.

## dev-container

This step builds the `goenv` container, which has a go compiler,
all the packages required by the code, as well as some tools for running tests.

This step is not hermetic, so it runs in a kaniko container. It
is also fairly complex, so it has its own subdirectory (`container/goenv`).

The `pipeline-jobs.yml` file provides the steps before running kaniko. Mainly, this part copies `go.mod` files from the packages we
build so they can get processed in the kaniko/Docker build.

The `Dockerfile` is the specification of the goenv container. It starts
from a golang alpine base image and installs libraries and modules
that our code requires including `protoc-gen-go` and `ginkgo` (the latter is for testing). It also runs `go mod download` on the `go.mod` files for our packages. All this is so that later steps can run in this goenv container and do not require further downloads.

The goenv container is never deployed to production. The `deploy` step
of the ci/cd pipeline creates very basic containers (from a distroless base image where possible) and only copies the binary we want to run into them.

I have a `version` file in this directory which I primarily use to force a rebuild of the goenv container without making actual code changes. This is because sometimes a pipeline run will fail and then the
next run will not realise that it still needs to build a fresh goenv image.

## dev-cpp-container

This step builds the `cpp` container, which is basically the container that comes out of the `optimizing-scheduler` pipeline plus a go compiler and the modules that our code needs.

"The container that comes out of optimizing-scheduler" is a ubuntu
image with a lot of other things installed, including cmake,
a locally built protobuf library, and a locally built scip.
This step here just adds golang and our modules to that so that I can
then build and link the optimizer.

This step is not hermetic. The docker file it builds on is pretty
big, and this step makes it even bigger. Of course
the image produced here is never deployed to production. The deploy
step creates a much smaller image.

Similar to the dev-container step, there is a `force-build` file here which is really just for forcing a rebuild.

The most common reason for needing to force a rebuild is using the wrong
value of `OPT_TAG`. This is the tag of the optimizer container you want to build on. This needs to be an optimizer container built on top of ubuntu:focal, not ubuntu:18.04. Currently this means you need the `build-experiments` tag. Once that has been merged, you can use the `develop` tag (or maybe, one day, the `latest` tag).

## lint

Implemented in the gitlab yaml file. This runs `staticcheck` over
the code and reports errors. Lint errors do not block subsequent steps
of the pipeline.

## test

This runs all the go tests (everything that does not require c++ code).
It runs in the `goenv` container and is hermetic.

## build

This builds all go code. It runs in the `goenv` container and is hermetic.
 It produces the `qosScheduler` (custom scheduler), `manager` (controller-manager service),
and `networkoperator` binaries and exports them to CI as artifacts.

## collect-configs

This creates a tar file with config files. This is how I package up
all the yaml files and scripts needed to bring up the system. You
could move this to a package registry instead.

## deploy

This tags and pushes docker images. It uses the artifacts created
by the `build` and `collect-configs` steps and
creates a docker image from each artifact.

The way this works is that it takes each artifact in turn, copies it
to a temporary location, and then runs `docker build` there using
one of the docker files in `container/deploy`. There is one docker
file there for every component of the system. The resulting
docker image is then tagged and pushed to the container registry (using kaniko).

Here is the list of docker images you should have at the end:

1. goenv (environment for building and testing go code)
2. controller (the controller-manager binary)
3. custom-scheduler (the custom scheduler binary)
4. networkoperator (maintains network information)
5. configs  (the package with all the yaml files and scripts)

Not all pipeline runs will rebuild all images. There are gitlab ci/cd settings in the yaml files that specify which file changes trigger which step.
