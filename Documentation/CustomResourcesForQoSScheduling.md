<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# How to use the custom resources for QoS scheduling

## Purpose

The QoS Scheduler extends Kubernetes scheduling with network awareness
and co-scheduling features:

1. ApplicationWorkloads specify both compute resource requirements and network requirements (bandwidth and latency to other workloads).
2. ApplicationWorkloads can request co-scheduling with other workloads (co-scheduled workloads get scheduled at the same time - unlike the Kubernetes scheduler, which schedules one Pod at a time).
3. A custom Workload Placement Solver determines the assignment of workloads to Kubernetes nodes. The Workload Placement Solver takes network topology and requirements into account.

## Custom Resources

Users interact with the QoS Scheduler via custom resources in Kubernetes. All resources can be created from yaml files.

The `config/demo` directory contain sample yaml files.

The `scheduler/api/v1alpha1` directory contains the source files for the custom resources.

The `helm/qos-scheduler/crds/` directory contains the CRD definitions that get installed in the Kubernetes cluster. These files are generated from the source files in `scheduler/api/v1alpha1`.

### Custom Resource: Application

An Application is a grouping of dependent ApplicationWorkloads and connections between them (so-called Channels).
The scheduler will treat an Application as an entity, meaning that either all ApplicationWorkloads and Channels
of an Application will be scheduled or none.

An Application contains one or more *ApplicationWorkloads*.

An Application has a *Spec* (declaring what the application should look like) and
a *Status*.

The `ApplicationSpec` has fields

1. Priority: an integer number, currently unused.
2. Availability: the application's minimal required availability. Currently unused.
3. Workloads: a list of *ApplicationWorkloadSpec*

The `ApplicationStatus` has fields

1. Phase: a string (enum) identifying where the application is in its lifecyle (e.g. "Waiting", "Pending"). The CRD declaration source file (`scheduler/api/v1alpha1/application_types.go`) has the full list of ApplicationPhase values with their meanings.

2. Conditions: a list of failures that have been recorded for the Application. These help in understanding why an application failed (e.g. "Channel went out of QoS" or "Pod failed to be scheduled").

Applications will record Kubernetes events when they change phase.

Because Applications are Kubernetes resources, they inherit standard metadata fields such as `Name` and `Labels`.

ApplicationWorkloads get turned into Pods. Their spec contains a pod spec and you can use pod spec
features as expected. This includes the ability to define multiple containers and init containers.
Please note the limitations around affinities outlined below though.

An `ApplicationWorkloadSpec` has fields

1. Basename: string. Pods generated from this workload spec will use this as part of their names. For example, if you have an application called *myApp1* and a workload with basename *myWorkload1*, you will get a pod named *myApp1-myWorkload1*. If you have more than one workload in an application, you need to give them different basenames.
2. Template: a Kubernetes *PodTemplateSpec*. When writing this spec, please keep in mind that the Workload Placement Solver (which calculates pod-node assignments) cannot handle the full range of pod template possibilities. In particular:
    - *affinity* settings are limited to *nodeAffinity* with a single *nodeSelectorTerm* using only *Exists* and *DoesNotExist* operators.
    - *resources* currently only support *cpu* and *memory*.
    - All Pods generated for an Application are in the same namespace as that Application.
    - Do not set a node id in the pod spec. If you want to "force" the Worklaod Placement Solver to assign a given workload to a particular node, use NodeAffinity.
    - There is no need to specify the custom scheduler in your pod specs. The system will do this for you based on a setting on the `controller-manager`.
3. Channels: a list of *NetworkChannel* specs.

A `NetworkChannel` declares a required unidirectional connection between this workload and another workload. Its fields are:

1. otherWorkload: identifies the workload to connect to. This is a combination of application name and workload basename (e.g., *myApp1-myWorkload1*).
2. bandwidth: the minimum bandwidth required (in bit/s).
3. maxDelay: the maximum tolerated latency (in seconds).
4. framesize: the requested framesize (optional; if known, Framesize and SendInterval may be used instead of bandwidth to better characterize the network traffic). This is in Bytes.
5. sendInterval: time in nanoseconds between messages sent over this link.
6. serviceClass: ASSURED or BESTEFFORT

The meaning of the `serviceClass` parameter is configuration dependant. For example, `ASSURED` channels could be implemented
using any network technology that is able to guarantee QoS requirements (like e.g. TSN streams). The default value is `BESTEFFORT`.

The `NetworkChannel` requirements accept the same syntax as the pod resource requirements. Thus, you can write `100M` for `100 MBits/s`. The resource library does not support a shorthand for `nano` or `micro`, but you can write `5e-9` to specify `5 nanoseconds`.

Here is a sample application. It contains two workloads. Both workloads request
not to be scheduled on nodes labeled `DB`. The channel requests specify that
these two workloads will talk to each other.

```yaml
apiVersion: qos-scheduler.siemens.com/v1alpha1
kind: Application
metadata:
  name: app1
  labels:
    application-group: applicationgroup-sample
spec:
  workloads:
  - basename: w1
    template:
      metadata:
        labels:
          app: pause
      spec:
        affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                - key: siemens.com.qosscheduler.DB
                  operator: DoesNotExist
        containers:
        - name: pause
          image: k8s.gcr.io/pause:3.2
          resources:
            limits:
              cpu: 1000m
              memory: 8Gi
            requests:
              cpu: 100m
              memory: 20Mi
    channels:
    - otherWorkload:
        basename: w2
        applicationName: app1
      bandwidth: "5M"
      maxDelay: "1e-3"
  - basename: w2
    template:
      metadata:
        labels:
          app: pause
      spec:
        affinity:
       affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                - key: siemens.com.qosscheduler.DB
                  operator: DoesNotExist
        containers:
        - name: pause
          image: k8s.gcr.io/pause:3.2
          resources:
            limits:
              cpu: 1000m
              memory: 8Gi
            requests:
              cpu: 100m
              memory: 20Mi
    channels:
    - otherWorkload:
        basename: w1
        applicationName: app1
      bandwidth: "2M"
      maxDelay: "10e-3"
```

#### Node labels and affinities

The qos scheduling system uses a namespace prefix for node labels.

Node labels that are used in application required/forbidden label lists have to be prefixed
with `siemens.com.qosscheduler`. This applies both on the node and in the application.
You can get rid of this behaviour by changing `scheduler/infrastructure/nodes.go`.

### Custom Resource: ApplicationGroup

ApplicationGroups bundle one or more Applications for co-scheduling.

Let's say you have structured your workloads into two Applications. You want the Workload Placement Solver to calculate an optimal allocation of pods to nodes for these two Applications together. Then you need to have a way to tell Kubernetes that it should hold off on placement (and on scheduling, etc.) until both Applications exist.

An `ApplicationGroup` has an `ApplicationGroupSpec` and an `ApplicationGroupStatus`.

An `ApplicationGroupSpec` has fields:

1. MinMember: how many applications are we expecting
2. OptimizerRetryPolicy: this controls retries when calling the Workload Placement Solver. It is described in more detail below.

Because ApplicationGroups are Kubernetes resources, they inherit standard metadata fields such as `Name` and `Labels`.

An `ApplicationGroupStatus` has a field `Phase` whose value is an enum. The full list of values is in the CRD declaration file (`scheduler/api/v1alpha1/applicationgroup_types.go`).

Here is a sample ApplicationGroup that expects two Applications.

```yaml
apiVersion: qos-scheduler.siemens.com/v1alpha1
kind: ApplicationGroup
metadata:
  name: applicationgroup-sample
spec:
  minMember: 2
```

#### Configuring how the ApplicationGroup uses the Workload Placement Solver

The application group collects applications into an `ApplicationModel` and infrastructure information (nodes, network links) into an `InfrastructureModel`. It sends those to the Worklopad Placement Solver and expects to receive an `Assignment` that says which workloads/pods should go onto which nodes, and which `NetworkPaths` should be used for the requested channels.

The request to the Workload Placement Solver always has a time limit. The default time limit is five minutes.

The Workload Placement Solver may send a reply indicating that no sufficiently good solution could be found in the available time.

At that point, the application group can decide whether to

1. give up (enter State `Failed`)
2. try again (the state of the infrastructure may be different now, maybe that helps)
3. try again and provide additional time and/or indicate that a less perfect solution is acceptable.

The application group's spec contains a struct called `OptimizerRetryPolicy` that controls this behaviour.

Note, that the `gap limit` (quality threshold) is not available with all Workload Placement Solvers.

Here is an example of an OptimizerRetryPolicy that limits retries to three and specifies
an initial time limit of two minutes:

```yaml
apiVersion: qos-scheduler.siemens.com/v1alpha1
kind: ApplicationGroup
metadata:
  name: applicationgroup-sample
spec:
  minMember: 5
  optimizerRetryPolicy:
     maxRetries: 3
     initialTimeLimit: 120
```

The Workload Placement Solver retries happen when the Workload Placement Solver returns a failure code that indicates the failure may go away if the Workload Placement Solver is retried. For example, if the Workload Placement Solver's time limit was exhausted, increasing the time limit makes it possible that the next Workload Placement Solver call will succeed.
Alternatively, if the Workload Placement Solver failed to find a solution because there were not enough
infrastructure resources, the infrastructure situation in the cluster may have changed since
the previous Workload Placement Solver request, so a retry can make sense.

When an application group receives a success response from the Workload Placement Solver, it resets its
retry count.

When an application group exhausts its retries, it enters a failure state and stops trying to
schedule applications. Existing applications will continue running. If you want to avoid this
failure state and just want to keep retrying, you can set `maxRetries` to `-1`, that means
there is no limit to the number of retries.

### Connecting an Application to an ApplicationGroup

ApplicationGroups do not maintain an explicit list of the applications they co-schedule. Instead, Applications "sign up" to be part of an ApplicationGroup. They do this by setting a label:

```yaml
metadata:
   name: app1
   labels:
      application-group: example-ag
```

This makes it so the ApplicationGroup `example-ag` will include `app1` in the Applications that it co-schedules.

If you create an ApplicationGroup with a certain `MinMember` value, it will call the Workload Placement Solver as soon as it has seen at least that many Applications with a label matching the ApplicationGroup's name.

You can create the ApplicationGroup before or after the Applications that you want it to manage. Nothing will happen until the ApplicationGroup and at least `MinMember` Applications exist.

The ApplicationGroup and all the Applications it manages have to be in the same Kubernetes namespace.

The ApplicationGroup runs a loop:

```text
while true:
   if enough applications are waiting:
      ask the Workload Placement Solver for a plan
      when the Workload Placement Solver replies with a working plan:
         launch those applications that have a plan entry
```

Since the Workload Placement Solver may reply with a plan that does not have assignments for all applications, this means it is possible for the remaining applications to receive an assignment later.

Similarly, if you cancel a running application or its pods, it will be rescheduled by the application group controller.

### Updating workloads

Once an application has been scheduled, changes to its WorkloadSpec will have no effect. In principle, just like with Deployments or other ReplicaSets, you can update the application and then delete one or more of its running pods. These
pods will then be recreated using the updated application spec. They will be scheduled to the same nodes as before, and use the same channels, if any.

However, if you update an application and add workloads, the new workloads will not have a node assignment because they have not gone through the Workload Placement Solver. If you have to get the updated application scheduled, you need to force another
Workload Placement Solver run, but the only way to do that is to stop and re-schedule the entire
application. You can achieve this by deleting the application, but this can have knock-on effects because deleting an application cancels the channels that it was using. If the other endpoints of those channels are parts of other applications,
then those applications will now be cancelled as well.

The reason it's done this way right now is because the Workload Placement Solver does not yet support "incremental" scheduling.

### Adding or retrying applications

You can create new applications for an application group after the application group has already
scheduled applications. The application group will send a Workload Placement Solver request for just the
so-far-unscheduled applications in that case.

The same thing happens if an application fails and is restarted.

The same thing also happens when there is an incomplete Workload Placement Solver reply; the Workload Placement Solver reply is not guaranteed to contain assignments for all applications. In this case, the remaining applications will be retried.

However, you cannot add or retry an application that specifies a workload channel to an application that is already running.

As an example, let's say we have an application app1 already running and we want to add or retry
the application app2. If app2 specifies a channel to a workload in app1, then the Workload Placement Solver
request will fail saying "app1 is unknown".
This is because the Workload Placement Solver model only contains "applications to schedule", not "applications that are already running and should stay where they are". So there is currently no way to tell the Workload Placement Solver "app1 exists and so does the target workload of this channel" without also asking the Workload Placement Solver to schedule app1.

In this situation, if you must get app2 scheduled, you need to delete app1 and then create app1
and app2 so the application group schedules them together.

### Concurrent ApplicationGroups

In theory, you can have as many ApplicationGroups as you want.

However, if two ApplicationGroups become ready to call the Workload Placement Solver at the same time, each of them will send the same snapshot of node and network status. This can cause the Workload Placement Solver to oversubscribe resources.

In order to avoid this, the application group controller currently implements a locking mechanism to only have single call to the Workload Placment Solver open.

If you run several application groups and they have complex Workload Placement Solver requests, the Workload Placement Solver service may become a bottleneck. You could run a second Workload Placement Solver instance, but then you can end up with oversubscribed infrastructure.

### Canceling Applications and ApplicationGroups

Applications take ownership of the Pods that are created from their WorkloadSpecs. This means that if you delete an Application, all its pods will be terminated and then removed as well.

ApplicationGroups take ownership of the Applications they manage, as well as of some internal resources that control the assignment of workloads to nodes and the channel status. ApplicationGroups are also responsible for coordinating the response when one of their Applications fails.

If you delete an ApplicationGroup, all its Applications are deleted, as well as
all channels requested for them, and the assignment plan that hold intermediate scheduling information.

### Custom Resource: Channel

Channels are the Kubernetes-side representation of the end-to-end connections created by the network controller.

When an application group gets scheduled, the controller determines which channels it needs based on its `Channels` and on the network paths returned by the Workload Placement Solver. It sends `Stream Requests` to the network controller, and creates
the channel CRDs.

A channel CRD has a `ChannelSpec` listing

1. ChannelFrom: the compute node where the channel originates
2. ChannelTo: the compute node where the channel ends
3. MinBandwidthMbits
4. MaxDelayNanos
5. Framesize
6. SendInterval
7. VlanId

A channel CRD also has a `ChannelStatus` that says
whether it has been acknowledged by the network controller, and what its last observed statistics are (`MaxLatencyNanos` and `MaxJitter`). This status gets updated when the network controller sends channel updates.

You don't normally have to interact with channels, but you can inspect their status. By convention and if no explicit basename has been set in the yaml deployment file, the channel name is of the form

```text
source-workload-to-target-workload
```

So if the channel implements a workload channel connecting `app1-wl1` to `app1-wl2`, the corresponding channel id would be

```text
app1-wl1-to-app1-wl2
```
