<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Releasing a new version of the QoS Scheduler and the related components

Multiple docker images related to the QoS Scheduler system are built by the same
gitlab pipeline, among them:

1. `custom-scheduler` contains the custom plugins for the Kubernetes scheduler.
2. `optimizer` contains the optimizer client and a basic server stub in Go.
3. `network` is base classes and sample network operator implementations
4. `controller` is the controller-manager for our custom resources.
5. `nodedaemon` gets deployed to the node daemon set, for information about compute nodes.

When the Gitlab pipeline builds a new version, it runs tests, builds images, labels them
with the name of the current branch, and publishes them to the project’s container registry.

You can reference docker images via their docker tags. However, for branches that we
reuse (such as `main`, which always gets the `latest` tag), this can mean that multiple
versions of an image get the same tag. Unless you always fetch the images fresh from the registry,
you can end up with stale images by mistake.

## Using git tags to label a release

When we have thoroughly tested a version of the code that is in the `main` branch, we will
attach a git label to the current state of that branch as follows:

```bash
git tag -a v1.0.0 -m "stable 1.0"
git push origin v1.0.0
```

This triggers a special `tag` pipeline in gitlab, which will create a set of docker images with
this tag. When you push further updates to the main branch afterwards, these tagged docker images
remain unchanged, so you can more easily revert to a known stable version.

## Choosing a version tag

I have created the first version tag as v1.0.0. I suggest that we increment the last number
for bugfix releases, and the second number for feature releases.
