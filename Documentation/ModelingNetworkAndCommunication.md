<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Design for multi-network support

For an earlier version, see [the ChannelServiceClasses design](./ChannelServiceClasses.md).

## What is this for?

This is to support several network types with different QoS mechanisms and capabilities in a single Kubernetes cluster. The simplest network type, which is always available, is the "native" Kubernetes network, where all Pods can talk to each other. Configuration, routes, etc for this network are handled by the CNI plugin that you configured for your cluster (e.g., Flanel or Weave).

Other network types can exist; for example, some or all of your nodes may be
connected to a TSN or other layer-2 network technology, or a wireless network (like 5G), or some sort of virtual network. Ordinary Kubernetes workloads usually cannot use these networks.

The reason to introduce and distinguish different network types is to consider their capabilities when scheduling Pods (network-aware scheduling). At the same time, workloads running in the Pods should be able to make use of the different network types (and their underlying low-level mechanisms, like VLAN tagging, addressing, etc.) transparently, i.e., without having to use specific APIs to access them.

Our software makes other networks besides the default Kubernetes network usable to
Kubernetes workloads without requiring those workloads to run with elevated privileges.

## Modeling Network Infrastructure

We use custom resources to represent network infrastructure components in Kubernetes.

### Kubernetes Resources: links, nodes, paths

Network infrastructure consists of *Network Links*, which are connections between
*Link Nodes* (this cannot be called Nodes as this term is already used in Kubernetes). A *Link Node* is either a *Compute Node* (a node on which workloads
can run, usually a Kubernetes worker node) or a *Network Node* (such as a switch or router).

*Network Paths*, which are lists of *Network Links*, define the usable routes that can be used to communicate between *Network Endpoints*. The *Network Endpoints* of network paths are always compute nodes.

There can be different network types (or network implementations) with different QoS capabilities in the network infrastructure. They are distinguished using a *Network Label* (which is a property of every *Network Link* and *Network Path*). A *Network Label* is a user-defined string.

If there is a path between compute nodes *c1* and *c2* with a given network label, then a workload running on *c1* could send information to a workload running on *c2* using that network type.

*Network Links* and *Network Paths* are *directed*.

At the moment, Network Paths always contain links from just one network type.
 A Network Path's network type is implicit in the namespace where the Network Path resides.
It would be possible to model Network Paths containing links from several different network types;
however, it is unclear how packet routing should work with paths like this.

![ER diagram of Kubernetes resources for network model](./NetworkCRDs.svg)

### Physical vs. Logical Networks

We introduce this concept in order to take into consideration more than one network type sharing at least some network resources (which needs to be considered in the placement decisions when simultaniously occupying resources in multiple such network types).

A network is either *physical* or *logical*. The *Network Links* in a physical network represent physical connections between nodes (such as wires, or a particular wireless network). A compute node must have a separate network interface for each physical network that it is connected to.

![An example with two physical networks](./TwoPhysicalNetworks.svg)

Logical networks are built on top of physical networks. Every network link in a logical network uses exactly one physical link.

In the custom resources, the *PhysicalBase* attribute tells you which physical network a link uses. If the *PhysicalBase* attribute is missing or empty, the link is physical.
Otherwise, the value of the *PhysicalBase* attribute is a network label, and a matching physical link has to exist.

In the example above, let's say the green physical network uses the "ETH" network label, while the orange network uses "TSN". A logical link from `C1` to `N1` could use either
"ETH" or "TSN" as the value of its *PhysicalBase* attribute.

Multiple logical network links can use the same physical network link. They can declare their own capacities, though of course the effective total capacity is limited by the capacity of the underlying physical link.

Network resources reside in their own namespaces. There is one namespace per network type.
We have a Helm template that creates a namespace, a role, and a pod for your network
 operator (which is the controller instance that manages the network resources of
 that network type).
This means you can delete a network entirely by deleting its namespace;
however, Channels that were created by your Channel controller will be left hanging.

## Managing Network Infrastructure

We use Kubernetes Controllers to manage the network infrastructure model. There will typically be several controllers, for different resources; the component that contains all of these controllers, and that implements the network management is called a *Network Operator*.

A Network Operator is responsible for one or more network types (or network implementations) identified by network labels. You pass the list of network labels to the network operator as a configuration option.

A network type (or network implementation) should be managed by only one operator. This is currently not enforced, but if you break this rule, you will probably see some failures and non-deterministic behaviour.

A network operator :

* Creates, updates, and deletes network links
* Maintains network paths based on the links it manages
* If it controls a physical network, it also updates the compute nodes' mapping of network implementation label to network interface name.

A network operator can obtain the information as to which network links should exist and what their capacities are in different ways. Common approaches include:

* Custom Resource: the operator obtains link and node information from a custom resource provided by a third party (such as a cluster admin). We have implemented a sample controller using the `NetworkTopology` custom resource.

In any case, it is the responsibility of the Network Operator to maintain an accurate list of Network Paths and Network Link Capacities.

A custom Network Operator does not need to create or maintain links from a node to itself (*loopback links*); there is an internal network operator that takes care of these for you. Loopback links
are always in the `network-loopback-namespace` namespace.

### Special cases for logical networks

Like physical network topologies, logical network topologies can be fed to the corresponding operators in whichever way you want to implement. However, logical network links always have to reference a physical network link that can bear them.

It is technically possible to build a logical network on top of more than one physical network. However, for every link you have to decide which physical network to use. You cannot have two different links between the same nodes in the same logical network.

You can declare capacities for your logical links without considering which other logical networks might use the same physical links. This can lead to *overpromising* where multiple logical networks advertise capacities whose sum exceeds the capacity of the underlying physical network. It is the responsibility of the *Scheduler* to place workloads in such a way that their communication requests can be accommodated within the physical network constraints.

A network operator for a logical network should include a controller that watches the underlying physical links. This is so the operator can adapt the capacities of the links it manages, and so that it notices if a link it relies on is deleted.

#### Differences between logical and physical network operators

Logical network operators exist so that you can have more than one behaviour implemented on top of the same physical network.

Here are some guidelines for the division of labour between the physical and logical operators.

The physical operator should have the monopoly on modifying settings on the underlying network hardware. It is also the ultimate authority on physical capacity management, and you can use it to manage resources that are limited per network, such as VLAN ids.

The logical operators can make the physical network's capabilities available in different ways. The main way that differences can be implemented here is via the CNI plugins. You could have several logical operators on the same physical network, and let them use different CNI plugins, or pass different CNIArgs to CNI plugins.

The physical and logical operators need to be coordinated in the sense that the capabilities that the logical operators attempt to use need to be meaningful in the context of what the physical operator does. To give an example, if a logical operator uses a CNI plugin that sets QoS egress priorities on outgoing packets, there should be some configuration in the underlying network that causes these egress priorities to make a difference to how the packets are treated.

There is no code in place to enforce this division of labour. If you wish to allow multiple network operators to manipulate the same switches in your network, you can do that, but the coordination overhead is likely to be painful.

### The internal Network Operator

There is always one internal network operator, which maintains network links based on the default Kubernetes network. It works by getting a list of all Kubernetes nodes and assuming that there is a link between every pair of them, with some default bandwidth and latency.

The internal network operator uses the network label "K8S" and its network is declared physical.

This internal network operator also creates *loopback links*, i.e. links from every compute node back to itself. These always have a high bandwidth and low latency, and they are considered to be a part of every physical and logical network.

The internal network operator runs a Channel controller in case you want to use Channels with the "K8S" label.

## Using a network: Channels (formerly known as Streams)

Besides maintaining the network links and paths (the *Network Topology*), a network operator
also manages the *Channels* that Kubernetes applications can request for the respective network type. This is done by a "Channel controller", which runs as part of a Network Operator.

The Channel controller watches a custom resource (*Channel*). Channels
are labeled with the network type (or network implementation) they require. When a network controller sees a Channel resource with a matching network type (network implementation), it decides whether this Channel can be implemented using the available network resources and then either implements the Channel or rejects it. It updates the Channel's status accordingly.

Implementing a Channel can be very simple (maybe involving just a little internal
resource accounting) or it can involve, for example, interacting with a TSN control
plane, or creating instances of a Multus network annotation definition so that
a custom CNI plugin can be used.

A Network Operator does not have to implement a Channel controller. You only need a Channel controller if you manage a network label that one or more CSLs get mapped to (see next section).

### Communication Service Class (CSC)

Communication Service Classes are requirements that users can place on their WorkloadChannels
(the network communications channels between workloads).

They come in levels, which could be:

* Guaranteed (3) - ASSURED
* Statistically assured (2)
* Expected (1)
* Best Effort (0) - BESTEFFORT

Currently, only Levels 0 and 3 are implemented.

### Mapping Communication Service Classes to Network Implementations

The controller that reconciles ApplicationGroups and Applications and creates Channel requests
has a mapping of CSCs to Network Labels. This mapping can be configured via a ConfigMap or a commandline parameter.
It tells the controller how the requirements (CSCs) are to be implemented in this particular cluster.
This is because clusters will have different network fabrics; a cluster where TSN is available may choose to implement the `Guaranteed` CSC using TSN streams, while another cluster can map `Guaranteed` to a different Network Label.

CSCs that have no entry in the implementation map will be implemented using a fallback Network Label (usually "K8S", a.k.a. the default Kubernetes network).

### Creating the input for the Workload Placement Solver

The application group controller assembles the infrastructure model for the Workload Placement Solver. The infrastructure model is the union of the network links and paths
for all network types (identified by network labels) present in the cluster.

In order to simplify processing in the Workload Placement Solver, we combine links and paths that connect the same nodes and differ only in their network labels and capacities, so a link in the scheduler's `InfrastructureModel` could look like this:

```yaml
source_node: c1
target_node: n1
capacities:
   - network: tsn
     bandwidth_bits: 1000000
     latency_nanos: 10
   - network: eth
     bandwidth_bits: 10000
     latency_nanos: 100
   - network: vlan-prios
     physical_base: tsn
     bandwidth_bits: 1000000
     latency_nanos: 20
```

This means: three networks have a link between `c1` and `n1`. Two of them are separate physical networks (`tsn` and `eth`), the third is a logical network built on top of `tsn`.

We will send paths similarly. Paths will only ever contain links from the same logical network, though it is possible that those are built on links from different physical networks if that is how the logical network is set up.

Besides the infrastructure model, the Workload Placement Solver input also contains an `ApplicationModel`, listing the workloads and the channels between them. The application controller will translate the connection service class (CSC) specifications to network labels before creating the application model, so the Workload Placement Solver will only get the mapped network types (identified via network labels).

```yaml
channels:
   - source_workload: wl1
   - target_workload: wl2
   - source_application: app1
   - target_application: app2
   - network_label: vlan-prios
   - bandwidth_bits: 1000
   - latency_nanos: 25
```

In this case, the workload channel requests the network label `vlan-prios`, and we can see that the link between `c1` and `n1` in the previous example would be sufficient for this connection.

Loopback links can be communicated to the Workload Placement Solver either with one capacity that has no network label (this is easier to read but requires special handling in the optimizing scheduler), or we create the list of network capacities similar to the `c1-n1` example above.

### The Life of a Connection

The Application Group Controller creates Channel resources when an application is ready to run.
These Channel resources will be annotated with the relevant Network Label based on the CSC in the corresponding workload channel.

A Channel controller will watch connection requests and pick up those that
have a label matching the operator's network label values. It will then perform the necessary operations so that these Channels can progress to the status `ACK` or `NACK`.

The Channel controller will also watch for Channels that are in state `CANCELED` and perform any necessary cleanup on them before deleting them.

The Channel controller may obtain Channel information from some source such as monitoring and use it to update the status of Channels, for example by setting a Channel to `OOQOS` when it determines that the Channel is not meeting its performance promises.

The application group controller will watch for Channels in the `ACK` state, perform further operations if necessary (such as creating services for the Channel target or setting up CNI configuration) and then set the Channel's status to `ALLOCATED`.

If the application group controller encounters a Channel in the `NACK` status, it will cancel the corresponding workloads and then set the Channel to `CANCELED`.
