<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Quick Start

Steps for getting a demo to run on a local Linux Helm environment and a local KiND cluster.

## Preconditions

- Docker
- kubectl
- Helm
- KinD
- make

## Ensure access to container registry

- Enable access to Docker hub  
  ```bash
  docker login (=> enter username and password or access token)
  ```

- Test registry access (optional)
  ```bash
  docker pull hecodeco/swm-custom-scheduler:1.0
  ```

## Copy required scripts to local environment

- One way is to clone the SWM repository

  ```bash
  cd <qos-scheduler-directory>
  git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/scheduling-and-workload-migration-swm/qos-scheduler.git
  ```

- **Attention:** If this is done in Windows (using Visual Studio Code - VSC), depending on the settings of VSC, it may happen that text files use a CRLF (Carriage Return Line Feed) as line separation (the "Windows way"). Scripts will not execute with this or throw errors. The solution to this problem:
  - Make sure the standard setting of VSC is to use LF instead of CRLF, or
  - For each shell script, open it in VSC and adjust the End of Line Sequence to "LF" (this can be seen and changed in the blue bar on the lower right corner of the VSC window, when the file is selected)

# Install K8s cluster using KinD

- In local shell (Linux):

  ```bash
  cd <qos-scheduler-directory>
  ./start_cluster.sh
  ```

- This will install a KinD cluster with one master and two worker nodes
  - The K8s config to access the cluster will be appended to ~/.kube/config and in case there are multiple clusters in the config file, the context (of kubectl) will be switched to the new KinD cluster
- Check whether cluster is working

  ```bash
  kubectl get nodes
  ```

  This should output something like this:

  ```text
  NAME                 STATUS   ROLES                  AGE     VERSION
  c1                   Ready    <none>                 118s    v1.23.1
  c2                   Ready    <none>                 118s    v1.23.1
  kind-control-plane   Ready    control-plane,master   2m31s   v1.23.1
  ```

# Install QoS scheduler

## Install QoS Scheduler and Optimizer

- In local shell:

  ```bash
  make chart
  helm install qostest --namespace=codeco-swm-controllers --create-namespace tmp/helm
  ```

- Show network topology/ network links (Custom Resources)

  ```bash
  kubectl get networklinks -A
  ```

  This should show you links in the network-k8s-namespace and network-loopback-namespace namespaces.
- Show network paths (Custom Resources)

  ```bash
  kubectl get networkpaths -A
  ```

  This should show you paths in both the above namespaces.

# Deploy sample ApplicationGroup and Application

- In local shell:

  ```bash
  kubectl apply -f config/demo/sample-topology.yaml
  ```

  This will create a sample network topology. If you are running a topology operator in the
  network-demo-namespace namespace (true if you are using the latest Helm chart without modifications), you should see the network links and paths in this namespace soon.

  ```bash
  kubectl get networklinks -A
  ```

  ```bash
  kubectl get networkpaths -A
  ```
  
  When the network links and paths you need are there:

  ```bash
  kubectl apply -f config/demo/applicationgroup.yaml
  kubectl apply -f config/demo/app1.yaml
  ```

- This will create an *ApplicationGroup* with minimum 1 Applications and the Application App1, which consists of
  - Two *Workloads* w1 and w2
    - each Workload has a container wbitt/network-multitool (which has a couple of networking tools to test and demonstrate communication)
  - *Channel* "ernie" from w1 to w2 (5Mbit/s, 150µs)
  - *Channel* with a generated name (app1-w2-to-app1-w1) from w2 to w1 (2Mbit/s, 200µs)

*Channel* "ernie" requests the *BESTEFFORT* network service class, which the default Helm chart
maps to the *k8s* network (which gets created automatically).

The other channel requests the *BESTEFFORT* network service class, which the default Helm chart
implements using the *k8s* network.

## Show optimizer call and output

- First thing that happens is that the QoS-Scheduler Controller, once the *ApplicationGroup* is complete, collects all inputs related to *Workloads* and *Channels*, and calls the WorkloadPlacementSolver (Pod name: solver) with this information, together with information related to the infrastructure (compute and network)
- The WorkloadPlacementSolver then calculates and returns a placement for all Pods of the *ApplicationGroup*
- Look into logs of Solver Pod (e.g. via Lens)
- The output of the WorkloadPlacementSolver (assignment of Pods to Nodes) is written to a custom resource *AssignmentPlan* (Lens: Custom Resources/qos-scheduler.siemens.com/AssignmentPlan)

## Show Channels

- Show *Channels* (Custom Resource)

  ```bash
  kubectl get channels
  ```

  - or use Lens: Custom Resources/qos-scheduler.siemens.com/Channels
  - One custom resource Channel is created per *Channel* that connects *Workloads* placed on different nodes, and it corresponds to the channel being set up
- Services (svc): Each *Channel* also gets a K8s Service with the name "svc-\<channel-name>-\<namespace>". This can be used to address the Pods behind this channel using a DNS name.

  ```bash
  kubectl get svc
  ```

  If there was no name specified in the deployment file, a name is generated: "svc-\<src-applicationname>-\<src-workloadname>\-\<tgt-applicationname>-\<tgt-workloadname>-\<namespace>
- Endpoint (ep): IP address behind a *Channel*. This is what the corresponding Service resolves to.

  ```bash
  kubectl get ep
  ```

# Uninstall ApplicationGroup and Applications

- Either in Lens Click on the *ApplicationGroup* -> Delete, or

  ```bash
  kubectl delete -f config/demo/applicationgroup.yaml
  ```

- This will delete all *Applications* in the *ApplicationGroup* and all dependent resources

# Uninstall QoS Scheduler and Optimizer

- In Lens:
  - Helm/Releases/qostest => delete  
- In a local shell

  ```bash
  helm list --namespace=controllers-system
  helm uninstall --namespace=controllers-system qostest
  ```

# Uninstall KinD cluster

- In a local shell

  ```bash
  kind delete cluster
  ```
