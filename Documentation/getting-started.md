<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Getting Started

Steps for getting a demo to run. This is for deploying the QoS Scheduler and the WOrkload Placement Solver to a K8s cluster.

## Preconditions

### You need a Kubernetes cluster

- Kubernetes server version >= 1.20, prefer >= 1.23, up to 1.26
- you must be able to obtain docker images from whichever registry you're using (Docker hub)
- you need to install a few things into the kube-system namespace and create one daemonset with network privileges, so you need the right access to your cluster
- some pods get installed with root privileges (e.g. node daemonset, cni plugin), your cluster access needs to make that possible.

It does not matter which CNI plugin you start the cluster with.

#### Container registry

Docker images have to come from a container registry. You can have a local registry, or a harbor/artifactory/etc
instance running in your network, or you can get images from the internet. 

Your Kubernetes nodes must be able to reach all the registries that they need to fetch images from.

## Setup steps

### Prepare the nodes in your cluster
In order to address specific nodes for workloads using the SWM CRs, you have to use specific labels on the nodes. When using the default environment (kind cluster), the labeling of the nodes is done in the start_cluster.sh script.
It you are using another k8s cluster, you have to label the nodes as follows
```bash
# label the nodes with their Kubernetes node names for easier use with
# WorkloadPlacementSolver compatible labels
export NODE_LABEL_PREFIX="siemens.com.qosscheduler"
export NODES=$(kubectl get no -o jsonpath="{.items[*].metadata.name}")
for n in $NODES; do
  kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.$n=
done
```

### How to run the Helm chart

All the files for the Helm chart are in the `helm/qos-scheduler` directory. You need to install a Helm client (v3 is good) and run Helm from the `helm/qos-scheduler` directory.

First, take a look at the file `values.yaml`. It has a `global` section where you can configure the installation. All parameters in the `values.yaml` file can either be modified by editing the yaml file or on the commandline using `--set`. The commandline always takes precedence over the values.yaml file.

Here are the parameters:

- `qosNamespace`: the namespace where the controllers will run.
- `image.repositoryPrefix`: where we should be getting the images for qos-scheduler, controllers etc from.
- `image.tag`: the docker image tag to use for all images except for the `solver`.
- `image.pullSecrets`: the chart will create image pull secrets for you using `image.credentials`. This is so the Kubernetes pods can get images from your registry.
- `image.pullPolicy`: the default Kubernetes image pull policy. `Always` is good when you're developing, otherwise `IfNotPresent` is more efficient. You can override this for each subchart.
- `image.credentials`: this is the login to the docker registry. For `password`, I recommend getting a token with registry read access. Do not put your actual password in here. These credentials will be stored in the Kubernetes cluster using secrets; they will not be encrypted.

Other things you may want to check or modify in the Helm chart:

- you may need to edit `charts/optimizer/values.yaml` and specify an image tag that works for you.,
- take a look at `charts/network/values.yaml`. This determines the network controllers that the Helm chart will start for you. You can add your own network sections to the values file here or you can start your own network controllers outside the chart.
- `templates/_helpers.tpl` contains the `serviceClassMap`. This is how the system decides which network to use for your channels' service classes. Each channel service class needs to have a key in the map, and the values need to be networks for which a channel operator exists.

```bash
make chart
helm install qostest --namespace=controllers-system --create-namespace tmp/helm
```

If you make changes to the helm chart and just want to apply those:

```bash
helm upgrade qostest --namespace=controllers-system tmp/helm
```

To uninstall the release:

```bash
helm uninstall qostest -n controllers-system
```

### Make sure the network is ready

On startup, the network operator (or operators if you're running several) need to identify the links and compute paths.

Run

```bash
kubectl get networklinks -A
```

to see the topology that exists so far. At a minimum, you should have a "loopback" link for
every Kubernetes node and a "k8s" link between every pair of Kubernetes nodes.

Run

```bash
kubectl get networkpaths -A
```

to make sure the paths have been computed. It is normal for there to be multiple paths between
some pairs of nodes if your network has redundant connections.

## Create applications

Now you can create application groups and applications. There are sample files in the config/demo directory. Make sure to create the namespace they're in before you try them.

## Troubleshooting

Look at application groups and applications.

```bash
kubectl get applicationgroups
kubectl get applications
```

### Everything is Waiting

Some amount of Waiting is normal.
However, if none of the expected workloads (Pods) are running, check
whether the application group is waiting for more applications to be created:

Look at the application group's spec. It has a Min Member entry. you need
at least this many applications with a label `application-group` whose value
is the name of your application group.

### Stuck in Optimizing

Look at the logs of the optimizer pod. You should be able to see whether it's recently started or finished an optimizer call.

Look at Kubernetes events.

Look at the Optimizer Attempt Count in the application group status.

See if the optimizer is running out of memory. Give it more memory or cpu if necessary.

### Only some applications are running, some are Pending or Waiting

Two possible reasons:

#### Reason 1: the optimizer returned an incomplete plan

It is possible for the optimizer to return an assignment that only covers some of the
applications in your application group. Those will be started, and another optimization request
will be sent for the remainder.

#### Reason 2: some applications failed

See also "Stuck in Pending" below.

Besides just staying pending, it is possible for one or more of your applications to fail.
This happens if their pods fail or if their channels fail. In this case, the application
will go back to `Waiting` and will be included in the next optimizer request.

A pod being rescheduled to another node (by Kubernetes) is one thing that can make a channel
fail. Thus you need to make sure your nodes are not oversubscribed.

### Stuck in Pending

#### Reason 1: channels not being created

```bash
kubectl get channels
```

If any of them do not have status 'RUNNING', you're waiting for them.

Look at the logs of the network operator that should handle the channels' network tag.

#### Reason 2: pods being scheduled to incompatible nodes

The optimizer computes a mapping of workloads (~pods) to nodes. It takes pod affinities into
account, but it does not observe anti-affinities or node taints (except for the 'master/NoExecute' taint). If your cluster's nodes use taints to prevent scheduling, it is possible that the optimizer
has created an assignment that the Kubernetes scheduler rejects.

You can verify this by looking at the assignment plan created for the scheduler (their names begin with
the name of your application group). You can also look at the pods with `describe`; their
conditions will show something about the pod not being able to be scheduled on the node chosen
by the optimizer.

Until the optimizer can handle node taints etc, you need to add labels to your workloads and
nodes to prevent unwanted scheduling.

Note that you can get a transient message saying "pod wants to be scheduled on node x not y" from
the custom scheduler. This means that the pod was assigned to a node that is different from the
one assigned by the optimizer; the custom scheduler forces that pod to be reassigned. It should be
alright in a few seconds.

#### Reason 3: disconnected topology

The optimizer will refuse to operate on a network that is not connected. You should see a message
to this effect in the optimizer logs and also in the application group status.

### ApplicationGroup Status Failed

Look at events (`kubectl get events`).

Example:

```text
25s         Normal    phase change                      applicationgroup/applicationgroup-demo   changed phase from Waiting to Optimizing
25s         Warning   nonretryable optimizer failures   applicationgroup/applicationgroup-demo   reason:  ([invalid argument exception: protobuf: 'Interface: basename is required'])
25s         Normal    phase change                      applicationgroup/applicationgroup-demo   changed phase from Optimizing to Failed
```

This tells you that the application group entered a failed state because it tried to
call the optimizer and the optimizer returned a non-retryable error.

Other reasons for failure include applications failing because their pods failed or because
their channels failed. Some of those result in the application being retried.

An application group can also enter a failure state if it has exhausted its optimizer retries.
This happens if the optimizer repeatedly fails to find an assignment. That usually means you
are trying to schedule workloads that are too demanding for your cluster.
