<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Supporting custom networks using CNI plugins

## Goal

We will have multiple, user-defined, networks active in a Kubernetes cluster.
Each network has its own operator controlling the network links, endpoints, and paths in
the network.

Applications can request `channel`s in these networks. Channels are communication paths between Kubernetes pods. Channels typically have network requirements such as latency guarantees. The network operators are responsible for establishing the channels, i.e., for modifying configurations so that the network requirements are met.

We need to enable Kubernetes pods to communicate via these channels in addition to the
"normal" Kubernetes overlay.

This means we need to provide ways to control routing as well as various settings on network
packets, such as VLAN tags or QoS (Ethernet) priorities.

## Example

This needs a picture

Nodes with two network interfaces each
One interface is connected to the Kubernetes network
The other is connected to a different network, which we'll label "tsn"

There are switches on the networks between the nodes

Show a pod on a node; by default, it gets one network interface, which is a veth pair connecting into the node. Traffic from this pod to other pods in Kubernetes goes via the default Kubernetes network.

Now add a second network interface to the pod, with its own veth end in the node. We want traffic that goes out of the second network interface to use
the "tsn" network, so it has to go through the second network interface on the node.

The bit about "modifying configurations so that the network requirements are met" above refers to configuration actions on the nodes or switches to set up things like priority queues and filters.

The bit about "enabling Kubernetes pods to communicate" refers to how we make it so that traffic leaving the pod can either *use the channel* (so it goes through the second network interface) or *use the default Kubernetes overlay* (so it goes through the first network interface).

And of course a pod can be a member of multiple networks, so there could be several additional network interfaces on it.

Modifying switch configurations etc is implementation-specific and has to be done by the network operators that users implement themselves.

Similarly, the precise operations needed to get pods to send just the right traffic in just the right way are implementation-specific, but the "normal" way of implementing them is via a CNI plugin, so below I explain how we can let users choose and configure the CNI plugin that we then attach via multus annotations.

## CNI

The usual way to provide network configuration options in Kubernetes is via the CNI subsystem.

Since we may have multiple networks, each with its own settings and requirements, we will
usually want Kubernetes Nodes (and sometimes Pods as well) to be able to participate in
multiple networks with different settings. One way of achieving this is using the Multus CNI plugin.
This allows us to attach Pods to multiple networks.

Users of the system can implement their own network operators, managing their own networks in whichever way they see fit.
This means we cannot supply a fixed set of CNI plugins; we have to let users supply their own.

Below, I outline a framework that supports users supplying their own CNI plugin. The framework
provides scaffolding and useful defaults so users do not have to start from scratch (though of
course they can do so if they want).

### Configuring network operators via the Helm chart

We currently let users specify custom networks by inserting them into the values.yaml of the `network` sub-chart.

This values file expects a list of `operators` to be provided. An entry in the
list should look as follows:

```yaml
- name: tsn
  pullSecret: qos-registry-access
  networks: ['TSN']
  isPhysical: true
  namespace: TSN
  image:
     repository: networkoperator
     tag: network-namespaces
     command: /bin/topology-nw-operator
```

Each operator specifies the networks implementations it will process, and
whether it manages logical or physical networks.

Normally, a network operator will process a single network implementation. It is possible to process several, and the standard topology operator does so, it just makes the implementation more complicated.

Each network implementation lives in its own namespace. The `network` sub-chart takes care of creating the namespaces as well as service accounts, roles, and rolebindings. The namespace for a network implementation named `x` will be `network-x-namespace`.

The network operator you specify here will run in the namespace of a network that it manages. In case you process more than one network implementation, the operator will run in the namespace belonging to the first network in your list.

In order to support CNI plugins, I propose to add the name of a CNI plugin to this chart.

The CNI plugin needs to have been installed on all nodes in the cluster. Our Helm chart does not do that for you, and it should be done after careful review of the plugin in question. Of course it is possible to name a "standard" plugin such as `bridge` that is installed by default.

If your operator declares a CNI plugin, the `network` sub-chart will generate an instance of the `NetworkAttachmentDefinition` CRD for you.
Our code will add multus annotations to pods generated for your `ApplicationWorkloads`, and those annotations will reference your NetworkAttachmentDefinition.

Multus Annotations also contain `CniArgs`, which will be passed to your CNI plugin when it is run. We let users control these CniArgs via a status field on the `Channel` CRD. Your network operator's channel controller (physical, logical, or both) should populate the `CniMetadata` status field on channels that require your CNI plugin, and our code will use that CniMetadata to set
CniArgs on your multus annotations.

## The Life of a Channel

Channels are requested via Application Workloads. An application workload can declare channels;
a channel request consists of the endpoints (two application workloads) and a spec outlining
the network capabilities (such as bandwidth, latency, or quality of service class).

Our system turns this into a Channel CRD. A channel CRD will, in addition to the parameters
from the application workload channel request, contain the following information:

1. The Kubernetes nodes (start and end---channels are directed) where the pods forming the endpoints of the channel will be deployed
2. The network path between these Kubernetes nodes that network packets traversing the channel should take.
3. The network implementation to use. This is determined based on the quality of service class requested.

Usually, the network implementation listed here will be a *logical* network. Every logical network has an underlying *physical* network. In order to get the channel running, we have to let the
operators for both the logical and the physical networks do some work.

First, the operator for the logical network will see the channel request. This is because
this operator watches for channel requests for its network. The operator will verify that
the channel request can be accommodated within the current capabilities of the logical network. It will then update the status of the channel request.

The logical operator has to determine which physical network the channel will use. This must
be the physical network underlying the channel's logical network. The logical operator
records the physical network as a label on the channel.

The operator for the physical network will now see the channel request. It watches for channel requests that match its physical network and that have been reviewed by the logical operator.
The physical network operator will also verify that the channel can be implemented given the
current resources. This might be false, for example if there are multiple logical networks
using the physical network and the network resources are oversubscribed. We recommend not doing
that, but it is up to the network operator implementors.

If the channel is physically implementable, the physical network operator will update the channel status
to "implementing" and perform whichever operations are necessary on the network. This could involve
things like switch or router configuration. Once this is done, the network operator will update the channel status to "implemented". It may also annotate the channel with
CNI meta data. The CNI meta data contains information that will be used as configuration parameters
to the CNI plugin later. Examples of CNI meta data entries would be a VLAN tag or QoS priority
that should be used by packages traversing the channel.

The logical network operator, when it sees that the channel is now implemented, may
extend or modify the CNI metadata. It updates the channel's status to "ACK".

Usually, most of the CNI preparation work will be done in the logical network operator. That way, you can have several logical network operators attaching different CNI plugins and/or configuring different Cni Args.

## Attaching a CNI plugin

The system's Application Group Controller watches for channels that enter the "ACK" status.
When it sees one, it will

1. Look up the NetworkAttachmentDefinition resource for the channel's logical namespace
2. If there is one, create an annotation for the pods constituting the channel's endpoints. These annotations are used by the Multus plugin.
3. Create a service and endpoint for this channel. If the channel uses a custom overlay network with its own IP range, the endpoint will have the correct IP address. This creates a DNS entry for the channel's endpoint.

The NetworkAttachmentDefinition resource will be created by the Helm chart based on configuration
information provided by the custom network's owner. It contains the name of the CNI plugin to use.
This CNI plugin must have been installed on all nodes.

## Useful defaults

We provide a sample CNI plugin with the following behaviour.

It takes parameters:

- pod overlay CIDR
- pod network interface name
- vlan id
- qos priority
- node overlay CIDR
- node network interface name

It creates subinterfaces on both the pod and the node, provides them with an ip address from the cidrs you give it, and configures the node subinterface to set the vlan tag and qos priority you provided.

It will enable forwarding via iptables.

It will modify the routing tables on the pod and the node so packets to the "other end" of the channel are routed via the new subinterfaces.

This CNI plugin is a proof of concept and is not ready for production use. You are advised to create your own.

## Network Types Example

Let's say we have a physical network called "TSN-capable". On top of this physical network are
two logical networks, "TSN" and "Basic".

The system is configured so that the QoS Service class "ASSURED" will be implemented by "TSN"
network links, while "BASIC" will be implemented by "Basic" network links.

We create an application with workloads requesting a channel with the service class "ASSURED" from workload `w1` to workload `w2`.

The optimizing scheduler decides to place `w1` on node `n1` and `w2` on node `n2`. There is
a network path connecting these nodes in the "TSN" logical network, and it goes via the
switch `s1`. There is a corresponding path in the underlying physical network.

The operator for the "TSN" network receives the channel request and finds it acceptable.
It updates the channel's status. The operator for the "TSN-capable" physical network picks
up the channel request, sets its status to "implementing", and then sets up a TSN stream implementing
the channel. When it is done, it updates the channel's status to "implemented" and adds CNI meta data
indicating that the VLAN tag associated with this TSN stream will be 123 and that the QoS
priority to use is 5.

The operator for the logical "TSN" network receives these updates and updates the channel status to "ACK". It requests and receives a pod and a node CIDR range and adds them to the channel's CNI meta data.

The application group controller sees a channel with the status "ACK". It retrieves the pods implementing the workloads on either end of the channel and adds annotations for them so that multus
can configure the CNI plugin. The pods exist but they have not been scheduled yet because
they are waiting for the channels to be ready.

The other logical network could be used to implement non-TSN channels that use the same network
fabric as the TSN channels. For example, we could have reserved a range of VLAN tags for "general
use", so they would not be used for TSN streams but traffic using them in combination with
a low QoS priority would get through the TSN network provided it did not get in the way of
the real TSN traffic.

### Special cases

It is technically possible to create a logical network that is based on more than one physicla network.
In that case, your logical channel controller still has to attach a physical-network-implementation label to your channel in order for the corresponding physical channel controller
to process the channel. It is up to you to make sure that physical channel controller can
deal with the fact that the channel might use more than one physical network.
