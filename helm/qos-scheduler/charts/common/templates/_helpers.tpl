# SPDX-FileCopyrightText: Siemens AG
# SPDX-License-Identifier: Apache-2.0

{{- define "image" -}}
{{ .Values.image.repositoryPrefix | default .Values.global.image.repositoryPrefix }}{{ .Values.image.repository }}:{{ .Values.image.tag | default .Values.global.image.tag }}
{{- end }}

{{- define "imagePullPolicy" -}}
{{ .Values.image.pullPolicy | default .Values.global.image.pullPolicy }}
{{- end }}

{{- define "imagePullSecret" }}
{{- if .Values.global.image.credentials }}
{{- with .Values.global.image.credentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- else }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"\",\"password\":\"\",\"email\":\"\",\"auth\":\"\"}}}" | b64enc }}
{{- end }}
{{- end }}

{{- define "imagePullSecrets" }}
{{- range concat (.Values.image.pullSecrets | default list) .Values.global.image.pullSecrets | uniq }}
- {{ toYaml . }}
{{- end }}
{{- end }}
