# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: kube-cni-install-ds
  namespace: kube-system
  labels:
    tier: node
    app: cni-install
    name: cni-install
spec:
  selector:
    matchLabels:
      name: cni-install
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        tier: node
        app: cni-install
        name: cni-install
    spec:
      hostNetwork: true
      imagePullSecrets:
      {{ include "imagePullSecrets" . | indent 8 }}
      tolerations:
      - operator: Exists
        effect: NoSchedule
      - operator: Exists
        effect: NoExecute
      serviceAccountName: {{ .Values.cniInstallServiceAccount }}
      containers:
      - name: busybox
        image: busybox
        command:
          - sleep
          - infinity
        resources:
          requests:
            cpu: "100m"
            memory: "50Mi"
          limits:
            cpu: "100m"
            memory: "50Mi"
      initContainers:
        {{- if .Values.installStandardCniPlugins }}
        - name: install-standard-plugins
          image: alpine
          command: ["ash", "-c"]
          args: ["cd /host/opt/cni/bin && wget https://github.com/containernetworking/plugins/releases/download/v1.0.1/cni-plugins-linux-amd64-v1.0.1.tgz -O cni-plugins-linux-amd64-v1.0.1.tgz && tar xzf cni-plugins-linux-amd64-v1.0.1.tgz"]
          securityContext:
            privileged: true
            runAsUser: 0
          volumeMounts:
            - name: cnibin
              mountPath: /host/opt/cni/bin
              mountPropagation: Bidirectional
        {{- end }}
        - name: install-demo-cni-plugin
          image: {{ include "image" . }}
          imagePullPolicy: {{ include "imagePullPolicy" . }}
          command:
            - "cp"
            - "/bin/{{ .Values.image.cniDemo }}"
            - "/host/opt/cni/bin/"
          resources:
            requests:
              cpu: "10m"
              memory: "15Mi"
          securityContext:
            privileged: true
            runAsUser: 0
          volumeMounts:
            - name: cnibin
              mountPath: /host/opt/cni/bin
              mountPropagation: Bidirectional
      terminationGracePeriodSeconds: 10
      volumes:
        - name: cni
          hostPath:
            path: /etc/cni/net.d
        - name: cnibin
          hostPath:
            path: /opt/cni/bin

