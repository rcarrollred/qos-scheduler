# SPDX-FileCopyrightText: Siemens AG
# SPDX-License-Identifier: Apache-2.0
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.12.1
  name: applicationgroups.qos-scheduler.siemens.com
spec:
  group: qos-scheduler.siemens.com
  names:
    kind: ApplicationGroup
    listKind: ApplicationGroupList
    plural: applicationgroups
    shortNames:
    - ag
    singular: applicationgroup
  scope: Namespaced
  versions:
  - additionalPrinterColumns:
    - description: Application group status
      jsonPath: .status.phase
      name: Status
      type: string
    name: v1alpha1
    schema:
      openAPIV3Schema:
        description: ApplicationGroup is a collection of Applications that should
          be scheduled together.
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: ApplicationGroupSpec describes an application group. Members
              of an application group are identified by having a label application-group
              with value equal to this group's name.
            properties:
              minMember:
                default: 1
                description: MinMember says how many applications matching the label
                  query need to be either running or waiting in order for scheduling
                  to begin. The default is 1.
                format: int32
                type: integer
              optimizerRetryPolicy:
                description: OptimizerRetryPolicy specifies the retry policy on optimizer
                  failures.
                properties:
                  gapLimitIncrease:
                    default: 1
                    description: GapLimitIncrease is the factor by which the initial
                      gap limit will be multiplied on retry. Has to be at least 1.0.
                      Like the gap limit, this field only has meaning when you use
                      an optimizer that supports a quality threshold.
                    type: integer
                  initialGapLimit:
                    description: InitialGapLimit is the initial value of the quality
                      gap limit passed to the optimizer. This field only has meaning
                      for optimizers that have a quality threshold. If this is supported
                      and set, then the optimizer is allowed to return as soon as
                      it finds a solution that is at most gapLimit percent worse than
                      the optimal solution. If you set both time limit and gap limit,
                      the optimizer will still return as soon as it hits the gap limit,
                      and if it does not find a solution within both the gap and time
                      limit, it will report the request infeasible.
                    type: integer
                  initialTimeLimit:
                    description: InitialTimeLimit is the initial value of the time
                      limit passed to the optimizer. If you set this to a value <=
                      0, the system will set it to a nonzero default. The value is
                      in seconds.
                    type: integer
                  maxRetries:
                    default: 5
                    description: MaxRetries is the maximum number of time an optimization
                      request will be retried. Retries happen when the optimizer result
                      indicates that no feasible schedule was found within the time
                      limit.
                    type: integer
                  timeLimitIncrease:
                    default: 1
                    description: TimeLimitIncrease is the factor by which the initial
                      time limit will be multiplied on retry. If you set this to a
                      value < 1, the system will use 1.
                    type: integer
                required:
                - initialTimeLimit
                - maxRetries
                type: object
            type: object
          status:
            description: ApplicationGroupStatus defines the observed state of ApplicationGroup
            properties:
              lastUpdateTime:
                format: date-time
                type: string
              optimizerAttemptCount:
                type: integer
              phase:
                type: string
            required:
            - optimizerAttemptCount
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
