# SPDX-FileCopyrightText: Siemens AG
# SPDX-License-Identifier: Apache-2.0


{{- define "namespace" -}}
{{ cat "network-" . "-namespace" | lower | nospace }}
{{- end }}

{{- define "role" -}}
{{ cat "network-" . "-role" | lower | nospace }}
{{- end }}

{{- define "serviceaccount" -}}
{{ cat "network-" . "-sa" | lower | nospace }}
{{- end }}

{{- define "nwoperator-rolebinding" -}}
{{ cat "nwoperator-" . "-rb" | lower | nospace }}
{{- end }}

{{- define "nwlistener-rolebinding" -}}
{{ cat "nwlistener-" . "-rb" | lower | nospace }}
{{- end }}

{{- define "epmaintainer-rolebinding" -}}
{{ cat "epmaintainer-" . "-rb" | lower | nospace }}
{{- end }}

{{- define "networkAttachmentArg" -}}
{{- if .cni -}}
- "--network-attachment-name={{ .cni.name }}"
{{- end -}}
{{- end }}

{{- define "defaultArgs" -}}
- "--health-probe-bind-address=:8081"
- "--metrics-bind-address=:8080"
- "--network-implementations={{ join "," .networks }}"
- "--channel-controller={{ .channelControllerImpl | default "" }}"
- "--physical-network={{ .isPhysical | default false }}"
{{ include "networkAttachmentArg" . }}
{{- end }}

