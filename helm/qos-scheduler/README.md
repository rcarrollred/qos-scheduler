<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# How to install a release with the helm chart

For details of what the parameters do, please look at Documentation/getting-started.md.

```bash
helm install qostest --namespace=controllers-system --create-namespace --set global.image.credentials.username=your.email@yourcompany.com --set global.image.credentials.password=$YOURTOKEN --set global.image.credentials.email=your.email@yourcompany.com .
```

If you make changes to the Helm chart and just want to apply those:

```bash
helm upgrade qostest --namespace=controllers-system --create-namespace --set global.image.credentials.username=your.email@yourcompany.com --set global.image.credentials.password=$YOURTOKEN --set global.image.credentials.email=your.email@yourcompany.com .
```

To uninstall the release:

```bash
helm uninstall qostest -n controllers-system
```
