<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# SWM QoS Scheduler

The SWM QoS scheduler is an extension of the Kubernetes scheduler that implements an extended model for describing enhanced application requirements and infrastructure capabilities, according to the Seamless Computing concept.

The SWM QoS scheduler considers the extended application (QoS) requirements when placing application components (further called workloads) to compute nodes in the K8s cluster. The main enhancements beyond the existing K8s scheduler are:

- schedule multiple workloads (K8s Pods) at once, considering dependencies between these workloads
- network aware scheduling: make K8s aware of the network topology, connectivity, and resource availability between worker nodes, and consider this in the scheduling decision (according to the communication requirements of the application)

## Getting Started

For installing the SWM QoS Scheduler (including all CRDs and required containers) and to deploy a small demo application, see [quick start](Documentation/quick-start-oss.md).

## License
All code files are licensed under Apache license version 2.0.
All documentation is licensed under Creative Commons Attribution-ShareAlike 4.0 International.
