#!/bin/bash
# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# label the nodes with their Kubernetes node names for easier use with
# optimizer-compatible labels
# put additional labels to selected nodes
export NODE_LABEL_PREFIX="siemens.com.qosscheduler"
export NODES=$(kubectl get no -o jsonpath="{.items[*].metadata.name}")
for n in $NODES 
do
  kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.$n=
  if [ $n = "ipc1" ];then kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.sensor=;fi  
  if [ $n = "ipc2" ];then kubectl label --overwrite nodes $n $NODE_LABEL_PREFIX.actuator=;fi  
done

